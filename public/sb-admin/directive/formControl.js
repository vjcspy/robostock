/**
 * Created by vjcspy on 11/18/15.
 */
(function (window, document, $, angular) {
    angular.module('izFormControlApp', [], function ($interpolateProvider) {
        $interpolateProvider.startSymbol('//');
        $interpolateProvider.endSymbol('//');
    }).directive('izSelectTypeInput', izSelectTypeInput);

    function izSelectTypeInput() {
        var _s = '';
        _s += '<div class="btn-group input-group">';
        _s += '<span class="input-group-addon"><i class="fa fa-cog"></i></span>';
        _s += '<label ng-repeat="button in data.buttons" id="//button.id//" class="btn btn-default" ng-class="{active:button.active}" ng-click="selectBt(button.id)">//button.nameCtr//</label>';
        _s += '</div>';

        _s += '<div class="form-group"  style="margin-top: 5px" ng-style="data.text.style">';
        _s += '<label for="inputElem_text">//data.inputLabel//</label>';
        _s += '<input type="text" class="form-control" id="inputElem_text" ng-model="data.text.value" ng-class="data.text.classData">';
        _s += '</div>';

        _s += '<div class="form-group"  style="margin-top: 5px" ng-style="data.select.style">';
        _s += '<label for="inputElem_select">//data.inputLabel//</label>';
        _s += '<select id="inputElem_select" ng-model="data.select.value" name="selectFilter" class="form-control" ng-options="option.label for option in data.dataSelect track by option.value" ng-class="data.select.classData"></select> ';
        _s += '</div>';

        return {
            restrict: 'E',
            scope: {
                btData: '=',
                izCtrData: '='
            },
            controller: ['$scope', function ($scope) {
                $scope.data = {};

                var classFaIn = 'animated rollIn';
                var selected = {
                    isSelected: false,
                    type: false,
                    object: false
                };

                function resetInput(_type) {
                    switch (_type) {
                        case 'select':
                            $scope.data.select = {
                                style: {display: 'none'},
                                value: {value: -1, label: 'Please select...'},
                                classData: classFaIn
                            };
                            break;
                        case 'text':
                            $scope.data.text = {
                                style: {display: 'none'},
                                value: '',
                                classData: classFaIn
                            };
                            break;
                    }
                }

                resetInput('text');
                resetInput('select');

                $scope.data.buttons = $scope.btData;

                $scope.selectBt = function (btId) {
                    selected.isSelected = true;
                    $.each($scope.data.buttons, function (k, v) {
                        // add class active to button ctrl
                        v.active = v.id == btId;

                        // show input
                        if (v.id == btId) {
                            switch (v.type) {
                                case 'text':
                                    $scope.data.text.style = {};
                                    $scope.data.inputLabel = v.inputLabel;
                                    selected.type = 'text';
                                    resetInput('select');
                                    break;
                                case 'select':
                                    $scope.data.select.style = {};
                                    $scope.data.inputLabel = v.inputLabel;
                                    $scope.data.dataSelect = v.dataSelect;
                                    selected.type = 'select';
                                    resetInput('text');
                                    break;

                            }
                        }
                        selected.object = v;
                    });
                };
                $scope.izCtrData = {
                    getModelData: function () {
                        switch (selected.type) {
                            case 'text':
                                return $scope.data.text.value;
                            case 'select':
                                return $scope.data.select.value;
                        }
                    },
                    getObjectSelect: function () {
                        return selected.object;
                    }
                };
            }],
            link: function (scope, element, attrs) {
            },
            template: _s
        }
    }

})(window, document, jQuery, angular);
