/**
 * Created by vjcspy on 11/21/15.
 */
(function (window, document, $, angular) {
    angular.module('izReportTableApp', ['ui.mask'], function ($interpolateProvider) {
        $interpolateProvider.startSymbol('//');
        $interpolateProvider.endSymbol('//');
    }).directive('izBootstrapMultiSelect', izBootstrapMultiSelect).directive('izYearSelect', izYearSelect)
        .directive('izReportDirective', izReportDirective)
        .service('izReportInitDataService', izReportInitDataService);


    function izYearSelect() {
        var _s = '';
    }

    function izBootstrapMultiSelect() {
        var _s = '';
        _s += '<select class="izBootstrapMultiSelect" multiple="multiple" ng-model="izMultiSelect">';
        _s += '<optgroup ng-repeat="group in data.multiSelect.groups" label="//group.label//">';
        _s += '<option ng-repeat="item in group.items" value="//item.value//">//item.label//</option>';
        _s += '</optgroup>';
        _s += '</select>';
        return {
            restrict: 'E',
            scope: {
                izMultiSelectData: '=',
                izTransferData: '='
            },
            controller: ['$scope', function ($scope) {
                //Dummy Data:
                $scope.data = {
                    multiSelect: {
                        groups: []
                    }
                };

                //$scope.data.multiSelect.groups = $scope.izMultiSelectData;
                $(document).ready(function () {
                    $('.izBootstrapMultiSelect').multiselect({
                        enableCollapsibleOptGroups: true,
                        //enableClickableOptGroups: true
                    });
                });
                $scope.izTransfer = {
                    setData: function (data) {
                        $('.izBootstrapMultiSelect').multiselect('dataprovider', data);
                    },
                    getData: function () {
                        return JSON.stringify($scope.izMultiSelect);
                    }
                };

            }],
            link: function (scope, elem, attrs, ctrl) {
                //$(document).ready(function () {
                //    $(elem).multiselect();
                //});
                scope.izTransferData = scope.izTransfer;
            },
            template: _s
        };

    }

    var _templateTable = '';

    _templateTable += ' <div class="panel panel-default">';
    _templateTable += '<div class="panel-heading">';
    _templateTable += '<form action="" method="POST" class="form-inline col-lg-pull-2" role="form">';
    _templateTable += '<div class="form-group formgroupFix">';
    _templateTable += '<label class="sr-only" for="">Mã</label>';
    _templateTable += '<input type="text" class="form-control tickertext" name="" id="" placeholder="Mã CP..." ng-model="report.data.ticker" ng-keyup="getDataTime($event)">';
    _templateTable += '</div>';
    _templateTable += '<div class="form-group formgroupFix">';
    _templateTable += '<label class="sr-only" for="">Kiểu thời gian</label>';
    _templateTable += '<select name="name" id="inputID" class="form-control" ng-model="report.data.timeType" ng-change="selectTimeType();getDataTime($event)">';
    _templateTable += '<option value="q"> Quý</option>';
    _templateTable += '<option value="y"> Năm</option>';
    _templateTable += '</select>';
    _templateTable += '</div>';

    _templateTable += '<div class="form-group formgroupFix" ng-hide="config.notTimeQuarter">';
    _templateTable += '<label class="sr-only" for="">t1</label>';
    _templateTable += '<iz-bootstrap-multi-select iz-transfer-data="report.data.quarterT1"></iz-bootstrap-multi-select>';
    _templateTable += '</select>';
    _templateTable += '</div>';
    _templateTable += '<div class="form-group formgroupFix" ng-hide="config.notTimeQuarter">';
    _templateTable += '<label class="sr-only" for="">t2</label>';
    _templateTable += '<iz-bootstrap-multi-select iz-transfer-data="report.data.quarterT2"></iz-bootstrap-multi-select>';
    _templateTable += '</div>';

    _templateTable += '<div class="form-group formgroupFix" ng-hide="config.notTimeYear">';
    _templateTable += '<label class="sr-only" for="">t1</label>';
    _templateTable += '<input type="text" class="form-control animated fadeIn" ng-model="report.data.yearT1" ui-mask="9999"  ui-mask-placeholder ui-mask-placeholder-char="_"/>';
    _templateTable += '</div>';
    _templateTable += '<div class="form-group formgroupFix" ng-hide="config.notTimeYear">';
    _templateTable += '<label class="sr-only" for="">t2</label>';
    _templateTable += '<input type="text" class="form-control animated fadeIn" ng-model="report.data.yearT2" ui-mask="9999"  ui-mask-placeholder ui-mask-placeholder-char="_"/>';
    _templateTable += '</div>';

    _templateTable += '';
    _templateTable += '<div class="pull-right">';
    _templateTable += '<button type="button" class="btn btn-default btn-sm" ng-click="downloadExcel()" style="margin-right: 7px" ng-hide="notDownloadAble">';
    _templateTable += ' <span class="glyphicon glyphicon-save" aria-hidden="true"></span> Excel';
    _templateTable += '</button>';
    _templateTable += '<button type="button" class="btn btn-primary" ng-click="selectTicker()">Show';
    _templateTable += '</button>';
    _templateTable += '</form>';
    _templateTable += '</div>';
    _templateTable += '</div>'; // end div panel-heading

    _templateTable += '<div class="panel-body">';

    _templateTable += '<table class="table table-hover">';

    _templateTable += '<thead>';
    _templateTable += '<tr>';
    _templateTable += '<th>//report.general.typeLabel//</th>';
    _templateTable += '<th align="right" style="font-size: 10px">//report.general.t1Label//</th>';
    _templateTable += '<th align="right" style="font-size: 10px">//report.general.t2Label//</th>';
    _templateTable += '<th align="right" style="padding-left: 30px;">Percent</th>';
    _templateTable += '<th align="right" style="padding-left: 70px;">Change</th>';
    _templateTable += '</tr>';
    _templateTable += '</thead>';

    _templateTable += '<tbody>';
    _templateTable += '<tr ng-repeat="row in dataReports" id="//row.id//" ng-click="expandRow(row.id)">';
    _templateTable += '<td>//row.criteriaLabel//</td>';
    _templateTable += '<td align="right" style="padding-right: 35px !important;">//row.t1//</td>';
    _templateTable += '<td align="right" style="padding-right: 35px !important;">//row.t2//</td>';
    _templateTable += '<td ng-class="row.class" align="right" style="padding-right: 35px !important;">//row.percent.formatMoney(2, ".", ",") + "%"//</td>';
    _templateTable += '<td align="right" style="padding-right: 35px !important;">//row.result//</td>';
    _templateTable += '</tr>';
    _templateTable += '</tbody>';

    _templateTable += '</table>';

    _templateTable += '</div>'; // end div panel-body

    _templateTable += '<div class="panel-footer">';
    _templateTable += 'Created by vjcspy';
    _templateTable += '</div>';

    _templateTable += '</div>'; //end div panel

    function izReportDirective() {
        return {
            restrict: 'E',
            scope: {
                izTransfer: '&',
                izReportType: '@'
            },
            controller: ['$scope', 'izReportInitDataService', '$window', function ($scope, izReportInitDataService, $window) {
                //init data:
                $scope.report = {};
                $scope.notDownloadAble = true;
                $scope.config = {
                    notTimeQuarter: true,
                    notTimeYear: true
                };

                $scope.report.general = {
                    /*Biến này chứa các data chung*/
                    typeLabel: 'Report Type',
                    t1Label: 'Time 1',
                    t2Label: 'Time 2',
                    result: 'Result'
                };

                $scope.report.data = {
                    // Phần chứa data của bảng
                    tickers: [],
                    ticker: null,
                    reportType: null,
                    quarterT1: null,
                    quarterT2: null,
                    monthT1: null,
                    monthT2: null,
                    yearT1: null,
                    yearT2: null,
                    criteria: [],
                    timeType: 'q'
                };

                //check select time type
                $scope.selectTimeType = function () {
                    switch ($scope.report.data.timeType) {
                        case 'q':
                            $.each($scope.config, function (_k, _v) {
                                $scope.config[_k] = true;
                            });
                            $scope.config.notTimeQuarter = false;
                            break;
                        case 'y':
                            $.each($scope.config, function (_k, _v) {
                                $scope.config[_k] = true;
                            });
                            $scope.config.notTimeYear = false;
                            break;
                    }
                };
                $scope.selectTimeType();


                /*Transfer data between directive and parent scope*/
                var _izTransfer = {
                    testFunction: function () {
                        var promise = izReportInitDataService.testFunction();
                        promise.then(function (string) {
                            var _dataReport = izReportInitDataService.getDataReports();
                            $scope.dataReports = _dataReport.dataReports;
                            $scope.report.general = {
                                t1Label: 'Quý ' + _dataReport.t1,
                                t2Label: 'Quý ' + _dataReport.t2,
                                typeLabel: _dataReport.reportType
                            }
                        }, function (error) {

                        });
                    },
                    initAutoComplete: function (tickers) {
                        tickers = JSON.parse(tickers);
                        $scope.report.data.tickers = tickers;
                        //console.log(tickers);
                        $(".tickertext").bind("keydown", function (event) {
                            if (event.keyCode === $.ui.keyCode.TAB &&
                                $(this).autocomplete("instance").menu.active) {
                                event.preventDefault();
                            }
                        }).autocomplete({
                            minLength: 0,
                            source: tickers,
                        });
                        function split(val) {
                            return val.split(/,\s*/);
                        }

                        function extractLast(term) {
                            return split(term).pop();
                        }
                    }
                };
                $scope.izTransfer({'transfer': _izTransfer});

                //get data all time quarter
                $scope.getDataTime = function (event) {
                    $scope.report.data.ticker = $scope.report.data.ticker.toUpperCase();
                    if (event.keyCode == 13 || $scope.report.data.ticker.length == 3) {
                        if ($scope.report.data.tickers.indexOf($scope.report.data.ticker) == -1) {
                            izReportInitDataService.showNotify('Không tìm thấy cố phiếu này');
                            return false;
                        }

                        var timeType = 'quarter';
                        switch ($scope.report.data.timeType) {
                            case 'q':
                                timeType = 'quarter';
                                break;
                            case 'y':
                                //DO: Không lấy data year mà bắt phải nhập
                                return;
                        }
                        var dataReport = {
                            'ticker': $scope.report.data.ticker,
                            'reportType': $scope.izReportType,
                            'timeType': timeType
                        };

                        izReportInitDataService.setDataReport(dataReport);
                        var getDataAllTimePromise = izReportInitDataService.getDataTimeFromSv();
                        getDataAllTimePromise.then(function (string) {
                            var dataOptionGroup = izReportInitDataService.processDataTime();
                            $scope.report.data.quarterT1.setData(dataOptionGroup);
                            $scope.report.data.quarterT2.setData(dataOptionGroup);
                        }, function (error) {
                            izReportInitDataService.showNotify(error);
                        });
                    }
                };

                $scope.selectTicker = function () {
                    $scope.notDownloadAble = true;
                    // Check ticker in tickers:
                    if ($scope.report.data.tickers.indexOf($scope.report.data.ticker) == -1) {
                        izReportInitDataService.showNotify('Không tìm thấy cố phiếu này');
                        return false;
                    }
                    var t1, t2, timeType;
                    // get data time t1/t2
                    switch ($scope.report.data.timeType) {
                        case 'q':
                            timeType = 'quarter';
                            t1 = $scope.report.data.quarterT1.getData();
                            t2 = $scope.report.data.quarterT2.getData();
                            break;
                        case 'y':
                            t1 = $scope.report.data.yearT1;
                            t2 = $scope.report.data.yearT2;
                            break;
                            break;
                    }

                    if ($scope.report.data.ticker != null && t1 != null && t2 != null && $scope.report.data.ticker != '') {
                        showLoadMask();
                        var dataReport = {
                            'ticker': $scope.report.data.ticker,
                            't1': t1,
                            't2': t2,
                            'reportType': $scope.izReportType,
                            'timeType': timeType
                        };

                        izReportInitDataService.setDataReport(dataReport);

                        var promise = izReportInitDataService.initDataReports();
                        promise.then(function (string) {
                            hideLoadMask();
                            $scope.notDownloadAble = false;
                            var _dataReport = izReportInitDataService.getDataReports();
                            $scope.dataReports = _dataReport.dataReports;
                            if (timeType == 'quarter')
                                $scope.report.general = {
                                    t1Label: 'Quý ' + _dataReport.t1,
                                    t2Label: 'Quý ' + _dataReport.t2,
                                    typeLabel: _dataReport.reportType
                                };
                            if (timeType == 'month')
                                $scope.report.general = {
                                    t1Label: t1.substring(0, 1) + 'th' + '/2015',
                                    t2Label: t2.substring(0, 1) + 'th' + '/2015',
                                    typeLabel: _dataReport.reportType
                                };
                        }, function (error) {
                            hideLoadMask();
                            izReportInitDataService.showNotify(error);
                        })
                    } else {
                        izReportInitDataService.showNotify('Xin hãy nhập đủ các trường');
                    }
                };

                /*DO: for download excel*/
                $scope.downloadExcel= function(){
                    izReportInitDataService.getDownloadExcel();
                }

            }],
            link: function (scope, elem, atts) {

            }
            ,
            template: _templateTable
        }
    }

    function izReportInitDataService($window, $http, $q) {
        this.data = {
            ticker: null,
            criteria: [],
            t1: null,
            t2: null,
            timeType: null,
            reportType: null,
            dataT1: [],
            dataT2: [],
            dataReports: [],
            dataAllTime: []
        };

        this.initingData = false; // Kiểm tra xem hiện tại có đang lấy dữ liệu hay không

        this.getDataReports = function () {
            return this.data;
        };

        this.setDataReport = function (data) {
            if (typeof data == 'object') {
                this.data.ticker = data.ticker;
                this.data.reportType = data.reportType;
                this.data.t1 = data.t1;
                this.data.t2 = data.t2;
                this.data.timeType = data.timeType
            }
        };

        this.testFunction = function () {
            if (this.initingData == true) {
                $window.alert('initing data');
                return false;
            }

            this.data.ticker = 'BMP';
            this.data.reportType = 'KQKD';
            this.data.t1 = '3-2015';
            this.data.t2 = '2-2015';
            return this.initDataReports();
        };

        this.getDataReportTicker = function () {
            return $q(function (reslove, reject) {
                var _url = window.urlReportGetData + '?dataType=dataReportTicker' + '&ticker=' + this.data.ticker + '&reportType=' + this.data.reportType;
                if (this.data.t1 != null)
                    _url += '&t1=' + this.data.t1;
                if (this.data.t2 != null)
                    _url += '&t2=' + this.data.t2;
                $http({
                    method: 'GET',
                    url: _url
                }).then(function successCallback(response) {
                    var _data = response.data;
                    if (_data.hasOwnProperty('error') && _data.error == true) {
                        reject(_data.message);
                    } else {
                        if (_data.hasOwnProperty('dataT1') && _data.dataT1 != null)
                            this.data.dataT1 = JSON.parse(_data.dataT1);
                        if (_data.hasOwnProperty('dataT2') && _data.dataT2 != null)
                            this.data.dataT2 = JSON.parse(_data.dataT2);
                        reslove('got data report tickers');
                    }
                }.bind(this), function errorCallback(response) {
                    // called asynchronously if an error occurs
                    // or server returns response with an error status.
                    reject('error');
                });
            }.bind(this));
        };

        this.getDataCriteria = function () {
            return $q(function (reslove, reject) {
                $http({
                    method: 'GET',
                    url: window.urlReportGetData + '?dataType=criteria' + '&ticker=' + this.data.ticker + '&reportType=' + this.data.reportType
                }).then(function successCallback(response) {
                    _data = response.data;
                    if (_data.hasOwnProperty('error') && _data.error == true) {
                        reject(_data.message);
                    } else {
                        this.data.criteria = _data.dataCriteria;
                        reslove('got data criteria ');
                    }
                }.bind(this), function errorCallback(response) {
                    // called asynchronously if an error occurs
                    // or server returns response with an error status.
                    reject('error');
                });
            }.bind(this));

        };

        this.initDataReports = function () {
            this.initingData = true;
            var _dataCriteria = this.getDataCriteria();
            var _dataReportTicker = this.getDataReportTicker();

            return $q(function (reslove, reject) {

                _dataCriteria.then(function (string) {
                    console.log(string);
                    return _dataReportTicker; // return new promise
                }, function (error) {
                    reject(error);
                }).then(function (string) {
                    console.log(string);
                    var dataReports = [];
                    $.each(this.data.criteria, function (_k, _v) {
                            var _currentRow = {};

                            _currentRow.id = _v.row_id;
                            _currentRow.criteriaLabel = _v.row_name;
                            if (this.data.reportType == 'CSTC') {
                                _currentRow.t1 = parseFloat(this.data.dataT1[_v.row_id]) != 'NaN' ? parseFloat(this.data.dataT1[_v.row_id]).formatMoney(2, '.', ',') : '';
                                _currentRow.t2 = parseFloat(this.data.dataT2[_v.row_id]) != 'NaN' ? parseFloat(this.data.dataT2[_v.row_id]).formatMoney(2, '.', ',') : '';
                            } else {
                                _currentRow.t1 = parseFloat(this.data.dataT1[_v.row_id]) != 'NaN' ? parseFloat(this.data.dataT1[_v.row_id]).formatMoney(0, '.', ',') : '';
                                _currentRow.t2 = parseFloat(this.data.dataT2[_v.row_id]) != 'NaN' ? parseFloat(this.data.dataT2[_v.row_id]).formatMoney(0, '.', ',') : '';
                            }
                            if (!isNaN(this.data.dataT1[_v.row_id]) || !isNaN(this.data.dataT2[_v.row_id])) {
                                if (isNaN(this.data.dataT1[_v.row_id]))
                                    this.data.dataT1[_v.row_id] = 0;
                                if (isNaN(this.data.dataT2[_v.row_id]))
                                    this.data.dataT2[_v.row_id] = 0;

                                if (parseFloat(this.data.dataT2[_v.row_id]) > parseFloat(this.data.dataT1[_v.row_id])) {
                                    _currentRow.class = 'percentRed';
                                } else if (parseFloat(this.data.dataT2[_v.row_id]) < parseFloat(this.data.dataT1[_v.row_id]))
                                    _currentRow.class = 'percentBlue';


                                _currentRow.result = parseFloat(this.data.dataT1[_v.row_id]) - parseFloat(this.data.dataT2[_v.row_id]);

                                if (this.data.reportType == 'CSTC')
                                    _currentRow.result = _currentRow.result.formatMoney(2, ".", ",");
                                else
                                    _currentRow.result = _currentRow.result.formatMoney(0, ".", ",");

                                if (parseFloat(this.data.dataT2[_v.row_id]) == 0 && parseFloat(this.data.dataT1[_v.row_id]) == 0)
                                    _currentRow.result = '-';

                                if (this.data.dataT2[_v.row_id] != 0)
                                    _currentRow.percent = ((parseFloat(this.data.dataT1[_v.row_id]) - parseFloat(this.data.dataT2[_v.row_id])) * 100 / parseFloat(this.data.dataT2[_v.row_id]));
                                else if (this.data.dataT1[_v.row_id] != 0)
                                    _currentRow.percent = 100;
                                else
                                    _currentRow.percent = '-';
                            }
                            dataReports.push(_currentRow);
                        }
                        .
                        bind(this)
                    );

                    this.data.dataReports = dataReports;
                    this.initingData = false;
                    reslove('init success data report');
                }.bind(this), function (error) {
                    reject(error);
                });
            }.bind(this));
        };

        this.getDataTimeFromSv = function () {
            // co nhiem vu lay tat ca cac quarter cua 1 co phieu + loai bctc
            return $q(function (reslove, reject) {
                $http({
                    method: 'GET',
                    url: window.urlReportGetData + '?dataType=time' + '&ticker=' + this.data.ticker + '&reportType=' + this.data.reportType + '&timeType=' + this.data.timeType
                }).then(function successCallback(response) {
                    _data = response.data;
                    if (_data.hasOwnProperty('error') && _data.error == true) {
                        reject(_data.message);
                    } else {
                        if (this.data.timeType = 'quarter')
                            this.data.dataAllTime = _data.quartTime;
                        reslove('got data all time ticker');
                    }
                }.bind(this), function errorCallback(response) {
                    // called asynchronously if an error occurs
                    // or server returns response with an error status.
                    reject('error');
                });
            }.bind(this));
        };
        this.processDataTime = function () {
            if (this.data.timeType = 'quarter') {
                // Sau khi lay duoc het data quarter thi thuc hien phan tich du lieu da tra ve kieu du lieu nhu dang duoi
                var optgroups = [
                    {
                        label: 'Group 1234', children: [
                        {label: 'Option 1.1', value: '1-1', selected: true},
                        {label: 'Option 1.2', value: '1-2'},
                        {label: 'Option 1.3', value: '1-3'}
                    ]
                    },
                    {
                        label: 'Group 2234324', children: [
                        {label: 'Option 2.1', value: '1'},
                        {label: 'Option 2.2', value: '2'},
                        {label: 'Option 2.3', value: '3', disabled: true}
                    ]
                    }
                ];

                var arrayAll = {};
                $.each(this.data.dataAllTime, function (k, v) {
                    var currentArray = v.split('-');
                    if (arrayAll.hasOwnProperty(currentArray[1])) {
                        arrayAll[currentArray[1]].push(currentArray[0]);
                    } else {
                        arrayAll[currentArray[1]] = [];
                        arrayAll[currentArray[1]].push(currentArray[0]);
                    }
                });
                //Convert to optionGroupt
                var optGroups = [];
                $.each(arrayAll, function (k, v) {
                    var children = [];
                    $.each(v, function (key, value) {
                        var currentChildren = {label: 'Quý ' + value + '/' + k, value: value + '-' + k};
                        children.push(currentChildren);
                    });
                    optGroups.push({label: k, children: children});
                });

                return optGroups;
            }
        };
        this.showNotify = function (mess, type) {
            if (typeof type != 'undefined')
                switch (type) {
                    case 'danger':
                        $.notify({
                            // options
                            message: mess
                        }, {
                            // settings
                            type: 'danger'
                        });
                        break;
                    default:
                        $.notify({
                            // options
                            message: mess
                        }, {
                            // settings
                            type: 'danger'
                        });
                        break;
                }
            else $.notify({
                // options
                message: mess
            }, {
                // settings
                type: 'danger'
            });
        };
        this.getDownloadExcel = function () {
            if (this.data.ticker == '' && this.data.reportType == '')
                return false;
            var url = window.urlDownloadExcelReport;
            var param = {};
            param.criteria = this.data.criteria;
            param.ticker = this.data.ticker;
            param.dataReports = this.data.dataReports;
            param.time1 = this.data.t1;
            param.time2 = this.data.t2;
            param.reportType = this.data.reportType;
            this.sendAjaxDownloadExcel(param, url);
        };
        this.sendAjaxDownloadExcel = function (pram, url) {
            var xhr = new XMLHttpRequest();
            if (typeof url == 'undefined')
                xhr.open('POST', window.urlDownload, true);
            else
                xhr.open('POST', url, true);
            xhr.responseType = 'arraybuffer';
            showLoadMask();
            xhr.onload = function () {
                if (this.status === 200) {
                    hideLoadMask();
                    var filename = "";
                    var disposition = xhr.getResponseHeader('Content-Disposition');
                    if (disposition && disposition.indexOf('attachment') !== -1) {
                        var filenameRegex = /filename[^;=\n]*=((['"]).*?\2|[^;\n]*)/;
                        var matches = filenameRegex.exec(disposition);
                        if (matches != null && matches[1]) filename = matches[1].replace(/['"]/g, '');
                    }
                    var type = xhr.getResponseHeader('Content-Type');

                    var blob = new Blob([this.response], {type: type});
                    if (typeof window.navigator.msSaveBlob !== 'undefined') {
                        // IE workaround for "HTML7007: One or more blob URLs were revoked by closing the blob for which they were created. These URLs will no longer resolve as the data backing the URL has been freed."
                        window.navigator.msSaveBlob(blob, filename);
                    } else {
                        var URL = window.URL || window.webkitURL;
                        var downloadUrl = URL.createObjectURL(blob);

                        if (filename) {
                            // use HTML5 a[download] attribute to specify filename
                            var a = document.createElement("a");
                            // safari doesn't support this yet
                            if (typeof a.download === 'undefined') {
                                window.location = downloadUrl;
                            } else {
                                a.href = downloadUrl;
                                a.download = filename;
                                document.body.appendChild(a);
                                a.click();
                            }
                        } else {
                            window.location = downloadUrl;
                        }

                        setTimeout(function () {
                            URL.revokeObjectURL(downloadUrl);
                        }, 100); // cleanup
                    }
                }
            };
            xhr.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
            xhr.send($.param(pram));
            //hideLoadMask();
        }
    }

    izReportInitDataService.$inject = ['$window', '$http', '$q'];

})
(window, document, jQuery, angular);
