/**
 * Created by vjcspy on 11/21/15.
 */
var reportApp = angular.module('reportApp', ['ui.bootstrap', 'izReportTableApp'], function ($interpolateProvider) {
    $interpolateProvider.startSymbol('//');
    $interpolateProvider.endSymbol('//');
}).controller('reportController', ['$scope', '$window', function ($scope, $window) {
    // transfer data with directive
    $scope.izTransferKQKD = function (tranfer) {
        //bind izTransfer
        $scope.izTransferKQKD = tranfer;
    };
    $scope.izTransferLC = function (tranfer) {
        //bind izTransfer
        $scope.izTransferLC = tranfer;
    };
    $scope.izTransferCDKT = function (tranfer) {
        //bind izTransfer
        $scope.izTransferCDKT = tranfer;
    };
    $scope.izTransferCSTC = function (tranfer) {
        //bind izTransfer
        $scope.izTransferCSTC = tranfer;
    };

    $(document).ready(function () {
        //console.log($scope.tickers);
        $scope.izTransferKQKD.initAutoComplete($scope.tickers);
        $scope.izTransferLC.initAutoComplete($scope.tickers);
        $scope.izTransferCDKT.initAutoComplete($scope.tickers);
        $scope.izTransferCSTC.initAutoComplete($scope.tickers);
    });
}]);


