/**
 * Created by vjcspy on 11/5/15.
 */
var downloadApp = angular.module('downloadApp', ['daterangepicker'], function ($interpolateProvider) {
    $interpolateProvider.startSymbol('//');
    $interpolateProvider.endSymbol('//');
});

downloadApp.controller('downloadCtrl', ['$scope', function ($scope) {
    $scope.tickerDownload = {
        date: {startDate: moment(), endDate: moment()}
    };
    $(document).ready(function () {
        $("#tickertext").bind("keydown", function (event) {
            if (event.keyCode === $.ui.keyCode.TAB &&
                $(this).autocomplete("instance").menu.active) {
                event.preventDefault();
            }
        }).autocomplete({
            minLength: 0,
            source: function (request, response) {
                // delegate back to autocomplete, but extract the last term
                response($.ui.autocomplete.filter(
                    $scope.tickers, extractLast(request.term)));
            },
            focus: function () {
                // prevent value inserted on focus
                return false;
            },
            select: function (event, ui) {
                var terms = split(this.value);
                // remove the current input
                terms.pop();
                // add the selected item
                terms.push(ui.item.value);
                // add placeholder to get the comma-and-space at the end
                terms.push("");
                this.value = terms.join(", ");
                return false;
            }
        });
    });

    $scope.downloadTicker = function () {
        _tickerTextElem = $('#tickertext');
        tickerValue = _tickerTextElem.val();
        _arrayTicker = tickerValue.split(",");
        console.log(_arrayTicker);


        _existedInArray = 0;
        _exist = false;
        $.each(_arrayTicker, function (_k, _v) {
            $.each(_arrayTicker, function (k, v) {
                if (_v == v) {
                    _existedInArray += 1;
                }
            });
            _existedInArray = 0;
            if (_existedInArray > 1) {
                _exist = true;
                return false;
            }
        });

        if (_exist) {
            alert('Có 2 mã trùng nhau');
            return false;
        }
        var param = {ticker: tickerValue, from: $('#from').val(), format: $('#format').val()};
        sendAjaxDownload(param);
        //$('#formTickerDownload').submit();
    };

    $scope.fastDownloadTicker = function () {
        sendAjaxDownload({},window.urlFastDownload);
    }

}]);
function split(val) {
    return val.split(/,\s*/);
}
function extractLast(term) {
    return split(term).pop();
}

function sendAjaxDownload(pram, url) {
    var xhr = new XMLHttpRequest();
    if (typeof url == 'undefined')
        xhr.open('POST', window.urlDownload, true);
    else
        xhr.open('POST', url, true);
    xhr.responseType = 'arraybuffer';
    showLoadMask();
    xhr.onload = function () {
        if (this.status === 200) {
            hideLoadMask();
            var filename = "";
            var disposition = xhr.getResponseHeader('Content-Disposition');
            if (disposition && disposition.indexOf('attachment') !== -1) {
                var filenameRegex = /filename[^;=\n]*=((['"]).*?\2|[^;\n]*)/;
                var matches = filenameRegex.exec(disposition);
                if (matches != null && matches[1]) filename = matches[1].replace(/['"]/g, '');
            }
            var type = xhr.getResponseHeader('Content-Type');

            var blob = new Blob([this.response], {type: type});
            if (typeof window.navigator.msSaveBlob !== 'undefined') {
                // IE workaround for "HTML7007: One or more blob URLs were revoked by closing the blob for which they were created. These URLs will no longer resolve as the data backing the URL has been freed."
                window.navigator.msSaveBlob(blob, filename);
            } else {
                var URL = window.URL || window.webkitURL;
                var downloadUrl = URL.createObjectURL(blob);

                if (filename) {
                    // use HTML5 a[download] attribute to specify filename
                    var a = document.createElement("a");
                    // safari doesn't support this yet
                    if (typeof a.download === 'undefined') {
                        window.location = downloadUrl;
                    } else {
                        a.href = downloadUrl;
                        a.download = filename;
                        document.body.appendChild(a);
                        a.click();
                    }
                } else {
                    window.location = downloadUrl;
                }

                setTimeout(function () {
                    URL.revokeObjectURL(downloadUrl);
                }, 100); // cleanup
            }
        }
    };
    xhr.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
    xhr.send($.param(pram));
    //hideLoadMask();
}
function downloadTickerAdj(ticker, format) {
    _s = ticker + ',';
    var param = {ticker: _s, from: '2000-11-05 - 2018-11-05', format: format};
    sendAjaxDownload(param);
}
