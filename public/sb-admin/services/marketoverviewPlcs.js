/**
 * Created by vjcspy on 10/2/15.
 */
var marketOverview = angular.module('marketOverview', [], function ($interpolateProvider) {
    $interpolateProvider.startSymbol('//');
    $interpolateProvider.endSymbol('//');
});


marketOverview.factory('marketoverv', function ($window) {
    return function (number) {
        if (number % 2 == 0) {
            $window.alert("Số " + number + " là số chẵn");
        }
    };
});
