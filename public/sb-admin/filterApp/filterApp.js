/**
 * Created by vjcspy on 12/15/15.
 */
window.filterApp = angular.module('filterApp', ['ui.bootstrap', 'ngTable', 'ui.mask'], function ($interpolateProvider) {
    $interpolateProvider.startSymbol('//');
    $interpolateProvider.endSymbol('//');
});
window.filterApp.controller('FilterAppCtrl', ['$scope', '$http', 'NgTableParams', function ($scope, $http, NgTableParams) {
    $scope.isCollapsed = true;
    $scope.filterType = 'ABSOLUTE';
    $scope.filterTime = 'year';
    $scope.reportType = 'CSTC';
    $scope.isResulted = false;
    $scope.fileExcel = null;

    $scope.showFilterLabel = 'Show Filter';

    $scope.showHideFilter = function () {
        if ($scope.isCollapsed)
            $scope.showFilterLabel = 'Show Filter';
        else
            $scope.showFilterLabel = 'Hide Filter';
    };

    $scope.data = {
        filterGroups: [],
        criterias: []
    };

    $scope.changeReportType = function () {
        $scope.initDataForCriteria($scope.reportType);
    };

    $scope.tableParams = createUsingFullOptions();
    function createUsingFullOptions() {
        var initialParams = {
            count: 5, // initial page size
            //sorting: {net: "desc"}
            //filter: {net: "0"}
        };
        var initialSettings = {
            // page size buttons (right set of buttons in demo)
            //counts: [],
            // determines the pager buttons (left set of buttons in demo)
            //paginationMaxBlocks: 13,
            //paginationMinBlocks: 2,
            data: []
        };
        return new NgTableParams(initialParams, initialSettings);
    }

    /*Khi truyen vao reportType de lay ra danh sach cac criteria*/
    $scope.initDataForCriteria = function (reportType) {
        $scope.data.criterias = $scope.dataCriterias[reportType];
    };

    $(document).ready(function () {
        $scope.initDataForCriteria($scope.reportType);
    });


    $scope.addToFilterGroups = function () {
        if ($scope.criteriaId == '' || $scope.criteriaId == null)
            return false;
        var criteriaName = getCriteriaName();


        /*Add to FilterGroups*/
        var currentFilter = {
            reportType: $scope.reportType,
            filterType: $scope.filterType,
            criteriaId: $scope.criteriaId,
            condition: 'gt',
            checkValue: 0,
            criteriaName: criteriaName,
            isAbsolute: true,
            timeLabel: 'Năm',
            timeType: $scope.filterTime,
            time1: 2013,
            time2: 2014,
            checkValue2: 0,
            isYear: true
        };

        currentFilter.isAbsolute = $scope.filterType == 'ABSOLUTE';
        currentFilter.timeLabel = $scope.filterTime == 'year' ? 'Time' : 'Time';

        var isExisted = false;
        //DO: check trung. remove theo yeu cau cua khach
        //$.each($scope.data.filterGroups, function (k, v) {
        //    if (v.criteriaId == currentFilter.criteriaId) {
        //        $.notify({
        //            // options
        //            message: 'Đã có filter này rồi!'
        //        }, {
        //            // settings
        //            type: 'danger'
        //        });
        //        isExisted = true;
        //        return false;
        //    }
        //});
        if (!isExisted) {
            $scope.data.filterGroups.push(currentFilter);
            /* Add to data row result*/
            if ($scope.filterType == 'ABSOLUTE')
                $scope.result.dataRow.push({
                    field: $scope.criteriaId,
                    title: criteriaName,
                    sortable: $scope.criteriaId + '_sort',
                    show: true
                });
            else
                $scope.result.dataRow.push({
                    field: 'TT_' + $scope.criteriaId,
                    title: criteriaName,
                    sortable: $scope.criteriaId + '_sort',
                    show: true
                });
        }
    };

    /*Xoa 1 filter khoi filterGroups*/
    $scope.removeFilter = function (criteriaId) {
        var index = null;
        var filterType;
        $.each($scope.data.filterGroups, function (k, v) {
            if (criteriaId == v.criteriaId) {
                //index = $scope.data.filterGroups.indexOf(v);
                index = k;
                filterType = v.filterType
            }
        });
        if (index != null)
            $scope.data.filterGroups.splice(index, 1);

        /*Remove khoi result row data*/
        removeCriteriaInDataRowResult(criteriaId, filterType);
    };

    /*Lay Label cua filter hien tai khi add vao filterGroups*/
    function getCriteriaName() {
        var result;
        $.each($scope.data.criterias, function (k, v) {
            if (v.id == $scope.criteriaId) {
                result = (v.name);
                return false;
            }
        });
        return result;
    }

    $scope.data.result = [];

    /*Send filter To server*/
    $scope.sendFilter = function () {
        showLoadMask();
        //console.log($scope.data.filterGroups);
        var req = {
            method: 'POST',
            url: window.urlPostFilter,
            headers: {
                'Content-type': 'application/json'
            },
            data: {filterData: $scope.data.filterGroups}
        };

        $http(req).then(function (response) {
            hideLoadMask();
            var data = response.data.resultFilter;
            var rocData = response.data.rocData;
            var result = [];
            if (data.hasOwnProperty('year')) {
                result = data['year'];
            } else if (data.hasOwnProperty('quarter')) {
                result = data['quarter'];
            } else {
                result = data;
            }
            $scope.isResulted = true;

            if (response.data.hasOwnProperty('fileName'))
                $scope.fileExcel = response.data.fileName;
            else
                $scope.fileExcel = null;

            $scope.data.result = [];

            $.each(result, function (k, v) {
                currentResult = v;
                currentResult.name = k;
                currentResult.sort = 0;
                if (rocData.hasOwnProperty(k)) {
                    currentResult.roc = parseFloat(rocData[k].roc);
                    currentResult.close = parseFloat(rocData[k].close);
                } else {
                    return true;
                }
                /*TODO: để tạo 1 trường number dùng so sánh*/
                $.each(v, function (key, value) {
                    temp = '' + value;
                    temp = temp.replace('%', '');
                    value = temp.replace(',', '');
                    currentResult[key + '_sort'] = isNaN(value) ? 0 : parseFloat(value);
                });

                $scope.data.result.push(currentResult);
            });

            console.log($scope.data.result.push);

            $scope.tableParams = createUsingFullOptions();
            function createUsingFullOptions() {
                var initialParams = {
                    count: 10 // initial page size
                    //sorting: {tnet: "desc"}
                };
                var initialSettings = {
                    // page size buttons (right set of buttons in demo)
                    //counts: [],
                    // determines the pager buttons (left set of buttons in demo)
                    //paginationMaxBlocks: 10,
                    //paginationMinBlocks: 2,
                    //sorting: {net: "desc"},
                    data: $scope.data.result
                };
                return new NgTableParams(initialParams, initialSettings);
            }

        }, function (response) {
            hideLoadMask();
            $.notify({
                // options
                message: 'Có lỗi khi filter!'
            }, {
                // settings
                type: 'danger'
            });
        });
    };

    /*RESULT*/
    $scope.result = {};
    $scope.result.dataRow = [
        {field: "name", title: "Mã", sortable: "name", show: true},
        {field: "close", title: "Close", sortable: "close", show: true},
        {field: "roc", title: "Percent", sortable: "roc", show: true}
    ];

    $scope.$on('ngRepeatFinished', function (ngRepeatFinishedEvent) {
        //window.DataTable1 = $('#example').DataTable();
    });
    function removeCriteriaInDataRowResult(criteriaId, filterType) {
        if (filterType == 'RELATIVE')
            criteriaId = 'TT_' + criteriaId;
        $.each($scope.result.dataRow, function (k, v) {
            if (criteriaId == v.field)
                $scope.result.dataRow.splice(k, 1);
        })
    }

    $scope.downloadExcel = function () {
        if ($scope.fileExcel != null) {
            var param = {fileName: $scope.fileExcel};
            sendAjaxDownloadExcel(param, window.urlPostDownloadExcelFilter);
        } else $.notify({
            // options
            message: 'Có lỗi khi tải file!'
        }, {
            // settings
            type: 'danger'
        });
    };
    function sendAjaxDownloadExcel(pram, url) {
        var xhr = new XMLHttpRequest();
        if (typeof url == 'undefined')
            xhr.open('POST', window.urlDownload, true);
        else
            xhr.open('POST', url, true);
        xhr.responseType = 'arraybuffer';
        showLoadMask();
        xhr.onload = function () {
            if (this.status === 200) {
                hideLoadMask();
                var filename = "";
                var disposition = xhr.getResponseHeader('Content-Disposition');
                if (disposition && disposition.indexOf('attachment') !== -1) {
                    var filenameRegex = /filename[^;=\n]*=((['"]).*?\2|[^;\n]*)/;
                    var matches = filenameRegex.exec(disposition);
                    if (matches != null && matches[1]) filename = matches[1].replace(/['"]/g, '');
                }
                var type = xhr.getResponseHeader('Content-Type');

                var blob = new Blob([this.response], {type: type});
                if (typeof window.navigator.msSaveBlob !== 'undefined') {
                    // IE workaround for "HTML7007: One or more blob URLs were revoked by closing the blob for which they were created. These URLs will no longer resolve as the data backing the URL has been freed."
                    window.navigator.msSaveBlob(blob, filename);
                } else {
                    var URL = window.URL || window.webkitURL;
                    var downloadUrl = URL.createObjectURL(blob);

                    if (filename) {
                        // use HTML5 a[download] attribute to specify filename
                        var a = document.createElement("a");
                        // safari doesn't support this yet
                        if (typeof a.download === 'undefined') {
                            window.location = downloadUrl;
                        } else {
                            a.href = downloadUrl;
                            a.download = filename;
                            document.body.appendChild(a);
                            a.click();
                        }
                    } else {
                        window.location = downloadUrl;
                    }

                    setTimeout(function () {
                        URL.revokeObjectURL(downloadUrl);
                    }, 100); // cleanup
                }
            }
        };
        xhr.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
        xhr.send($.param(pram));
        //hideLoadMask();
    }
}]);

window.filterApp.directive('onFinishRender', function ($timeout) {
    return {
        restrict: 'A',
        link: function (scope, element, attr) {
            if (scope.$last === true) {
                $timeout(function () {
                    console.log('eventName:' + attr.onFinishRender);
                    scope.$emit(attr.onFinishRender);
                    scope.$last = false;
                });
            }
        }
    }
});

window.filterApp.directive('percentColor', function () {
    return {
        restrict: 'A',
        link: function (scope, elemnt, attr) {
            if (attr.percentLabel == 'roc') {
                if (parseFloat(attr.percentValue) < 0)
                    $(elemnt).css('color', 'red');
                else if (parseFloat(attr.percentValue) > 0)
                    $(elemnt).css('color', 'green');
                else
                    $(elemnt).css('color', 'yellow');
                $(elemnt).text(attr.percentValue + '%');
            }
            else
                $(elemnt).text(attr.percentValue);
        }
    }
});
