/**
 * Created by vjcspy on 10/3/15.
 */
var market = angular.module('market', ['chart.js', 'ngTable'], function ($interpolateProvider) {
    $interpolateProvider.startSymbol('//');
    $interpolateProvider.endSymbol('//');
});


market.controller('marketController', ['$scope', '$http', '$timeout', 'calMarket', 'NgTableParams', function ($scope, $http, $timeout, calMarket, NgTableParams, simpleList) {
    /*DO: INIT DATA PLCS*/
    $scope.all = {
        'percent': 0,
        'vol': 0,
        'value': 0,
        'sv1': '0',
        'svbq': '0'
    };

    $scope.hose = {
        'percent': 0,
        'vol': 0,
        'value': 0,
        'sv1': '0',
        'svbq': '0'
    };

    $scope.hax = {
        'percent': 0,
        'vol': 0,
        'value': 0,
        'sv1': '0',
        'svbq': '0'
    };
    $scope.coban = {
        'percent': 0,
        'vol': 0,
        'value': 0,
        'sv1': '0',
        'svbq': '0'
    };
    $scope.dauco = {
        'percent': 0,
        'vol': 0,
        'value': 0,
        'sv1': '0',
        'svbq': '0'
    };
    $scope.blue = {
        'percent': 0,
        'vol': 0,
        'value': 0,
        'sv1': '0',
        'svbq': '0'
    };
    $scope.penny = {
        'percent': 0,
        'vol': 0,
        'value': 0,
        'sv1': '0',
        'svbq': '0'
    };


    /*DO: INIT DATA NN*/

    var data = [{
        ticker: '',
        boar: '',
        percent: '0.00%',
        vol: 0,
        nnm: 0,
        mValue: 0,
        bValue: 0,
        bPercent: 0,
        sPercent: 0,
        nnb: 0,
        net: 0
    }];

    $scope.customConfigParams = createUsingFullOptions();
    function createUsingFullOptions() {
        var initialParams = {
            count: 5, // initial page size
            sorting: {net: "desc"}
            //filter: {net: "0"}
        };
        var initialSettings = {
            // page size buttons (right set of buttons in demo)
            //counts: [],
            // determines the pager buttons (left set of buttons in demo)
            //paginationMaxBlocks: 13,
            //paginationMinBlocks: 2,
            data: []
        };
        return new NgTableParams(initialParams, initialSettings);
    }

    $scope.detailedForeign = {};

    //$scope.nnm1 = {
    //    'ticker': 0,
    //    'percent': 0,
    //    'vol': 0,
    //    'value': 0,
    //    'percentValue': 0,
    //    'net': 0
    //};
    //$scope.nnm2 = {
    //    'ticker': 0,
    //    'percent': 0,
    //    'vol': 0,
    //    'value': 0,
    //    'percentValue': 0,
    //    'net': 0
    //};
    //$scope.nnm3 = {
    //    'ticker': 0,
    //    'percent': 0,
    //    'vol': 0,
    //    'value': 0,
    //    'percentValue': 0,
    //    'net': 0
    //};
    //$scope.nnm4 = {
    //    'ticker': 0,
    //    'percent': 0,
    //    'vol': 0,
    //    'value': 0,
    //    'percentValue': 0,
    //    'net': 0
    //};
    //$scope.nnm5 = {
    //    'ticker': 0,
    //    'percent': 0,
    //    'vol': 0,
    //    'value': 0,
    //    'percentValue': 0,
    //    'net': 0
    //};
    //
    //$scope.nnb1 = {
    //    'ticker': 0,
    //    'percent': 0,
    //    'vol': 0,
    //    'value': 0,
    //    'percentValue': 0,
    //    'net': 0
    //};
    //$scope.nnb2 = {
    //    'ticker': 0,
    //    'percent': 0,
    //    'vol': 0,
    //    'value': 0,
    //    'percentValue': 0,
    //    'net': 0
    //};
    //$scope.nnb3 = {
    //    'ticker': 0,
    //    'percent': 0,
    //    'vol': 0,
    //    'value': 0,
    //    'percentValue': 0,
    //    'net': 0
    //};
    //$scope.nnb4 = {
    //    'ticker': 0,
    //    'percent': 0,
    //    'vol': 0,
    //    'value': 0,
    //    'percentValue': 0,
    //    'net': 0
    //};
    //$scope.nnb5 = {
    //    'ticker': 0,
    //    'percent': 0,
    //    'vol': 0,
    //    'value': 0,
    //    'percentValue': 0,
    //    'net': 0
    //};
    $scope.foreign = [];
    $scope.bSelect = 'all';
    $scope.mSelect = 'all';
    /*Cash flow Chart*/
    $scope.optionsChart = {
        showScale: true,
        scaleShowLabels: true,
        segmentShowStroke: true,
        segmentStrokeColor: "#fff",
        segmentStrokeWidth: 2,
        percentageInnerCutout: 50,
        animation: true,
        animationSteps: 100,
        animationEasing: "easeOutBounce",
        animateRotate: true,
        animateScale: true,
        onAnimationComplete: null,
        labelFontFamily: "Arial",
        labelFontStyle: "normal",
        labelFontSize: 24,
        labelFontColor: "#666"
    };
    $scope.legendChart = true;
    $scope.coloursChart = ['#31E85C', '#CF0000', '#ABABAB'];
    $scope.cashFlowTable = {
        dataCashFlow: [],
        dataDetail: {}
    };

    /*Index va nuoc ngoai chart*/
    $scope.intraday = {
        labelsChartIndexFore: [],
        seriesChartIndexFore: ['Blue Index', 'Penny Index'],
        dataChartIndexFore: [],
        colors: ['#1EF9A1', '#FD1F5E']
    };
    $scope.onClickChartIndexFore = function (points, evt) {
        console.log(points, evt);
    };


    /*TODO:init DATA XU THE VA NONG LANH*/
    $scope.trendSelect = 'all';

    $scope.trendShow = {
        'oneM': {pos: 0, neg: 0, info: 0},
        'thirdM': {pos: 0, neg: 0, info: 0},
        'sixM': {pos: 0, neg: 0, info: 0},
        'sma10': {pos: 0, neg: 0, info: 0},
        'sma20': {pos: 0, neg: 0, info: 0, add: ''},
        'sma50': {pos: 0, neg: 0, info: 0, add: ''},
        'sma100': {pos: 0, neg: 0, info: 0, add: ''},
        'uptrend': {pos: 0, neg: 0, info: 0},
        'rs': {pos: 0, neg: 0, info: 0},
        'r1': {pos: 0, neg: 0, info: 0},
        'penny': {pos: 0, neg: 0, closeUpTwoDay: 0, closeDownTwoDay: 0}
    };

    $scope.inde = {
        'sv1': {volIn: 0, volDe: 0},
        'svbq': {volIn: 0, volDe: 0},
        'all': {
            priceIn: 0,
            priceInSv1VolIn: 0,
            priceInSv1VolDe: 0,
            priceInSvbqVolIn: 0,
            priceInSvbqVolDe: 0,
            priceDe: 0,
            priceDeSv1VolIn: 0,
            priceDeSv1VolDe: 0,
            priceDeSvbqVolIn: 0,
            priceDeSvbqVolDe: 0
        }
    };

    $scope.index = {
        vni: {index: 0, percent: 0, vol: 0, value: 0, udh: 0, gtnnm: 0, gtnnb: 0, gtr: 0},
        hnx: {index: 0, percent: 0, vol: 0, value: 0, udh: 0, gtnnm: 0, gtnnb: 0, gtr: 0}
    };


    $scope.changeBoardBForeign = function (type) {
        calMarket.showForeign($scope, $scope.foreign, type);
    };

    $scope.changeShowTrend = function () {
        calMarket.showXuthe($scope.dataShowTrend, $scope);
    };

    $scope.cashFlowSelect = 'net';
    $scope.tableDetailStyle = {display: 'none'};
    $scope.changeShowCashFlow = function () {
        calMarket.showTrend(result.topTrend, $scope);
    };

    /*TODO: Chứa các giá trị toàn cục cho các controller con sử dụng*/
    /*DO: ticker_inday*/
    $scope.tickerInday = {};

    $scope.$on('ngRepeatFinished', function (ngRepeatFinishedEvent) {
    });

    $scope.getTickerInday = function () {
        $http({
            method: 'POST',
            url: window.urlGetTickerInday,
            data: {'date': window.currentDate},
            headers: {'Content-Type': 'application/x-www-form-urlencoded'}
        }).then(function (response) {
                /*test ng-table*/
                //console.log(response);
                $scope.tickerInday = response.data.tickerInDay;
                //console.log($scope.cachePlcsNextDay);
                //console.log($scope.tickerClass.VCG);
                //console.log($scope.tickerBoard.VCG.board);
                //console.log($scope.tickerInday.data.VCG.close);
                //console.log($scope.trendData.higtLow['1M']);
                //console.log($scope.cachePlcsNextDay.svbqTop);
                result = calMarket.cal($scope.tickerInday, $scope.tickerBoard, $scope.tickerClass, $scope.tickerLastDay, $scope.cachePlcsNextDay, $scope.trendData, $scope.trendSmaData, $scope.uptrendData, $scope.dataRs, $scope.dataPennyCloseTwoDayAgo, null);
                result.hose.percent = response.data.indexInday.VNINDEX.percent;
                result.hax.percent = response.data.indexInday.HNXINDEX.percent;

                /*Hien thi ra bang index*/
                $scope.index.vni.index = response.data.indexInday.VNINDEX.index;
                $scope.index.vni.percent = response.data.indexInday.VNINDEX.change + '(' + response.data.indexInday.VNINDEX.percent + '%)';
                $scope.index.vni.vol = response.data.indexInday.VNINDEX.vol;
                $scope.index.vni.value = response.data.indexInday.VNINDEX.value;
                $scope.index.vni.udh = (result.index['hose']).up + '/' + (result.index['hose']).down + '/' + (result.index['hose']).hold;
                $scope.index.vni.gtnnm = (result.index['hose'].gtnnm / 1000000).toFixed(2);
                $scope.index.vni.gtnnb = (result.index['hose'].gtnnb / 1000000).toFixed(2);

                $scope.index.hnx.index = response.data.indexInday.HNXINDEX.index;
                $scope.index.hnx.percent = response.data.indexInday.HNXINDEX.change + '(' + response.data.indexInday.HNXINDEX.percent + '%)';
                $scope.index.hnx.vol = response.data.indexInday.HNXINDEX.vol;
                $scope.index.hnx.value = response.data.indexInday.HNXINDEX.value;
                $scope.index.hnx.udh = (result.index['hnx']).up + '/' + (result.index['hnx']).down + '/' + (result.index['hnx']).hold;
                $scope.index.hnx.gtnnm = (result.index['hnx'].gtnnm / 1000000).toFixed(2);
                $scope.index.hnx.gtnnb = (result.index['hnx'].gtnnb / 1000000).toFixed(2);

                /*Hien thi chart intraday*/
                var dataChartIntraday = response.data.dataIntraday,
                    arrayLabel = [],
                    arrayDataBlue = [],
                    arrayDataPenny = [];

                $.each(dataChartIntraday, function (_k, _v) {
                    arrayLabel.push(_v['create_at']);
                    arrayDataBlue.push(_v['bPercent']);
                    arrayDataPenny.push(_v['pPercent']);
                });
                //console.log(arrayLabel);
                //console.log(arrayDataBlue);
                //console.log(arrayDataPenny);
                $scope.intraday.labelsChartIndexFore = arrayLabel;
                $scope.intraday.seriesChartIndexFore = ['Blue Index', 'Penny Index'];
                $scope.intraday.dataChartIndexFore = [
                    arrayDataBlue,
                    arrayDataPenny
                ];

                /*DO: DOI CLASS PERCENT DE HIEN THI MAU*/
                var arrPl = ['hax', 'hose', 'all', 'coban', 'dauco', 'blue', 'penny'];
                for (temp = 0; temp < arrPl.length; temp++) {
                    var i = arrPl[temp];
                    var percent = i + "Percent";
                    var sv1Percent = i + "Sv1Percent";
                    var svbqPercent = i + "SvbqPercent";
                    $scope[percent] = 'percentYellow';
                    $scope[sv1Percent] = 'percentYellow';
                    $scope[svbqPercent] = 'percentYellow';
                    if (result[i].percent > 0) {
                        $scope[percent] = 'percentBlue';
                    }
                    if (result[i].percent < 0) {
                        $scope[percent] = 'percentRed';
                    }

                    if (result[i].sv1 > 0) {
                        $scope[sv1Percent] = 'percentBlue';
                    }
                    if (result[i].sv1 < 0) {
                        $scope[sv1Percent] = 'percentRed';
                    }

                    if (result[i].svbq > 0) {
                        $scope[svbqPercent] = 'percentBlue';
                    }
                    if (result[i].svbq < 0) {
                        $scope[svbqPercent] = 'percentRed';
                    }
                }

                /*DO: Gan gia tri*/
                $scope.hax = result.hax;
                $scope.hose = result.hose;
                $scope.all = result.all;
                $scope.coban = result.coban;
                $scope.dauco = result.dauco;
                $scope.blue = result.blue;
                $scope.penny = result.penny;


                $scope.hax.vol = (result.hax.vol).formatMoney(0, '.', ',') + " cp";
                $scope.hose.vol = (result.hose.vol).formatMoney(0, '.', ',') + " cp";
                $scope.all.vol = (result.all.vol).formatMoney(0, '.', ',') + " cp";
                $scope.coban.vol = (result.coban.vol).formatMoney(0, '.', ',') + " cp";
                $scope.dauco.vol = (result.dauco.vol).formatMoney(0, '.', ',') + " cp";
                $scope.blue.vol = (result.blue.vol).formatMoney(0, '.', ',') + " cp";
                $scope.penny.vol = (result.penny.vol).formatMoney(0, '.', ',') + " cp";


                $scope.hax.value = (result.hax.value / 1000000).formatMoney(2, '.', ',') + " tỷ";
                $scope.hose.value = (result.hose.value / 1000000).formatMoney(2, '.', ',') + " tỷ";
                $scope.all.value = (result.all.value / 1000000).formatMoney(2, '.', ',') + " tỷ";
                $scope.coban.value = (result.coban.value / 1000000).formatMoney(2, '.', ',') + " tỷ";
                $scope.dauco.value = (result.dauco.value / 1000000).formatMoney(2, '.', ',') + " tỷ";
                $scope.blue.value = (result.blue.value / 1000000).formatMoney(2, '.', ',') + " tỷ";
                $scope.penny.value = (result.penny.value / 1000000).formatMoney(2, '.', ',') + " tỷ";

                $scope.hax.percent = (result.hax.percent * 1).toFixed(2) + "%";
                $scope.hose.percent = (result.hose.percent * 1).toFixed(2) + "%";
                $scope.all.percent = (result.all.percent * 100).toFixed(2) + "%";
                $scope.coban.percent = (result.coban.percent * 100).toFixed(2) + "%";
                $scope.dauco.percent = (result.dauco.percent * 100).toFixed(2) + "%";
                $scope.blue.percent = (result.blue.percent * 100).toFixed(2) + "%";
                $scope.penny.percent = (result.penny.percent * 100).toFixed(2) + "%";

                $scope.hax.sv1 = (result.hax.sv1 * 100).toFixed(2) + "%";
                $scope.hose.sv1 = (result.hose.sv1 * 100).toFixed(2) + "%";
                $scope.all.sv1 = (result.all.sv1 * 100).toFixed(2) + "%";
                $scope.coban.sv1 = (result.coban.sv1 * 100).toFixed(2) + "%";
                $scope.dauco.sv1 = (result.dauco.sv1 * 100).toFixed(2) + "%";
                $scope.blue.sv1 = (result.blue.sv1 * 100).toFixed(2) + "%";
                $scope.penny.sv1 = (result.penny.sv1 * 100).toFixed(2) + "%";

                $scope.hax.svbq = (result.hax.svbq * 100).toFixed(2) + "%";
                $scope.hose.svbq = (result.hose.svbq * 100).toFixed(2) + "%";
                $scope.all.svbq = (result.all.svbq * 100).toFixed(2) + "%";
                $scope.coban.svbq = (result.coban.svbq * 100).toFixed(2) + "%";
                $scope.dauco.svbq = (result.dauco.svbq * 100).toFixed(2) + "%";
                $scope.blue.svbq = (result.blue.svbq * 100).toFixed(2) + "%";
                $scope.penny.svbq = (result.penny.svbq * 100).toFixed(2) + "%";

                /*DO: show foreign*/
                $scope.foreign = result.foreign;
                $scope.detailedForeign = result.detailedForeign;

                $scope.customConfigParams = createUsingFullOptions();
                function createUsingFullOptions() {
                    var initialParams = {
                        count: 10, // initial page size
                        sorting: {tnet: "desc"}
                    };
                    var initialSettings = {
                        // page size buttons (right set of buttons in demo)
                        //counts: [],
                        // determines the pager buttons (left set of buttons in demo)
                        //paginationMaxBlocks: 10,
                        //paginationMinBlocks: 2,
                        //sorting: {net: "desc"},
                        data: $scope.foreign
                    };
                    return new NgTableParams(initialParams, initialSettings);
                }

                //calMarket.showForeign($scope, $scope.foreign, 'all');
                $scope.dataShowCashFlow = result.topTrend;
                calMarket.showTrend($scope.dataShowCashFlow, $scope);
                /*IMPORTANCE: LOOP*/


                /*DO: show Xu the*/
                $scope.dataShowTrend = result.dataTrend;
                calMarket.showXuthe(result.dataTrend, $scope);

                /*DO: show tang giam*/
                //$scope.dataInde = result.inde;
                calMarket.showInde(result.inde, $scope);
                $timeout($scope.getTickerInday, 60000);
            }
            ,
            function (response) {
                console.log('Error');

                $timeout($scope.getIndexPercent, 60000);
            }
        );
    };
    $(document).ready(function () {
        $scope.getTickerInday();
    });
}
])
;

/*TODO: Phân lớp và chỉ số*/
market.controller('plcs', ['$scope', 'connectDb', '$http', '$timeout', function ($scope, connectDb, $http, $timeout) {
    $scope.getIndexPercent1 = function () {
        /*FIXME: TAI SAO CAI NAY LAI CHAY LIEN TUC*/
        var data = {'date': window.currentDate};
        $http.post(window.urlPostIndexPercent, data).then(function (response) {
            setTimeout($scope.getIndexPercent(), 10000);
        }, function () {
            setTimeout($scope.getIndexPercent(), 10000);
        });
    };

    $scope.getIndexPercent2 = function () {
        /*FIXME: Chay cai nay thi loi: AngularJS TypeError: Cannot read property 'protocol' of undefined*/
        $http(connectDb.urlGetPercentIndexCurrentDate()).then(function (response) {
            console.log('a');
            $timeout($scope.getIndexPercent2, 10000);
        }, function (response) {
            console.log('b');
            $timeout($scope.getIndexPercent2, 10000);
        });
    };

    $scope.getIndexPercent = function () {
        $http({
            method: 'POST',
            url: window.urlPostIndexPercent,
            data: {'date': window.currentDate},
            headers: {'Content-Type': 'application/x-www-form-urlencoded'}
        }).then(function (response) {

            $timeout($scope.getIndexPercent, 10000);
        }, function (response) {
            console.log('Error');

            $timeout($scope.getIndexPercent, 10000);
        });
    };

    $scope.getSv = function () {

    };

    jQuery(document).ready(function () {
        //$scope.getIndexPercent();
    });
}]);

market.service('connectDb', function ($window, $http) {
    var data = {'date': window.currentDate};
    this.getPercentIndex = function () {
        $http.post(window.urlPostIndexPercent, data).then(function (response) {
            return response;
        }, function () {
            return false;
        });
    };
    this.urlGetPercentIndexCurrentDate = function () {
        var reQuest = $http({
            method: 'POST',
            url: window.urlPostIndexPercent,
            data: {'date': window.currentDate},
            headers: {'Content-Type': 'application/x-www-form-urlencoded'}
        });
        return {
            events: reQuest
        };
    }
});

market.service('calMarket', function () {
    this.cal = function (arrayAllTicker, tickerBoard, tickerClass, arrayAllTickerLastDay, cachePlcs, trendData, trendSmaData, uptrendData, dataRs, dataPennyCloseTwoDayAgo, addData) {
        var arrayPlcs = [];
        arrayPlcs['haxVol'] = 0;
        arrayPlcs['haxValue'] = 0;
        arrayPlcs['hoseVol'] = 0;
        arrayPlcs['hoseValue'] = 0;
        arrayPlcs['allVol'] = 0;
        arrayPlcs['allValue'] = 0;
        arrayPlcs['daucoVol'] = 0;
        arrayPlcs['daucoValue'] = 0;
        arrayPlcs['cobanVol'] = 0;
        arrayPlcs['cobanValue'] = 0;
        arrayPlcs['blueVol'] = 0;
        arrayPlcs['blueValue'] = 0;
        arrayPlcs['pennyVol'] = 0;
        arrayPlcs['pennyValue'] = 0;

        arrayPlcs['haxPercent'] = 0;
        arrayPlcs['hosePercent'] = 0;
        arrayPlcs['allPercent'] = 0;
        arrayPlcs['daucoPercent'] = 0;
        arrayPlcs['cobanPercent'] = 0;
        arrayPlcs['bluePercent'] = 0;
        arrayPlcs['pennyPercent'] = 0;

        var numOfHax = 0;
        var numOfHose = 0;
        var numOfAll = 0;
        var numOfDauco = 0;
        var numOfCoban = 0;
        var numOfBlue = 0;
        var numOfPenny = 0;

        var foreign = [];
        var detailedForeign = {};
        detailedForeign.sumNnM = 0;
        detailedForeign.sumNnB = 0;
        detailedForeign.sumGtM = 0;
        detailedForeign.sumGtB = 0;

        var topTrend = [];
        topTrend['up'] = [];
        topTrend['down'] = [];
        topTrend['hold'] = [];
        topTrend['netValueUp'] = [];
        topTrend['netValueDown'] = [];
        topTrend['netValueHold'] = [];
        topTrend['netValue'] = 0;

        var dataTrend = [];
        dataTrend['150'] = [];
        dataTrend['300'] = [];

        dataTrend['150']['1M'] = {'pos': 0, 'neg': 0};
        dataTrend['300']['1M'] = {'pos': 0, 'neg': 0};
        dataTrend['150']['3M'] = {'pos': 0, 'neg': 0};
        dataTrend['300']['3M'] = {'pos': 0, 'neg': 0};
        dataTrend['150']['6M'] = {'pos': 0, 'neg': 0};
        dataTrend['300']['6M'] = {'pos': 0, 'neg': 0};

        dataTrend['150']['sma10'] = {'pos': 0, 'neg': 0};
        dataTrend['150']['sma20'] = {'pos': 0, 'neg': 0, average: 0};
        dataTrend['300']['sma10'] = {'pos': 0, 'neg': 0};
        dataTrend['300']['sma20'] = {'pos': 0, 'neg': 0, average: 0};
        dataTrend['150']['sma50'] = {'pos': 0, 'neg': 0, average: 0};
        dataTrend['300']['sma50'] = {'pos': 0, 'neg': 0, average: 0};
        dataTrend['150']['sma100'] = {'pos': 0, 'neg': 0, average: 0};
        dataTrend['300']['sma100'] = {'pos': 0, 'neg': 0, average: 0};

        dataTrend['150']['uptrend'] = {'pos': 0, 'neg': 0};
        dataTrend['300']['uptrend'] = {'pos': 0, 'neg': 0};

        dataTrend['150']['rs'] = {'pos': 0, 'neg': 0};
        dataTrend['300']['rs'] = {'pos': 0, 'neg': 0};

        dataTrend['150']['r1'] = {'pos': 0, 'neg': 0};
        dataTrend['300']['r1'] = {'pos': 0, 'neg': 0};

        dataTrend['300']['penny'] = {'pos': 0, 'neg': 0, closeUpTwoDay: 0, closeDownTwoDay: 0};

        var inde = [];
        inde['300'] = [];
        inde['300']['sv1'] = {'pos': 0, 'neg': 0};
        inde['300']['svbq'] = {'pos': 0, 'neg': 0};
        inde['300']['all'] = {
            'priceIn': 0,
            'priceInSv1VolIn': 0,
            'priceInSv1VolDe': 0,
            'priceInSvbqVolIn': 0,
            'priceInSvbqVolDe': 0,

            'priceDe': 0,
            'priceDeSv1VolIn': 0,
            'priceDeSv1VolDe': 0,
            'priceDeSvbqVolIn': 0,
            'priceDeSvbqVolDe': 0
        };


        var index = [];
        index['hnx'] = [];
        index['hnx'] = {up: 0, down: 0, hold: 0, gtnnm: 0, gtnnb: 0};
        index['hose'] = [];
        index['hose'] = {up: 0, down: 0, hold: 0, gtnnm: 0, gtnnb: 0};

        /*INIT DATA ARRAY*/
        jQuery.each(arrayAllTicker, function (key, value) {

            //if (key == 'VHG')
            //    console.log('1VHG-' + value.ticker + tickerClass[key].top + ' :' + value.close + " lastDay: " + arrayAllTickerLastDay[key].close);

            if (!arrayAllTickerLastDay.hasOwnProperty(key)) {
                /*DO: not found*/
                //console.log('NOT Found last day: ' + key);
                return true;
            }

            /*DO: data Foreign*/
            if (value.vol > 0 && value.average > 0 && (parseFloat(value.nnm) > 0 || parseFloat(value.nnb) > 0)) {
                foreign.push({
                    'ticker': key,
                    'nnm': ((value.nnm).formatMoney(0, '.', ',')),
                    'tnnm': parseFloat(value.nnm),
                    'nnb': ((value.nnb).formatMoney(0, '.', ',')),
                    'tnnb': parseFloat(value.nnb),
                    'vol': ((value.vol).formatMoney(0, '.', ',')),
                    'tvol': parseFloat(value.vol),
                    'percent': (value.close * 100 / arrayAllTickerLastDay[key].close - 100).toFixed(2) + "%",
                    'tpercent': (value.close * 100 / arrayAllTickerLastDay[key].close - 100),
                    'mValue': ((value.nnm * value.average)).formatMoney(0, '.', ','),
                    'tmValue': parseFloat(value.nnm * value.average),
                    'bValue': ((value.nnb * value.average)).formatMoney(0, '.', ','),
                    'tbValue': parseFloat(value.nnb * value.average),
                    'board': tickerBoard[key].board,
                    'bPercent': (value.nnm / value.vol * 100).toFixed(2) + "%",
                    'sPercent': (value.nnb / value.vol * 100).toFixed(2) + "%",
                    'net': (((value.nnm * value.average - value.nnb * value.average)).formatMoney(0, '.', ',')),
                    'tnet': ((value.nnm * value.average - value.nnb * value.average))
                });
            }
            detailedForeign.sumNnM += value.nnm;
            detailedForeign.sumNnB += value.nnb;
            detailedForeign.sumGtB += value.nnb * value.average;
            detailedForeign.sumGtM += value.nnm * value.average;


            /*DO: data Cash Flow*/
            if (parseFloat(value.close) > parseFloat(arrayAllTickerLastDay[key].close)) {
                //if (key == 'VHG')
                //    console.log('2VHG-' + value.ticker + tickerClass[key].top + ' :' + value.close + " lastDay: " + arrayAllTickerLastDay[key].close);

                topTrend['up'].push({
                    'ticker': key,
                    'close': value.average,
                    'percent': (value.close * 100 / arrayAllTickerLastDay[key].close - 100).toFixed(2),
                    'gtgd': value.average * value.vol
                });
                topTrend['netValueUp'].push({
                    'ticker': key,
                    'close': value.average,
                    'percent': (value.close * 100 / arrayAllTickerLastDay[key].close - 100).toFixed(2),
                    'netValue': (value.close - arrayAllTickerLastDay[key].close) * value.average * value.vol / arrayAllTickerLastDay[key].close
                });
            }
            if (parseFloat(value.close) < parseFloat(arrayAllTickerLastDay[key].close)) {
                topTrend['down'].push({
                    'ticker': key,
                    'close': value.average,
                    'percent': (value.close * 100 / arrayAllTickerLastDay[key].close - 100).toFixed(2),
                    'gtgd': value.average * value.vol
                });
                topTrend['netValueDown'].push({
                    'ticker': key,
                    'close': value.average,
                    'percent': ((value.close / arrayAllTickerLastDay[key].close - 1) * 100).toFixed(2),
                    'netValue': (value.close - arrayAllTickerLastDay[key].close) * value.average * value.vol / arrayAllTickerLastDay[key].close
                });
            }
            if (parseFloat(value.close) == parseFloat(arrayAllTickerLastDay[key].close)) {
                topTrend['hold'].push({
                    'ticker': key,
                    'close': value.average,
                    'percent': (value.close * 100 / arrayAllTickerLastDay[key].close - 100).toFixed(2),
                    'gtgd': value.average * value.vol
                });
                topTrend['netValueHold'].push({
                    'ticker': key,
                    'close': value.average,
                    'percent': (value.close * 100 / arrayAllTickerLastDay[key].close - 100).toFixed(2),
                    'netValue': (value.close - arrayAllTickerLastDay[key].close) * value.average * value.vol / arrayAllTickerLastDay[key].close
                });
            }


            /*TODO: PLCS*/
            /*DO: Check Board*/
            if (tickerBoard.hasOwnProperty(key) && tickerBoard[key].board == 'hax') {
                arrayPlcs['haxVol'] += value.vol;
                arrayPlcs['haxValue'] += value.average * value.vol;
                arrayPlcs['haxPercent'] += value.close / arrayAllTickerLastDay[key].close - 1;
                numOfHax += 1;

                /*DO: tinh tang/giam/giu gia index*/
                if (value.close > arrayAllTickerLastDay[key].close) {
                    index['hnx'].up += 1;
                }
                if (value.close < arrayAllTickerLastDay[key].close) {
                    index['hnx'].down += 1;
                }
                if (value.close == arrayAllTickerLastDay[key].close) {
                    index['hnx'].hold += 1;
                }
                index['hnx'].gtnnm += value.average * value.nnm;
                index['hnx'].gtnnb += value.average * value.nnb;

            }
            if (tickerBoard.hasOwnProperty(key) && tickerBoard[key].board == 'hose') {
                arrayPlcs['hoseVol'] += value.vol;
                arrayPlcs['hoseValue'] += value.average * value.vol;
                arrayPlcs['hosePercent'] += value.close / arrayAllTickerLastDay[key].close - 1;
                numOfHose += 1;

                /*DO: tinh tang/giam/giu gia index*/
                if (value.close > arrayAllTickerLastDay[key].close) {
                    index['hose'].up += 1;
                }
                if (value.close < arrayAllTickerLastDay[key].close) {
                    index['hose'].down += 1;
                }
                if (value.close == arrayAllTickerLastDay[key].close) {
                    index['hose'].hold += 1;
                }
                index['hose'].gtnnm += value.average * value.nnm;
                index['hose'].gtnnb += value.average * value.nnb;
            }

            /*DO: check class*/
            if (tickerClass.hasOwnProperty(key) && (tickerClass[key].top == 'top1' || tickerClass[key].top == 'top2')) {
                //if (key == 'VHG')
                //    console.log('3VHG-' + value.ticker + tickerClass[key].top + ' :' + value.close + " lastDay: " + arrayAllTickerLastDay[key].close);
                arrayPlcs['allPercent'] += value.close / arrayAllTickerLastDay[key].close - 1;
                numOfAll += 1;
                /*DO: Xu the va do nong lanh*/
                if (tickerClass[key].top == 'top1') {
                    //    DO: chi trinh top 150
                    //high low
                    if (parseFloat(value.close) > parseFloat(trendData.higtLow['1M'][key].max)) {
                        dataTrend['150']['1M'].pos += 1;
                    }
                    if (parseFloat(value.close) < parseFloat(trendData.higtLow['1M'][key].min)) {
                        dataTrend['150']['1M'].neg += 1;
                    }
                    if (parseFloat(value.close) > parseFloat(trendData.higtLow['3M'][key].max)) {
                        dataTrend['150']['3M'].pos += 1;
                    }
                    if (parseFloat(value.close) < parseFloat(trendData.higtLow['3M'][key].min)) {
                        dataTrend['150']['3M'].neg += 1;
                    }
                    if (parseFloat(value.close) > parseFloat(trendData.higtLow['6M'][key].max)) {
                        dataTrend['150']['6M'].pos += 1;
                    }
                    if (parseFloat(value.close) < parseFloat(trendData.higtLow['6M'][key].min)) {
                        dataTrend['150']['6M'].neg += 1;
                    }

                    //  SMA
                    if (parseFloat(value.close) > parseFloat(trendSmaData.sma['10'][key])) {
                        dataTrend['150']['sma10'].pos += 1;
                    }
                    if (parseFloat(value.close) < parseFloat(trendSmaData.sma['10'][key])) {
                        dataTrend['150']['sma10'].neg += 1;
                    }
                    if (parseFloat(value.close) > parseFloat(trendSmaData.sma['20'][key])) {
                        dataTrend['150']['sma20'].pos += 1;
                    }
                    if (parseFloat(value.close) < parseFloat(trendSmaData.sma['20'][key])) {
                        dataTrend['150']['sma20'].neg += 1;
                    }
                    dataTrend['150']['sma20'].average += parseFloat(value.close) / parseFloat(trendSmaData.sma['20'][key]) - 1;
                    if (parseFloat(value.close) > parseFloat(trendSmaData.sma['50'][key])) {
                        dataTrend['150']['sma50'].pos += 1;
                    }
                    if (parseFloat(value.close) < parseFloat(trendSmaData.sma['50'][key])) {
                        dataTrend['150']['sma50'].neg += 1;
                    }
                    dataTrend['150']['sma50'].average += parseFloat(value.close) / parseFloat(trendSmaData.sma['50'][key]) - 1;
                    if (parseFloat(value.close) > parseFloat(trendSmaData.sma['100'][key])) {
                        dataTrend['150']['sma100'].pos += 1;
                    }
                    if (parseFloat(value.close) < parseFloat(trendSmaData.sma['100'][key])) {
                        dataTrend['150']['sma100'].neg += 1;
                    }
                    dataTrend['150']['sma100'].average += parseFloat(value.close) / parseFloat(trendSmaData.sma['100'][key]) - 1;

                    /*DO: uptrend*/
                    if (uptrendData.uptrend[key].max == 99999) {
                        console.log('Không có dữ liệu uptrend key: ' + key);
                    }
                    if (parseFloat(value.close) > parseFloat(uptrendData.uptrend[key].max)) {
                        dataTrend['150']['uptrend'].pos += 1;
                    }
                    if (parseFloat(value.close) < parseFloat(uptrendData.uptrend[key].min)) {
                        dataTrend['150']['uptrend'].neg += 1;
                    }

                    /*DO: RS*/
                    if (parseFloat(value.close) > parseFloat(dataRs.rs[key]['6'].close)) {
                        dataTrend['150']['rs'].pos += 1;
                    }
                    if (parseFloat(value.close) < parseFloat(dataRs.rs[key]['6'].close)) {
                        dataTrend['150']['rs'].neg += 1;
                    }

                    /*DO: R1*/
                    if (parseFloat(value.close) > parseFloat(arrayAllTickerLastDay[key].close)) {
                        dataTrend['150']['r1'].pos += 1;
                    }
                    if (parseFloat(value.close) < parseFloat(arrayAllTickerLastDay[key].close)) {
                        dataTrend['150']['r1'].neg += 1;
                    }
                }
                //    DO: tinh top 300
                if (parseFloat(value.close) > parseFloat(trendData.higtLow['1M'][key].max)) {
                    dataTrend['300']['1M'].pos += 1;
                }
                if (parseFloat(value.close) < parseFloat(trendData.higtLow['1M'][key].min)) {
                    dataTrend['300']['1M'].neg += 1;
                }
                if (parseFloat(value.close) > parseFloat(trendData.higtLow['3M'][key].max)) {
                    dataTrend['300']['3M'].pos += 1;
                }
                if (parseFloat(value.close) < parseFloat(trendData.higtLow['3M'][key].min)) {
                    dataTrend['300']['3M'].neg += 1;
                }
                if (parseFloat(value.close) > parseFloat(trendData.higtLow['6M'][key].max)) {
                    dataTrend['300']['6M'].pos += 1;
                }
                if (parseFloat(value.close) < parseFloat(trendData.higtLow['6M'][key].min)) {
                    dataTrend['300']['6M'].neg += 1;
                }

                //  SMA
                if (parseFloat(value.close) > parseFloat(trendSmaData.sma['10'][key])) {
                    dataTrend['300']['sma10'].pos += 1;
                }
                if (parseFloat(value.close) < parseFloat(trendSmaData.sma['10'][key])) {
                    dataTrend['300']['sma10'].neg += 1;
                }
                if (parseFloat(value.close) > parseFloat(trendSmaData.sma['20'][key])) {
                    dataTrend['300']['sma20'].pos += 1;
                }
                if (parseFloat(value.close) < parseFloat(trendSmaData.sma['20'][key])) {
                    dataTrend['300']['sma20'].neg += 1;
                }
                dataTrend['300']['sma20'].average += parseFloat(value.close) / parseFloat(trendSmaData.sma['20'][key]) - 1;
                if (parseFloat(value.close) > parseFloat(trendSmaData.sma['50'][key])) {
                    dataTrend['300']['sma50'].pos += 1;
                }
                if (parseFloat(value.close) < parseFloat(trendSmaData.sma['50'][key])) {
                    dataTrend['300']['sma50'].neg += 1;
                }
                dataTrend['300']['sma50'].average += parseFloat(value.close) / parseFloat(trendSmaData.sma['50'][key]) - 1;
                if (parseFloat(value.close) > parseFloat(trendSmaData.sma['100'][key])) {
                    dataTrend['300']['sma100'].pos += 1;
                }
                if (parseFloat(value.close) < parseFloat(trendSmaData.sma['100'][key])) {
                    dataTrend['300']['sma100'].neg += 1;
                }
                dataTrend['300']['sma100'].average += parseFloat(value.close) / parseFloat(trendSmaData.sma['100'][key]) - 1;

                /*UPTREND*/
                if (parseFloat(value.close) > parseFloat(uptrendData.uptrend[key].max)) {
                    //console.log('Ticker: '+ key +'  |  ' + 'c0: ' + value.close +'  |  ' + 'max: ' + uptrendData.uptrend[key].max)  ;
                    dataTrend['300']['uptrend'].pos += 1;
                }
                if (parseFloat(value.close) < parseFloat(uptrendData.uptrend[key].min)) {
                    dataTrend['300']['uptrend'].neg += 1;
                }

                /*DO: RS*/
                if (parseFloat(value.close) > parseFloat(dataRs.rs[key]['6'].close)) {
                    //console.log('ticker: ' + key + ' | ' + 'c0: ' + value.close + ' | ' + 'c6: ' + dataRs.rs[key]['6'].close);
                    dataTrend['300']['rs'].pos += 1;
                }
                if (parseFloat(value.close) < parseFloat(dataRs.rs[key]['6'].close)) {
                    dataTrend['300']['rs'].neg += 1;
                }

                //DO R1
                if (parseFloat(value.close) > parseFloat(arrayAllTickerLastDay[key].close)) {
                    dataTrend['300']['r1'].pos += 1;
                }
                if (parseFloat(value.close) < parseFloat(arrayAllTickerLastDay[key].close)) {
                    dataTrend['300']['r1'].neg += 1;
                }

                /*DO: Cuong do tang giam*/
                if (parseFloat(value.vol) > parseFloat(arrayAllTickerLastDay[key].vol)) {
                    inde['300']['sv1'].pos += 1;
                }
                if (parseFloat(value.vol) < parseFloat(arrayAllTickerLastDay[key].vol)) {
                    inde['300']['sv1'].neg += 1;
                }
                if (parseFloat(value.vol) * 21 > parseFloat(cachePlcs['svbqTop'][key])) {
                    inde['300']['svbq'].pos += 1;
                }
                if (parseFloat(value.vol) * 21 < parseFloat(cachePlcs['svbqTop'][key])) {
                    inde['300']['svbq'].neg += 1;
                }

                inde['300']['all'].priceIn = dataTrend['300']['r1'].pos;
                if (parseFloat(value.vol) > parseFloat(arrayAllTickerLastDay[key].vol) && parseFloat(value.close) > parseFloat(arrayAllTickerLastDay[key].close)) {
                    inde['300']['all'].priceInSv1VolIn += 1;
                }
                if (parseFloat(value.vol) < parseFloat(arrayAllTickerLastDay[key].vol) && parseFloat(value.close) > parseFloat(arrayAllTickerLastDay[key].close)) {
                    inde['300']['all'].priceInSv1VolDe += 1;
                }
                if (parseFloat(value.vol) * 21 > parseFloat(cachePlcs['svbqTop'][key]) && parseFloat(value.close) > parseFloat(arrayAllTickerLastDay[key].close)) {
                    inde['300']['all'].priceInSvbqVolIn += 1;
                }
                if (parseFloat(value.vol) * 21 < parseFloat(cachePlcs['svbqTop'][key]) && parseFloat(value.close) > parseFloat(arrayAllTickerLastDay[key].close)) {
                    inde['300']['all'].priceInSvbqVolDe += 1;
                }

                inde['300']['all'].priceDe = dataTrend['300']['r1'].neg;
                if (parseFloat(value.vol) > parseFloat(arrayAllTickerLastDay[key].vol) && parseFloat(value.close) < parseFloat(arrayAllTickerLastDay[key].close)) {
                    inde['300']['all'].priceDeSv1VolIn += 1;
                }
                if (parseFloat(value.vol) < parseFloat(arrayAllTickerLastDay[key].vol) && parseFloat(value.close) < parseFloat(arrayAllTickerLastDay[key].close)) {
                    inde['300']['all'].priceDeSv1VolDe += 1;
                }
                if (parseFloat(value.vol) * 21 > parseFloat(cachePlcs['svbqTop'][key]) && parseFloat(value.close) < parseFloat(arrayAllTickerLastDay[key].close)) {
                    inde['300']['all'].priceDeSvbqVolIn += 1;
                }
                if (parseFloat(value.vol) * 21 < parseFloat(cachePlcs['svbqTop'][key]) && parseFloat(value.close) < parseFloat(arrayAllTickerLastDay[key].close)) {
                    inde['300']['all'].priceDeSvbqVolDe += 1;
                }
            }
            arrayPlcs['allValue'] += value.average * value.vol;
            arrayPlcs['allVol'] += value.vol;
            if (tickerClass.hasOwnProperty(key) && (tickerClass[key].cd == 'dauco')) {
                arrayPlcs['daucoVol'] += value.vol;
                arrayPlcs['daucoValue'] += value.average * value.vol;
                arrayPlcs['daucoPercent'] += value.close / arrayAllTickerLastDay[key].close - 1;
                numOfDauco += 1;
            }
            if (tickerClass.hasOwnProperty(key) && (tickerClass[key].cd == 'coban')) {
                arrayPlcs['cobanVol'] += value.vol;
                arrayPlcs['cobanValue'] += value.average * value.vol;
                arrayPlcs['cobanPercent'] += value.close / arrayAllTickerLastDay[key].close - 1;
                numOfCoban += 1;
            }
            if (tickerClass.hasOwnProperty(key) && (tickerClass[key].bp == 'blue')) {
                arrayPlcs['blueVol'] += value.vol;
                arrayPlcs['blueValue'] += value.average * value.vol;
                arrayPlcs['bluePercent'] += value.close / arrayAllTickerLastDay[key].close - 1;
                numOfBlue += 1;
            }
            if (tickerClass.hasOwnProperty(key) && (tickerClass[key].bp == 'penny')) {
                arrayPlcs['pennyVol'] += value.vol;
                arrayPlcs['pennyValue'] += value.average * value.vol;
                arrayPlcs['pennyPercent'] += value.close / arrayAllTickerLastDay[key].close - 1;
                //console.log('ticker: ' + key + ' | ' + 'c0: ' + value.close + ' | ' + 'c1: ' + arrayAllTickerLastDay[key].close + ' | ' + 'change: ' + (value.close / arrayAllTickerLastDay[key].close - 1));
                numOfPenny += 1;

                //DO: xu the penny
                if (parseFloat(value.close) > parseFloat(arrayAllTickerLastDay[key].close)) {
                    dataTrend['300']['penny'].pos += 1;
                }
                if (parseFloat(value.close) < parseFloat(arrayAllTickerLastDay[key].close)) {
                    dataTrend['300']['penny'].neg += 1;
                }
                if (parseFloat(value.close) > parseFloat(dataPennyCloseTwoDayAgo[key])) {
                    dataTrend['300']['penny'].closeUpTwoDay += 1;
                }
                if (parseFloat(value.close) < parseFloat(dataPennyCloseTwoDayAgo[key])) {
                    dataTrend['300']['penny'].closeDownTwoDay += 1;
                }

            }
        });

        /*DO: Calculate Percent*/
        arrayPlcs['haxPercent'] = arrayPlcs['haxPercent'] / numOfHax;
        arrayPlcs['hosePercent'] = arrayPlcs['hosePercent'] / numOfHose;
        arrayPlcs['allPercent'] = arrayPlcs['allPercent'] / numOfAll;
        arrayPlcs['daucoPercent'] = arrayPlcs['daucoPercent'] / numOfDauco;
        arrayPlcs['cobanPercent'] = arrayPlcs['cobanPercent'] / numOfCoban;
        arrayPlcs['bluePercent'] = arrayPlcs['bluePercent'] / numOfBlue;
        arrayPlcs['pennyPercent'] = arrayPlcs['pennyPercent'] / numOfPenny;
        var result = {};
        result.all = {
            'vol': arrayPlcs['allVol'],
            'value': arrayPlcs['allValue'],
            'percent': arrayPlcs['allPercent'],
            'sv1': arrayPlcs['allValue'] / cachePlcs.allValue,
            'svbq': arrayPlcs['allValue'] * 21 / cachePlcs.svbqAllValue
        };
        result.hose = {
            'vol': arrayPlcs['hoseVol'],
            'value': arrayPlcs['hoseValue'],
            'percent': arrayPlcs['hosePercent'],
            'sv1': arrayPlcs['hoseValue'] / cachePlcs.vniValue,
            'svbq': arrayPlcs['hoseValue'] * 21 / cachePlcs.svbqVniValue
        };
        result.hax = {
            'vol': arrayPlcs['haxVol'],
            'value': arrayPlcs['haxValue'],
            'percent': arrayPlcs['haxPercent'],
            'sv1': arrayPlcs['haxValue'] / cachePlcs.haxValue,
            'svbq': arrayPlcs['haxValue'] * 21 / cachePlcs.svbqHaxValue
        };
        result.coban = {
            'vol': arrayPlcs['cobanVol'],
            'value': arrayPlcs['cobanValue'],
            'percent': arrayPlcs['cobanPercent'],
            'sv1': arrayPlcs['cobanValue'] / cachePlcs.cobanValue,
            'svbq': arrayPlcs['cobanValue'] * 21 / cachePlcs.svbqCobanValue
        };
        result.dauco = {
            'vol': arrayPlcs['daucoVol'],
            'value': arrayPlcs['daucoValue'],
            'percent': arrayPlcs['daucoPercent'],
            'sv1': arrayPlcs['daucoValue'] / cachePlcs.daucoValue,
            'svbq': arrayPlcs['daucoValue'] * 21 / cachePlcs.svbqDaucoValue
        };
        result.blue = {
            'vol': arrayPlcs['blueVol'],
            'value': arrayPlcs['blueValue'],
            'percent': arrayPlcs['bluePercent'],
            'sv1': arrayPlcs['blueValue'] / cachePlcs.blueValue,
            'svbq': arrayPlcs['blueValue'] * 21 / cachePlcs.svbqBlueValue
        };
        result.penny = {
            'vol': arrayPlcs['pennyVol'],
            'value': arrayPlcs['pennyValue'],
            'percent': arrayPlcs['pennyPercent'],
            'sv1': arrayPlcs['pennyValue'] / cachePlcs.pennyValue,
            'svbq': arrayPlcs['pennyValue'] * 21 / cachePlcs.svbqPennyValue
        };
        result.foreign = foreign;
        result.topTrend = topTrend;
        result.dataTrend = dataTrend;
        result.inde = inde;
        result.index = index;
        result.detailedForeign = detailedForeign;

        return result;
    };

    this.showForeign = function ($scope, foreign, type) {
        if ((typeof foreign[0]) == 'undefined' || true) {
            return;
        }
        /*DO: FOREGIN*/
        if (type == 'm' || type == 'all') {
            //nnm
            if ($scope.mSelect == "all") {
                //console.log('xep mALL');
                foreign.sort(function (a, b) {
                    return b.mValue - a.mValue
                });
            }
            if ($scope.mSelect == "hose") {
                //console.log('xep mHOSE');
                foreign.sort(function (a, b) {
                    if (a.board != "hose")
                        return 99999999999;
                    return b.mValue - a.mValue
                });
            }
            if ($scope.mSelect == "hax") {
                //console.log('xep mHAX');
                foreign.sort(function (a, b) {
                    if (a.board != "hax")
                        return 99999999999;
                    return b.mValue - a.mValue
                });
            }
            $scope.nnm1 = {
                'ticker': foreign[0].ticker,
                'percent': ($scope.tickerInday[foreign[0].ticker].close * 100 / $scope.tickerLastDay[foreign[0].ticker].close - 100).toFixed(2) + "%",
                'vol': (foreign[0].nnm).formatMoney(0, '.', ',') + " cp",
                'value': (foreign[0].mValue / 1000000).formatMoney(2, '.', ',') + " tỷ",
                'percentValue': (foreign[0].nnm / $scope.tickerInday[foreign[0].ticker].vol * 100).toFixed(4) + "%",
                'net': ((foreign[0].nnm - foreign[0].nnb) * $scope.tickerInday[foreign[0].ticker].average / 1000000).formatMoney(2, '.', ',') + " tỷ"
            };
            $scope.nnm2 = {
                'ticker': foreign[1].ticker,
                'percent': ($scope.tickerInday[foreign[1].ticker].close * 100 / $scope.tickerLastDay[foreign[1].ticker].close - 100).toFixed(2) + "%",
                'vol': (foreign[1].nnm).formatMoney(0, '.', ',') + " cp",
                'value': (foreign[1].mValue / 1000000).formatMoney(2, '.', ',') + " tỷ",
                'percentValue': (foreign[1].nnm / $scope.tickerInday[foreign[1].ticker].vol * 100).toFixed(4) + "%",
                'net': ((foreign[1].nnm - foreign[1].nnb) * $scope.tickerInday[foreign[1].ticker].average / 1000000).formatMoney(2, '.', ',') + " tỷ"
            };
            $scope.nnm3 = {
                'ticker': foreign[2].ticker,
                'percent': ($scope.tickerInday[foreign[2].ticker].close * 100 / $scope.tickerLastDay[foreign[2].ticker].close - 100).toFixed(2) + "%",
                'vol': (foreign[2].nnm).formatMoney(0, '.', ',') + " cp",
                'value': (foreign[2].mValue / 1000000).formatMoney(2, '.', ',') + " tỷ",
                'percentValue': (foreign[2].nnm / $scope.tickerInday[foreign[2].ticker].vol * 100).toFixed(4) + "%",
                'net': ((foreign[2].nnm - foreign[2].nnb) * $scope.tickerInday[foreign[2].ticker].average / 1000000).formatMoney(2, '.', ',') + " tỷ"
            };
            $scope.nnm4 = {
                'ticker': foreign[3].ticker,
                'percent': ($scope.tickerInday[foreign[3].ticker].close * 100 / $scope.tickerLastDay[foreign[3].ticker].close - 100).toFixed(2) + "%",
                'vol': (foreign[3].nnm).formatMoney(0, '.', ',') + " cp",
                'value': (foreign[3].mValue / 1000000).formatMoney(2, '.', ',') + " tỷ",
                'percentValue': (foreign[3].nnm / $scope.tickerInday[foreign[3].ticker].vol * 100).toFixed(4) + "%",
                'net': ((foreign[3].nnm - foreign[3].nnb) * $scope.tickerInday[foreign[3].ticker].average / 1000000).formatMoney(2, '.', ',') + " tỷ"
            };
            $scope.nnm5 = {
                'ticker': foreign[4].ticker,
                'percent': ($scope.tickerInday[foreign[4].ticker].close * 100 / $scope.tickerLastDay[foreign[4].ticker].close - 100).toFixed(2) + "%",
                'vol': (foreign[4].nnm).formatMoney(0, '.', ',') + " cp",
                'value': (foreign[4].mValue / 1000000).formatMoney(2, '.', ',') + " tỷ",
                'percentValue': (foreign[4].nnm / $scope.tickerInday[foreign[4].ticker].vol * 100).toFixed(4) + "%",
                'net': ((foreign[4].nnm - foreign[4].nnb) * $scope.tickerInday[foreign[4].ticker].average / 1000000).formatMoney(2, '.', ',') + " tỷ"
            };
        }

        if (type == 'b' || type == 'all') {
            //nnb
            if ($scope.bSelect == "all") {
                foreign.sort(function (a, b) {
                    return b.bValue - a.bValue
                });
            }
            if ($scope.bSelect == "hose") {
                foreign.sort(function (a, b) {
                    if (a.board != "hose")
                        return 99999999999;
                    return b.bValue - a.bValue
                });
            }
            if ($scope.bSelect == "hax") {
                foreign.sort(function (a, b) {
                    if (a.board != "hax")
                        return 99999999999;
                    return b.bValue - a.bValue
                });
            }
            $scope.nnb1 = {
                'ticker': foreign[0].ticker,
                'percent': ($scope.tickerInday[foreign[0].ticker].close * 100 / $scope.tickerLastDay[foreign[0].ticker].close - 100).toFixed(2) + "%",
                'vol': (foreign[0].nnb).formatMoney(0, '.', ',') + " cp",
                'value': (foreign[0].bValue / 1000000).formatMoney(2, '.', ',') + " tỷ",
                'percentValue': (foreign[0].nnb / $scope.tickerInday[foreign[0].ticker].vol * 100).toFixed(4) + "%",
                'net': ((foreign[0].nnb - foreign[0].nnm) * $scope.tickerInday[foreign[0].ticker].average / 1000000).formatMoney(2, '.', ',') + " tỷ"
            };
            $scope.nnb2 = {
                'ticker': foreign[1].ticker,
                'percent': ($scope.tickerInday[foreign[1].ticker].close * 100 / $scope.tickerLastDay[foreign[1].ticker].close - 100).toFixed(2) + "%",
                'vol': (foreign[1].nnb).formatMoney(0, '.', ',') + " cp",
                'value': (foreign[1].bValue / 1000000).formatMoney(2, '.', ',') + " tỷ",
                'percentValue': (foreign[1].nnb / $scope.tickerInday[foreign[1].ticker].vol * 100).toFixed(4) + "%",
                'net': ((foreign[1].nnb - foreign[1].nnm) * $scope.tickerInday[foreign[1].ticker].average / 1000000).formatMoney(2, '.', ',') + " tỷ"
            };
            $scope.nnb3 = {
                'ticker': foreign[2].ticker,
                'percent': ($scope.tickerInday[foreign[2].ticker].close * 100 / $scope.tickerLastDay[foreign[2].ticker].close - 100).toFixed(2) + "%",
                'vol': (foreign[2].nnb).formatMoney(0, '.', ',') + " cp",
                'value': (foreign[2].bValue / 1000000).formatMoney(2, '.', ',') + " tỷ",
                'percentValue': (foreign[2].nnb / $scope.tickerInday[foreign[2].ticker].vol * 100).toFixed(4) + "%",
                'net': ((foreign[2].nnb - foreign[2].nnm) * $scope.tickerInday[foreign[2].ticker].average / 1000000).formatMoney(2, '.', ',') + " tỷ"
            };
            $scope.nnb4 = {
                'ticker': foreign[3].ticker,
                'percent': ($scope.tickerInday[foreign[3].ticker].close * 100 / $scope.tickerLastDay[foreign[3].ticker].close - 100).toFixed(2) + "%",
                'vol': (foreign[3].nnb).formatMoney(0, '.', ',') + " cp",
                'value': (foreign[3].bValue / 1000000).formatMoney(2, '.', ',') + " tỷ",
                'percentValue': (foreign[3].nnb / $scope.tickerInday[foreign[3].ticker].vol * 100).toFixed(4) + "%",
                'net': ((foreign[3].nnb - foreign[3].nnm) * $scope.tickerInday[foreign[3].ticker].average / 1000000).formatMoney(2, '.', ',') + " tỷ"
            };
            $scope.nnb5 = {
                'ticker': foreign[4].ticker,
                'percent': ($scope.tickerInday[foreign[4].ticker].close * 100 / $scope.tickerLastDay[foreign[4].ticker].close - 100).toFixed(2) + "%",
                'vol': (foreign[4].nnb).formatMoney(0, '.', ',') + " cp",
                'value': (foreign[4].bValue / 1000000).formatMoney(2, '.', ',') + " tỷ",
                'percentValue': (foreign[4].nnb / $scope.tickerInday[foreign[4].ticker].vol * 100).toFixed(4) + "%",
                'net': ((foreign[4].nnb - foreign[4].nnm) * $scope.tickerInday[foreign[4].ticker].average / 1000000).formatMoney(2, '.', ',') + " tỷ"
            };
        }
    };

    this.showTrend = function (trend, $scope) {
        var up = trend['up'];
        var down = trend['down'];
        var hold = trend['hold'];

        var totalUp = 0;
        var totalDown = 0;
        var totalHold = 0;
        var total = 0;
        if ($scope.cashFlowSelect == 'total') {
            $scope.tableDetailStyle = {display: 'none'};
            jQuery.each(up, function (key, value) {
                totalUp += value.gtgd;
            });
            jQuery.each(down, function (key, value) {
                totalDown += value.gtgd;
            });
            jQuery.each(hold, function (key, value) {
                totalHold += value.gtgd;
            });
            total = totalUp + totalDown + totalHold;
            up.sort(function (a, b) {
                return b.gtgd - a.gtgd;
            });
            down.sort(function (a, b) {
                return b.gtgd - a.gtgd;
            });
            hold.sort(function (a, b) {
                return b.gtgd - a.gtgd;
            });
            if (typeof up[0] == 'undefined') {
                up.push({ticker: '', gtgd: ''});
                up.push({ticker: '', gtgd: ''});
                up.push({ticker: '', gtgd: ''});
                up.push({ticker: '', gtgd: ''});
                up.push({ticker: '', gtgd: ''});
            }
            if (typeof down[0] == 'undefined') {
                down.push({ticker: '', gtgd: ''});
                down.push({ticker: '', gtgd: ''});
                down.push({ticker: '', gtgd: ''});
                down.push({ticker: '', gtgd: ''});
                down.push({ticker: '', gtgd: ''});
            }
            if (typeof hold[0] == 'undefined') {
                hold.push({ticker: '', gtgd: ''});
                hold.push({ticker: '', gtgd: ''});
                hold.push({ticker: '', gtgd: ''});
                hold.push({ticker: '', gtgd: ''});
                hold.push({ticker: '', gtgd: ''});
            }
            $scope.cashFlowTable.dataCashFlow.length = 0;
            for (var i = 0; i < 5; i++) {
                $scope.cashFlowTable.dataCashFlow.push({
                    up: up[i].ticker + ' | ' + up[i].close + ' | ',
                    upPercent: up[i].percent + '%',
                    down: down[i].ticker + ' | ' + down[i].close + ' | ',
                    downPercent: down[i].percent + '%'
                });
            }

            $scope.labelsChart = ['Yesterday', 'Today'];
            $scope.seriesTrend = ['Up', 'Down', 'Unchange'];

            totalUp = (totalUp / 1000000).toFixed(2);
            totalDown = (totalDown / 1000000).toFixed(2);
            totalHold = (totalHold / 1000000).toFixed(2);
            var dataYesterDay = ($scope.dataCashFlowYesterday);
            $scope.dataChart = [
                [(dataYesterDay.totalUp / 1000000).toFixed(2), totalUp],
                [(dataYesterDay.totalDown / 1000000).toFixed(2), totalDown],
                [(dataYesterDay.totalHold / 1000000).toFixed(2), totalHold]
            ];
        } else {
            $scope.tableDetailStyle = {display: 'block'};
            jQuery.each(up, function (key, value) {
                totalUp += value.gtgd;
            });
            jQuery.each(down, function (key, value) {
                totalDown += value.gtgd;
            });
            jQuery.each(hold, function (key, value) {
                totalHold += value.gtgd;
            });
            total = totalUp + totalDown + totalHold;


            var totalNetUp = 0, totalNetDown = 0, totalNetHold = 0, totalNet, netIndex;
            var netUps = trend['netValueUp'];
            var netDowns = trend['netValueDown'];
            var netHolds = trend['netValueHold'];
            netUps.sort(function (a, b) {
                return b.netValue - a.netValue;
            });
            netDowns.sort(function (a, b) {
                return -b.netValue + a.netValue;
            });
            $.each(netUps, function (_k, _v) {
                totalNetUp += _v.netValue;
            });
            $.each(netDowns, function (_k, _v) {
                totalNetDown += _v.netValue;
            });
            $.each(netHolds, function (_k, _v) {
                totalNetHold += _v.netValue;
            });

            totalNet = totalNetUp + totalNetDown + totalNetHold;
            netIndex = totalNet * 100 / total;

            $scope.labelsChart = ['Yesterday', 'Today'];
            $scope.seriesTrend = ['Up', 'Down'];

            totalUp = (totalNetUp / 1000000).toFixed(2);
            totalDown = (totalNetDown / 1000000).toFixed(2);
            $scope.cashFlowTable.dataCashFlow.length = 0;

            for (var i = 0; i < 5; i++) {
                $scope.cashFlowTable.dataCashFlow.push({
                    up: netUps[i].ticker + ' | ' + netUps[i].close + ' | ',
                    upPercent: netUps[i].percent + '%',
                    down: netDowns[i].ticker + ' | ' + netDowns[i].close + ' | ',
                    downPercent: netDowns[i].percent + '%'
                });
            }
            var dataYesterDay = ($scope.dataCashFlowYesterday);
            $scope.cashFlowTable.dataDetail = {
                today: {net: (totalNet / 1000000).toFixed(2), index: netIndex.toFixed(2)},
                yesterday: {net: ((dataYesterDay.netUp+dataYesterDay.netDown) / 1000000).toFixed(2), index: (dataYesterDay.indexNet).toFixed(2)}
            };


            $scope.dataChart = [
                [(dataYesterDay.netUp / 1000000).toFixed(2), totalUp],
                [(-dataYesterDay.netDown / 1000000).toFixed(2), -totalDown]
            ];
        }
    };

    this.showXuthe = function (trendData, $scope) {
        if ($scope.trendSelect == 'all') {
            /*DO: HIGH LOW*/
            $scope.trendShow.oneM.pos = trendData['300']['1M'].pos;
            $scope.trendShow.oneM.neg = trendData['300']['1M'].neg;
            $scope.trendShow.thirdM.pos = trendData['300']['3M'].pos;
            $scope.trendShow.thirdM.neg = trendData['300']['3M'].neg;
            $scope.trendShow.sixM.pos = trendData['300']['6M'].pos;
            $scope.trendShow.sixM.neg = trendData['300']['6M'].neg;
            if (trendData['300']['1M'].pos > trendData['300']['1M'].neg)
                $scope.trendShow.oneM.info = "UP";
            else
                $scope.trendShow.oneM.info = "DOWN";
            if (trendData['300']['3M'].pos > trendData['300']['3M'].neg)
                $scope.trendShow.thirdM.info = "UP";
            else
                $scope.trendShow.thirdM.info = "DOWN";
            if (trendData['300']['6M'].pos > trendData['300']['6M'].neg)
                $scope.trendShow.sixM.info = "UP";
            else
                $scope.trendShow.sixM.info = "DOWN";

            /*DO: SMA*/
            $scope.trendShow.sma10.pos = (trendData['300']['sma10'].pos / 300 * 100).toFixed(2) + "%";
            $scope.trendShow.sma10.neg = (trendData['300']['sma10'].neg / 300 * 100).toFixed(2) + "%";
            $scope.trendShow.sma20.pos = trendData['300']['sma20'].pos;
            $scope.trendShow.sma20.neg = trendData['300']['sma20'].neg;
            $scope.trendShow.sma50.pos = trendData['300']['sma50'].pos;
            $scope.trendShow.sma50.neg = trendData['300']['sma50'].neg;
            $scope.trendShow.sma100.pos = trendData['300']['sma100'].pos;
            $scope.trendShow.sma100.neg = trendData['300']['sma100'].neg;
            if (trendData['300']['sma10'].pos > trendData['300']['sma10'].neg)
                $scope.trendShow.sma10.info = "UP";
            else
                $scope.trendShow.sma10.info = "DOWN";

            if (trendData['300']['sma20'].pos > trendData['300']['sma20'].neg)
                $scope.trendShow.sma20.info = "UP";
            else
                $scope.trendShow.sma20.info = "DOWN";
            if (trendData['300']['sma20'].average > 0)
                $scope.trendShow.sma20.add = "Bq tt nằm trên " + (trendData['300']['sma20'].average / 3).toFixed(2) + "%" + " đường MA20";
            else
                $scope.trendShow.sma20.add = "Bq tt nằm dưới " + (trendData['300']['sma20'].average / 3).toFixed(2) + "%" + " đường MA20";
            if (trendData['300']['sma50'].pos > trendData['300']['sma50'].neg)
                $scope.trendShow.sma50.info = "UP";
            else
                $scope.trendShow.sma50.info = "DOWN";
            if (trendData['300']['sma50'].average > 0)
                $scope.trendShow.sma50.add = "Bq tt nằm trên " + (trendData['300']['sma50'].average / 3).toFixed(2) + "%" + " đường MA50";
            else
                $scope.trendShow.sma50.add = "Bq tt nằm dưới " + (trendData['300']['sma50'].average / 3).toFixed(2) + "%" + " đường MA50";
            if (trendData['300']['sma100'].pos > trendData['300']['sma100'].neg)
                $scope.trendShow.sma100.info = "UP";
            else
                $scope.trendShow.sma100.info = "DOWN";
            if (trendData['300']['sma100'].average > 0)
                $scope.trendShow.sma100.add = "Bq tt nằm trên " + (trendData['300']['sma100'].average / 3).toFixed(2) + "%" + " đường MA100";
            else
                $scope.trendShow.sma100.add = "Bq tt nằm dưới " + (trendData['300']['sma100'].average / 3).toFixed(2) + "%" + " đường MA100";

            /*DO UPTREND*/
            $scope.trendShow.uptrend.pos = trendData['300']['uptrend'].pos;
            $scope.trendShow.uptrend.neg = trendData['300']['uptrend'].neg;
            if (trendData['300']['uptrend'].pos > trendData['300']['uptrend'].neg)
                $scope.trendShow.uptrend.info = "UP";
            else
                $scope.trendShow.uptrend.info = "DOWN";

            /*DO RS*/
            $scope.trendShow.rs.pos = (trendData['300']['rs'].pos / 300 * 100).toFixed(2) + "%";
            $scope.trendShow.rs.neg = (trendData['300']['rs'].neg / 300 * 100).toFixed(2) + "%";
            if (trendData['300']['rs'].pos > trendData['300']['rs'].neg)
                $scope.trendShow.rs.info = "UP";
            else
                $scope.trendShow.rs.info = "DOWN";

            /*DO R1*/
            $scope.trendShow.r1.pos = (trendData['300']['r1'].pos / 300 * 100).toFixed(2) + "%";
            $scope.trendShow.r1.neg = (trendData['300']['r1'].neg / 300 * 100).toFixed(2) + "%";
            if (trendData['300']['r1'].pos > trendData['300']['r1'].neg)
                $scope.trendShow.r1.info = "UP";
            else
                $scope.trendShow.r1.info = "DOWN";


            /*DO penny*/
            $scope.trendShow.penny.neg = (trendData['300']['penny'].neg / 130 * 100).toFixed(2) + '%';
            $scope.trendShow.penny.closeUpTwoDay = (trendData['300']['penny'].closeUpTwoDay / 130 * 100).toFixed(2) + '%';
            $scope.trendShow.penny.closeDownTwoDay = (trendData['300']['penny'].closeDownTwoDay / 130 * 100).toFixed(2) + '%';
        } else {
            /*DO: HIGH LOW*/
            $scope.trendShow.oneM.pos = trendData['150']['1M'].pos;
            $scope.trendShow.oneM.neg = trendData['150']['1M'].neg;
            $scope.trendShow.thirdM.pos = trendData['150']['3M'].pos;
            $scope.trendShow.thirdM.neg = trendData['150']['3M'].neg;
            $scope.trendShow.sixM.pos = trendData['150']['6M'].pos;
            $scope.trendShow.sixM.neg = trendData['150']['6M'].neg;
            if (trendData['150']['1M'].pos > trendData['150']['1M'].neg)
                $scope.trendShow.oneM.info = "UP";
            else
                $scope.trendShow.oneM.info = "DOWN";
            if (trendData['150']['3M'].pos > trendData['150']['3M'].neg)
                $scope.trendShow.thirdM.info = "UP";
            else
                $scope.trendShow.thirdM.info = "DOWN";
            if (trendData['150']['6M'].pos > trendData['150']['6M'].neg)
                $scope.trendShow.sixM.info = "UP";
            else
                $scope.trendShow.sixM.info = "DOWN";

            /*DO: SMA*/
            $scope.trendShow.sma10.pos = (trendData['150']['sma10'].pos / 300 * 100).toFixed(2) + "%";
            $scope.trendShow.sma10.neg = (trendData['150']['sma10'].neg / 300 * 100).toFixed(2) + "%";
            $scope.trendShow.sma20.pos = trendData['150']['sma20'].pos;
            $scope.trendShow.sma20.neg = trendData['150']['sma20'].neg;
            $scope.trendShow.sma50.pos = trendData['150']['sma50'].pos;
            $scope.trendShow.sma50.neg = trendData['150']['sma50'].neg;
            $scope.trendShow.sma100.pos = trendData['150']['sma100'].pos;
            $scope.trendShow.sma100.neg = trendData['150']['sma100'].neg;
            if (trendData['150']['sma10'].pos > trendData['150']['sma10'].neg)
                $scope.trendShow.sma10.info = "UP";
            else
                $scope.trendShow.sma10.info = "DOWN";
            if (trendData['150']['sma20'].pos > trendData['150']['sma10'].neg)
                $scope.trendShow.sma20.info = "UP";
            else
                $scope.trendShow.sma20.info = "DOWN";
            if (trendData['300']['sma100'].average > 0)
                $scope.trendShow.sma20.add = "Bq tt nằm trên " + (trendData['150']['sma20'].average / 3).toFixed(2) + "%" + " đường MA20";
            else
                $scope.trendShow.sma20.add = "Bq tt nằm dưới " + (trendData['150']['sma20'].average / 3).toFixed(2) + "%" + " đường MA20";
            if (trendData['150']['sma50'].pos > trendData['150']['sma50'].neg)
                $scope.trendShow.sma50.info = "UP";
            else
                $scope.trendShow.sma50.info = "DOWN";
            if (trendData['300']['sma50'].average > 0)
                $scope.trendShow.sma50.add = "Bq tt nằm trên " + (trendData['150']['sma50'].average / 3).toFixed(2) + "%" + " đường MA50";
            else
                $scope.trendShow.sma50.add = "Bq tt nằm dưới " + (trendData['150']['sma50'].average / 3).toFixed(2) + "%" + " đường MA50";
            if (trendData['150']['sma100'].pos > trendData['150']['sma100'].neg)
                $scope.trendShow.sma100.info = "UP";
            else
                $scope.trendShow.sma100.info = "DOWN";
            if (trendData['300']['sma100'].average > 0)
                $scope.trendShow.sma100.add = "Bq tt nằm trên " + (trendData['150']['sma100'].average / 3).toFixed(2) + "%" + " đường MA100";
            else
                $scope.trendShow.sma100.add = "Bq tt nằm dưới " + (trendData['150']['sma100'].average / 3).toFixed(2) + "%" + " đường MA100";

            /*DO UPTREND*/
            $scope.trendShow.uptrend.pos = trendData['150']['uptrend'].pos;
            $scope.trendShow.uptrend.neg = trendData['150']['uptrend'].neg;
            if (trendData['150']['uptrend'].pos > trendData['150']['uptrend'].neg)
                $scope.trendShow.uptrend.info = "UP";
            else
                $scope.trendShow.uptrend.info = "DOWN";

            /*DO RS*/
            $scope.trendShow.rs.pos = (trendData['150']['rs'].pos / 300 * 100).toFixed(2) + "%";
            $scope.trendShow.rs.neg = (trendData['150']['rs'].neg / 300 * 100).toFixed(2) + "%";
            if (trendData['150']['rs'].pos > trendData['150']['rs'].neg)
                $scope.trendShow.rs.info = "UP";
            else
                $scope.trendShow.rs.info = "DOWN";


            /*DO R1*/
            $scope.trendShow.r1.pos = (trendData['150']['r1'].pos / 300 * 100).toFixed(2) + "%";
            $scope.trendShow.r1.neg = (trendData['150']['r1'].neg / 300 * 100).toFixed(2) + "%";
            if (trendData['150']['r1'].pos > trendData['150']['r1'].neg)
                $scope.trendShow.r1.info = "UP";
            else
                $scope.trendShow.r1.info = "DOWN";

            /*DO penny*/
            $scope.trendShow.penny.neg = (trendData['300']['penny'].neg / 130 * 100).toFixed(2) + '%';
            $scope.trendShow.penny.closeUpTwoDay = (trendData['300']['penny'].closeUpTwoDay / 130 * 100).toFixed(2) + '%';
            $scope.trendShow.penny.closeDownTwoDay = (trendData['300']['penny'].closeDownTwoDay / 130 * 100).toFixed(2) + '%';
        }
    };
    this.showInde = function (indeData, $scope) {
        $scope.inde.sv1.volIn = indeData['300']['sv1'].pos;
        $scope.inde.sv1.volDe = indeData['300']['sv1'].neg;

        $scope.inde.svbq.volIn = indeData['300']['svbq'].pos;
        $scope.inde.svbq.volDe = indeData['300']['svbq'].neg;

        $scope.inde.all.priceIn = indeData['300']['all'].priceIn;
        $scope.inde.all.priceInSv1VolIn = indeData['300']['all'].priceInSv1VolIn;
        $scope.inde.all.priceInSv1VolDe = indeData['300']['all'].priceInSv1VolDe;
        $scope.inde.all.priceInSvbqVolIn = indeData['300']['all'].priceInSvbqVolIn;
        $scope.inde.all.priceInSvbqVolDe = indeData['300']['all'].priceInSvbqVolDe;

        $scope.inde.all.priceDe = indeData['300']['all'].priceDe;
        $scope.inde.all.priceDeSv1VolIn = indeData['300']['all'].priceDeSv1VolIn;
        $scope.inde.all.priceDeSv1VolDe = indeData['300']['all'].priceDeSv1VolDe;
        $scope.inde.all.priceDeSvbqVolIn = indeData['300']['all'].priceDeSvbqVolIn;
        $scope.inde.all.priceDeSvbqVolDe = indeData['300']['all'].priceDeSvbqVolDe;
    };
});


market.directive('onFinishRender', function ($timeout) {
    return {
        restrict: 'A',
        link: function (scope, element, attr) {
            if (scope.$last === true) {
                $timeout(function () {
                    scope.$emit('ngRepeatFinished');
                });
            }
        }
    }
});

market.directive('izPercentEffect', function () {
        function link(scope, element, attrs) {
            var truePercent;

            function upDateCssPercent() {
                if (truePercent > 0)
                    jQuery(element).css("color", "green");
                if (truePercent < 0)
                    jQuery(element).css("color", "red");
                if (truePercent == 0)
                    jQuery(element).css("color", "yellow");
            }

            scope.$watch(attrs.truePercent, function (value) {
                truePercent = value;
                upDateCssPercent();
            });
        }

        return {
            restrict: 'A',
            link: link
        };
    }
);

