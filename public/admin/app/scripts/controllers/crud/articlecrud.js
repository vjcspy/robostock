/**
 * Created by vjcspy on 2/5/16.
 */
app.controller('ArticleCrudCtrl', ['$scope', '$timeout', function ($scope, $timeout) {
    /*Config Data table*/
    $scope.dataTable = {
        article: null,
        tableOptions: {
            processing: true,
            serverSide: true,
            "scrollX": true,
            "paging": true,
            scrollCollapse: true,
            responsive: true,
            ajax: 'http://blog.dev/table/article',
            "columns": [
                {"data": "id"},
                {"data": "title"},
                {"data": "short_content"},
                {"data": "article_type"},
                {"data": "image_title_resize_url"}
            ], "columnDefs": [ //FIXME: HAVE TO FILTER BY CLASS, sẽ chuyển cái này thành code
                {className: "aid", "targets": [0]},
                {className: "title", "targets": [1]},
                {className: "short_content", "targets": [2]},
                {className: "article_type", "targets": [3]},
                {className: "image_title_resize_url", "targets": [4]}
            ]
        },
        filterConfig: [
            {
                columnId: "aid",
                name: "Id",
                show: true,
                filterType: "text",
                filterChecked: false
            },
            {
                columnId: "title",
                name: "Title",
                show: true,
                filterType: "text",
                filterChecked: false
            },
            {
                columnId: "short_content",
                name: "Short Content",
                show: true,
                filterType: "text",
                filterChecked: false
            },
            {
                columnId: "article_type",
                name: "Article Type",
                show: true,
                filterType: "text",
                filterChecked: false
            },
            {
                columnId: "image_title_resize_url",
                name: "Image Url",
                show: true,
                filterType: "text",
                filterChecked: false
            }
        ]
    };
}
]);
