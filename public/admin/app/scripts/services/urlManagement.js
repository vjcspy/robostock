/**
 * Created by vjcspy on 2/2/16.
 */
(function () {
    var urlManagement = angular.module('urlManagement', []);
    urlManagement.service('urlManagement', function () {
        this.url = {
            'restful_config': 'http://blog.dev/api/v1/config',
            'restful_systemControl': 'http://' + window.location.hostname + '/api/v1/control'
        };
        this.getUrlByKey = function (key) {
            return this.url[key];
        }.bind(this);
        this.setUrlFromServer = function (url) {
            this.url = url;
        }.bind(this);
    })
})(angular);
