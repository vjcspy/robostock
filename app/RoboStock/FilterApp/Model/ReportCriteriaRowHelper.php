<?php
/**
 * Created by IntelliJ IDEA.
 * User: vjcspy
 * Date: 12/16/15
 * Time: 2:10 PM
 */

namespace App\RoboStock\FilterApp\Model;


use App\ReportCriteriaRow;

class ReportCriteriaRowHelper {


    private $_reportCriteriaRow;

    public function __construct(ReportCriteriaRow $reportCriteriaRow) {
        $this->_reportCriteriaRow = $reportCriteriaRow;
    }


    private $_arrayCriteria = array();

    public function getAllCriteriaGroupByType() {

        if (count($this->_arrayCriteria) == 0 || is_null($this->_arrayCriteria)) {
            $collection = $this->_reportCriteriaRow->query();

            $collection->chunk(100, function ($criterias) {
                foreach ($criterias as $criteria) {
                    if (isset($this->_arrayCriteria[$criteria->report_type]))
                        $this->_arrayCriteria[$criteria->report_type][] = array(
                            'id' => $criteria->criteria_id,
                            'name' => $criteria->row_name
                        );
                    else {
                        $this->_arrayCriteria[$criteria->report_type] = array();
                        $this->_arrayCriteria[$criteria->report_type][] = array(
                            'id' => $criteria->criteria_id,
                            'name' => $criteria->row_name
                        );
                    }
                }
            });
        }

        return $this->_arrayCriteria;

    }

    protected $_cacheRowName = array();

    public function getRowNameByCriteriaId($criteria_id) {
        if (!isset($this->_cacheRowName[$criteria_id])) {
            $first = $this->_reportCriteriaRow->query()->where('criteria_id', $criteria_id)->first();
            $this->_cacheRowName[$criteria_id] = $first->row_name;
        }
        return $this->_cacheRowName[$criteria_id];
    }
}
