<?php
/**
 * Created by IntelliJ IDEA.
 * User: vjcspy
 * Date: 12/13/15
 * Time: 1:45 AM
 */

namespace App\RoboStock\FilterApp\Model;


use App\ReportTypeRow;

class CollectTypeRow {
    protected $_reportType = null;
    protected $_arrayCriteria = array();

    private $_arrayTypeRowIdFilter = array();

    public function __construct() {
    }


    /**
     * Tra ve data report_type_row theo criteria
     * @return array
     * @throws \Exception
     */
    public function collect() {
        if (is_null($this->_arrayTypeRowIdFilter)) {
            if (count($this->_arrayCriteria) == 0)
                throw new \Exception('Must have data criteria id');
            $reportTypeRowModel = $this->getReportTypeRowModel();
            $collection = $reportTypeRowModel->query()->whereIn('criteria_id', $this->_arrayCriteria);
            if (!is_null($this->_reportType))
                $collection->where('report_type', $this->_reportType);
            $items = $collection->get();
            foreach ($items as $item) {
                $dataObject = app()->make('App\RoboStock\FilterApp\Model\Magento\Object\DataObject');
                $dataObject->setData($item->attributesToArray());
                $this->_arrayTypeRowIdFilter[] = $dataObject;
            }
        }
        return $this->_arrayTypeRowIdFilter;
    }

    /**
     * @param null $reportType
     */
    public function setReportType($reportType) {
        $this->_reportType = $reportType;
    }

    /**
     * @param array $arrayCriteria
     */
    public function setArrayCriteria($arrayCriteria) {
        $this->_arrayCriteria = $arrayCriteria;
    }

    private $_ReportTypeRowModel;

    /**
     * @return ReportTypeRow
     */
    private function getReportTypeRowModel() {
        if (is_null($this->_ReportTypeRowModel))
            $this->_ReportTypeRowModel = app()->make('App\ReportTypeRow');
        return $this->_ReportTypeRowModel;
    }

}
