<?php
/**
 * Created by mr.vjcspy@gmail.com/khoild@smartosc.com.
 * Date: 12/30/15
 * Time: 10:27 PM
 */

namespace App\RoboStock\FilterApp\Model\ExportExcel;


use App\RoboStock\FilterApp\Model\ReportCriteriaRowHelper;
use Maatwebsite\Excel\Facades\Excel;

class Export {
    protected $_criteriaRowHelper;

    public function __construct(ReportCriteriaRowHelper $criteriaRowHelper) {
        $this->_criteriaRowHelper = $criteriaRowHelper;
    }

    public function export(array $data) {
        $fileName = 'filter_' . microtime(true);

        $resultFilter = $data['resultFilter'];
        if (isset($resultFilter['year']))
            $resultFilter = $resultFilter['year'];
        if (isset($resultFilter['quarter']))
            $resultFilter = $resultFilter['quarter'];
        $rocData = $data['rocData'];
        $this->initDataToExport($resultFilter, $rocData);

        Excel::create($fileName, function ($excel) {

            $excel->sheet('Filter', function ($sheet) {

                foreach ($this->_dataExcel as $item) {
                    $item = str_replace(',','',$item);
                    $sheet->appendRow($item);
                }
            });

        })->store('xls');

        return $fileName;
    }

    protected $_dataExcel;

    protected function initDataToExport($resultFiler, $rocData) {
        $isFirst = true;
        $dataExcel = array();
        foreach ($resultFiler as $key => $itemValues) {
            $rowHeader = array();
            $row = array();

            if ($isFirst) {
                $rowHeader[] = 'Ticker';
                $rowHeader[] = 'Close';
                $rowHeader[] = 'RoC';
            }

            $row[] = $key;
            if(!isset($rocData[$key]))
                continue;
            $row[] = $rocData[$key]['close'];
            $row[] = $rocData[$key]['roc'];

            foreach ($itemValues as $criteria_id => $itemValue) {
                if ($isFirst)
                    $rowHeader[] = $this->_criteriaRowHelper->getRowNameByCriteriaId($criteria_id);
                $row[] = $itemValue;
            }

            if ($isFirst)
                $dataExcel[] = $rowHeader;
            $dataExcel[] = $row;
            $isFirst = false;
        }
        $this->_dataExcel = $dataExcel;
    }
}
