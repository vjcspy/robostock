<?php
/**
 * Created by IntelliJ IDEA.
 * User: vjcspy
 * Date: 12/13/15
 * Time: 6:27 PM
 */

namespace App\RoboStock\FilterApp\Model\DataFilter;


use App\ReportTickerDataQuarter;
use App\ReportTickerDataYear;
use App\RoboStock\FilterApp\Model\CollectTypeRow;
use app\RoboStock\FilterApp\Model\Filter\FilterGroups;
use App\RoboStock\FilterApp\Model\Magento\Object\DataObject;
use App\RoboStock\Report\Model\ReportTickerDataQuarterHelper;
use App\RoboStock\Report\Model\ReportTickerDataYearHelper;
use App\RoboStock\Report\Model\ReportTypeRowHelper;
use Symfony\Component\VarDumper\Cloner\Data;

abstract class CollectionAbstract implements Collection {


    /**
     * Chứa tất cả các report sẽ dùng để làm data filter
     * @var null
     */
    protected $_arrayReport = null;

    protected $_resultReportAfterFilter = array();

    /**
     * All filter here
     * @var null
     */
    protected $_filterGroups = null;

    protected $_collectionType = null;

    const COLLECTION_TYPE_QUARTER = 'QUARTER';
    const COLLECTION_TYPE_YEAR = 'YEAR';

    public function collect(FilterGroups $filterGroups) {
        // TODO: Collect all report to filter and push to arrayReport
    }

    /**
     * @param FilterGroups|null $filterGroups
     */
    public function setFilterGroups(FilterGroups $filterGroups) {
        $this->_filterGroups = $filterGroups;
    }

    public function __construct(ReportTickerDataQuarterHelper $reportTickerDataQuarterHelper, ReportTickerDataYearHelper $reportTickerDataYearHelper, ReportTypeRowHelper $reportTypeRowHelper) {
        $this->_report_ticker_data_quarter_helper = $reportTickerDataQuarterHelper;
        $this->_report_type_row_helper = $reportTypeRowHelper;
        $this->_report_ticker_data_year_helper = $reportTickerDataYearHelper;
    }

    public function runFilter() {
        if (!is_null($this->_arrayReport))
            foreach ($this->_arrayReport as $ticker => $reports) {

                if (!$this->doFilterAbsolute($ticker, $reports) || !$this->doFilterRelative($ticker, $reports))
                    if (isset($this->_resultReportAfterFilter[$ticker]))
                        unset($this->_resultReportAfterFilter[$ticker]);

            }
        else
            throw new \Exception('Must have data to filter');
        return $this->_resultReportAfterFilter;
    }

    protected function doFilterAbsolute($ticker, array $reports) {
        if ($this->_collectionType == self::COLLECTION_TYPE_QUARTER)
            $filterGroupsAbsolute = $this->getAllFiltersAbsolute()['quarter'];
        elseif ($this->_collectionType == self::COLLECTION_TYPE_YEAR)
            $filterGroupsAbsolute = $this->getAllFiltersAbsolute()['year'];
        else
            throw new \Exception('Must declare type of collection');
        $isSatisfy = true;
        foreach ($filterGroupsAbsolute as $filter) {
            /* @var $filter DataObject */
            if (!($filter instanceof DataObject))
                throw new \Exception('Each Filter must instance of DataObject');
            $currentTimeFilter = $filter->getTime();
            $checkValue = $filter->getCheckValue();

            $reportType = $filter->getReportType();


            /*DO: Phải lấy được row_id của ticker hiện tại*/
            // Dựa vào criteria_id thì lấy được report_type
            // Từ report_type thì biết được loại report_ticker_type của cp hiện tại
            // Từ report_ticker_type & criteria_id của cp hiện tại thì lấy được row_id
            if ($this->_collectionType == self::COLLECTION_TYPE_QUARTER)
                $reportTickerType = $this->getReportTickerTypeInQuarter($ticker, $reportType);
            else
                $reportTickerType = $this->getReportTickerTypeInYear($ticker, $reportType);

            $row_id = $this->getRowId($filter->getCriteriaId(), $reportTickerType);

            /*IMPORTANCE: Nếu không tìm thấy được row_id  chứng tỏ cổ phiếu hiện tại không có trường để filter. Trả về không thỏa mãn*/
            if (is_null($row_id))
                return $isSatisfy = false;

            // DO: check with current filter
            $isSatisfy = $this->checkAbsoluteCondition($reports, $currentTimeFilter, $row_id, $checkValue, $reportType);
            if ($isSatisfy !== FALSE) {
                if (!isset($this->_resultReportAfterFilter[$ticker]))
                    $this->_resultReportAfterFilter[$ticker] = array();
                $this->_resultReportAfterFilter[$ticker][$filter->getCriteriaId()] = $isSatisfy;
            } else {
                /*Nếu chỉ check 1 trong các filter thỏa mãn mà cho vào result luôn thì sẽ sai. Vì có trường hợp các filter khác không thỏa mãn*/
                /*Vì vậy nếu phát hiện ra 1 filter không thỏa mãn thì xóa thằng đấy ra khỏi result và return*/
                if (isset($this->_resultReportAfterFilter[$ticker]))
                    unset($this->_resultReportAfterFilter[$ticker]);
                return FALSE;
            }
        }
        return $isSatisfy;
    }

    protected function checkAbsoluteCondition(array $reports, $currentTime, $rowId, $checkValue, $reportType) {
        $report = $this->getReportByCurrentTime($reports, $currentTime, $reportType);

        /*DO: Nếu không tìm được data report để check điều kiện chứng tỏ cổ phiếu đó chưa có data để filter. Return không thỏa mãn*/
        if (is_null($report))
            return FALSE;

        return $this->checkAbsoluteValue($report, $rowId, $checkValue);
    }

    protected function checkAbsoluteValue($report, $rowId, $checkValue) {
        if (is_array($report)) {
            /*DO: truong hop so sanh nhieu quy 1 luc*/
            $value = 0;
            foreach ($report as $item) {
                /* @var $item DataObject */
                if (!($item instanceof DataObject))
                    throw new \Exception('Each item in reports must instance of DataObject');
                $dataCurrentReport = $item->getData('data');

                $arrayCurrentDataReport = json_decode($dataCurrentReport, true);
                if (isset($arrayCurrentDataReport[$rowId])) {
                    $value += floatval(str_replace(',', '', $arrayCurrentDataReport[$rowId]));
                } else
                    throw new \Exception('Trả về giá trị này chứng tỏ data cổ phiếu:  ' . $item->getTicker() .
                        ' có vấn đề. Cổ phiếu có cùng kiểu report_ticker_type mà lại không chứa row_id của loại đấy. Gần như không thể xảy ra. Cần xem xét lại');

            }
        } else {
            /*DO: trong truong hop khong phai la so sanh nhieu quy*/
            $dataReport = $report->getData('data');
            $arrayData = json_decode($dataReport, true);

            if (isset($arrayData[$rowId])) {
                $value = floatval(str_replace(',', '', $arrayData[$rowId]));
            } else
                throw new \Exception('Trả về giá trị này chứng tỏ data cổ phiếu:  ' . $report->getTicker() .
                    ' có vấn đề. Cổ phiếu có cùng kiểu report_ticker_type mà lại không chứa row_id của loại đấy. Gần như không thể xảy ra. Cần xem xét lại');
        }
        $arrayCheckValue = explode('|', $checkValue);

        if (!in_array(count($arrayCheckValue), array(2, 3)))
            throw new \Exception('Check Value Wrong formatted');

        switch ($arrayCheckValue[0]) {
            case 'gt':
                return ($value >= floatval($arrayCheckValue[1])) ? $value : FALSE;
            case 'lt':
                return ($value <= floatval($arrayCheckValue[1])) ? $value : FALSE;
            case 'in':
                return ($value >= $arrayCheckValue[1] && $value <= $arrayCheckValue[2]) ? $value : FALSE;
            default:
                return FALSE;
        }
    }


    protected function getReportByCurrentTime(array $reports, $currentTimeFilter, $reportType) {
        /*IMPORTANCE: Sẽ có trường hợp xảy ra đó là cố phiếu hiện tại không có data report của quý cần filter. Trả về không thỏa mãn*/
        $result = [];
        foreach ($reports as $report) {
            if (!($report instanceof DataObject))
                throw new \Exception('Each report must instance of DataObject');
            if ($this->_collectionType == self::COLLECTION_TYPE_QUARTER) {
                /*DO: add new feature work with multi quarter*/
                $arrayTimeQuarter = explode(',', $currentTimeFilter);

                if (in_array($report->getQuarter(), $arrayTimeQuarter) && $report->getReportType() == $reportType)
                    $result[] = $report;
            } else
                if ($report->getYear() == $currentTimeFilter && $report->getReportType() == $reportType)
                    $result = $report;
        }
        return $result;
    }

    protected function doFilterRelative($ticker, array $reports) {
        if ($this->_collectionType == self::COLLECTION_TYPE_QUARTER)
            $filterGroupsRelative = $this->getAllFiltersRelative()['quarter'];
        elseif ($this->_collectionType == self::COLLECTION_TYPE_YEAR)
            $filterGroupsRelative = $this->getAllFiltersRelative()['year'];
        else
            throw new \Exception('Must declare collection type');
        $isSatisfy = true;
        foreach ($filterGroupsRelative as $filter) {
            /* @var $filter DataObject */
            if (!($filter instanceof DataObject))
                throw new \Exception('Each Filter must instace of DataObject');
            $currentTimeFilter = $filter->getTime();
            $checkValue = $filter->getCheckValue();

            $reportType = $filter->getReportType();


            /*DO: Phải lấy được row_id của ticker hiện tại*/
            // Dựa vào criteria_id thì lấy được report_type
            // Từ report_type thì biết được loại report_ticker_type của cp hiện tại
            // Từ report_ticker_type & criteria_id của cp hiện tại thì lấy được row_id

            if ($this->_collectionType == self::COLLECTION_TYPE_QUARTER)
                $reportTickerType = $this->getReportTickerTypeInQuarter($ticker, $reportType);
            else
                $reportTickerType = $this->getReportTickerTypeInYear($ticker, $reportType);
            $row_id = $this->getRowId($filter->getCriteriaId(), $reportTickerType);

            /*IMPORTANCE: Nếu không tìm thấy được row_id  chứng tỏ cổ phiếu hiện tại không có trường để filter. Trả về không thỏa mãn*/
            if (is_null($row_id))
                return $isSatisfy = false;

            // DO: check with current filter
            $isSatisfy = $this->checkRelativeCondition($reports, $currentTimeFilter, $row_id, $checkValue, $reportType);
            if ($isSatisfy !== FALSE) {
                if (!isset($this->_resultReportAfterFilter[$ticker]))
                    $this->_resultReportAfterFilter[$ticker] = array();
                $this->_resultReportAfterFilter[$ticker]['TT_' . $filter->getCriteriaId()] = $isSatisfy;
            } else {
                /*Nếu chỉ check 1 trong các filter thỏa mãn mà cho vào result luôn thì sẽ sai. Vì có trường hợp các filter khác không thỏa mãn*/
                /*Vì vậy nếu phát hiện ra 1 filter không thỏa mãn thì xóa thằng đấy ra khỏi result và return*/
                if (isset($this->_resultReportAfterFilter[$ticker]))
                    unset($this->_resultReportAfterFilter[$ticker]);
                return FALSE;
            }
        }
        return $isSatisfy;
    }

    protected function checkRelativeCondition($reports, $currentTimeFilter, $row_id, $checkValue, $reportType) {
        $reports = $this->getReportByRelativeByTime($reports, $currentTimeFilter, $reportType);

        /*DO: Nếu không tìm được data report để check điều kiện chứng tỏ cổ phiếu đó chưa có data để filter. Return không thỏa mãn*/
        if (count($reports) != 2)
            return FALSE;

        return $this->checkRelativeValue($reports, $row_id, $checkValue);
    }

    protected function getReportByRelativeByTime($reports, $currentTimeFilter, $reportType) {
        $arrayRelativeQuater = explode('|', $currentTimeFilter);
        if (count($arrayRelativeQuater) != 2)
            throw new \Exception('Data relative time wrong formatted');
        $result = array();
        $result[0] = [];
        $result[1] = [];
        foreach ($reports as $report) {
            if (!($report instanceof DataObject))
                throw new \Exception('Each report must instance of DataObject');
            if ($this->_collectionType == self::COLLECTION_TYPE_QUARTER) {
                /*DO: add new feature work with many time quarter*/
                $arrayQuarterTime0 = explode(',', $arrayRelativeQuater[0]);
                $arrayQuarterTime1 = explode(',', $arrayRelativeQuater[1]);
                if (in_array($report->getQuarter(), $arrayQuarterTime0) && $report->getReportType() == $reportType)
                    $result[0][] = $report;
                if (in_array($report->getQuarter(), $arrayQuarterTime1) && $report->getReportType() == $reportType)
                    $result[1][] = $report;
            } else {
                if ($report->getYear() == $arrayRelativeQuater[0] && $report->getReportType() == $reportType)
                    $result[0] = $report;
                if ($report->getYear() == $arrayRelativeQuater[1] && $report->getReportType() == $reportType)
                    $result[1] = $report;
            }
        }
        return $result;
    }

    protected function checkRelativeValue($reports, $rowId, $checkValue) {
        if (!($reports[0] instanceof DataObject)) {
            /*DO: day la truong hop many quarter*/
            $value0 = 0;
            $value1 = 0;
            foreach ($reports[0] as $report) {
                $arrayCurrentDataReport = json_decode($report->getData('data'), true);
                if (isset($arrayCurrentDataReport[$rowId]))
                    $value0 += floatval(str_replace(',', '', $arrayCurrentDataReport[$rowId]));
                else
                    throw new \Exception('Trả về giá trị này chứng tỏ data cổ phiếu:  ' . $reports[0]->getTicker() .
                        ' có vấn đề. Cổ phiếu có cùng kiểu report_ticker_type mà lại không chứa row_id của loại đấy. Gần như không thể xảy ra. Cần xem xét lại');
            }
            foreach ($reports[1] as $report) {
                $arrayCurrentDataReport = json_decode($report->getData('data'), true);
                if (isset($arrayCurrentDataReport[$rowId]))
                    $value1 += floatval(str_replace(',', '', $arrayCurrentDataReport[$rowId]));
                else
                    throw new \Exception('Trả về giá trị này chứng tỏ data cổ phiếu:  ' . $reports[0]->getTicker() .
                        ' có vấn đề. Cổ phiếu có cùng kiểu report_ticker_type mà lại không chứa row_id của loại đấy. Gần như không thể xảy ra. Cần xem xét lại');
            }
        } else {
            /*DO: truong hop cu khong co many time quarter*/
            $dataReport = $reports[0]->getData('data');
            $arrayData0 = json_decode($dataReport, true);
            $dataReport = $reports[1]->getData('data');
            $arrayData1 = json_decode($dataReport, true);

            if (isset($arrayData0[$rowId]) && isset($arrayData1[$rowId])) {
                $value0 = floatval(str_replace(',', '', $arrayData0[$rowId]));
                $value1 = floatval(str_replace(',', '', $arrayData1[$rowId]));
            } else
                throw new \Exception('Trả về giá trị này chứng tỏ data cổ phiếu:  ' . $reports[0]->getTicker() .
                    ' có vấn đề. Cổ phiếu có cùng kiểu report_ticker_type mà lại không chứa row_id của loại đấy. Gần như không thể xảy ra. Cần xem xét lại');
        }
        /*1 trong 2 gia tri =0*/
        if ($value0 == 0 || $value1 == 0)
            return FALSE;

        $arrayCheckValue = explode('|', $checkValue);

        if (!in_array(count($arrayCheckValue), array(2, 3)))
            throw new \Exception('Check Value Wrong formatted');

        switch ($arrayCheckValue[0]) {
            case 'gt':
                return ((($value1 * 100 / $value0) - 100) >= floatval($arrayCheckValue[1])) ? round((($value1 * 100 / $value0) - 100), 2) . '%' : FALSE;
            case 'lt':
                return ((($value1 * 100 / $value0) - 100) <= floatval($arrayCheckValue[1])) ? round((($value1 * 100 / $value0) - 100), 2) . '%' : FALSE;
            case 'in':
                return ((($value1 * 100 / $value0) - 100) >= $arrayCheckValue[1] && (($value1 * 100 / $value0) - 100) <= $arrayCheckValue[2]) ? (($value1 * 100 / $value0) - 100) : FALSE;
        }
        return FALSE;

    }

    private $_arrayFilterGroupsAbsolute = null;

    /**
     * Retrieve tất cả các filter theo giá trị tuyệt đối
     * @return array|null
     */
    protected function getAllFiltersAbsolute() {
        if (is_null($this->_arrayFilterGroupsAbsolute)) {
            $this->_arrayFilterGroupsAbsolute = array();
            $this->_arrayFilterGroupsAbsolute['quarter'] = array();
            $this->_arrayFilterGroupsAbsolute['year'] = array();
            foreach ($this->_filterGroups->getAllFilterByQuarter() as $filterGroup) {
                if ($filterGroup->getFilterType() == FilterGroups::FILTER_TYPE_ABSOLUTE)
                    $this->_arrayFilterGroupsAbsolute['quarter'][] = $filterGroup;
            }
            foreach ($this->_filterGroups->getAllFilterByYear() as $filterGroup) {
                if ($filterGroup->getFilterType() == FilterGroups::FILTER_TYPE_ABSOLUTE)
                    $this->_arrayFilterGroupsAbsolute['year'][] = $filterGroup;
            }
        }
        return $this->_arrayFilterGroupsAbsolute;
    }

    private $_arrayFilterGroupsRelative = null;

    /**
     * Retrieve all filter get by relative
     * @return array|null
     * @throws \Exception
     */
    protected function getAllFiltersRelative() {
        if (is_null($this->_arrayFilterGroupsRelative)) {
            $this->_arrayFilterGroupsRelative = array();
            $this->_arrayFilterGroupsRelative['quarter'] = array();
            $this->_arrayFilterGroupsRelative['year'] = array();
            foreach ($this->_filterGroups->getAllFilterByQuarter() as $filterGroup) {
                if ($filterGroup->getFilterType() == FilterGroups::FILTER_TYPE_RELATIVE)
                    $this->_arrayFilterGroupsRelative['quarter'][] = $filterGroup;
            }
            foreach ($this->_filterGroups->getAllFilterByYear() as $filterGroup) {
                if ($filterGroup->getFilterType() == FilterGroups::FILTER_TYPE_RELATIVE)
                    $this->_arrayFilterGroupsRelative['year'][] = $filterGroup;
            }
        }
        return $this->_arrayFilterGroupsRelative;
    }

    private $_report_ticker_data_quarter_helper;

    protected function getReportTickerTypeInQuarter($ticker, $report_type) {
        if (is_null($this->_report_ticker_data_quarter_helper))
            throw new \Exception('Must instance class by __construct');
        return $this->_report_ticker_data_quarter_helper->getReportTickerType($ticker, $report_type);
    }

    private $_report_type_row_helper;

    protected function getRowId($criteria_id, $report_ticker_type) {
        if (is_null($this->_report_type_row_helper))
            throw new \Exception('Must instance class by __construct');
        return $this->_report_type_row_helper->getRowId($criteria_id, $report_ticker_type);
    }

    private $_report_ticker_data_year_helper;

    protected function getReportTickerTypeInYear($ticker, $report_type) {
        if (is_null($this->_report_ticker_data_year_helper))
            throw new \Exception('Must instance class by __construct');
        return $this->_report_ticker_data_year_helper->getReportTickerType($ticker, $report_type);
    }

    private $_ReportTickerDataQuarterModel;

    /**
     * @return ReportTickerDataQuarter
     */
    protected function getReportTickerDataQuarterModel() {
        if (is_null($this->_ReportTickerDataQuarterModel))
            $this->_ReportTickerDataQuarterModel = new ReportTickerDataQuarter();
        return $this->_ReportTickerDataQuarterModel;
    }

    private $_ReportTickerDataYearModel;

    /**
     * @return ReportTickerDataYear
     */
    protected function getReportTickerDataYearModel() {
        if (is_null($this->_ReportTickerDataYearModel))
            $this->_ReportTickerDataYearModel = new ReportTickerDataYear();
        return $this->_ReportTickerDataYearModel;
    }
}
