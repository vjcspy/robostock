<?php
/**
 * Created by IntelliJ IDEA.
 * User: vjcspy
 * Date: 12/13/15
 * Time: 2:49 AM
 */

namespace App\RoboStock\FilterApp\Model\DataFilter\Quarter;


use App\RoboStock\FilterApp\Model\DataFilter\CollectionAbstract;
use App\RoboStock\FilterApp\Model\Filter\FilterGroups;
use App\RoboStock\FilterApp\Model\Magento\Object\DataObject;

class Collection extends CollectionAbstract implements \app\RoboStock\FilterApp\Model\DataFilter\Collection {

    protected $_collectionType = CollectionAbstract::COLLECTION_TYPE_QUARTER;

    public function collect(FilterGroups $filterGroups = null) {
        if (is_null($filterGroups))
            $filterGroups = $this->_filterGroups;

        if (is_null($this->_arrayReport)) {
            $model = $this->getReportTickerDataQuarterModel();

            $collection = $model->query();

            /*DO: Chỉ collect những report có quý thỏa mãn nằm trong filter*/
            $collection->whereIn('quarter', $filterGroups->getAllQuarterFilter());

            /*DO: Chỉ collect những report có report_type nằm trong filter*/
            if (count($filterGroups->getReportTypeByQuarter()) > 0)
                $collection->whereIn('report_type', $filterGroups->getReportTypeByQuarter());

            $collection->chunk(100, function ($reports) {
                foreach ($reports as $report) {
//                   DO: collect all report and group by ticker
                    if (!isset($this->_arrayReport[$report->ticker]))
                        $this->_arrayReport[$report->ticker] = array();
                    $dataObject = new DataObject();
                    $dataObject->setData($report->attributesToArray());
                    $this->_arrayReport[$report->ticker][] = $dataObject;
                }
            });

        }
        return $this->_arrayReport;
    }

}
