<?php
/**
 * Created by IntelliJ IDEA.
 * User: vjcspy
 * Date: 12/13/15
 * Time: 2:50 AM
 */

namespace App\RoboStock\FilterApp\Model\DataFilter;


use App\RoboStock\FilterApp\Model\Filter\FilterGroups;

interface Collection {
    public function collect(FilterGroups $filterGroups);
}
