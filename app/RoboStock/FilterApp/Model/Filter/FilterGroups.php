<?php
/**
 * Created by IntelliJ IDEA.
 * User: vjcspy
 * Date: 12/13/15
 * Time: 2:27 AM
 */

namespace App\RoboStock\FilterApp\Model\Filter;


use App\RoboStock\FilterApp\Model\Magento\Object\DataObject;

/*
 * Chứa tất cả các tiêu chí filter
 * Mỗi tiêu chí filter bao gồm: filter_type|report_type|criteria_id|time|check_value
 * Va cac ham de lay duoc trang thai filter
 * */

class FilterGroups extends DataObject {

    const REPORT_TICKER_TYPE_ALL = "ALL";

    const FILTER_TYPE_ABSOLUTE = "ABSOLUTE";
    const FILTER_TYPE_RELATIVE = "RELATIVE";

    protected $_arrayFilters = array();
    protected $_report_ticker_type = self::REPORT_TICKER_TYPE_ALL;

    /**
     * @return null
     */
    public function getReportTickerType() {
        return $this->_report_ticker_type;
    }

    /**
     * @param null $report_ticker_type
     */
    public function setReportTickerType($report_ticker_type) {
        $this->_report_ticker_type = $report_ticker_type;
    }

    /**
     * Add các filter vào GroupFilters
     * @param $filterEntity
     * @throws \Exception
     */
    public function addFilter($filterEntity) {
        if ($filterEntity instanceof DataObject)
            $this->_arrayFilters[] = $filterEntity;
        else {
            if (!is_array($filterEntity))
                throw new \Exception('Can\'t add filter into FilterGroups');
            $dataObject = new DataObject();
            $dataObject->setData($filterEntity);
            $this->_arrayFilters[] = $dataObject;
        }
    }

    public function hasFilterQuarter() {
        /*time: 1-2015|2-1015*/
        foreach ($this->_arrayFilters as $filter) {
            if (strpos($filter->getTime(), '-') !== false)
                return true;
        }
        return false;
    }

    public function hasFilterYear() {
        foreach ($this->_arrayFilters as $filter) {
            if (strpos($filter->getTime(), '-') === false)
                return true;
        }
        return false;
    }

    private $_allQuartersFilter = array();

    /**
     * Trả về tất cả các quý được filter. Sẽ collect tất cả các report có quý nằm trong này.
     * @return array
     */
    public function getAllQuarterFilter() {
        foreach ($this->_arrayFilters as $filter) {
            if (strpos($filter->getTime(), '-') !== FALSE) {
                $arrayTime = explode('|', $filter->getTime());
                foreach ($arrayTime as $time) {
                    /*DO: new feature: work with many quarter*/
                    $arrayQuarter = explode(',', $time);
                    foreach ($arrayQuarter as $quarter) {
                        if ($quarter != '')
                            if (!in_array($quarter, $this->_allQuartersFilter))
                                $this->_allQuartersFilter[] = $quarter;
                    }
                }
            }
        }

        return $this->_allQuartersFilter;
    }

    private $_allYearsFilter = array();

    /**
     * Trả về tất cả các năm được filter. Sẽ collect tất cả các report có năm nằm trong này.
     * @return array
     */
    public function getAllYearFilter() {
        foreach ($this->_arrayFilters as $filter) {
            if (strpos($filter->getTime(), '-') === FALSE) {
                $arrayTime = explode('|', $filter->getTime());
                foreach ($arrayTime as $time) {
                    if (!in_array($time, $this->_allYearsFilter))
                        $this->_allYearsFilter[] = $time;
                }
            }
        }
        return $this->_allYearsFilter;
    }

    private $_arrayReportType = array();

    /**
     * Trả về tất cả các loại report_type được filter theo quý
     * @return mixed
     */
    public function getReportTypeByQuarter() {
        $this->_arrayReportType['quarter'] = array();
        foreach ($this->_arrayFilters as $filter) {
            if (strpos($filter->getTime(), '-') !== FALSE) {
                if (!in_array($filter->getReportType(), $this->_arrayReportType['quarter']))
                    $this->_arrayReportType['quarter'][] = $filter->getReportType();
            }
        }
        return $this->_arrayReportType['quarter'];
    }

    /**
     * Trả về tất cả các loại report_type được filter theo năm
     * @return mixed
     */
    public function getReportTypeByYear() {
        $this->_arrayReportType['year'] = array();
        foreach ($this->_arrayFilters as $filter) {
            if (strpos($filter->getTime(), '-') === FALSE) {
                if (!in_array($filter->getReportType(), $this->_arrayReportType['year']))
                    $this->_arrayReportType['year'][] = $filter->getReportType();
            }
        }
        return $this->_arrayReportType['year'];
    }

    private $_allFilterByQuarter;

    /**
     * Trả về tất cả các filter bằng quý
     * @return array
     * @throws \Exception
     */
    public function getAllFilterByQuarter() {
        if (is_null($this->_allFilterByQuarter)) {
            $this->_allFilterByQuarter = array();
            foreach ($this->_arrayFilters as $filter) {
                if (strpos($filter->getTime(), '-') !== FALSE)
                    if ($filter instanceof DataObject)
                        $this->_allFilterByQuarter[] = $filter;
                    else if (is_array($filter)) {
                        $dataObject = new DataObject();
                        $dataObject->setData($filter);
                    } else
                        throw new \Exception('Can\'t get Filter');

            }
        }
        return $this->_allFilterByQuarter;
    }

    private $_allFiltersByYear;

    /**
     * Trả về tất cả các filter bằng năm
     * @return array
     * @throws \Exception
     */
    public function getAllFilterByYear() {
        if (is_null($this->_allFiltersByYear)) {
            $this->_allFiltersByYear = array();
            foreach ($this->_arrayFilters as $filter) {
                if (strpos($filter->getTime(), '-') === FALSE)
                    if ($filter instanceof DataObject)
                        $this->_allFiltersByYear[] = $filter;
                    else if (is_array($filter)) {
                        $dataObject = new DataObject();
                        $dataObject->setData($filter);
                    } else
                        throw new \Exception('Can\'t get Filter');

            }
        }
        return $this->_allFiltersByYear;
    }
}
