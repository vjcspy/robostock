<?php
/**
 * Created by IntelliJ IDEA.
 * User: vjcspy
 * Date: 10/2/15
 * Time: 11:55 AM
 */

namespace App\RoboStock\Dashboard\MarketOverview;


use App\IndexUpto;
use App\RoboStock\Model\WorkWithTime\WorkWithTime;

class PlcsHelper {

    /*TODO: Ph này là lấy dữ liệu realtime*/
    public function getPercentIndex($date) {
        // Lấy percent từ bảng index upto
        $indexUpto = $this->getIndexUptoModel();
        $indexUptoCurrentDateModel = $indexUpto->query()->where('date', $date)->get();
        $arrayIndexUpto = array();
        foreach ($indexUptoCurrentDateModel as $index) {
            $arrayIndexUpto[$index->name] = array(
                'name' => $index->name,
                'change' => $index->change,
                'index' => $index->index,
                'percent' => $index->percent,
                'value' => $index->value,
                'vol' => $index->vol,
            );
        }
        return $arrayIndexUpto;
    }

    private $_IndexUptoModel;

    /**
     * @return IndexUpto
     */
    private function getIndexUptoModel() {
        if (is_null($this->_IndexUptoModel))
            $this->_IndexUptoModel = new IndexUpto();
        return $this->_IndexUptoModel;
    }
}
