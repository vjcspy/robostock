<?php
/**
 * Created by IntelliJ IDEA.
 * User: vjcspy
 * Date: 11/16/15
 * Time: 3:40 PM
 */

namespace App\RoboStock\Report\Run;
include_once('./app/RoboStock/Model/Curl/simple_html_dom.php');

use App\RoboStock\Model\Log\LogHelper;
use App\RoboStock\Model\WorkWithTime\WorkWithTime;
use App\RoboStock\Model\WorkWithUrl\WorkWithUrl;
use App\RoboStock\Report\Model\ReportTickerDataQuarterHelper;
use App\RoboStock\Report\Model\ReportTickerDataYearHelper;
use App\RoboStock\Report\Model\ReportTypeSizeHelper;
use App\TickerInDay;

class GetDataReport {
    private $_urlData = 'http://finance.vietstock.vn/Controls/Report/Data/GetReport.ashx?rpt',
        $_type = 'Type=',
        $_code = '&scode=',
        $_bizType = '&bizType',
        $_rptUnit = '&rptUnit=',
        $_rptTermTypeID = '&rptTermTypeID=',
        $_page = '&page=';

    /*TODO:TEST*/
    public function testGetData() {
        $model = new TickerInDay();
        $allTickers = $model->all();
//        $allTickers = array('BMP');
        $helper = new ReportTickerDataYearHelper();
//        $arrayTickerExisted = $helper->getArrayTickerExisted('CSTC');
        $count = 0;
        foreach ($allTickers as $allTicker) {
//            if (in_array($allTicker, $arrayTickerExisted))
//                continue;
            if ($count > 50)
                return 'Worked with 50 ticker';

            $this->saveDataReportTickerYear($allTicker, 'CSTC');
            $count += 1;
        }
    }


    public function saveDataReportTickerQuarter($ticker, $reportType) {
        if (is_null($ticker) || is_null($reportType))
            return 'Null';
        $numOfPageCheck = 1;
        $countPage = 0;
        $reportTickerType = null;
        $logHelper = new LogHelper();
        do {
            $countPage += 1;
            $url = $this->_urlData . $this->_type . $reportType . $this->_code . $ticker . $this->_bizType . '1' . $this->_rptUnit . '1000' . $this->_rptTermTypeID . '2' . $this->_page . $countPage;
            $dataReport = $this->getDataReportQuarterFromVietStockUrl($url);
            if (is_null($dataReport))
                break;

            $arrayRowIdData = array();
            $arrayRowQ = array();
            foreach ($dataReport['data'] as $rowId => $row) {
                $arrayRowIdData[] = array(
                    'row_id' => $rowId,
                    'row_name' => $row[0]
                );

                $_start = $this->getStartArrayRow($reportType);
                $_end = $_start + 4;

                do {
                    if (isset($row[$_start]))
                        if (isset($arrayRowQ[$_start]))
                            $arrayRowQ[$_start][$rowId] = $row[$_start];
                        else {
                            $arrayRowQ[$_start] = array();
                            $arrayRowQ[$_start][$rowId] = $row[$_start];
                        }
                    else
                        $logHelper->addLog('not isset $row[' . $_start . ']. Url:' . $url);

                    $_start += 1;
                } while ($_start < $_end);

            }

            if (is_null($reportTickerType))
                $reportTickerType = $this->getReportTypeSizeHelper()->getReportType($arrayRowIdData, $reportType);

            $_start = $this->getStartArrayRow($reportType);
            $_end = $_start + 4;

            do {
                if (isset($dataReport['dataQ'][$_start - $_end + 4])) {
                    $dataQ = $dataReport['dataQ'][$_start - $_end + 4];
                    if (isset($row[$_start]))
                        $this->getReportTickerDataQuarter()->saveToDbQuarterTable($ticker, $reportType, $dataQ, $arrayRowQ[$_start], $reportTickerType);
                }
                $_start += 1;
            } while ($_start < $_end);


        } while ($countPage < $numOfPageCheck);
        return $reportTickerType;
    }

    public function saveDataReportTickerYear($ticker, $reportType) {
        if (is_null($ticker) || is_null($reportType))
            return 'Null';
        $numOfPageCheck = 1;
        $countPage = 0;
        $reportTickerType = null;
        $logHelper = new LogHelper();
        do {
            $countPage += 1;
            $url = $this->_urlData . $this->_type . $reportType . $this->_code . $ticker . $this->_bizType . '1' . $this->_rptUnit . '1000' . $this->_rptTermTypeID . '1' . $this->_page . $countPage;
            $dataReport = $this->getDataReportYearsFromVietStockUrl($url);
            if (is_null($dataReport))
                break;

            $arrayRowIdData = array();
            $arrayRowY = array();
            foreach ($dataReport['data'] as $rowId => $row) {
                $arrayRowIdData[] = array(
                    'row_id' => $rowId,
                    'row_name' => $row[0]
                );

                $_start = $this->getStartArrayRow($reportType);
                $_end = $_start + 4;

                do {
                    if (isset($row[$_start]))
                        if (isset($arrayRowY[$_start]))
                            $arrayRowY[$_start][$rowId] = $row[$_start];
                        else {
                            $arrayRowY[$_start] = array();
                            $arrayRowY[$_start][$rowId] = $row[$_start];
                        }
                    else
                        $logHelper->addLog('not isset $row[' . $_start . ']. Url:' . $url);

                    $_start += 1;
                } while ($_start < $_end);

            }

            if (is_null($reportTickerType))
                $reportTickerType = $this->getReportTypeSizeHelper()->getReportType($arrayRowIdData, $reportType);

            $_start = $this->getStartArrayRow($reportType);
            $_end = $_start + 4;

            do {
                if (isset($dataReport['dataY'][$_start - $_end + 4])) {
                    $dataY = $dataReport['dataY'][$_start - $_end + 4];
                    if (isset($row[$_start]))
                        $this->getReportTickerDataYearHelper()->saveToDbYearTable($ticker, $reportType, $dataY, $arrayRowY[$_start], $reportTickerType);
                }
                $_start += 1;
            } while ($_start < $_end);


        } while ($countPage < $numOfPageCheck);
        return $reportTickerType;
    }

    private function getStartArrayRow($reportType) {
        if ($reportType == 'CSTC')
            $_start = 3;
        else
            $_start = 2;
        return $_start;
    }

    private function getUrlVietStockReport($reportType, $ticker, $countPage, $bizType = 1) {
        $url = $this->_urlData . $this->_type . $reportType . $this->_code . $ticker . $this->_bizType . $bizType . $this->_rptUnit . '1000' . $this->_rptTermTypeID . '2' . $this->_page . $countPage;
        return $url;
    }

    /**
     * @param $url
     * @return array|mixed|string
     */
    private function getDataReportQuarterFromVietStockUrl($url) {
        $workWithUrl = new WorkWithUrl();
        $data = $workWithUrl->getDataFromUrl($url);

        if ($data == false)
            return null;

        $html = str_get_html($data);

        //BR_rowHeader
        $string = $html->find('#BR_rowHeader')[0]->plaintext;
        $dataTime = $this->getQuarterInData($string);

        $data = array();

        foreach ($html->find('#BR_tBody') as $table) {
            foreach ($table->find('tr') as $tr) {
                $row = array();
                foreach ($tr->find('td') as $td) {
                    $row[] = ($td->plaintext);
                }
                $data[$tr->id] = $row;

            }
        }
        return array(
            'dataQ' => $dataTime,
            'data' => $data
        );
    }

    private function getDataReportYearsFromVietStockUrl($url) {
        $workWithUrl = new WorkWithUrl();
        $data = $workWithUrl->getDataFromUrl($url);

        if ($data == false)
            return null;

        $html = str_get_html($data);

        //BR_rowHeader
        $string = $html->find('#BR_rowHeader')[0]->plaintext;
        $dataTime = $this->getYearInData($string);

        $data = array();

        foreach ($html->find('#BR_tBody') as $table) {
            foreach ($table->find('tr') as $tr) {
                $row = array();
                foreach ($tr->find('td') as $td) {
                    $row[] = ($td->plaintext);
                }
                $data[$tr->id] = $row;

            }
        }
        return array(
            'dataY' => $dataTime,
            'data' => $data
        );
    }

    private $_arrayCachedYears;

    private function getYearInData($data) {
        if (is_null($this->_arrayCachedYears))
            $this->_arrayCachedYears = WorkWithTime::getArrayYearByDec();
        $arrayY = array();
        foreach ($this->_arrayCachedYears as $item) {
            if (strrpos($data, $item['string']) != FALSE)
                $arrayY[] = $item['y'];
        }
        return $arrayY;
    }

    private $_arrayCacheQuarter;

    private function getQuarterInData($data) {
        if (is_null($this->_arrayCacheQuarter))
            $this->_arrayCacheQuarter = WorkWithTime::getArrayQuarterByDec();
        $arrayQuarter = $this->_arrayCacheQuarter;

        $arrayQ = array();
        foreach ($arrayQuarter as $item) {
            if (strrpos($data, $item['string']) != false)
                $arrayQ[] = $item;
        }
        return $arrayQ;
    }

    private $_ReportTypeSizeHelper;

    private function getReportTypeSizeHelper() {
        if (is_null($this->_ReportTypeSizeHelper))
            $this->_ReportTypeSizeHelper = new ReportTypeSizeHelper();
        return $this->_ReportTypeSizeHelper;
    }

    private $_ReportTickerDataQuarter;

    private function getReportTickerDataQuarter() {
        if (is_null($this->_ReportTickerDataQuarter))
            $this->_ReportTickerDataQuarter = new ReportTickerDataQuarterHelper();
        return $this->_ReportTickerDataQuarter;
    }

    private $_ReportTickerDataYearHelper;

    private function getReportTickerDataYearHelper() {
        if (is_null($this->_ReportTickerDataYearHelper))
            $this->_ReportTickerDataYearHelper = app()->make('App\RoboStock\Report\Model\ReportTickerDataYearHelper');
        return $this->_ReportTickerDataYearHelper;
    }
}
