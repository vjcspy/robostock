<?php
/**
 * Created by IntelliJ IDEA.
 * User: vjcspy
 * Date: 11/16/15
 * Time: 4:02 PM
 */

namespace App\RoboStock\Report\Model;


use App\ReportTypeRow;
use Mockery\CountValidator\Exception;

/**
 * Class ReportTypeRowModelHelper
 * @package app\RoboStock\Report\Model
 */
class ReportTypeRowHelper {
    /**
     * @param array $rowIdData
     * @param $reportType
     * @param $newTypeName
     * @return mixed
     */
    public function createNewRowType(Array $rowIdData, $reportType, $newTypeName) {
        $modelTypeRow = $this->getReportTypeRowModel();
        foreach ($rowIdData as $row) {
            $modelTypeRow->create(array(
                'report_type' => $reportType,
                'report_ticker_type' => $newTypeName,
                'row_id' => $row['row_id'],
                'row_name' => $row['row_name']
            ));
        }
        return $newTypeName;
    }

    private $_arrayCacheDataReportRow = array();

    public function checkRow(Array $rowIds, Array $reportTickerTypes, $reportType) {
        $model = $this->getReportTypeRowModel();
        if (!isset($this->_arrayCacheDataReportRow[$reportType]))
            $this->_arrayCacheDataReportRow[$reportType] = array();
        $isType = array();

        foreach ($reportTickerTypes as $reportTickerType) {
            $isType[$reportTickerType] = array(
                'reportType' => $reportType,
                'reportTickerType' => $reportTickerType,
                'isMatch' => true
            );
            if (!isset($this->_arrayCacheDataReportRow[$reportType][$reportTickerType])) {
                $allRowId = $model->query()->where(array(
                    'report_type' => $reportType,
                    'report_ticker_type' => $reportTickerType
                ))->get();
                $dataRows = array();
                foreach ($allRowId as $row) {
                    $dataRows[] = $row->row_id;
                }
                $this->_arrayCacheDataReportRow[$reportType][$reportTickerType] = $dataRows;
            }
            foreach ($rowIds as $rowId) {
                if (!in_array($rowId, $this->_arrayCacheDataReportRow[$reportType][$reportTickerType])) {
                    // Bởi vì trùng size nên chắc chắn check kiểu này sẽ đúng
                    $isType[$reportTickerType] = array(
                        'reportType' => $reportType,
                        'reportTickerType' => $reportTickerType,
                        'isMatch' => false,
                    );
                    break;
                };
            }
        }

        $numOfMatch = 0;
        foreach ($isType as $item) {
            if ($item['isMatch'] == true)
                $numOfMatch += 1;
        }
        if ($numOfMatch > 1)
            throw new Exception('There is more than one type_row meet criteria');

        return $isType;
    }

    private $_ReportTypeRowModel;

    /**
     * @return ReportTypeRow
     */
    private function getReportTypeRowModel() {
        if (is_null($this->_ReportTypeRowModel))
            $this->_ReportTypeRowModel = new ReportTypeRow();
        return $this->_ReportTypeRowModel;
    }

    public $arrayRowId = array();

    /**
     * Retrieve row_id by criteria_id and report_ticker_type in report_ticker_data_quarter table.
     * @param $criteria_id
     * @param $report_ticker_type
     * @return mixed
     */
    public function getRowId($criteria_id, $report_ticker_type) {
        $key = $this->getKeyByCriteriaAndReportTickerType($criteria_id, $report_ticker_type);

        if (!isset($this->arrayRowId[$key])) {
            $model = $this->getReportTypeRowModel();
            $collection = $model->query();
            $collection->where(array(
                'report_ticker_type' => $report_ticker_type,
                'criteria_id' => $criteria_id));
            $first = $collection->first();
            if (is_null($first))
                $this->arrayRowId[$key] = null;
            else
                $this->arrayRowId[$key] = $first->row_id;
        }

        return $this->arrayRowId[$key];
    }

    private function getKeyByCriteriaAndReportTickerType($criteria_id, $report_ticker_type) {
        return $criteria_id . '|' . $report_ticker_type;
    }

}
