<?php
/**
 * Created by IntelliJ IDEA.
 * User: vjcspy
 * Date: 11/16/15
 * Time: 3:34 PM
 */

namespace App\RoboStock\Report\Model;


use App\ReportTypeRow;
use App\ReportTypeSize;
use Mockery\CountValidator\Exception;

/**
 * Class ReportTypeSizeModelHelper
 * @package App\RoboStock\Report\Model
 */
class ReportTypeSizeHelper {

    public function getReportType(Array $rowIdData, $reportType, $newTypeName = null) {
        $size = count($rowIdData);

        $reportTypeRowHelper = $this->getReportTypeRowModelHelper();

        // Check size:
        $checkSize = $this->checkSize($size, $reportType);

        if ($checkSize['existed'] == true) {

            $arrayRowId = array();
            foreach ($rowIdData as $row) {
                $arrayRowId[] = $row['row_id'];
            }

            $resultCheckRowId = $reportTypeRowHelper->checkRow($arrayRowId, $checkSize['reportTickerType'], $reportType);

            foreach ($checkSize['reportTickerType'] as $reportTickerType) {
                if ($resultCheckRowId[$reportTickerType]['isMatch'] == true)
                    return $reportTickerType;
            }
        }
        // if go here. it's error
//            throw new Exception('Not found any report type!');

        // Create new:
        if (is_null($newTypeName))
            $newTypeName = $reportType . '_' . microtime(true);

        $this->createNewTypeSize($size, $reportType, $newTypeName);

        $reportTypeRowHelper->createNewRowType($rowIdData, $reportType, $newTypeName);

        return $newTypeName;
    }

    private $_ReportTypeRowModelHelperModel;

    /**
     * @return ReportTypeRowHelper
     */
    private function getReportTypeRowModelHelper() {
        if (is_null($this->_ReportTypeRowModelHelperModel))
            $this->_ReportTypeRowModelHelperModel = new ReportTypeRowHelper();
        return $this->_ReportTypeRowModelHelperModel;
    }

    /**
     * @param $size
     * @param $reportType
     * @return array
     */
    public function checkSize($size, $reportType) {
        // check trong bang report_type_size
        $existed = false;

        // Kiểu loại là một array bởi vì có thể nhiều loại cùng chung một size
        $reportTickerType = array();

        // Nếu lưu cache size các loại ở đây thì không hợp lý bởi vì lúc save có thể thêm loại mới
        $reportTypeSizeModel = $this->getReportTypeSizeModel();
        $allReportSize = $reportTypeSizeModel->query()->where('report_type', $reportType)->get();
        foreach ($allReportSize as $report) {
            if ($report->size == $size) {
                $existed = true;
                $reportTickerType[] = $report->report_ticker_type;
            }
        }
        return array(
            'existed' => $existed,
            'reportTickerType' => $reportTickerType,
            'reportType' => $reportType);
    }

    /**
     * @param $size
     * @param $reportType
     * @param $newTypeName
     * @return mixed
     */
    public function createNewTypeSize($size, $reportType, $newTypeName) {

        //table: report_type_size
        $modelTypeSize = $this->getReportTypeSizeModel();
        $modelTypeSize->create(array(
            'report_type' => $reportType,
            'report_ticker_type' => $newTypeName,
            'size' => $size
        ));
        return $newTypeName;
    }

    private $_ReportTypeSizeModel;

    /**
     * @return ReportTypeSize
     */
    private function getReportTypeSizeModel() {
        if (is_null($this->_ReportTypeSizeModel))
            $this->_ReportTypeSizeModel = new ReportTypeSize();
        return $this->_ReportTypeSizeModel;
    }

    private $_ReportTypeRowModel;

    /**
     * @return ReportTypeRow
     */
    private function getReportTypeRowModel() {
        if (is_null($this->_ReportTypeRowModel))
            $this->_ReportTypeRowModel = new ReportTypeRow();
        return $this->_ReportTypeRowModel;
    }
}
