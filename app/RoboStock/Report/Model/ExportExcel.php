<?php
/**
 * Created by mr.vjcspy@gmail.com/khoild@smartosc.com.
 * Date: 1/7/16
 * Time: 2:52 PM
 */

namespace App\RoboStock\Report\Model;


use Maatwebsite\Excel\Facades\Excel;

class ExportExcel {
    private $_dataReports;
    private $_dataTime;
    public function initFileDownload($dataReport){
        $fileName = 'report_' . $dataReport['ticker'].'_'.$dataReport['reportType'];
        $this->_dataTime = array(
            'time1'=>$dataReport['time1'],
            'time2'=>$dataReport['time2']
        );
        $this->_dataReports = $dataReport['dataReports'];

        Excel::create($fileName, function ($excel) {

            $excel->sheet('Filter', function ($sheet) {

                /*add header*/
                $sheet->appendRow(array(
                    'criteria',$this->_dataTime['time1'],$this->_dataTime['time2'],'relative','percent'
                ));

                foreach ($this->_dataReports as $item) {
                    $value0 = str_replace(',','',$item['t1']);
                    $value1 = str_replace(',','',$item['t2']);
                    $result = str_replace(',','',$item['result']);
                    $percent = round($item['percent'],2);
                    $sheet->appendRow(array(
                        $item['criteriaLabel'],$value0,$value1,$result,$percent
                    ));
                }
            });

        })->store('xls');

        return $fileName;
    }
}
