<?php
/**
 * Created by IntelliJ IDEA.
 * User: vjcspy
 * Date: 11/16/15
 * Time: 9:29 PM
 */

namespace App\RoboStock\Report\Model;


use App\ReportTickerDataQuarter;

class ReportTickerDataQuarterHelper {

    /**MAINFUNCTION
     * Lấy report_ticker_type của một cổ phiếu/kiểu report
     * @param $ticker
     * @param $reportType
     * @return mixed|null
     */
    public function getReportTickerType($ticker, $reportType) {
        $reportDataQuarterModel = $this->getReportTickerDataQuarterModel();
        $fist = $reportDataQuarterModel->query()->where(array(
            'report_type' => $reportType,
            'ticker' => $ticker))->first();
        if (is_null($fist))
            return null;
        else
            return $fist->report_ticker_type;
    }

    /**MAINFUNCTION
     *Lấy data theo cổ phiểu/kiểu/thời gian
     * Trả về null nếu không tìm thấy
     * @param $ticker
     * @param $reportType
     * @param $time
     * @return mixed|null
     */
    public function getDataReportTickerQuarter($ticker, $reportType, $time) {
        $columnQuarter = $this->converToQuarterColumn($time);
        $modelQuarter = $this->getReportTickerDataQuarterModel();
        $collection = $modelQuarter->query()->where(array(
            'ticker' => $ticker,
            'report_type' => $reportType,
        ))->whereIn('quarter', $columnQuarter)->get();

        if (is_null($collection))
            return null;
        else {
            $data = array();
            foreach ($collection as $item) {
                $data[] = $item->data;
            }
            return $this->sumQuarter($data);
        }
    }

    private function sumQuarter($data) {
        $result = array();
        foreach ($data as $item) {
            $arrayItems = json_decode($item, true);
            foreach ($arrayItems as $key => $value) {
                if (isset($result[$key])) {
                    $value = str_replace(',', '', $value);
                    $result[$key] += floatval($value);
                } else {
                    $value = str_replace(',', '', $value);
                    $result[$key] = floatval($value);;
                }
            }
        }
        return json_encode($result);
    }

    /**MAINFUNCTION
     *  Lấy array của các cổ phiếu đã tồn tại
     * @return array
     */
    public function getArrayTickerExisted($reportType) {
        $model = $this->getReportTickerDataQuarterModel();
        $collection = $model->query()->where('report_type',$reportType)->groupBy('ticker')->get();
        $arrayTickerExisted = array();
        foreach ($collection as $item) {
            $arrayTickerExisted[] = $item->ticker;
        }
        return $arrayTickerExisted;
    }

    public function getArrayTimeQuarter($ticker, $reportType) {
        $model = $this->getReportTickerDataQuarterModel();
        $collection = $model->query()->where(array(
            'ticker' => $ticker,
            'report_type' => $reportType))->get();

        $arrayQuarter = array();

        foreach ($collection as $data) {
            $arrayQuarter[] = $data->quarter;
        }
        return $arrayQuarter;
    }

    private function converToQuarterColumn($time) {
        $arrayTimes = $time['times'];
        $data = array();
        foreach ($arrayTimes as $arrayTime) {
            $data[] = $arrayTime['q'] . '-' . $arrayTime['y'];
        }
        return $data;
    }

    public function saveToDbQuarterTable($ticker, $reportType, $quarter, $data, $reportTickerType) {
        $model = $this->getReportTickerDataQuarterModel();
        $report = $model->firstOrNew(array(
            'ticker' => $ticker,
            'report_type' => $reportType,
            'quarter' => $quarter['q'] . '-' . $quarter['y']
        ));
        $report->data = json_encode($data);
        $report->report_ticker_type = $reportTickerType;
        $report->save();
    }

    private $_ReportTickerDataQuarterModel;

    /**
     * @return ReportTickerDataQuarter
     */
    private function getReportTickerDataQuarterModel() {
        if (is_null($this->_ReportTickerDataQuarterModel))
            $this->_ReportTickerDataQuarterModel = new ReportTickerDataQuarter();
        return $this->_ReportTickerDataQuarterModel;
    }


}
