<?php
/**
 * Created by IntelliJ IDEA.
 * User: vjcspy
 * Date: 12/14/15
 * Time: 4:55 PM
 */

namespace App\RoboStock\Report\Model;


use App\ReportTickerDataYear;

class ReportTickerDataYearHelper {

    public function saveToDbYearTable($ticker, $reportType, $year, $data, $reportTickerType) {
        $model = $this->getReportTickerDataYearModel();
        $report = $model->firstOrNew(array(
            'ticker' => $ticker,
            'report_type' => $reportType,
            'year' => $year
        ));
        $report->data = json_encode($data);
        $report->report_ticker_type = $reportTickerType;
        $report->save();
    }

    public function getDataReportTickerYear($ticker, $reportType, $checkTime1) {
        $modelYear = $this->getReportTickerDataYearModel();
        $collection = $modelYear->query()->where(array(
            'ticker' => $ticker,
            'report_type' => $reportType,
            'year' => $checkTime1['y']
        ))->first();

        if (is_null($collection))
            return null;
        else {
            $arrayData = json_decode($collection->data, true);
            $data = [];
            foreach ($arrayData as $key => $item) {
                $data[$key] = str_replace(',', '', $item);
            }
        }
        return json_encode($data);
    }

    private $_ReportTickerDataYearModel;

    /**
     * @return ReportTickerDataYear
     */
    private function getReportTickerDataYearModel() {
        if (is_null($this->_ReportTickerDataYearModel))
            $this->_ReportTickerDataYearModel = new ReportTickerDataYear();
        return $this->_ReportTickerDataYearModel;
    }

    /**MAINFUNCTION
     *  Lấy array của các cổ phiếu đã tồn tại
     * @return array
     */
    public function getArrayTickerExisted($reportType) {
        $model = $this->getReportTickerDataYearModel();
        $collection = $model->query()->where('report_type', $reportType)->groupBy('ticker')->get();
        $arrayTickerExisted = array();
        foreach ($collection as $item) {
            $arrayTickerExisted[] = $item->ticker;
        }
        return $arrayTickerExisted;
    }

    private $_cacheReportTickerType = array();

    public function getReportTickerType($ticker, $reportType) {
        if (!isset($this->_cacheReportTickerType[$this->getKeyByArray(array($ticker, $reportType))])) {
            $reportDataQuarterModel = $this->getReportTickerDataYearModel();
            $fist = $reportDataQuarterModel->query()->where(array(
                'report_type' => $reportType,
                'ticker' => $ticker))->first();
            if (is_null($fist))
                $this->_cacheReportTickerType[$this->getKeyByArray(array($ticker, $reportType))] = null;
            else
                $this->_cacheReportTickerType[$this->getKeyByArray(array($ticker, $reportType))] = $fist->report_ticker_type;
        }
        return $this->_cacheReportTickerType[$this->getKeyByArray(array($ticker, $reportType))];
    }

    private function getKeyByArray($data) {
        $key = '';
        foreach ($data as $item) {
            $key .= $item;
        }
        return $key;
    }
}
