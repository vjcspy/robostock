<?php
/**
 * Created by IntelliJ IDEA.
 * User: vjcspy
 * Date: 9/27/15
 * Time: 2:27 PM
 */

namespace App\RoboStock\Model\WorkWithFile;


class WorkWithFileHelper {
    private $_fileName = 'newFile';
    private $_file;

    /**
     * WorkWithFileHelper constructor.
     * @param string $_fileName
     */
    public function __construct($_fileName = null) {
        if ($_fileName == null)
            $this->_fileName = $this->_fileName . md5(microtime());
        else
            $this->_fileName = $_fileName;
    }

    public function createFile() {
        $this->_file = fopen($this->_fileName, "w") or die("Unable to open/create file!");
        return getcwd() . "\n";
    }

    public function writeLine($string) {
        if (!is_null($this->_file)) {
            $string = $string . "\n";
            fwrite($this->_file, $string);
            return true;
        } else
            return false;
    }

    public function closeFile() {
        if (!is_null($this->_file)) {
            fclose($this->_file);
            return true;
        } else
            return false;
    }
}
