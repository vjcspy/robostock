<?php
/**
 * Created by IntelliJ IDEA.
 * User: vjcspy
 * Date: 9/22/15
 * Time: 10:09 PM
 */

namespace App\RoboStock\Model\TickerLastDay;


use App\RoboStock\Model\TickerUpto\ImportFromBoardPrice;
use App\RoboStock\Model\VariableDatabase\VariableDatabaseHelper;
use App\RoboStock\Model\WorkWithTime\WorkWithTime;
use App\TickerLastDay;
use App\TickerUpto;

class TickerLastDayHelper {


    /**
     * MAINFUNCTION
     * TODO: init ticker lastday
     * @return bool|int|string
     */
    public function initTickerLastDay($checkExchange = true, $lastdate = null) {
        $importFromBoard = new ImportFromBoardPrice();
        $allTickerInday = $importFromBoard->getAllDataTickerCurrentDayFromVietStock(false, false);
        $isExchange = $importFromBoard->isExchangeInDayNew($allTickerInday);
        if (!$isExchange['status'] && $checkExchange)
            return 'Not exchange in day';
        $model = $this->getTickerUptoModel();
        $tickerLastDayModel = $this->getTickerLastDayModel();
        $flag = true;
        if (is_null($lastdate))
            $date = WorkWithTime::last_day();
        else
            $date = $lastdate;
        do {
            if ($model->query()->where('date', $date)->count() > 0) {
//                if ($this->checkExisted($date))
//                    return true;
                /*DO:empty table*/
                $tickerLastDayModel->truncate();
                $tickerLastDayModel = $this->getTickerLastDayModel();
                /*DO: update variable lastDay*/
                VariableDatabaseHelper::updateVariable('lastDay', $date);
                $allTicker = $model->query()->where('date', $date)->get();
                foreach ($allTicker as $ticker) {
                    $currentTicker = array(
                        'ticker' => $ticker->ticker,
                        'date' => $ticker->date,
                        'open' => $ticker->open,
                        'high' => $ticker->high,
                        'low' => $ticker->low,
                        'close' => $ticker->close,
                        'average' => $ticker->average,
                        'vol' => $ticker->vol,
                        'hsdc' => $ticker->hsdc,
                    );
                    $tickerLastDayModel->create($currentTicker);
                }
                break;
            }
            $date = WorkWithTime::last_day($date);
        } while ($flag);
        VariableDatabaseHelper::updateVariable('data_lastDay', $date);
        return $date;
    }

    private function checkExisted($date) {
        if ($model = $this->getTickerLastDayModel()->query()->where('date', $date)->count() > 0)
            return true;
        else
            return false;
    }

    private $_TickerLastDayModel;

    /**
     * @return TickerLastDay
     */
    private function getTickerLastDayModel() {
        if (is_null($this->_TickerLastDayModel))
            $this->_TickerLastDayModel = new TickerLastDay();
        return $this->_TickerLastDayModel;
    }


    private $_TickerUptoModel;

    /**
     * @return TickerUpto
     */
    private function getTickerUptoModel() {
        if (is_null($this->_TickerUptoModel))
            $this->_TickerUptoModel = new TickerUpto();
        return $this->_TickerUptoModel;
    }
}
