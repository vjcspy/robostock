<?php
/**
 * Created by IntelliJ IDEA.
 * User: vjcspy
 * Date: 9/19/15
 * Time: 12:26 AM
 */

namespace App\RoboStock\Model\TickerInDay;


use App\RoboStock\Model\Variable;
use App\RoboStock\Model\WorkWithTime\WorkWithTime;
use App\TickerInDay;

class TickerInDayHelper {


    private $_arrayAllTickerNotExchange;

    /**
     * MAINFUNCTION
     * TODO: get ALL TICKER NOT EXCHANGE IN DAY
     * @return mixed
     */
    public function getTickerNotExchangeInDay() {
        if (is_null($this->_arrayAllTickerNotExchange)) {
            $model = new TickerInDay();
            /*DO:check has existed data inday*/
            if ($model->query()->where('date', WorkWithTime::getCurrentDate())->count() > Variable::$_numOfTickerCheckIsExchangeInDay) {
                $model = new TickerInDay();
                $model->query()->where('date', '<>', WorkWithTime::getCurrentDate());
                if ($model->count() > 0) {
                    foreach ($model as $ticker) {
                        $this->_arrayAllTickerNotExchange[$ticker->ticker] = $ticker->ticker;
                    }
                }
            }
        } else {
            return false;
        }
        return $this->_arrayAllTickerNotExchange;
    }
}
