<?php
/**
 * Created by IntelliJ IDEA.
 * User: vjcspy
 * Date: 9/20/15
 * Time: 9:07 PM
 */

namespace App\RoboStock\Model\Email;


use Illuminate\Support\Facades\Mail;

class EmailHelper {
    public function test() {

        /*Mail::send('pages.aboutme', array('name' => 'abc', 'age' => '21'), function ($message) {

            $message->to('mr.vjcspy@gmai.com')->subject('abcd email');

        });*/

        Mail::send('pages.aboutme', array('name' => 'abc', 'age' => '21'), function ($message) {
            $message->to('mr.vjcspy@gmail.com', 'Jon Doe')->subject('Welcome to the Laravel 4 Auth App!');
        });
        return "Your email has been sent successfully";
    }
}
