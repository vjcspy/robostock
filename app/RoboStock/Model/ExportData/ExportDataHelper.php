<?php
/**
 * Created by IntelliJ IDEA.
 * User: vjcspy
 * Date: 9/27/15
 * Time: 2:08 PM
 */

namespace App\RoboStock\Model\ExportData;

    /*<TICKER>,<PER>,<DTYYYYMMDD>,<TIME>,<OPEN>,<HIGH>,<LOW>,<CLOSE>,<VOL>,<OPENINT>*/ /*META*/
    /*<Ticker>,<DTYYYYMMDD>,<Open>,<High>,<Low>,<Close>,<Volume>*/ /*AMI*/

use App\RoboStock\Model\WorkWithFile\WorkWithFileHelper;
use App\RoboStock\Model\WorkWithTime\WorkWithTime;
use App\TickerInDay;
use App\TickerUpto;

class ExportDataHelper {
    public $_file;
    public $_fileName;
    private $_isGetedCurrentDate = false;

    public function ImportFile($table = 'upto', $ticker = null, $from, $to = null, Array $column = null, $format = 'meta') {
        if (is_null($from))
            return false;
        $_isGetCurrentDate = false;

        if ($from == WorkWithTime::getCurrentDate() || $to == WorkWithTime::getCurrentDate())
            $_isGetCurrentDate = true;
        if ($format == 'ami') {
            $model = $this->getTickerUptoModel();
            /*TODO: get data from ticker_upto*/
            if ($table == 'upto') {
                $this->_fileName = $format . json_encode($ticker) . $from . $to . '.txt';
                $this->_file = new WorkWithFileHelper($this->_fileName);
                $this->_file->createFile();
                $this->_file->writeLine("<Ticker>,<DTYYYYMMDD>,<Open>,<High>,<Low>,<Close>,<Volume>");
                if ($ticker == null) {
                    /*DO: Lay ca bang*/
                    if ($to == null) {
                        /*DO: Lay duy nhat From*/
                        $allTickerModel = $model->query()->where('date', $from)->orderBy('ticker', 'asc');
                    } else {
                        /*DO: lay khoang thoi gian*/
                        $allTickerModel = $model->query()->where('date', '>=', $from)->where('date', '<=', $to)->orderBy('ticker', 'asc');
                    }
                } else {
                    /*DO: Lay 1 Ticker*/
                    if ($to == null) {
                        /*DO: Lay duy nhat From*/
                        $allTickerModel = $model->query()->where('date', $from)->whereIn('ticker', $ticker)->orderBy('date', 'asc');
                    } else {
                        /*DO: lay khoang thoi gian*/
                        $allTickerModel = $model->query()->where('date', '>=', $from)->where('date', '<=', $to)->whereIn('ticker', $ticker)->orderBy('date', 'asc');
                    }
                }
                $allTickerModel->chunk(200, function ($tickers) {
                    foreach ($tickers as $ticker) {
                        if ($ticker->date == WorkWithTime::getCurrentDate())
                            $this->_isGetedCurrentDate = true;
                        $currentRow = $ticker->ticker . "," . str_replace('-', '', $ticker->date) . "," . $ticker->open . "," . $ticker->high . "," . $ticker->low . "," . $ticker->close . "," . $ticker->vol;
                        $this->_file->writeLine($currentRow);
                    }
                });

                if ($_isGetCurrentDate == true && $this->_isGetedCurrentDate == false) {
                    $model = $this->getTickerIndayModel();
                    if (!is_null($ticker)) {
                        $tickerInday = $model->query()->whereIn('ticker', $ticker)->get();
                    } else
                        $tickerInday = $model->get();
                    foreach ($tickerInday as $ticker) {
                        $currentRow = $ticker->ticker . "," . str_replace('-', '', $ticker->date) . "," . $ticker->open . "," . $ticker->high . "," . $ticker->low . "," . $ticker->close . "," . $ticker->vol;
                        $this->_file->writeLine($currentRow);
                    }
                }

                $this->_file->closeFile();
                return $this->_fileName;
            }
        }
        if ($format == 'meta') {
            $model = $this->getTickerUptoModel();
            /*TODO: get data from ticker_upto*/
            if ($table == 'upto') {
                $this->_fileName = $format . json_encode($ticker) . $from . $to . '.txt';
                $this->_file = new WorkWithFileHelper($this->_fileName);
                $this->_file->createFile();
                $this->_file->writeLine("<TICKER>,<PER>,<DTYYYYMMDD>,<TIME>,<OPEN>,<HIGH>,<LOW>,<CLOSE>,<VOL>,<OPENINT>");
                if ($ticker == null) {
                    /*DO: Lay ca bang*/
                    if ($to == null) {
                        /*DO: Lay duy nhat From*/
                        $allTickerModel = $model->query()->where('date', $from)->orderBy('ticker', 'asc');
                    } else {
                        /*DO: lay khoang thoi gian*/
                        $allTickerModel = $model->query()->where('date', '>=', $from)->where('date', '<=', $to)->orderBy('ticker', 'asc');
                    }
                } else {
                    /*DO: Lay 1 Ticker*/
                    if ($to == null) {
                        /*DO: Lay duy nhat From*/
                        $allTickerModel = $model->query()->where('date', $from)->whereIn('ticker', $ticker)->orderBy('date', 'asc');
                    } else {
                        /*DO: lay khoang thoi gian*/
                        $allTickerModel = $model->query()->where('date', '>=', $from)->where('date', '<=', $to)->whereIn('ticker', $ticker)->orderBy('date', 'asc');
                    }

                }
                $allTickerModel->chunk(200, function ($tickers) {
                    foreach ($tickers as $ticker) {
                        if ($ticker->date == WorkWithTime::getCurrentDate())
                            $this->_isGetedCurrentDate = true;
                        $currentRow = $ticker->ticker . "," . "D" . "," . str_replace('-', '', $ticker->date) . "," . "000000" . "," . $ticker->open . "," . $ticker->high . "," . $ticker->low . "," . $ticker->close . "," . $ticker->vol . "," . "0";
                        $this->_file->writeLine($currentRow);
                    }
                });

                if ($_isGetCurrentDate == true && $this->_isGetedCurrentDate == false) {
                    $model = $this->getTickerIndayModel();
                    if (!is_null($ticker)) {
                        $tickerInday = $model->query()->whereIn('ticker', $ticker)->get();
                    } else
                        $tickerInday = $model->get();
                    foreach ($tickerInday as $ticker) {
                        $currentRow = $ticker->ticker . "," . "D" . "," . str_replace('-', '', $ticker->date) . "," . "000000" . "," . $ticker->open . "," . $ticker->high . "," . $ticker->low . "," . $ticker->close . "," . $ticker->vol . "," . "0";
                        $this->_file->writeLine($currentRow);
                    }
                }

                $this->_file->closeFile();
                return $this->_fileName;
            }
        }
    }

    private $_TickerUptoModel;

    /**
     * @return TickerUpto
     */
    private function getTickerUptoModel() {
        if (is_null($this->_TickerUptoModel))
            $this->_TickerUptoModel = new TickerUpto();
        return $this->_TickerUptoModel;
    }

    private $_TickerIndayModel;

    /**
     * @return TickerUpto
     */
    private function getTickerIndayModel() {
        if (is_null($this->_TickerIndayModel))
            $this->_TickerIndayModel = new TickerInDay();
        return $this->_TickerIndayModel;
    }
}
