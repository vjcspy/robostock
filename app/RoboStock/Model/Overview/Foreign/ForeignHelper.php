<?php
/**
 * Created by IntelliJ IDEA.
 * User: vjcspy
 * Date: 9/22/15
 * Time: 5:01 PM
 */

namespace App\RoboStock\Model\Overview\Foreign;


use App\RoboStock\Model\WorkWithTime\WorkWithTime;
use App\TickerBoard;
use App\TickerForeign;
use App\TickerInDay;
use App\TickerUpto;

class ForeignHelper {
    private $_allTickerBoard;
    private $_dataForeign;
    private $_lastDateCalculated;
    private $_notFoundClose;

    /**
     * MAINFUNCTION
     * TODO: get Data Foreign Overview
     * @param $board
     * @param $date
     * @return mixed
     */
    public function getDataForeign($date, $mob, $board) {
        if (is_null($this->_dataForeign)) {
            $this->calData($date, $mob, $board);
        }
        return array(
            'lastDate' => $this->_lastDateCalculated,
            'dataForeign' => $this->_dataForeign);
    }

    private $_arrayAllForeign;

    /*
     * mob =1 :mua, mob =2: ban
     * */
    private function calData($date, $mob, $board) {

        /*DO: get All board*/
        $allTickerBoardModel = $this->getTickerBoardModel()->all();
        $this->_allTickerBoard = array();
        foreach ($allTickerBoardModel as $k) {
            $this->_allTickerBoard[$k->ticker] = $k->board;
        }


        /*DO: cal current date*/
        $model = $this->getTickerForeignModel();
        if ($date == WorkWithTime::getCurrentDate('Y-m-d') || $date == WorkWithTime::getCurrentDate()) {
            $allForeign = $this->getTickerIndayModel()->query()->where('date', $date)->where(function ($query) {
                $query->where('nnb', '>', '0');
                $query->orWhere('nnm', '>', 0);
            })->get();
        } else
            $allForeign = $model->query()->where('date', $date)->get();
        $arrayAllForeign = array();
        $count = 0;
        foreach ($allForeign as $k) {
            if ($this->_allTickerBoard[$k->ticker] != $board && $board != 'all')
                continue;
            if ($count > 1900)
                break;
            $count += 1;
            $this->_arrayAllForeign[$k->ticker] = array(
                'nnb' => $k->nnb,
                'nnm' => $k->nnm
            );
        }

        /*DO: get ALl ticker in current Date*/
        $this->getDataTickerCurrent($date, $arrayAllForeign);
        /*DO: get All ticker in last date*/
        $this->getDataTickerLastDate($date, $arrayAllForeign);
        /*DO: get All foreign last date*/
//        $this->getDataForeignLastDate($date);


        $this->_dataForeign = array();
        foreach ($this->_arrayAllForeign as $k => $v) {
//            if (isset($this->_allForeignLastDate[$k->ticker]['nnb']) && isset($this->_allTickerLastDate[$k->ticker]))
//                $gtb1 = $this->_allForeignLastDate[$k->ticker]['nnb'] * $this->_allTickerLastDate[$k->ticker]['average'];
//            else
//                continue;
//            if (isset($this->_allForeignLastDate[$k->ticker]['nnm']) && isset($this->_allTickerLastDate[$k->ticker]))
//                $gtm1 = $this->_allForeignLastDate[$k->ticker]['nnm'] * $this->_allTickerLastDate[$k->ticker]['average'];
//            else
//                continue;
            $gtb = $v['nnb'] * $this->_allTickerCurrentDate[$k]['average'];
            $gtm = $v['nnm'] * $this->_allTickerCurrentDate[$k]['average'];
            if (!isset($this->_allTickerLastDate[$k]['close']) || ($gtb == 0 && $gtm == 0) || $this->_allTickerCurrentDate[$k]['vol'] == 0) {
                $this->_notFoundClose[] = $k;
                continue;
            }

            $this->_dataForeign[$k] = array(
                'ticker' => $k,
                'nnb' => $v['nnb'],
                'nnm' => $v['nnm'],
                'gtb' => $gtb,
                'gtm' => $gtm,
                'percent' => round($this->_allTickerCurrentDate[$k]['close'] / $this->_allTickerLastDate[$k]['close'] - 1, 4) * 100,
                'board' => $this->_allTickerBoard[$k],
                'pB' => round($v['nnb'] / ($this->_allTickerCurrentDate[$k]['vol']), 4) * 100,
                'pM' => round($v['nnm'] / ($this->_allTickerCurrentDate[$k]['vol']), 4) * 100,
            );
        }
    }

    private $_allForeignLastDate;

    private function getDataForeignLastDate($date) {
        if (is_null($this->_allForeignLastDate)) {
            $this->_allForeignLastDate = array();
            $flag = true;
            if (is_null($this->_lastDateCalculated))
                $lastDate = $date;
            else {
                $lastDate = $this->_lastDateCalculated;
                $flag = false;
            }
            do {
                if ($flag)
                    $lastDate = WorkWithTime::last_day($lastDate);
                $model = $this->getTickerForeignModel();
                if ($model->query()->where('date', $lastDate)->count() > 0) {
                    $model->query()->where('date', $lastDate)->chunk(200, function ($tickers) {
                        foreach ($tickers as $ticker) {
                            $this->_allForeignLastDate[$ticker->ticker] = array(
                                'nnm' => $ticker->nnm,
                                'nnb' => $ticker->nnb,
                            );
                        }
                    });
                    $this->_lastDateCalculated = $lastDate;
                    $flag = false;
                }
            } while ($flag);
        }
        return $this->_allForeignLastDate;
    }

    private $_allTickerCurrentDate;

    private function getDataTickerCurrent($date, $arrayTicker = null) {
        if (is_null($this->_allTickerCurrentDate)) {
            if ($date == WorkWithTime::getCurrentDate('Y-m-d'))
                $model = $this->getTickerIndayModel();
            else
                $model = $this->getTickerUptoModel();
            $this->_allTickerCurrentDate = array();
            $model->query()->where('date', $date)->where(function ($query) {
                foreach ($this->_arrayAllForeign as $k => $v) {
                    $query->orWhere('ticker', '=', $k);
                }
            })->chunk(200, function ($tickers) {
                foreach ($tickers as $ticker) {
                    $this->_allTickerCurrentDate[$ticker->ticker] = array(
                        'average' => $ticker->average,
                        'close' => $ticker->close,
                        'vol' => $ticker->vol,
                    );
                }
            });
        }
        return $this->_allTickerCurrentDate;
    }

    private $_allTickerLastDate;

    private
    function getDataTickerLastDate($date, $arrayTicker = null) {
        if (is_null($this->_allTickerLastDate)) {
            $flag = true;
            if (is_null($this->_lastDateCalculated))
                $lastDate = $date;
            else {
                $lastDate = $this->_lastDateCalculated;
                $flag = false;
            }
            $this->_allTickerLastDate = array();
            do {
                if ($flag)
                    $lastDate = WorkWithTime::last_day($lastDate);
                $model = $this->getTickerUptoModel();
                if ($model->query()->where('date', $lastDate)->count() > 0) {
                    $model->query()->where('date', $lastDate)->where(function ($query) {
                        foreach ($this->_arrayAllForeign as $k => $v) {
                            $query->orWhere('ticker', '=', $k);
                        }
                    })->chunk(200, function ($tickers) {
                        foreach ($tickers as $ticker) {
                            $this->_allTickerLastDate[$ticker->ticker] = array(
//                                'average' => $ticker->average,
'close' => $ticker->close,
//                                'vol' => $ticker->vol,
                            );
                        }
                    });
                    $this->_lastDateCalculated = $lastDate;
                    $flag = false;
                }
            } while ($flag);
        }
        return $this->_allTickerLastDate;
    }

    private
        $_TickerUptoModel;

    /**
     * @return TickerUpto
     */
    private
    function getTickerUptoModel() {
        if (is_null($this->_TickerUptoModel))
            $this->_TickerUptoModel = new TickerUpto();
        return $this->_TickerUptoModel;
    }

    private
        $_TickerBoardModel;

    /**
     * @return TickerBoard
     */
    private
    function getTickerBoardModel() {
        if (is_null($this->_TickerBoardModel))
            $this->_TickerBoardModel = new TickerBoard();
        return $this->_TickerBoardModel;
    }

    private
        $_TickerForeignModel;

    /**
     * @return TickerForeign
     */
    private
    function getTickerForeignModel() {
        if (is_null($this->_TickerForeignModel))
            $this->_TickerForeignModel = new TickerForeign();
        return $this->_TickerForeignModel;
    }

    private $_TickerIndayModel;

    /**
     * @return TickerInday
     */
    private function getTickerIndayModel() {
        if (is_null($this->_TickerIndayModel))
            $this->_TickerIndayModel = new TickerInDay();
        return $this->_TickerIndayModel;
    }
}
