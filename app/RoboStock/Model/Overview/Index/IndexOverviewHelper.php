<?php
/**
 * Created by IntelliJ IDEA.
 * User: vjcspy
 * Date: 9/25/15
 * Time: 11:47 PM
 */

namespace App\RoboStock\Model\Overview\Index;


use App\RoboStock\Model\WorkWithTime\WorkWithTime;
use App\TickerBoard;
use App\TickerForeign;
use App\TickerInDay;
use App\TickerUpto;

class IndexOverviewHelper {

    public function getDataCurrentDate($date) {

        /*DO: current date*/
        if ($date == WorkWithTime::getCurrentDate() || $date == WorkWithTime::getCurrentDate('Y-m-d')) {
            $allTicker = $this->getTickerIndayModel()->query()->where('date', $date)->get();
        } else
            $allTicker = $this->getTickerUptoModel()->query()->where('date', $date)->get();
        $arrayTickerIndate = array();
        $arrayTickerForeign = array();
        foreach ($allTicker as $ticker) {
            $arrayTickerIndate[$ticker->ticker] = array(
                'close' => $ticker->close
            );

            /*DO: Foreign*/
            if ($date == WorkWithTime::getCurrentDate() || $date == WorkWithTime::getCurrentDate('Y-m-d')) {
                $arrayTickerForeign[$ticker->ticker] = array(
                    'nnm' => $ticker->nnm,
                    'nnb' => $ticker->nnb,
                );
            }
        }


        /*DO: Foreign*/
        if (!($date == WorkWithTime::getCurrentDate() || $date == WorkWithTime::getCurrentDate('Y-m-d'))) {
            $ForeignModel = $this->getTickerForeignModel();
            $all = $ForeignModel->query()->where('date', $date)->get();
            foreach ($all as $ticker) {
                $arrayTickerForeign[$ticker->ticker] = array(
                    'nnm' => $ticker->nnm,
                    'nnb' => $ticker->nnb,
                );
            }
        }


        /*Last date*/
        $arrayTickerLastDate = array();
        $lastDate = $date;
        do {
            $lastDate = WorkWithTime::last_day($lastDate);
            $model = $this->getTickerUptoModel()->query()->where('date', $lastDate);
            if ($model->count() > 0) {
                $allTickerLastDay = $model->get();
                foreach ($allTickerLastDay as $ticker) {
                    $arrayTickerLastDate[$ticker->ticker] = array(
                        'close' => $ticker->close,
                    );
                }
                break;
            }
        } while (true);

        /*get allTickerBoard*/
        $arrayTickerBoard = array();
        $TickerBoardModel = $this->getTickerBoardModel()->all();
        foreach ($TickerBoardModel as $ticker) {
            $arrayTickerBoard[$ticker->ticker] = array(
                'ticker' => $ticker->ticker,
                'board' => $ticker->board,
            );
        }


        /*DO: tinh bao nhieu ma tang bao nhieu ma giam*/
        $upVni = 0;
        $downVni = 0;
        $holdVni = 0;

        $upHax = 0;
        $downHax = 0;
        $holdHax = 0;

        foreach ($arrayTickerIndate as $k => $v) {
            if ($v['close'] > $arrayTickerLastDate[$k]['close']) {
                if ($arrayTickerBoard[$k]['board'] == 'hose')
                    $upVni += 1;
                elseif ($arrayTickerBoard[$k]['board'] == 'hax') {
                    $upHax += 1;
                }
                continue;
            }
            if ($v['close'] < $arrayTickerLastDate[$k]['close']) {
                if ($arrayTickerBoard[$k]['board'] == 'hose')
                    $downVni += 1;
                elseif ($arrayTickerBoard[$k]['board'] == 'hax') {
                    $downHax += 1;
                }
                continue;
            }
            if ($v['close'] == $arrayTickerLastDate[$k]['close']) {
                if ($arrayTickerBoard[$k]['board'] == 'hose')
                    $holdVni += 1;
                elseif ($arrayTickerBoard[$k]['board'] == 'hax') {
                    $holdHax += 1;
                }
                continue;
            }
        }

        /*DO: Tinh gtNm, NNB trong ngay*/
        $nnmVni = 0;
        $nnbVni = 0;
        $nnbHax = 0;
        $nnmHax = 0;
        foreach ($arrayTickerForeign as $k => $ticker) {
            if ($arrayTickerBoard[$k]['board'] == 'hose') {
                $nnmVni += $ticker['nnm'];
                $nnbVni += $ticker['nnb'];
            } elseif ($arrayTickerBoard[$k]['board'] == 'hax') {
                $nnbHax += $ticker['nnm'];
                $nnmHax += $ticker['nnb'];
            }
        }

        return array(
            'upVni' => $upVni,
            'downVni' => $downVni,
            'holdVni' => $holdVni,
            'upHax' => $upHax,
            'downHax' => $downHax,
            'holdHax' => $holdHax,
            'nnbVni' => $nnbVni,
            'nnmVni' => $nnmVni,
            'nnbHax' => $nnbHax,
            'nnmHax' => $nnmHax,
        );
    }

    private
        $_TickerIndayModel;

    /**
     * @return TickerInday
     */
    private
    function getTickerIndayModel() {
        if (is_null($this->_TickerIndayModel))
            $this->_TickerIndayModel = new TickerInDay();
        return $this->_TickerIndayModel;
    }

    private
        $_TickerUptoModel;

    /**
     * @return TickerUpto
     */
    private
    function getTickerUptoModel() {
        if (is_null($this->_TickerUptoModel))
            $this->_TickerUptoModel = new TickerUpto();
        return $this->_TickerUptoModel;
    }


    private $_TickerBoardModel;

    /**
     * @return TickerBoard
     */
    private function getTickerBoardModel() {
        if (is_null($this->_TickerBoardModel))
            $this->_TickerBoardModel = new TickerBoard();
        return $this->_TickerBoardModel;
    }


    private $_TickerForeignModel;

    /**
     * @return TickerForeign
     */
    private function getTickerForeignModel() {
        if (is_null($this->_TickerForeignModel))
            $this->_TickerForeignModel = new TickerForeign();
        return $this->_TickerForeignModel;
    }
}
