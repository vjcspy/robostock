<?php
/**
 * Created by IntelliJ IDEA.
 * User: vjcspy
 * Date: 11/10/15
 * Time: 6:32 PM
 * Để lấy được toptal value: cần lấy được ngày hiện tại và ngày quá khứ
 */

namespace App\RoboStock\Model\Overview\MarketOverview;

use App\CacheNextDay;
use App\RoboStock\Model\Ticker\TickerHelper;
use App\RoboStock\Model\WorkWithTime\WorkWithTime;
use App\TickerUpto;

class CashFlowHelper {
    private $arrayTickers = array();

    public function initDataCashFlow($date = null) {
        if (is_null($date))
            $date = WorkWithTime::getCurrentDate();
        $tickerUptoModel = $this->getTickerUpToModel();
        $tickerUptoModel = $tickerUptoModel->query()->where('date', $date)->get();
        $totalUp = 0;
        $netUp = 0;
        $netDown = 0;
        $netHold = 0;
        $totalDown = 0;
        $totalHold = 0;
        if ($tickerUptoModel->count() > 0) {
            $arrayTickerLastDay = $this->getTickerHelper()->getArrayTickerByNumOfTrade(1, $date, false, 0);
            foreach ($tickerUptoModel as $t) {
                if (!isset($arrayTickerLastDay[$t->ticker]))
                    continue;
                if ($t->close > $arrayTickerLastDay[$t->ticker][1]['close']) {
                    $totalUp += $t->close * $t->vol;
                    $netUp += ($t->close - $arrayTickerLastDay[$t->ticker][1]['close']) * $t->close * $t->vol / $arrayTickerLastDay[$t->ticker][1]['close'];
                }
                if ($t->close < $arrayTickerLastDay[$t->ticker][1]['close']) {
                    $totalDown += $t->close * $t->vol;
                    $netDown += ($t->close - $arrayTickerLastDay[$t->ticker][1]['close']) * $t->close * $t->vol / $arrayTickerLastDay[$t->ticker][1]['close'];

                }
                if ($t->close == $arrayTickerLastDay[$t->ticker][1]['close']) {
                    $totalHold += $t->close * $t->vol;
                }
            }
            $data = array(
                'totalUp' => $totalUp,
                'totalDown' => $totalDown,
                'totalHold' => $totalHold,
                'netUp' => $netUp,
                'netDown' => $netDown,
                'indexNet' => ($netUp + $netDown) * 100 / ($totalHold + $totalUp + $totalDown)
            );
            $first = $this->getCacheNextDayModel()->firstOrNew(array(
                'date' => $date,
                'entity' => 'cash_flow'
            ));
            $first->data = json_encode($data);
            $first->save();
        }
    }

    private $_CacheNextDayModel;

    /**
     * @return CacheNextDay
     */
    private function getCacheNextDayModel() {
        if (is_null($this->_CacheNextDayModel))
            $this->_CacheNextDayModel = new CacheNextDay();
        return $this->_CacheNextDayModel;
    }

    private $tickerHelper;

    public function getTickerHelper() {
        if (is_null($this->tickerHelper))
            $this->tickerHelper = new TickerHelper();
        return $this->tickerHelper;
    }

    private $_TickerUpToModel;

    /**
     * @return TickerUpTo
     */
    private function getTickerUpToModel() {
        if (is_null($this->_TickerUpToModel))
            $this->_TickerUpToModel = new TickerUpto();
        return $this->_TickerUpToModel;
    }
}
