<?php
/**
 * Created by IntelliJ IDEA.
 * User: vjcspy
 * Date: 9/25/15
 * Time: 10:41 AM
 */

namespace App\RoboStock\Model\Overview\MarketOverview;


use App\OverviewIntraday;
use App\RoboStock\Model\Ticker\TickerHelper;
use App\RoboStock\Model\VariableDatabase\VariableDatabaseHelper;
use App\RoboStock\Model\WorkWithTime\WorkWithTime;
use App\TickerInDay;
use App\TickerUpto;
use App\VariableDatabase;
use Illuminate\Support\Facades\Cache;

class TrendOverviewHelper {


    private $_volumeAndPrice;

    /*Bien de luu all ticker current day va last day*/
    private $_allTickerCurrentDate;
    private $_allTickerVolumeLastDay;

    private $_numOfSession;

    /**
     * TrendOverviewHelper constructor.
     */
    public function __construct() {
        $this->_volumeAndPrice = array();
    }


    private function checkVolAndPrice($date, $numOfSession) {
        $this->_numOfSession = $numOfSession;
        if (!isset($this->_volumeAndPrice[$numOfSession])) {
            if ($date == WorkWithTime::getCurrentDate('Y-m-d') || $date == WorkWithTime::getCurrentDate())
                $tickerIndateModel = $this->getTickerIndayModel();
            else
                $tickerIndateModel = $this->getTickerUptoModel();

            /*DO: get Current date*/
            $this->_allTickerCurrentDate = array();
            $allCurrent = $tickerIndateModel->query()->where('date', $date)->get();
            foreach ($allCurrent as $ticker) {
                $this->_allTickerCurrentDate[$ticker->ticker] = array(
                    'ticker' => $ticker->ticker,
                    'vol' => $ticker->vol,
                    'close' => $ticker->close,
                );
            }

            /*DO: get last date by NumOfSS*/

            $count = 0;
            $lastDate = $date;
            $this->_allTickerVolumeLastDay = array();
            $this->_allTickerVolumeLastDay[$this->_numOfSession] = array();
            do {
                $lastDate = WorkWithTime::last_day($lastDate);
                $model = $this->getTickerUptoModel();
                $allTickerLastDate = $model->query()->where('date', $date);
                if ($allTickerLastDate->count() > 0) {
                    $count += 1;
                    $allTickerLastDate->chunk(200, function ($allTicker, $count) {
                        foreach ($allTicker as $ticker) {
                            if (isset($this->_allTickerVolumeLastDay[$this->_numOfSession][$ticker->ticker]['vol']))
                                $this->_allTickerVolumeLastDay[$this->_numOfSession][$ticker->ticker]['vol'] += $ticker->vol;
                            else
                                $this->_allTickerVolumeLastDay[$this->_numOfSession][$ticker->ticker] = array(
                                    'vol' => $ticker->vol
                                );
                            if ($count == 1)
                                $this->_allTickerVolumeLastDay[$this->_numOfSession][$ticker->ticker] = array(
                                    'close' => $ticker->close,
                                    'ticker' => $ticker->ticker
                                );
                        }
                    });
                }
            } while ($count < $numOfSession);

            /*DO: check Vol*/
            $this->_volumeAndPrice[$numOfSession] = array();
            $this->_volumeAndPrice[$numOfSession]['larger'] = array();
            foreach ($this->_allTickerCurrentDate as $ticker) {
                if ($ticker['vol'] > ($this->_allTickerVolumeLastDay[$this->_numOfSession][$ticker->ticker]['vol'] / $this->_numOfSession)) {
                    if ($ticker['close'] > $this->_allTickerVolumeLastDay[$this->_numOfSession][$ticker->ticker]['close'])
                        $this->_volumeAndPrice[$numOfSession]['larger'][$ticker['ticker']] = array(
                            'ticker' => $ticker['ticker'],
                        );
                }
            }


        }
        return $this->_volumeAndPrice[$numOfSession];

    }

    private $_TickerUptoModel;

    /**
     * @return TickerUpto
     */
    private function getTickerUptoModel() {
        if (is_null($this->_TickerUptoModel))
            $this->_TickerUptoModel = new TickerUpto();
        return $this->_TickerUptoModel;
    }

    private $_TickerIndayModel;

    /**
     * @return TickerInday
     */
    private function getTickerIndayModel() {
        if (is_null($this->_TickerIndayModel))
            $this->_TickerIndayModel = new TickerInDay();
        return $this->_TickerIndayModel;
    }

    /**
     * Ham nay se chay de luu gia tri index cua blue va penny vao db
     */
    public function initDataIntraDay() {
        date_default_timezone_set('Asia/Bangkok');
        $date = new \DateTime();
        $openTime1 = $date->setTime(9, 00);
        $close1 = $date->setTime(11, 30);
        $openTime2 = $date->setTime(13, 00);
        $close2 = $date->setTime(15, 00);
        if (!(($date >= $openTime1 && $date < $close1) || ($date >= $openTime2 && $date <= $close2)))
            return;

        // get data from viet stokc
        $tickerHelper = new TickerHelper();
        $arrayTicker = $tickerHelper->getAllTickerFromVietStock();

        // ticker class:
        $tickerClass = Cache::get('dataTickerClass');
        $arrayTickerClass = json_decode($tickerClass, true);

        //last day:
        $tickerLastDay = Cache::get('dataTickerLastDay');
        $arrayTickerLastDay = json_decode($tickerLastDay, true);

        $aveP = 0;
        $aveB = 0;
        $countP = 0;
        $countB = 0;
        foreach ($arrayTicker as $ticker) {
            if (!isset($arrayTickerClass[$ticker['ticker']]))
                continue;
            if ($arrayTickerClass[$ticker['ticker']]['bp'] == "penny") {
                $countP += 1;
                $aveP += $ticker['close'] / $arrayTickerLastDay[$ticker['ticker']]['close'] - 1;
            }
            if ($arrayTickerClass[$ticker['ticker']]['bp'] == "blue") {
                $countB += 1;
                $aveB += $ticker['close'] / $arrayTickerLastDay[$ticker['ticker']]['close'] - 1;
            }
        }
        $percentP = round($aveP / $countP * 100, 2);
        $percentB = round($aveB / $countB * 100, 2);

        //getVariableDb
        $currentIntra = VariableDatabaseHelper::getVariable('current_intraday');
        $currentIntra += 1;
        $intradayModel = $this->getOverviewIntradayModel();
        $data = array(
            'date' => WorkWithTime::getCurrentDate(),
            'blue_percent' => $percentB,
            'penny_percent' => $percentP,
            'id_inday' => $currentIntra
        );
        $intradayModel->create($data);

    }

    private $_OverviewIntradayModel;

    /**
     * @return OverviewIntraday
     */
    private function getOverviewIntradayModel() {
        if (is_null($this->_OverviewIntradayModel))
            $this->_OverviewIntradayModel = new OverviewIntraday();
        return $this->_OverviewIntradayModel;
    }
}
