<?php
/**
 * Created by IntelliJ IDEA.
 * User: vjcspy
 * Date: 9/21/15
 * Time: 11:01 AM
 */

namespace App\RoboStock\Model\Overview\MarketOverview;

use App\CacheNextDay;
use App\CashFlow;
use App\MarketOverview;
use App\RoboStock\Model\VariableDatabase\VariableDatabaseHelper;
use App\RoboStock\Model\WorkWithTime\WorkWithTime;
use App\TickerBoard;
use App\TickerClass;
use App\TickerInDay;
use App\TickerUpto;

class MarketOverviewHelper {

    private $_currentDate;
    private $_LastDate;
    private $_temp;

    public function createCached() {
        $date = '2015-09-11';

        do {
            $date = WorkWithTime::last_day($date);
            $this->initDataInTableMarketOverview($date);
        } while (true);
    }

    private $_dataNextDay;

    /**TODO: Tao cache cho bang plcs vao ngay hom sau
     * @return bool
     */
    public function createCacheNextDay($nextDate = null) {
        /*TODO: Trong này sẽ lấy 2 giá trị của ngày mai là value ngày hôm nay và value 20 ngày trước*/
        $this->_dataNextDay = array();
        /*INIT BEGIN VALUE*/
        $this->_dataNextDay['haxValue'] = 0;
        $this->_dataNextDay['vniValue'] = 0;
        $this->_dataNextDay['blueValue'] = 0;
        $this->_dataNextDay['pennyValue'] = 0;
        $this->_dataNextDay['daucoValue'] = 0;
        $this->_dataNextDay['cobanValue'] = 0;
        $this->_dataNextDay['allValue'] = 0;


        $this->_dataNextDay['svbqAllValue'] = 0;
        $this->_dataNextDay['svbqHaxValue'] = 0;
        $this->_dataNextDay['svbqVniValue'] = 0;
        $this->_dataNextDay['svbqBlueValue'] = 0;
        $this->_dataNextDay['svbqPennyValue'] = 0;
        $this->_dataNextDay['svbqDaucoValue'] = 0;
        $this->_dataNextDay['svbqCobanValue'] = 0;

        /*init co phieu va volume binh quan dùng trong bảng cường độ tăng giảm */
        $this->_dataNextDay['svbqTop'] = array();
        $model = $this->getTickerUptoModel();

        /*DO: init data sv-1 and sv-bq date*/
        if (is_null($nextDate))
            $date = WorkWithTime::nextDay();
        else
            $date = $nextDate;
        $count = 0;
        $this->_temp = 1;
        $countTemp = 0;
        $lastDate = $date;
        do {
            $countTemp += 1;
            $lastDate = WorkWithTime::last_day($lastDate);
            $allTicker = $model->query()->where('date', $lastDate);
            if ($allTicker->count() == 0) {
                if ($lastDate == '20151016')
                    return 'here';
                continue;
            } else {
                $count += 1;
                if ($this->_temp === 1)
                    VariableDatabaseHelper::updateVariable('data_plcsLastDay', $lastDate);

                $allTicker->chunk(200, function ($currentDate) {
                    foreach ($currentDate as $ticker) {
                        if (in_array($ticker->ticker, ['VNINDEX', 'HNXINDEX', 'HNX30', 'UPCOMINDEX', 'VN30']))
                            continue;
                        if ($this->_temp === 1) {
                            /*DO: check BOARD*/
                            if (isset($this->getDataBoard()[$ticker->ticker])) {
                                if ($this->getDataBoard()[$ticker->ticker]->board == 'hax') {
                                    $this->_dataNextDay['haxValue'] += $ticker->vol * $ticker->average;
                                }
                                if ($this->getDataBoard()[$ticker->ticker]->board == 'hose') {
                                    $this->_dataNextDay['vniValue'] += $ticker->vol * $ticker->average;
                                }
                            }

                            /*DO: check CLASS*/
                            if (isset($this->getDataClass()[$ticker->ticker])) {
                                if ($this->getDataClass()[$ticker->ticker]->bp == 'blue') {
                                    $this->_dataNextDay['blueValue'] += $ticker->vol * $ticker->average;
                                }
                                if ($this->getDataClass()[$ticker->ticker]->bp == 'penny') {
                                    $this->_dataNextDay['pennyValue'] += $ticker->vol * $ticker->average;
                                }
                                if ($this->getDataClass()[$ticker->ticker]->cd == 'dauco') {
                                    $this->_dataNextDay['daucoValue'] += $ticker->vol * $ticker->average;
                                }
                                if ($this->getDataClass()[$ticker->ticker]->cd == 'coban') {
                                    $this->_dataNextDay['cobanValue'] += $ticker->vol * $ticker->average;
                                }
                            }
                            /*DO: all volume and all value*/
                            $this->_dataNextDay['allValue'] += $ticker->vol * $ticker->average;
                        }
                        /*DO: check BOARD*/
                        if (isset($this->getDataBoard()[$ticker->ticker])) {
                            if ($this->getDataBoard()[$ticker->ticker]->board == 'hax') {
                                $this->_dataNextDay['svbqHaxValue'] += $ticker->vol * $ticker->average;
                            }
                            if ($this->getDataBoard()[$ticker->ticker]->board == 'hose') {
                                $this->_dataNextDay['svbqVniValue'] += $ticker->vol * $ticker->average;
                            }
                        }

                        /*DO: check CLASS*/
                        if (isset($this->getDataClass()[$ticker->ticker])) {
                            if ($this->getDataClass()[$ticker->ticker]->bp == 'blue') {
                                $this->_dataNextDay['svbqBlueValue'] += $ticker->vol * $ticker->average;
                            }
                            if ($this->getDataClass()[$ticker->ticker]->bp == 'penny') {
                                $this->_dataNextDay['svbqPennyValue'] += $ticker->vol * $ticker->average;
                            }
                            if ($this->getDataClass()[$ticker->ticker]->cd == 'dauco') {
                                $this->_dataNextDay['svbqDaucoValue'] += $ticker->vol * $ticker->average;
                            }
                            if ($this->getDataClass()[$ticker->ticker]->cd == 'coban') {
                                $this->_dataNextDay['svbqCobanValue'] += $ticker->vol * $ticker->average;
                            }

                            if ($this->getDataClass()[$ticker->ticker]->top == 'top1' || $this->getDataClass()[$ticker->ticker]->top == 'top2') {
                                if (isset($this->_dataNextDay['svbqTop'][$ticker->ticker]))
                                    $this->_dataNextDay['svbqTop'][$ticker->ticker] += $ticker->vol;
                                else
                                    $this->_dataNextDay['svbqTop'][$ticker->ticker] = $ticker->vol;
                            }
                        }
                        $this->_dataNextDay['svbqAllValue'] += $ticker->vol * $ticker->average;
                    }
                });
                $this->_temp = 0;
            }
        } while ($count < 21 && $countTemp < 100);
        if ($countTemp > 99)
            $date = 'error loop >100 rounds';
        $cacheNextDayModel = $this->getCacheNextDayModel();
        $first = $cacheNextDayModel->firstOrNew(array('date' => $date, 'entity' => 'plcs'));
        $first->data = json_encode($this->_dataNextDay);
        $first->save();
        return $date;
    }


    /**
     * MAINFUNCTION
     * TODO: get data in table market_overview, if not existed data will be create
     * @param $date
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function initDataInTableMarketOverview($date) {
        $model = $this->getMarketOverviewModel();
        $first = $model->query()->where('date', $date)->first();
        if (is_null($first) || $date == WorkWithTime::getCurrentDate('Y-m-d') || $date == WorkWithTime::getCurrentDate()) {
            /*DO: generate data to table*/
            if (is_null($this->getDataCurrentDate($date)))
                return null;
            $data = array();
            $temp = 0;
            $count = 0;
            foreach ($this->_dataCurrentDate['vniAvg'] as $k => $v) {
                if (isset($this->_dataCurrentDate['vniAvg1'][$k])) {
                    $count += 1;
                    $temp += $this->_dataCurrentDate['vniAvg'][$k] / $this->_dataCurrentDate['vniAvg1'][$k] - 1;
                } else
                    continue;
            }
            $data['vni'] = array(
                'date' => $date,
                'class' => 'vni',
                'sum_nnm' => '0',
                'sum_nnb' => '0',
                'percent' => round($temp / $count, 4) * 100,
                'vol' => $this->_dataCurrentDate['vniVol'],
                'value' => $this->_dataCurrentDate['vniValue'],
                'sv1' => round($this->_dataCurrentDate['vniValue'] / $this->_dataCurrentDate['sv1VniValue'], 2),
                'svbq' => round($this->_dataCurrentDate['vniValue'] * 21 / $this->_dataCurrentDate['svbqVniValue'], 2),
            );
            $temp = 0;
            $count = 0;
            foreach ($this->_dataCurrentDate['haxAvg'] as $k => $v) {
                if (isset($this->_dataCurrentDate['haxAvg1'][$k])) {
                    $count += 1;
                    $temp += $this->_dataCurrentDate['haxAvg'][$k] / $this->_dataCurrentDate['haxAvg1'][$k] - 1;
                } else
                    continue;
            }
            $data['hax'] = array(
                'date' => $date,
                'class' => 'hax',
                'sum_nnm' => '0',
                'sum_nnb' => '0',
                'percent' => round($temp / $count, 4) * 100,
                'vol' => $this->_dataCurrentDate['haxVol'],
                'value' => $this->_dataCurrentDate['haxValue'],
                'sv1' => round($this->_dataCurrentDate['haxValue'] / $this->_dataCurrentDate['sv1HaxValue'], 2),
                'svbq' => round($this->_dataCurrentDate['haxValue'] * 21 / $this->_dataCurrentDate['svbqHaxValue'], 2),
            );
            $temp = 0;
            $count = 0;
            foreach ($this->_dataCurrentDate['allAvg'] as $k => $v) {
                if (isset($this->_dataCurrentDate['allAvg1'][$k])) {
                    $count += 1;
                    $temp += $this->_dataCurrentDate['allAvg'][$k] / $this->_dataCurrentDate['allAvg1'][$k] - 1;
                } else
                    continue;
            }
            $data['all'] = array(
                'date' => $date,
                'class' => 'all',
                'sum_nnm' => '0',
                'sum_nnb' => '0',
                'percent' => round($temp / $count, 4) * 100,
                'vol' => $this->_dataCurrentDate['allVol'],
                'value' => $this->_dataCurrentDate['allValue'],
                'sv1' => round($this->_dataCurrentDate['allValue'] / $this->_dataCurrentDate['sv1AllValue'], 2),
                'svbq' => round($this->_dataCurrentDate['allValue'] * 21 / $this->_dataCurrentDate['svbqAllValue'], 2),
            );
            $temp = 0;
            $count = 0;
            foreach ($this->_dataCurrentDate['cobanAvg'] as $k => $v) {
                if (isset($this->_dataCurrentDate['cobanAvg1'][$k])) {
                    $count += 1;
                    $temp += $this->_dataCurrentDate['cobanAvg'][$k] / $this->_dataCurrentDate['cobanAvg1'][$k] - 1;
                } else
                    continue;
            }
            $data['coban'] = array(
                'date' => $date,
                'class' => 'coban',
                'sum_nnm' => '0',
                'sum_nnb' => '0',
                'percent' => round($temp / $count, 4) * 100,
                'vol' => $this->_dataCurrentDate['cobanVol'],
                'value' => $this->_dataCurrentDate['cobanValue'],
                'sv1' => round($this->_dataCurrentDate['cobanValue'] / $this->_dataCurrentDate['sv1CobanValue'], 2),
                'svbq' => round($this->_dataCurrentDate['cobanValue'] * 21 / $this->_dataCurrentDate['svbqCobanValue'], 2),
            );
            $temp = 0;
            $count = 0;
            foreach ($this->_dataCurrentDate['daucoAvg'] as $k => $v) {
                if (isset($this->_dataCurrentDate['daucoAvg1'][$k])) {
                    $count += 1;
                    $temp += $this->_dataCurrentDate['daucoAvg'][$k] / $this->_dataCurrentDate['daucoAvg1'][$k] - 1;
                } else
                    continue;
            }
            $data['dauco'] = array(
                'date' => $date,
                'class' => 'dauco',
                'sum_nnm' => '0',
                'sum_nnb' => '0',
                'percent' => round($temp / $count, 4) * 100,
                'vol' => $this->_dataCurrentDate['daucoVol'],
                'value' => $this->_dataCurrentDate['daucoValue'],
                'sv1' => round($this->_dataCurrentDate['daucoValue'] / $this->_dataCurrentDate['sv1DaucoValue'], 2),
                'svbq' => round($this->_dataCurrentDate['daucoValue'] * 21 / $this->_dataCurrentDate['svbqDaucoValue'], 2),
            );
            $temp = 0;
            $count = 0;
            foreach ($this->_dataCurrentDate['blueAvg'] as $k => $v) {
                if (isset($this->_dataCurrentDate['blueAvg1'][$k])) {
                    $count += 1;
                    $temp += $this->_dataCurrentDate['blueAvg'][$k] / $this->_dataCurrentDate['blueAvg1'][$k] - 1;
                } else
                    continue;
            }
            $data['blue'] = array(
                'date' => $date,
                'class' => 'blue',
                'sum_nnm' => '0',
                'sum_nnb' => '0',
                'percent' => round($temp / $count, 4) * 100,
                'vol' => $this->_dataCurrentDate['blueVol'],
                'value' => $this->_dataCurrentDate['blueValue'],
                'sv1' => round($this->_dataCurrentDate['blueValue'] / $this->_dataCurrentDate['sv1BlueValue'], 2),
                'svbq' => round($this->_dataCurrentDate['blueValue'] * 21 / $this->_dataCurrentDate['svbqBlueValue'], 2),
            );
            $temp = 0;
            $count = 0;
            foreach ($this->_dataCurrentDate['pennyAvg'] as $k => $v) {
                if (isset($this->_dataCurrentDate['pennyAvg1'][$k])) {
                    $count += 1;
                    $temp += $this->_dataCurrentDate['pennyAvg'][$k] / $this->_dataCurrentDate['pennyAvg1'][$k] - 1;
                } else
                    continue;
            }
            $data['penny'] = array(
                'date' => $date,
                'class' => 'penny',
                'sum_nnm' => '0',
                'sum_nnb' => '0',
                'percent' => round($temp / $count, 4) * 100,
                'vol' => $this->_dataCurrentDate['pennyVol'],
                'value' => $this->_dataCurrentDate['pennyValue'],
                'sv1' => round($this->_dataCurrentDate['pennyValue'] / $this->_dataCurrentDate['sv1PennyValue'], 2),
                'svbq' => round($this->_dataCurrentDate['pennyValue'] * 21 / $this->_dataCurrentDate['svbqPennyValue'], 2),
            );


            /*DO: init data cashflow*/
            $up = 0;
            $down = 0;
            $equa = 0;
            $topUp = [
                array(
                    'ticker' => '',
                    'value' => 0),
                array(
                    'ticker' => '',
                    'value' => 0),
                array(
                    'ticker' => '',
                    'value' => 0)
            ];
            $topDown = [
                array(
                    'ticker' => '',
                    'value' => 0),
                array(
                    'ticker' => '',
                    'value' => 0),
                array(
                    'ticker' => '',
                    'value' => 0)
            ];
            $temp = 0;
            $tem1 = 0;
            foreach ($this->_dataCurrentDate['cashFlow'] as $k => $v) {
                if (!isset($this->_dataCurrentDate['cashFlow1'][$k]))
                    continue;

                if ($v['close'] > $this->_dataCurrentDate['cashFlow1'][$k]['close']) {
                    if ($temp == 0) {
                        $topUp[0] = array(
                            'ticker' => $k,
                            'value' => $v['value']);
                        $temp = 1;
                    }
                    $up += $v['value'];
                    if ($v['value'] > $topUp[2]['value']) {
                        if ($v['value'] > $topUp[1]['value']) {
                            if ($v['value'] > $topUp[0]['value']) {
                                $topUp[2] = $topUp[1];
                                $topUp[1] = $topUp[0];
                                $topUp[0] = array(
                                    'ticker' => $k,
                                    'value' => $v['value']);
                            } else {
                                $topUp[2] = $topUp[1];
                                $topUp[1] = array(
                                    'ticker' => $k,
                                    'value' => $v['value']);
                            }
                        } else {
                            $topUp[2] = array(
                                'ticker' => $k,
                                'value' => $v['value']);
                        }
                    }
                } elseif ($v['close'] < $this->_dataCurrentDate['cashFlow1'][$k]['close']) {
                    if ($tem1 == 0) {
                        $topDown[0] = array(
                            'ticker' => $k,
                            'value' => $v['value']);
                        $tem1 = 1;
                    }
                    $down += $v['value'];
                    if ($v['value'] > $topDown[2]['value']) {
                        if ($v['value'] > $topDown[1]['value']) {
                            if ($v['value'] > $topDown[0]) {
                                $topDown[2] = $topDown[1];
                                $topDown[1] = $topDown[0];
                                $topDown[0] = array(
                                    'ticker' => $k,
                                    'value' => $v['value']);
                            } else {
                                $topDown[2] = $topDown[1];
                                $topDown[1] = array(
                                    'ticker' => $k,
                                    'value' => $v['value']);
                            }
                        } else {
                            $topDown[2] = array(
                                'ticker' => $k,
                                'value' => $v['value']);
                        }
                    }
                } else {
                    $equa += $v['value'];
                }
            }

            /*DO: get percent*/
            foreach ($topUp as $v => $k) {
                $topUp[$v]['percent'] = $this->_dataCurrentDate['cashFlow'][$k['ticker']]['close'] / $this->_dataCurrentDate['cashFlow1'][$k['ticker']]['close'] - 1;
            }
            foreach ($topDown as $v => $k) {
                $topDown[$v]['percent'] = $this->_dataCurrentDate['cashFlow'][$k['ticker']]['close'] / $this->_dataCurrentDate['cashFlow1'][$k['ticker']]['close'] - 1;
            }

            $dataCashFlow = array(
                'up' => $up,
                'down' => $down,
                'hold' => $equa,
                'date' => $date,
                'data_up' => json_encode($topUp),
                'data_down' => json_encode($topDown),
            );
            if (!$this->getCashFlowModel()->query()->where('date', $data)->count() > 0)
                $this->getCashFlowModel()->create($dataCashFlow);

            foreach ($data as $k => $i) {
                $count = $model->query()->where('date', $date)->where('class', $k)->count();
                if ($count > 0 && $date != WorkWithTime::getCurrentDate('Y-m-d')) {
                    continue;
                } else {
                    $ticker = $model->firstOrNew(array(
                        'date' => $date,
                        'class' => $k,
                    ));
                    $ticker->save($i);
                }
            }

        }
        $cashFlow = $this->getCashFlowModel()->query()->where('date', $date)->first();
        $allMarketOverview = $model->query()->where('date', $date)->get();
        return array(
            'marketOverview' => $allMarketOverview,
            'cashFlow' => $cashFlow
        );
    }

    private $_CashFlowModel;

    /**
     * @return CashFlow
     */
    private function getCashFlowModel() {
        if (is_null($this->_CashFlowModel))
            $this->_CashFlowModel = new CashFlow();
        return $this->_CashFlowModel;
    }

    private $_dataCurrentDate;

    private function getDataCurrentDate($date) {
        if (is_null($this->_dataCurrentDate)) {
            $this->_currentDate = $date;
            $this->_dataCurrentDate = array();
            $model = $this->getTickerUptoModel();
            /*DO: sum current date*/
            $this->getDataClass();
            $this->getDataBoard();

            /*INIT BEGIN VALUE*/
            $this->_dataCurrentDate['allVol'] = 0;
            $this->_dataCurrentDate['allValue'] = 0;
            $this->_dataCurrentDate['sv1HaxVol'] = 0;
            $this->_dataCurrentDate['sv1HaxValue'] = 0;
            $this->_dataCurrentDate['sv1VniVol'] = 0;
            $this->_dataCurrentDate['sv1VniValue'] = 0;
            $this->_dataCurrentDate['sv1BlueVol'] = 0;
            $this->_dataCurrentDate['sv1BlueValue'] = 0;
            $this->_dataCurrentDate['sv1PennyVol'] = 0;
            $this->_dataCurrentDate['sv1PennyValue'] = 0;
            $this->_dataCurrentDate['sv1DaucoVol'] = 0;
            $this->_dataCurrentDate['sv1DaucoValue'] = 0;
            $this->_dataCurrentDate['sv1CobanVol'] = 0;
            $this->_dataCurrentDate['sv1CobanValue'] = 0;


            $this->_dataCurrentDate['sv1AllVol'] = 0;
            $this->_dataCurrentDate['sv1AllValue'] = 0;
            $this->_dataCurrentDate['haxVol'] = 0;
            $this->_dataCurrentDate['haxValue'] = 0;
            $this->_dataCurrentDate['vniVol'] = 0;
            $this->_dataCurrentDate['vniValue'] = 0;
            $this->_dataCurrentDate['blueVol'] = 0;
            $this->_dataCurrentDate['blueValue'] = 0;
            $this->_dataCurrentDate['pennyVol'] = 0;
            $this->_dataCurrentDate['pennyValue'] = 0;
            $this->_dataCurrentDate['daucoVol'] = 0;
            $this->_dataCurrentDate['daucoValue'] = 0;
            $this->_dataCurrentDate['cobanVol'] = 0;
            $this->_dataCurrentDate['cobanValue'] = 0;


            $this->_dataCurrentDate['svbqAllVol'] = 0;
            $this->_dataCurrentDate['svbqAllValue'] = 0;
            $this->_dataCurrentDate['svbqHaxVol'] = 0;
            $this->_dataCurrentDate['svbqHaxValue'] = 0;
            $this->_dataCurrentDate['svbqVniVol'] = 0;
            $this->_dataCurrentDate['svbqVniValue'] = 0;
            $this->_dataCurrentDate['svbqBlueVol'] = 0;
            $this->_dataCurrentDate['svbqBlueValue'] = 0;
            $this->_dataCurrentDate['svbqPennyVol'] = 0;
            $this->_dataCurrentDate['svbqPennyValue'] = 0;
            $this->_dataCurrentDate['svbqDaucoVol'] = 0;
            $this->_dataCurrentDate['svbqDaucoValue'] = 0;
            $this->_dataCurrentDate['svbqCobanVol'] = 0;
            $this->_dataCurrentDate['svbqCobanValue'] = 0;


            /*DO: Dùng để tính percent. Tức bằng bìn quân tổng c/c1*/
            $this->_dataCurrentDate['cobanAvg'] = array();
            $this->_dataCurrentDate['allAvg'] = array();
            $this->_dataCurrentDate['vniAvg'] = array();
            $this->_dataCurrentDate['haxAvg'] = array();
            $this->_dataCurrentDate['blueAvg'] = array();
            $this->_dataCurrentDate['pennyAvg'] = array();
            $this->_dataCurrentDate['daucoAvg'] = array();

            $this->_dataCurrentDate['cobanAvg1'] = array();
            $this->_dataCurrentDate['allAvg1'] = array();
            $this->_dataCurrentDate['vniAvg1'] = array();
            $this->_dataCurrentDate['haxAvg1'] = array();
            $this->_dataCurrentDate['blueAvg1'] = array();
            $this->_dataCurrentDate['pennyAvg1'] = array();
            $this->_dataCurrentDate['daucoAvg1'] = array();

            /*DO: dùng để tính cash flow*/
            $this->_dataCurrentDate['cashFlow'] = array();
            $this->_dataCurrentDate['cashFlow1'] = array();


            /*DO: Remove code blow to increase performance*/
//            if ($model->query()->where('date', $date)->count() == 0) {
//                return null;
//            }


            /*DO: init data current date*/
            $this->_temp = 0;
            if ($date == WorkWithTime::getCurrentDate('Y-m-d') || $date == WorkWithTime::getCurrentDate())
                $tickerInday = $this->getTickerIndayModel();
            else
                $tickerInday = $this->getTickerUptoModel();
            $tickerInday->query()->where('date', $date)->chunk(200, function ($currentDate) {
                foreach ($currentDate as $ticker) {
                    $this->_temp += 1;
                    if (in_array($ticker->ticker, ['VNINDEX', 'HNXINDEX', 'HNX30', 'UPCOMINDEX', 'VN30']))
                        continue;
                    /*DO: All to calculate Cash Flow*/
                    $this->_dataCurrentDate['cashFlow'][$ticker->ticker] = array(
                        'value' => $ticker->average * $ticker->vol,
                        'close' => $ticker->close,
                    );


                    /*DO: check BOARD*/
                    if (isset($this->getDataBoard()[$ticker->ticker])) {
                        if ($this->getDataBoard()[$ticker->ticker]->board == 'hax') {
                            $this->_dataCurrentDate['haxVol'] += $ticker->vol;
                            $this->_dataCurrentDate['haxValue'] += $ticker->vol * $ticker->average;
                            $this->_dataCurrentDate['haxAvg'][$ticker->ticker] = $ticker->close;;
                        }
                        if ($this->getDataBoard()[$ticker->ticker]->board == 'hose') {
                            $this->_dataCurrentDate['vniVol'] += $ticker->vol;
                            $this->_dataCurrentDate['vniValue'] += $ticker->vol * $ticker->average;
                            $this->_dataCurrentDate['vniAvg'][$ticker->ticker] = $ticker->close;;
                        }
                    }

                    /*DO: check CLASS*/
                    if (isset($this->getDataClass()[$ticker->ticker])) {
                        if ($this->getDataClass()[$ticker->ticker]->bp == 'blue') {
                            $this->_dataCurrentDate['blueVol'] += $ticker->vol;
                            $this->_dataCurrentDate['blueValue'] += $ticker->vol * $ticker->average;
                            $this->_dataCurrentDate['blueAvg'][$ticker->ticker] = $ticker->close;;
                        }
                        if ($this->getDataClass()[$ticker->ticker]->bp == 'penny') {
                            $this->_dataCurrentDate['pennyVol'] += $ticker->vol;
                            $this->_dataCurrentDate['pennyValue'] += $ticker->vol * $ticker->average;
                            $this->_dataCurrentDate['pennyAvg'][$ticker->ticker] = $ticker->close;;
                        }
                        if ($this->getDataClass()[$ticker->ticker]->cd == 'dauco') {
                            $this->_dataCurrentDate['daucoVol'] += $ticker->vol;
                            $this->_dataCurrentDate['daucoValue'] += $ticker->vol * $ticker->average;
                            $this->_dataCurrentDate['daucoAvg'][$ticker->ticker] = $ticker->close;;
                        }
                        if ($this->getDataClass()[$ticker->ticker]->cd == 'coban') {
                            $this->_dataCurrentDate['cobanVol'] += $ticker->vol;
                            $this->_dataCurrentDate['cobanValue'] += $ticker->vol * $ticker->average;
                            $this->_dataCurrentDate['cobanAvg'][$ticker->ticker] = $ticker->close;;
                        }
                        if ($this->getDataClass()[$ticker->ticker]->top == 'top1' || $this->getDataClass()[$ticker->ticker]->top == 'top2') {
                            /*DO: ALL*/
                            $this->_dataCurrentDate['allAvg'][$ticker->ticker] = $ticker->close;;
                        }
                    }
                    /*DO: all volume and all value*/
                    $this->_dataCurrentDate['allVol'] += $ticker->vol;
                    $this->_dataCurrentDate['allValue'] += $ticker->vol * $ticker->average;
                }
            });
            if ($this->_temp == 0)
                return null;
            /*DO: init data last date*/
            $lastDate = $date;
            do {
                $lastDate = WorkWithTime::last_day($lastDate);
                $allTicker = $model->query()->where('date', $lastDate);
                if ($allTicker->count() == 0)
                    $flag = true;
                else {
                    $flag = false;
                    $allTicker->chunk(200, function ($currentDate) {
                        foreach ($currentDate as $ticker) {
                            if (in_array($ticker->ticker, ['VNINDEX', 'HNXINDEX', 'HNX30', 'UPCOMINDEX', 'VN30']))
                                continue;

                            /*DO: All to calculate Cash Flow*/
                            $this->_dataCurrentDate['cashFlow1'][$ticker->ticker] = array(
//                                'value' => $ticker->average * $ticker->vol,
'close' => $ticker->close,
                            );


                            /*DO: check BOARD*/
                            if (isset($this->getDataBoard()[$ticker->ticker])) {
                                if ($this->getDataBoard()[$ticker->ticker]->board == 'hax') {
                                    $this->_dataCurrentDate['sv1HaxVol'] += $ticker->vol;
                                    $this->_dataCurrentDate['sv1HaxValue'] += $ticker->vol * $ticker->average;
                                    $this->_dataCurrentDate['haxAvg1'][$ticker->ticker] = $ticker->close;;
                                }
                                if ($this->getDataBoard()[$ticker->ticker]->board == 'hose') {
                                    $this->_dataCurrentDate['sv1VniVol'] += $ticker->vol;
                                    $this->_dataCurrentDate['sv1VniValue'] += $ticker->vol * $ticker->average;
                                    $this->_dataCurrentDate['vniAvg1'][$ticker->ticker] = $ticker->close;;
                                }
                            }

                            /*DO: check CLASS*/
                            if (isset($this->getDataClass()[$ticker->ticker])) {
                                if ($this->getDataClass()[$ticker->ticker]->bp == 'blue') {
                                    $this->_dataCurrentDate['sv1BlueVol'] += $ticker->vol;
                                    $this->_dataCurrentDate['sv1BlueValue'] += $ticker->vol * $ticker->average;
                                    $this->_dataCurrentDate['blueAvg1'][$ticker->ticker] = $ticker->close;;
                                }
                                if ($this->getDataClass()[$ticker->ticker]->bp == 'penny') {
                                    $this->_dataCurrentDate['sv1PennyVol'] += $ticker->vol;
                                    $this->_dataCurrentDate['sv1PennyValue'] += $ticker->vol * $ticker->average;
                                    $this->_dataCurrentDate['pennyAvg1'][$ticker->ticker] = $ticker->close;;
                                }
                                if ($this->getDataClass()[$ticker->ticker]->cd == 'dauco') {
                                    $this->_dataCurrentDate['sv1DaucoVol'] += $ticker->vol;
                                    $this->_dataCurrentDate['sv1DaucoValue'] += $ticker->vol * $ticker->average;
                                    $this->_dataCurrentDate['daucoAvg1'][$ticker->ticker] = $ticker->close;;
                                }
                                if ($this->getDataClass()[$ticker->ticker]->cd == 'coban') {
                                    $this->_dataCurrentDate['sv1CobanVol'] += $ticker->vol;
                                    $this->_dataCurrentDate['sv1CobanValue'] += $ticker->vol * $ticker->average;
                                    $this->_dataCurrentDate['cobanAvg1'][$ticker->ticker] = $ticker->close;;
                                }
                                if ($this->getDataClass()[$ticker->ticker]->top == 'top1' || $this->getDataClass()[$ticker->ticker]->top == 'top2') {
                                    /*DO: ALL*/
                                    $this->_dataCurrentDate['allAvg1'][$ticker->ticker] = $ticker->close;;
                                }
                            }
                            $this->_dataCurrentDate['sv1AllVol'] += $ticker->vol;
                            $this->_dataCurrentDate['sv1AllValue'] += $ticker->vol * $ticker->average;
                        }
                    });
                }
            } while ($flag);


            /*DO: init data sv-bq date*/
            $lastDate = $date;
            $count = 0;
            do {
                $lastDate = WorkWithTime::last_day($lastDate);
                $allTicker = $model->query()->where('date', $lastDate);
                if ($allTicker->count() == 0)
                    continue;
                else {
                    $count += 1;
                    $allTicker->chunk(200, function ($currentDate) {
                        foreach ($currentDate as $ticker) {
                            if (in_array($ticker->ticker, ['VNINDEX', 'HNXINDEX', 'HNX30', 'UPCOMINDEX', 'VN30']))
                                continue;

                            /*DO: check BOARD*/
                            if (isset($this->getDataBoard()[$ticker->ticker])) {
                                if ($this->getDataBoard()[$ticker->ticker]->board == 'hax') {
                                    $this->_dataCurrentDate['svbqHaxVol'] += $ticker->vol;
                                    $this->_dataCurrentDate['svbqHaxValue'] += $ticker->vol * $ticker->average;
                                }
                                if ($this->getDataBoard()[$ticker->ticker]->board == 'hose') {
                                    $this->_dataCurrentDate['svbqVniVol'] += $ticker->vol;
                                    $this->_dataCurrentDate['svbqVniValue'] += $ticker->vol * $ticker->average;
                                }
                            }

                            /*DO: check CLASS*/
                            if (isset($this->getDataClass()[$ticker->ticker])) {
                                if ($this->getDataClass()[$ticker->ticker]->bp == 'blue') {
                                    $this->_dataCurrentDate['svbqBlueVol'] += $ticker->vol;
                                    $this->_dataCurrentDate['svbqBlueValue'] += $ticker->vol * $ticker->average;
                                }
                                if ($this->getDataClass()[$ticker->ticker]->bp == 'penny') {
                                    $this->_dataCurrentDate['svbqPennyVol'] += $ticker->vol;
                                    $this->_dataCurrentDate['svbqPennyValue'] += $ticker->vol * $ticker->average;
                                }
                                if ($this->getDataClass()[$ticker->ticker]->cd == 'dauco') {
                                    $this->_dataCurrentDate['svbqDaucoVol'] += $ticker->vol;
                                    $this->_dataCurrentDate['svbqDaucoValue'] += $ticker->vol * $ticker->average;
                                }
                                if ($this->getDataClass()[$ticker->ticker]->cd == 'coban') {
                                    $this->_dataCurrentDate['svbqCobanVol'] += $ticker->vol;
                                    $this->_dataCurrentDate['svbqCobanValue'] += $ticker->vol * $ticker->average;
                                }
                                if ($this->getDataClass()[$ticker->ticker]->top == 'top1' || $this->getDataClass()[$ticker->ticker]->top == 'top2') {
                                    /*DO: ALL*/
                                }
                            }
                            $this->_dataCurrentDate['svbqAllVol'] += $ticker->vol;
                            $this->_dataCurrentDate['svbqAllValue'] += $ticker->vol * $ticker->average;
                        }
                    });
                }
            } while ($count < 21);

        }
        return $this->_dataCurrentDate;
    }


    private $_dataClass;

    private function getDataClass() {
        if (is_null($this->_dataClass)) {
            $model = $this->getTickerClassModel();
            $class = $model->all();
            $this->_dataClass = array();
            foreach ($class as $ticker) {
                $this->_dataClass[$ticker->ticker] = $ticker;
            }
        }
        return $this->_dataClass;
    }

    private $_dataBoard;

    private function getDataBoard() {
        if (is_null($this->_dataBoard)) {
            $model = $this->getTickerBoardModel();
            $model->chunk(200, function ($currentDate) {
                foreach ($currentDate as $ticker) {
                    $this->_dataBoard[$ticker->ticker] = $ticker;
                }
            });
        }
        return $this->_dataBoard;
    }

    private $_TickerBoardModel;

    /**
     * @return TickerBoard
     */
    private function getTickerBoardModel() {
        if (is_null($this->_TickerBoardModel))
            $this->_TickerBoardModel = new TickerBoard();
        return $this->_TickerBoardModel;
    }

    private $_TickerClassModel;

    /**
     * @return TickerClass
     */
    private function getTickerClassModel() {
        if (is_null($this->_TickerClassModel))
            $this->_TickerClassModel = new TickerClass();
        return $this->_TickerClassModel;
    }

    private $_TickerUptoModel;

    /**
     * @return TickerUpto
     */
    private function getTickerUptoModel() {
        if (is_null($this->_TickerUptoModel))
            $this->_TickerUptoModel = new TickerUpto();
        return $this->_TickerUptoModel;
    }

    private $_MarketOverviewModel;

    /**
     * @return MarketOverview
     */
    private function getMarketOverviewModel() {
        if (is_null($this->_MarketOverviewModel))
            $this->_MarketOverviewModel = new MarketOverview();
        return $this->_MarketOverviewModel;
    }

    private $_TickerIndayModel;

    /**
     * @return TickerInday
     */
    private function getTickerIndayModel() {
        if (is_null($this->_TickerIndayModel))
            $this->_TickerIndayModel = new TickerInDay();
        return $this->_TickerIndayModel;
    }

    private $_CacheNextDayModel;

    /**
     * @return CacheNextDay
     */
    private function getCacheNextDayModel() {
        if (is_null($this->_CacheNextDayModel))
            $this->_CacheNextDayModel = new CacheNextDay();
        return $this->_CacheNextDayModel;
    }
}
