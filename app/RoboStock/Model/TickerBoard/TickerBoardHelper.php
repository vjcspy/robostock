<?php
/**
 * Created by IntelliJ IDEA.
 * User: vjcspy
 * Date: 9/18/15
 * Time: 9:16 PM
 */
namespace App\RoboStock\Model\TickerBoard;

use App\RoboStock\Model\TickerUpto\ImportFromBoardPrice;
use App\RoboStock\Model\WorkWithTime\WorkWithTime;
use App\TickerBoard;
use App\TickerBoardChange;
use Illuminate\Support\Facades\Cache;

class TickerBoardHelper {

    /**TODO: Check Update Board and save to TickerBoardChange & TickerBoard Table
     * MAINFUNCTION
     * @param $ticker
     * @return bool
     */
    public function importDataFromVietStockBoard($ticker = null) {

        $model = $this->getTickeckBoardModel();
        if ($model->count() > 0) {
            return $this->checkUpdateBoard();
        } else {
            $import = new ImportFromBoardPrice();
            $allDataFromVietStock = $import->getAllDataTickerCurrentDayFromVietStock(false, false);
            foreach ($allDataFromVietStock as $ticker) {
                $model->create($ticker);
            }
        }
        return true;
    }

    /**MAINFUNCTION
     * TODO: get array all ticker name
     * @return mixed
     */
    public function getArrayTicker() {
        if (!Cache::has('arrayAllTickers')) {
            /*DO: Sàn*/
            $tickerboardModel = new TickerBoard();
            $colection = $tickerboardModel->query()->get();
            $arrayTicker = array();

            foreach ($colection as $ticker) {
                $arrayTicker[] = $ticker->ticker;
            }

            $jsonTickers = json_encode($arrayTicker);

            Cache::put('arrayAllTickers', json_encode($jsonTickers), 99999999);
        }
        $data = Cache::get('arrayAllTickers');
        return $data;

    }

    private $_arrayAllTickerBoardChange;

    /**
     * @return array
     * @internal param array $ticker
     */
    public function checkUpdateBoard() {
        $import = new ImportFromBoardPrice();
        $allDataFromVietStock = $import->getAllDataTickerCurrentDayFromVietStock(false, false);
        $allTickerBoard = $this->getAllTickerBoard();
        $this->_arrayAllTickerBoardChange = array();
        foreach ($allDataFromVietStock as $ticker) {
            /*Importance: Neu khong ton tai thi phai tao moi. Do co phieu moi len san*/
            if (!isset($allTickerBoard[$ticker['ticker']])) {
                $model = $this->getTickeckBoardModel();
                $model->create($ticker);

                $tickerBoardChangeModel = $this->getTickerBoardChangeModel();
                $data = array(
                    'ticker' => $ticker['ticker'],
                    'date' => WorkWithTime::getCurrentDate(),
                    'from' => null,
                    'to' => $ticker['board']
                );
                $tickerBoardChangeModel->create($data);

                continue;
            }
            if ($ticker['board'] != $allTickerBoard[$ticker['ticker']]['board']) {
//                Change Board
                /*DO:Save to ticker_board_change*/
                $tickerBoardChangeModel = $this->getTickerBoardChangeModel();
                $data = array(
                    'ticker' => $ticker['ticker'],
                    'date' => WorkWithTime::getCurrentDate(),
                    'from' => $allTickerBoard[$ticker['ticker']]['board'],
                    'to' => $ticker['board']
                );
                $tickerBoardChangeModel->create($data);
                /*DO:Update to ticker_board_change*/
                $tickerBoardModel = $this->getTickeckBoardModel();
                $tickerBoardModel->query()->where('ticker', $ticker['ticker'])->update(array('board' => $ticker['board']));

                $this->_arrayAllTickerBoardChange[] = array(
                    'ticker' => $ticker['ticker'],
                    'date' => WorkWithTime::getCurrentDate(),
                    'from' => $allTickerBoard[$ticker['ticker']]['board'],
                    'to' => $ticker['board']);
            }
        }
        return $this->_arrayAllTickerBoardChange;
    }


    private $_allTickerBoard;

    /**
     * @return array
     */
    public function getAllTickerBoard() {
        if (is_null($this->_allTickerBoard)) {
            $this->_allTickerBoard = array();
            $model = $this->getTickeckBoardModel()->all();
            foreach ($model as $ticker) {
                $this->_allTickerBoard[$ticker->ticker] = array(
                    'ticker' => $ticker->ticker,
                    'board' => $ticker->board
                );
            }
        }
        return $this->_allTickerBoard;
    }

    private $_tickerBoardModel;

    /**
     * @return TickerBoard
     */
    private function getTickeckBoardModel() {
        if (is_null($this->_tickerBoardModel)) {
            $this->_tickerBoardModel = new TickerBoard();
        }
        return $this->_tickerBoardModel;
    }


    private $_tickerBoardChangeModel;

    /**
     * @return TickerBoardChange
     */
    private function getTickerBoardChangeModel() {
        if (is_null($this->_tickerBoardChangeModel)) {
            $this->_tickerBoardChangeModel = new TickerBoardChange();
        }
        return $this->_tickerBoardChangeModel;
    }


}
