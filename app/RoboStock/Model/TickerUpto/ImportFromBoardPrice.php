<?php
/**
 * Created by PhpStorm.
 * User: vjcspy
 * Date: 9/15/15
 * Time: 5:19 PM
 */
namespace App\RoboStock\Model\TickerUpto;

use App\RoboStock\Model\Log\LogHelper;
use App\RoboStock\Model\Ticker\Ticker;
use App\RoboStock\Model\TickerBoard\TickerBoardHelper;
use App\RoboStock\Model\Variable;
use App\RoboStock\Model\WorkWithTime\WorkWithTime;
use App\RoboStock\Model\WorkWithUrl\WorkWithUrl;
use App\TickerForeign;
use App\TickerInDay;
use App\TickerUpto;

class ImportFromBoardPrice {

    /*URL Board Price*/
    private $_urlTvsi = array(
        'hose' => 'http://prs.tvsi.com.vn/DataForLoad.ashx?FloorCode=10',
        'hax' => 'http://prs.tvsi.com.vn/DataForLoad.ashx?FloorCode=02',
        'upcom' => 'http://prs.tvsi.com.vn/DataForLoad.ashx?FloorCode=04'
    );

    private $_urlVietStock = array(
        'hose' => 'http://banggia.vietstock.vn/StockHandler.ashx?option=rt&catid=1',
        'hax' => 'http://banggia.vietstock.vn/StockHandler.ashx?option=rt&catid=2',
        'upcom' => 'http://banggia.vietstock.vn/StockHandler.ashx?option=rt&catid=3'
    );

    public function updateAllData($dieIfError = false) {
        return $this->updateTickerFromVietStock($dieIfError);
    }

    private $_urlsToGetData;

    /**
     * @return mixed
     */
    public function getUrlsToGetData() {
        /*Default use url VietStock*/
        if (is_null($this->_urlsToGetData))
            $this->_urlsToGetData = $this->_urlVietStock;
        return $this->_urlsToGetData;
    }

    /**
     * @param mixed $urlsToGetData
     */
    public function setUrlsToGetData($urlsToGetData) {
        $this->_urlsToGetData = $urlsToGetData;
    }

    private $_resultTickerUpdate;

    /**
     * MAINFUNCTION
     * TODO: update/insert ticker in current day From Viet Stock to table ticker_upto
     * return true if OK || message if error
     * @param bool|false $dieIfError
     * @return array
     */
    public function updateTickerFromVietStock($dieIfError = false, $showUpdated = false) {
        $workWithUrl = $this->getWorkWithUrl();
        $urls = $this->_urlVietStock;
        $updatedInDayBefore = array();
        $updated = array();
        $boardChange = array();
        foreach ($urls as $k => $url) {
            $urlData = $workWithUrl->getDataFromUrl($url);
            $dataArrayTickers = $this->getDataFromVietStock($urlData);

            /*DO:Kiểm tra xem hôm nay có giao dịch không*/
            /*IMPORTANCE*/
            if ($this->isExchangeInDay($dataArrayTickers)) {
                foreach ($dataArrayTickers as $ticker) {
                    if (!$this->insertTickerToDb($ticker, true)) {
                        if ($dieIfError)
                            die('Existed ticker "' . $ticker['ticker'] . '" in Database. It is updated in this day.');
                        else {
                            $updatedInDayBefore[] = $ticker;
                            continue;
                        }
                    }
                    $updated[] = $ticker;
                    /*DO:update ticker board*/
                    $ticker['board'] = $k;
                    $ticker['date'] = WorkWithTime::getCurrentDate();
                    /*DO: insert to ticker_foreign*/
                    $this->getTickerForeignModel()->create($ticker);
                }
            } else {
                return $this->_resultTickerUpdate = array(
                    'status' => false,
                    'description' => 'Not exchange in day',
                    'data' => null,
                );
            }
        }
        $this->_resultTickerUpdate = array(
            'status' => true,
            'description' => 'Update success ' . count($updated) . ' ticker(s)',
            'data' => array(
                'ticker updated' => $showUpdated == true ? $updated : count($updated),
                'ticker updated Before In day' => $updatedInDayBefore,
                'board change' => $boardChange
            ),
        );
        return $this->_resultTickerUpdate;

    }

    private $_allDataTickerFromVietStock;

    /**
     * MAINFUNCTION
     * TODO: Get all ticker in current day from Viet Stock
     * Check if existed data in day in db, it'will be collect
     * else: save data to DB and collect it
     * @param bool $useDb
     * @param bool $updateToTable
     * @return array
     */
    public function getAllDataTickerCurrentDayFromVietStock($useDb = false, $updateToTable = false) {
        if (is_null($this->_allDataTickerFromVietStock)) {
            /*Check data existed in DB*/
            if ($useDb && $this->checkDataIndayExisted(100)) {
                $this->_allDataTickerFromVietStock = array();
                $model = new TickerInDay();
                $tickers = $model->all();
                foreach ($tickers as $ticker) {
                    $currentTicker = array(
                        'ticker' => $ticker->ticker,
                        'date' => $ticker->date,
                        'refer' => $ticker->refer,
                        'ceiling' => $ticker->ceiling,
                        'floor' => $ticker->floor,
                        'open' => $ticker->open,
                        'high' => $ticker->high,
                        'low' => $ticker->low,
                        'close' => $ticker->close,
                        'average' => $ticker->average,
                        'nnm' => $ticker->nnm,
                        'nnb' => $ticker->nnb,
                        'board' => $ticker->board,
                        'vol' => $ticker->vol,
                    );
                    $this->_allDataTickerFromVietStock[$currentTicker['ticker']] = $currentTicker;
                }

            } else {
                $this->_allDataTickerFromVietStock = array();
                $workWithUrl = $this->getWorkWithUrl();
                $urls = $this->_urlVietStock;
                foreach ($urls as $k => $url) {
                    $urlData = $workWithUrl->getDataFromUrl($url);
                    $dataArrayTickers = $this->getDataFromVietStock($urlData);
                    foreach ($dataArrayTickers as $ticker) {
                        $ticker['board'] = $k;
                        $this->_allDataTickerFromVietStock[$ticker['ticker']] = $ticker;
                        $ticker['date'] = WorkWithTime::getCurrentDate();
                        if ($updateToTable)
                            $this->updatetDataToTickerInDay($ticker);
                    }
                }
            }
        }
        return $this->_allDataTickerFromVietStock;
    }


    private $_isExchange = null;

    /**MAINFUNCTION
     * TODO: check exchange in array ticker current day
     * @param array $dataTickerInDay
     * @return bool|null
     */
    public function isExchangeInDay(Array $dataTickerInDay) {
        $hsdc = new CheckHsdc();
        $count = 0;
        if ($this->_isExchange === null) {
            foreach ($dataTickerInDay as $ticker) {
                if (Variable::$_numOfTickerCheckIsExchangeInDay < $count) {
                    $this->_isExchange = false;
                    break;
                }
                $lastDayClosePrice = $hsdc->getClosePriceLastDate($ticker);
                if ($lastDayClosePrice != $ticker['close']) {
                    $this->_isExchange = true;
                    break;
                }
                $count += 1;
            }
        }
        return $this->_isExchange;
    }

    public function isExchangeInDayNew(Array $dataTickerInDay) {
        $hsdc = new CheckHsdc();
        $count = 0;
        $diffArray = array();
        $nullPricfe = 1;
        if ($this->_isExchange === null) {
            foreach ($dataTickerInDay as $ticker) {
                if (Variable::$_numOfTickerCheckIsExchangeInDay < $count) {
                    $this->_isExchange = false;
                    break;
                }
                $lastDayClosePrice = $hsdc->getClosePriceLastDate($ticker);
                if (is_null($lastDayClosePrice)) {
                    LogHelper::addLog($ticker['ticker'], "notFoundPrice.log");
                    $nullPricfe += 1;
                    continue;
                }
                if ($lastDayClosePrice != $ticker['close']) {
                    $this->_isExchange = true;
                    $diffArray = array(
                        'ticker' => $ticker['ticker'],
                        'lastDayClose' => $lastDayClosePrice,
                        'todayPrice' => $ticker['close']
                    );
                    break;
                }
                $count += 1;
            }
        }
        return array(
            'status' => $this->_isExchange, 'data' => $diffArray, 'nullPrice' => $nullPricfe
        );
    }


    private function checkDataIndayExisted($numOfItemsCheck) {
        $model = new TickerInDay();
        $currentDay = WorkWithTime::getCurrentDate();
        $items = $model->query()->where('date', $currentDay);
        $isExisted = false;
        $count = $items->count();
        if ($count > $numOfItemsCheck)
            return true;
        return $isExisted;
    }

    private $_isFirstUpdate = true;

    private function updatetDataToTickerInDay($ticker) {
        $model = new TickerInDay();
        if ($this->_isFirstUpdate) {
            $model->truncate();
            $this->_isFirstUpdate = false;
        }
        $model->create($ticker);
        return true;
    }

    private $_workWithUrl;

    public function getWorkWithUrl() {
        if (is_null($this->_workWithUrl))
            $this->_workWithUrl = new WorkWithUrl();
        return $this->_workWithUrl;
    }

    private $_currentDate;


    /**
     * TODO: insert ticker to table
     * @param $ticker
     * @return bool
     */
    public function insertTickerToDb($ticker, $checkExisted = false) {
        if (is_null($this->_currentDate)) {
            $this->_currentDate = WorkWithTime::getCurrentDate();
        }
        $ticker['date'] = $this->_currentDate;

        if ($checkExisted && $this->checkExistedTickerInDb($ticker)) {
            return false;
        } else {
            $model = $this->getTickerUptoModel();
            $model->create($ticker);
            return true;
        }
    }


    private $_tickerUpto;

    /**
     * @return TickerUpto
     */
    public function getTickerUptoModel() {
        if (is_null($this->_tickerUpto)) {
            $this->_tickerUpto = new TickerUpto();
        }
        return $this->_tickerUpto;

    }


    /**
     * TODO: Check ticker exited in DB; if existed will not insert
     * @param $ticker
     * @return bool
     */
    public function checkExistedTickerInDb($ticker) {
        $model = $this->getTickerUptoModel();
        $matches = array(
            'date' => $ticker['date'],
            'ticker' => $ticker['ticker']
        );
        $model = $model::query()->where($matches);
        $count = $model->count();
        if ($count > 0)
            return true;
        else
            return false;

    }


    /*TODO: Function below work with TVSI BOARD PRICE*/
    public function workWithTvsiBoardPrice($data) {

    }


    /*TODO: function below work with VietStock Board Price*/
    private $_vietSotckServerVersion;

    /**
     * param : url data from vietstock
     * Return array tickers
     * @param $data
     * @return array
     */
    private function getDataFromVietStock($data) {
        $jsonDecode = json_decode($data);
        Variable::$_vietSotckServerVersion = $this->_vietSotckServerVersion = $jsonDecode->serverVersion;
        $arrDataTickers = json_decode($jsonDecode->listvalue, true);
        $data = array();
        foreach ($arrDataTickers as $k => $stTicker) {
            $stTicker = $stTicker[0];
            $dataTicker = explode('|', $stTicker);
            if (count($dataTicker) != 43) continue;
            $ticker = array(
                'ticker' => $dataTicker[4],
                'refer' => $dataTicker[1],
                'ceiling' => $dataTicker[2],
                'floor' => $dataTicker[3],
                'open' => $dataTicker[32],
                'average' => $dataTicker[33],
                'high' => $dataTicker[34],
                'low' => $dataTicker[35],
                'close' => $dataTicker[13],
                'vol' => doubleval(str_replace(',', '', $dataTicker[25])),
                'nnm' => doubleval(str_replace(',', '', $dataTicker[27])),
                'nnb' => doubleval(str_replace(',', '', $dataTicker[28])),
            );

            /*Giá đóng cửa C0 sẽ là giá khớp gần nhất, nếu giá khớp gần nhất cho giá trị =0 thì giá đóng cửa = giá tham chiếu chuẩn tc* ngày hôm đó. */
            /*DO:check close price*/
            if ($ticker['close'] == 0) {
                $dc = new CheckHsdc();
                $recalReferPrice = $dc->getReferPriceCal($ticker);
                $ticker['open'] = $ticker['high'] = $ticker['low'] = $ticker['close'] = $ticker['average'] = $recalReferPrice;
//                $ticker = array(
//                    'open' => $recalReferPrice,
//                    'high' => $recalReferPrice,
//                    'low' => $recalReferPrice,
//                    'close' => $recalReferPrice,
//                    'average' => $recalReferPrice,
//                );
            }

            /*DO:check vol*/
            /*nếu Vol =0, trả giá trị O,H,L,C bằng giá tham chiếu tc**/
            if ($ticker['vol'] == 0) {
                $t = new Ticker();
                $t->setData($ticker);
                $referPriceCal = $t->getReferPriceCal();
                $ticker['open'] = $ticker['high'] = $ticker['low'] = $ticker['close'] = $ticker['average'] = $referPriceCal;
                /*$ticker = array(
                    'open' => $referPriceCal,
                    'high' => $referPriceCal,
                    'low' => $referPriceCal,
                    'close' => $referPriceCal,
                    'average' => $referPriceCal,
                );*/
            }

            $data[$ticker['ticker']] = $ticker;
        }
        return $data;
    }

    private $_TickerForeignModel;

    /**
     * @return TickerForeign
     */
    private function getTickerForeignModel() {
        if (is_null($this->_TickerForeignModel))
            $this->_TickerForeignModel = new TickerForeign();
        return $this->_TickerForeignModel;
    }

}
