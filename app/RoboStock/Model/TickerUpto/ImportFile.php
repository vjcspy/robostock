<?php
/**
 * Created by PhpStorm.
 * User: vjcspy
 * Date: 9/15/15
 * Time: 3:54 PM
 */
namespace App\RoboStock\Model\TickerUpto;

use App\TickerUpto;
use Mockery\CountValidator\Exception;

class ImportFile {
    public function importFileNotCheckData($numberOfRow = null) {
        $myfile = fopen("app/RoboStock/Model/TickerUpto/stockdata.txt", "r") or die("Unable to open file!");
// Output one line until end-of-file
        $count = 0;
        while (!feof($myfile)) {
            if ($count == 0) {
                $count += 1;
                fgets($myfile);
                continue;
            }
            $currentRow = fgets($myfile);
            $data = explode(',', $currentRow);
            if (count($data) != 10 /*|| $data[0] != 'SFN'*/)
                continue;
            try {
                $ticker = array(
                    'ticker' => $data[0],
                    'date' => $data[2],
                    'open' => $data[4],
                    'high' => $data[5],
                    'low' => $data[6],
                    'close' => $data[7],
                    'average' => $data[7],
                    'vol' => doubleval(str_replace(',', '', $data[8])),
                    'hsdc' => 1,
                );
                $count += 1;
                $this->saveToDb($ticker);
            } catch (Exception $e) {
                continue;
            }
            if (!!$numberOfRow && $count >= $numberOfRow) {
                break;
            }
        }
        fclose($myfile);
        echo 'Save ' . $count . '(s) Sucsses ';
    }

    /**
     * Add data to table ticker_upto
     * @param $ticker
     * @return static
     */
    public function saveToDb($ticker) {
        $modelTickerUpto = new TickerUpto();
        return $modelTickerUpto->create($ticker);
    }

    public function getCurrentDir() {
        echo getcwd();
    }

    public function importDataNn() {
        $myfile = fopen("../app/RoboStock/Model/TickerUpto/nndata.txt", "r") or die("Unable to open file!");
// Output one line until end-of-file
        $count = 0;
        while (!feof($myfile)) {
            if ($count == 0) {
                $count += 1;
                fgets($myfile);
                continue;
            }
            $currentRow = fgets($myfile);
            $data = explode(',', $currentRow);
            if (count($data) != 10 /*|| $data[0] != 'SFN'*/)
                continue;
            try {
                $ticker = array(
                    'ticker' => substr($data[0], 0, -3),
                    'date' => $data[2],
                    'nnb' => intval($data[4]),
                    'nnm' => intval($data[7]),
                );
                $count += 1;
//                $this->saveToDb($ticker);
            } catch (Exception $e) {
                continue;
            }
        }
        fclose($myfile);
        echo 'Save ' . $count . '(s) Sucsses ';
    }

    public function updateNnDataToDb($ticker) {

    }
}
