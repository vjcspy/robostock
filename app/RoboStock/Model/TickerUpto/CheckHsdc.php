<?php

/**
 * Created by PhpStorm.
 * User: vjcspy
 * Date: 9/16/15
 * Time: 12:45 AM
 */
namespace App\RoboStock\Model\TickerUpto;

use App\RoboStock\Model\Log\LogHelper;
use App\RoboStock\Model\Variable;
use App\RoboStock\Model\VariableDatabase\VariableDatabaseHelper;
use App\RoboStock\Model\WorkWithTime\WorkWithTime;
use App\TickerAjust;
use App\TickerUpto;

class CheckHsdc {

    private $_arrayTickerAjust;
    private $_notFoundCloseTicker;

    /**
     * MAINFUNCTION
     * TODO: MAIN FUNCTION, Return array ticker ajust and not found close ticker
     * @return mixed
     */
    public function checkReferenceStandard() {

        $tickerAdjustModel = $this->getTickerAdjustModel();
        /*DO:update or insert*/
        $first = $tickerAdjustModel->query()->where('date', WorkWithTime::getCurrentDate())->first();
        if (!is_null($first))
            return array(
                'status' => false,
                'message' => 'Hôm nay đã check điều chỉnh rồi',
            );
        /*
         * Giá tham chiếu trên sàn : tc
         * Giá tham chiếu tính tc# = (Trần + Sàn)/2  {tc# <=tc}
         * Giá tham chiếu chuẩn tc*: Nếu tc#/tc<0.99 thì tc*=tc#, else = tc
         * */
        $boardPrice = new ImportFromBoardPrice();
        $dataTicker = $boardPrice->getAllDataTickerCurrentDayFromVietStock(true, false);
        if (!$boardPrice->isExchangeInDay($dataTicker))
            return array(
                'status' => false,
                'message' => 'Sàn không giao dịch hôm nay'
            );;
//        $count = 0;
        foreach ($dataTicker as $ticker) {

//            if ($ticker['ticker'] == 'CIG') {
//            $count += 1;
//            if ($count > 100) {
//                break;
//            }
            $referCal = $this->getReferPriceCal($ticker);
            $closeLastDay = $this->getClosePriceLastDate($ticker);
            if ($closeLastDay < 0)
                return array(
                    'status' => false,
                    'message' => 'Không tìm thấy dữ liệu 30 ngày gần nhất'
                );
            if (!$closeLastDay) {
                $this->_notFoundCloseTicker[$ticker['ticker']] = $ticker;
                continue;
            }
            if ($ticker['board'] != 'upcom')
                $ticker['hsdc'] = $referCal / $closeLastDay;
            else {
                $averageLastDay = $this->getClosePriceLastDate($ticker, true);
                if ($averageLastDay < 0.0000001) {
                    $this->_notFoundCloseTicker[$ticker['ticker']] = $ticker;
                    continue;
                } else
                    $ticker['hsdc'] = $referCal / $averageLastDay;
            }
            if ($ticker['hsdc'] < 0.99) {
                if ($ticker['board'] != 'upcom')
                    $ticker['closeLastDay'] = $closeLastDay;
                else
                    $ticker['averageLastDay'] = $closeLastDay;
                $ticker['tc*'] = $referCal;
                $this->_arrayTickerAjust[$ticker['ticker']] = $ticker;
            }
//            }
        }
        $this->updateTickerAdjustToTable($this->_arrayTickerAjust);
        VariableDatabaseHelper::updateVariable('data_checkDc', WorkWithTime::getCurrentDate());
        LogHelper::addLog(json_encode($this->_notFoundCloseTicker), 'notFoundClose.log', 'error');
        return array(
            'status' => true,
            'tickerAjust' => $this->_arrayTickerAjust,
            'notFoundClose' => $this->_notFoundCloseTicker
        );
    }

    /**
     * TODO: sau khi check hsdc thi dung ham nay de them vao table
     * @param $dataTickers
     */
    private function updateTickerAdjustToTable($dataTickers) {
        if (is_null($dataTickers))
            return;
        foreach ($dataTickers as $ticker) {
            $tickerAdjustModel = $this->getTickerAdjustModel();
            /*DO:update or insert*/
            $first = $tickerAdjustModel->query()->where('ticker', $ticker['ticker'])->where('date', WorkWithTime::getCurrentDate())->first();
            if (!$first) {
                $tickerAdjustModel = $this->getTickerAdjustModel();
                $ticker = $tickerAdjustModel->firstOrNew(array(
                    'ticker' => $ticker['ticker'],
                    'date' => WorkWithTime::getCurrentDate(),
                    'hsdc' => floatval($ticker['hsdc']),
                    'board' => $ticker['board'],
                    'active' => 0
                ));
                $ticker->save();
            }
        }
    }


    public function getDataAjustTicker($ticker) {
        //need write
    }

    /**
     * MAINFUNCTION
     * TODO: lấy giá TC*
     * @param $ticker
     * @return float
     */
    public function getReferPriceCal($ticker) {
        $tickSharp = ($ticker['ceiling'] + $ticker['floor']) / 2;
        if ($tickSharp / $ticker['refer'] < .99) {
            return $tickSharp;
        } else {
            return $ticker['refer'];
        }
    }

//    public static $_numOfDayToCheckLastDay = 30;

    /**
     * MAINFUNCTION
     * TODO:get close price last day
     * RETURN: trả về giá close hoặc giá trung bình nếu param average = true của cổ phiếu
     * @param $ticker
     * @param bool $average
     * @return int
     */
    public function getClosePriceLastDate($ticker, $average = false) {
        $count = 0;
        $currentDate = WorkWithTime::getCurrentDate();
        do {
            $count += 1;
            if ($count > Variable::$_numOfDayToCheckLastDay && is_null(CheckHsdc::$_allTickerLastDay)) {
                /*DO:truong hop nay la san dung giao dich tren 30 ngay*/
                return -1;
            }
            $lastTicker = $this->getTickerLastDate($ticker, $currentDate);
            /*DO:khi không tìm thấy giá close mà đã tìm thấy all ticker last day rồi thi do không có dữ liệu của cp đấy*/
            if ($lastTicker === null && is_null(CheckHsdc::$_allTickerLastDay)) {
                $flag = true;
                $currentDate = WorkWithTime::last_day($currentDate);
            } else
                $flag = false;

        } while ($flag === true);
        return $average == true ? $lastTicker[$ticker['ticker']]['average'] : $lastTicker[$ticker['ticker']]['close'];

    }

    public static $_allTickerLastDay;

    /**
     * TODO: Dung trong vong lap giam ngay
     * @param $ticker
     * @param $date
     * @return null||array
     */
//    public static $_numOfTickerToCheckLastDay = 500;

    public function getTickerLastDate($ticker = null, $date = null) {
        if (is_null($date))
            $date = WorkWithTime::getCurrentDate();
        if (is_null(CheckHsdc::$_allTickerLastDay)) {
            $model = new TickerUpto();
            $lastDate = WorkWithTime::last_day($date);
            $matches = array(
                'date' => $lastDate,
            );
            $items = $model->query()->where($matches);
            $count = $items->count();
            if ($count > Variable::$_numOfTickerToCheckLastDay) {
                foreach ($items->get() as $tickerS) {
                    $item = array(
                        'ticker' => $tickerS->ticker,
                        'close' => $tickerS->close,
                        'average' => $tickerS->average,
                    );
                    CheckHsdc::$_allTickerLastDay[$item['ticker']] = $item;
                }
            } else
                return null;
        }
        if ($ticker != null & isset(CheckHsdc::$_allTickerLastDay[$ticker['ticker']]))
            return CheckHsdc::$_allTickerLastDay;
        else
            return null;
    }

    /**
     * MAINFUNCTION
     *TODO: điều chỉnh lại giá trong bảng upto theo bảng ticker_adjust
     */
    public function ajustAllTickerFromTickerAdjustTable() {
        $tickerAjustModel = new TickerAjust();
        $allTickerAdjust = $tickerAjustModel->query()->where('active', '<>', '1')->get();
        foreach ($allTickerAdjust as $ticker) {
            $this->ajustTicker($ticker);
        }
        VariableDatabaseHelper::updateVariable('data_dieuchinh', WorkWithTime::getCurrentDate());
    }

    /**
     * IMPORTANCE: only use date from ticker_adjust table
     * TODO: update ticker adjust - data get from table ticker_adjust
     * @param $ticker
     */
    public function ajustTicker($ticker) {
        $model = $this->getTickerUptoModel();
        $tickers = $model->query()->where('ticker', $ticker['ticker'])->where('date', '<', $ticker['date'])->get();
        foreach ($tickers as $tickerNew) {
            $m = $this->getTickerUptoModel();
            $matches = array(
                'ticker' => $tickerNew->ticker,
                'date' => $tickerNew->date,
            );
            $m->query()->where($matches)->update(array(
                'open' => round($tickerNew->open * $ticker['hsdc'], 1),
                'close' => round($tickerNew->close * $ticker['hsdc'], 1),
                'high' => round($tickerNew->high * $ticker['hsdc'], 1),
                'low' => round($tickerNew->low * $ticker['hsdc'], 1),
            ));
        }
        $tickerAdjustModel = $this->getTickerAdjustModel();
        $tickerAdjustModel->query()->where('ticker', $ticker['ticker'])->where('date', $ticker['date'])->update(array('active' => '1'));
        return $ticker;
    }

    private $_tickerAdjustModel;

    /**
     * @return TickerAjust
     */
    private function getTickerAdjustModel() {
        if (is_null($this->_tickerAdjustModel))
            $this->_tickerAdjustModel = new TickerAjust();
        return $this->_tickerAdjustModel;
    }

    public function testDate() {
        $model = $this->getTickerUptoModel();
        $tickers = $model->query()->where('ticker', 'MBB')->where('date', '>', '20150912')->get();
        $data = array();
        foreach ($tickers as $ticker) {
            $data[] = round($ticker->close * 0.932454353453524);
        }
        return $data;
    }

    private $_tickerUptoModel;

    private function getTickerUptoModel() {
        if (is_null($this->_tickerUptoModel)) {
            $this->_tickerUptoModel = new TickerUpto();
        }
        return $this->_tickerUptoModel;
    }
}
