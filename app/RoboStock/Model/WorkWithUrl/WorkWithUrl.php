<?php
/**
 * Created by PhpStorm.
 * User: vjcspy
 * Date: 9/15/15
 * Time: 9:46 PM
 */
namespace App\RoboStock\Model\WorkWithUrl;
class WorkWithUrl {
    public function getDataFromUrl($url) {
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $result = curl_exec($ch);

        curl_close($ch);

        return ($result);
    }

    public function getDataFromVietSockUs($url){

        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_COOKIE, "finance_lang=en-US");
        $result = curl_exec($ch);

        curl_close($ch);

        return ($result);
    }
}
