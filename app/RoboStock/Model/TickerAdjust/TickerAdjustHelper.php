<?php
/**
 * Created by IntelliJ IDEA.
 * User: vjcspy
 * Date: 9/25/15
 * Time: 10:52 AM
 */

namespace App\RoboStock\Model\TickerAdjust;


use App\RoboStock\Model\WorkWithTime\WorkWithTime;
use App\TickerAjust;

class TickerAdjustHelper {

    private static $_allArrayTickerAdjustCurrentDate;

    private static function getAllTickerAvailableAdjustCurrentDay($reCollect = false) {
        if (is_null(TickerAdjustHelper::$_allArrayTickerAdjustCurrentDate) || $reCollect == true) {
            $model = TickerAdjustHelper::getTickerAdjustModel();
            $all = $model->query()->where('date', WorkWithTime::getCurrentDate())->where('active', '0')->get();
            TickerAdjustHelper::$_allArrayTickerAdjustCurrentDate = array();
            foreach ($all as $tickerAdjust) {
                TickerAdjustHelper::$_allArrayTickerAdjustCurrentDate[$tickerAdjust->ticker] = array(
                    'hsdc' => $tickerAdjust->hsdc,
                );
            }
        }
        return TickerAdjustHelper::$_allArrayTickerAdjustCurrentDate;
    }

    private static $_TickerAdjustModel;

    /**
     * FIXME: SAU NÀY CẦN LƯU DƯỚI DẠNG CACHED
     * @return mixed
     */
    private static function getTickerAdjustModel() {
        if (is_null(TickerAdjustHelper::$_TickerAdjustModel))
            TickerAdjustHelper::$_TickerAdjustModel = new TickerAjust();
        return TickerAdjustHelper::$_TickerAdjustModel;
    }

    /**
     * MAINFUNCTION
     * TODO: get hsdc by name
     * @param $tickerName
     * @param bool|false $reCollect
     * @return int
     */
    public static function getHsdcByName($tickerName, $reCollect = false) {
        TickerAdjustHelper::getAllTickerAvailableAdjustCurrentDay($reCollect);
        if (isset(TickerAdjustHelper::$_allArrayTickerAdjustCurrentDate[$tickerName]))
            return TickerAdjustHelper::$_allArrayTickerAdjustCurrentDate[$tickerName]['hsdc'];
        else
            return 1;
    }


    /**MAINFUNCTION
     * Trả về mảng các cổ phiếu điều chỉnh kể ngày được truyền vào và các ngày trước đó dựa vào biến numOfday
     * Nếu không truyền vào ngày cần lấy thì sẽ lấy ngày hiện tại
     * @param null $currentDate
     * @param $numOfDay
     * @return array
     */
    public function getArrayTickersAdjust($currentDate = null, $numOfDay) {
        if (is_null($currentDate))
            $currentDate = WorkWithTime::getCurrentDate();
        $tickerAdjustModel = $this->getTickerAdjustModel();
        $date = $currentDate;
        $c = 0;
        $arrayTickers = array();
        do {
            $c += 1;
            $allTickerAdj = $tickerAdjustModel->query()->where('date', $date)->get();
            if ($allTickerAdj->count() > 0) {
                foreach ($allTickerAdj as $item) {
                    $arrayTickers[] = $item->ticker;
                }
            }
            $date = WorkWithTime::last_day($date);
        } while ($c < $numOfDay);
        return $arrayTickers;
    }

}
