<?php
/**
 * Created by IntelliJ IDEA.
 * User: vjcspy
 * Date: 10/1/15
 * Time: 10:43 AM
 */

namespace App\RoboStock\Model\Log;


use App\LogWork;
use App\RoboStock\Model\WorkWithTime\WorkWithTime;
use Monolog\Handler\StreamHandler;
use Monolog\Logger;

class LogHelper {

    public static function addLog($s, $fileName = "robostock.log", $level = null) {
        date_default_timezone_set('Etc/GMT-7');
        $log = new Logger('Robostock');
        $log->pushHandler(new StreamHandler('./log/' . $fileName, Logger::WARNING));

        switch ($level) {
            case 'error':
                $log->addError($s);
                break;
            default:
                $log->addWarning($s);
                break;
        }
    }

    public function addLogDb($work, $data, $date = null) {
        if (is_null($date))
            $date = WorkWithTime::getCurrentDate();
        $model = $this->getLogWorkModel();
        $data = array(
            'date' => $date,
            'work' => $work,
            'data' => $data
        );
        $model->create($data);

    }

    private $_LogWorkModel;

    /**
     * @return LogWork
     */
    private function getLogWorkModel() {
        if (is_null($this->_LogWorkModel))
            $this->_LogWorkModel = new LogWork();
        return $this->_LogWorkModel;
    }
}
