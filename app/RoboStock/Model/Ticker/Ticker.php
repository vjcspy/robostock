<?php
/**
 * Created by IntelliJ IDEA.
 * User: vjcspy
 * Date: 9/18/15
 * Time: 10:10 PM
 */

namespace App\RoboStock\Model\Ticker;


/**
 * Class Ticker
 * @package App\RoboStock\Model\Ticker
 */
class Ticker {
    /**
     * data cua bien ticker :
     * $ticker = array(
     * 'ticker' => $dataTicker[4],
     * 'refer' => $dataTicker[1],
     * 'ceiling' => $dataTicker[2],
     * 'floor' => $dataTicker[3],
     * 'open' => $dataTicker[32],
     * 'high' => $dataTicker[34],
     * 'low' => $dataTicker[35],
     * 'close' => $dataTicker[13],
     * 'vol' => $dataTicker[25],
     * );
     *
     * @var array
     */
    protected $_data = array();

    /**
     * @return array
     */
    public function getData() {
        return $this->_data;
    }

    /**
     * @param array $data
     */
    public function setData($data) {
        $this->_data = $data;
    }

    /**
     * TODO: get tc* ticker
     * @param null $ticker
     * @return bool|float
     */
    public function getReferPriceCal($ticker = null) {
        if (is_null($ticker))
            $ticker = $this->getData();
        if (is_null($ticker))
            return false;
        if (!isset($ticker['ceiling']) || !isset($ticker['floor']) || !isset($ticker['refer']))
            return false;
        $tickSharp = ($ticker['ceiling'] + $ticker['floor']) / 2;
        if ($tickSharp / $ticker['refer'] < .99) {
            return $tickSharp;
        } else {
            return $ticker['refer'];
        }
    }







}
