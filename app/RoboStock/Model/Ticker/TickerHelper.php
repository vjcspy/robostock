<?php
/**
 * Created by IntelliJ IDEA.
 * User: vjcspy
 * Date: 9/30/15
 * Time: 10:39 AM
 */

namespace App\RoboStock\Model\Ticker;


use App\RoboStock\Model\TickerUpto\ImportFromBoardPrice;
use App\RoboStock\Model\WorkWithTime\WorkWithTime;
use App\TickerInDay;
use App\TickerLastDay;
use App\TickerUpto;

class TickerHelper {
    private $_temp;

    private $_arrayModel; /*IMPORTANCE Save model by dates*/

    private $_arrayTickerByNumOfTrade;
    private $_currentNumOfTrade;

    /**
     * TickerHelper constructor.
     */
    public function __construct() {
        $this->_arrayTickerByNumOfTrade = array();
        $this->_arrayModel = array();
    }

    public function getRateOfChangeInDayByArrayTicker(array $tickers) {
        $lastDayModel = $this->getTickerLastDayModel();
        $collectionLastDay = $lastDayModel->query()->whereIn('ticker', $tickers)->get();

        $innDayModel = $this->getTickerInDayModel();
        $collectionInDay = $innDayModel->query()->whereIn('ticker', $tickers)->get();

        $allTickerInDay = array();
        $allTickerLastDay = array();

        foreach ($collectionInDay as $item) {
            $allTickerInDay[$item->ticker] = $item->close;
        }
        foreach ($collectionLastDay as $item) {
            $allTickerLastDay[$item->ticker] = $item->close;
        }

        $arrayRateOfChange = array();
        foreach ($allTickerInDay as $key => $item) {
            if (isset($allTickerLastDay[$key]))
                $arrayRateOfChange[$key] = array(
                    'close' => $item,
                    'roc' => round(($item / $allTickerLastDay[$key] - 1), 2)
                );
            else
                $arrayRateOfChange[$key] = array(
                    'close' => $item,
                    'roc' => 0
                );
        }
        return $arrayRateOfChange;
    }


    /**MAINFUNCTION
     * TODO: get array ticker theo so phien giao dich. Lay tat ca cac phien
     * IMPORTANCE: Ket qua tra ve khong lay phien hien tai
     * @param $numOfTrade
     * @param $currentDate
     * @param bool $includeNotChange
     * @return
     */
    public function getArrayTickerByNumOfTrade($numOfTrade, $currentDate, $includeNotChange = false, $exclNumOfTrade = 0) {
        $this->getKeyByDateAndNumOfTrade($numOfTrade, $currentDate);
        if (!isset($this->_arrayTickerByNumOfTrade[$this->_keyByDateAndNumOfTrade])) {
            $this->_currentNumOfTrade = $numOfTrade;
            $this->_arrayTickerByNumOfTrade[$this->_keyByDateAndNumOfTrade] = array();
            $lastDate = $currentDate;
            $this->_temp = 0;
            $countExcl = 0;
            do {
                $lastDate = WorkWithTime::last_day($lastDate);
                $model = $this->getTickerUptoModel()->query()->where('date', $lastDate);
                if ($includeNotChange) {
                    $countExcl += 1;
                    $this->_temp += 1;
                    /*DO: Cho tinh UPTREND*/
                    if ($countExcl <= $exclNumOfTrade)
                        continue;
                }
                if ($model->count() > 0) {
                    if (!$includeNotChange) {
                        $countExcl += 1;
                        $this->_temp += 1;
                        /*DO: Cho tinh UPTREND*/
                        if ($countExcl <= $exclNumOfTrade)
                            continue;
                    }
                    if (!isset($this->_arrayModel[$lastDate]))
                        $this->_arrayModel[$lastDate] = $model->get();
                    $tickers = $this->_arrayModel[$lastDate];
                    foreach ($tickers as $ticker) {
                        if (isset($this->_arrayTickerByNumOfTrade[$this->_keyByDateAndNumOfTrade][$ticker->ticker])) {
                            $this->_arrayTickerByNumOfTrade[$this->_keyByDateAndNumOfTrade][$ticker->ticker][$this->_temp] = array(
                                'close' => $ticker->close,
                                'vol' => $ticker->vol
                            );
                        } else {
                            $this->_arrayTickerByNumOfTrade[$this->_keyByDateAndNumOfTrade][$ticker->ticker] = array();
                            $this->_arrayTickerByNumOfTrade[$this->_keyByDateAndNumOfTrade][$ticker->ticker][$this->_temp] = array(
                                'close' => $ticker->close,
                                'vol' => $ticker->vol
                            );
                        }
                    }
                }
            } while ($this->_temp < $numOfTrade);
        }
        return $this->_arrayTickerByNumOfTrade[$this->_keyByDateAndNumOfTrade];
    }

    /**MAINFUNCTION
     * TODO: get min max by numOfTrade and currentDate
     * Return: Array
     * @param $tickerName
     * @param $numOftrade
     * @param $currentDate
     * @return array
     */
    public function getMinMaxPriceTickerInNumOfTrade($tickerName, $numOftrade, $currentDate, $includeNotChange = false, $exclNumOfTrade = 0) {
        if (isset($tickerName['ticker']))
            $tickerName = $tickerName['ticker'];


        $this->getKeyByDateAndNumOfTrade($numOftrade, $currentDate);
        if (!isset($this->_arrayTickerByNumOfTrade[$this->_keyByDateAndNumOfTrade])) {
            $this->getArrayTickerByNumOfTrade($numOftrade, $currentDate, false, $exclNumOfTrade);
        }

        $arrayPrice = array();
        if (!isset($this->getArrayTickerByNumOfTrade($numOftrade, $currentDate, false, $exclNumOfTrade)[$tickerName]))
            return array(
                'ticker' => $tickerName,
                'numOfTrade' => $numOftrade,
                'currentDate' => $currentDate,
                'max' => 99999,
                'min' => -99999
            );
        foreach ($this->getArrayTickerByNumOfTrade($numOftrade, $currentDate, false, $exclNumOfTrade)[$tickerName] as $temp => $ticker) {
            $arrayPrice[] = $ticker['close'];
        }

        $max = max($arrayPrice);
        $min = min($arrayPrice);
        return array(
            /*GIAM DATA*/
            //            'ticker' => $tickerName,
            //            'numOfTrade' => $numOftrade,
            //            'currentDate' => $currentDate,
            'max' => $max,
            'min' => $min,
        );
    }


    private $_arrayTickerBySma;

    /**MAINFUNCTION
     * TODO: get Array SMA all ticker
     * @param $sma
     * @param $currentDate
     * @return array
     */
    public function getSMA($sma, $currentDate) {
        $this->getKeyBySmaAndDate($sma, $currentDate);
        if (!isset($this->_arrayTickerBySma[$this->_keyBySmaAndDate])) {
            $this->_arrayTickerBySma[$this->_keyBySmaAndDate] = array();
            $lastDate = $currentDate;
            $this->_temp = 0;
            do {
                $lastDate = WorkWithTime::last_day($lastDate);
                $model = $this->getTickerUptoModel()->query()->where('date', $lastDate);
                if ($model->count() > 0) {
                    $this->_temp += 1;
                    if (!isset($this->_arrayModel[$lastDate]))
                        $this->_arrayModel[$lastDate] = $model->get();
                    $tickers = $this->_arrayModel[$lastDate];
                    foreach ($tickers as $ticker) {
                        if (isset($this->_arrayTickerBySma[$this->_keyBySmaAndDate][$ticker->ticker])) {
                            $this->_arrayTickerBySma[$this->_keyBySmaAndDate][$ticker->ticker][$this->_temp] = array(
                                'close' => $ticker->close,
                                //                                'vol' => $ticker->vol
                            );
                        } else {
                            $this->_arrayTickerBySma[$this->_keyBySmaAndDate][$ticker->ticker] = array();
                            $this->_arrayTickerBySma[$this->_keyBySmaAndDate][$ticker->ticker][$this->_temp] = array(
                                'close' => $ticker->close,
                                //                                'vol' => $ticker->vol
                            );
                        }
                    }
                }
            } while ($this->_temp < $sma);
        }

        $arraySma = array();
        foreach ($this->_arrayTickerBySma[$this->_keyBySmaAndDate] as $tickerName => $arrayPriceTicker) {
            $currentTotalPrice = 0;
            foreach ($arrayPriceTicker as $price) {
                $currentTotalPrice += $price['close'];
            }
            $arraySma[$tickerName] = $currentTotalPrice / $sma;
        }

        return $arraySma;
    }

    private $_arrayTickerLastDay;

    /**MAINFUNCTION
     * TODO: get array ticker in last day. Trả về duy nhất ngày hôm đấy
     * @param $numOfDay
     * @return mixed
     */
    public function getArrayTickerLastDay($numOfDay) {
        if (!isset($this->_arrayTickerLastDay[$numOfDay])) {
            $this->_arrayTickerLastDay[$numOfDay] = array();
            /*FIXME: Wrong if current date not exchange*/
            $lastDate = WorkWithTime::getCurrentDate();
            $this->_temp = 0;
            do {
                $lastDate = WorkWithTime::last_day($lastDate);
                $model = $this->getTickerUptoModel()->query()->where('date', $lastDate);
                if ($model->count() > 0) {
                    $this->_temp += 1;
                    if ($this->_temp == $numOfDay) {
                        if (!isset($this->_arrayModel[$lastDate]))
                            $this->_arrayModel[$lastDate] = $model->get();
                        $tickers = $this->_arrayModel[$lastDate];
                        foreach ($tickers as $ticker) {
                            if (isset($this->_arrayTickerLastDay[$numOfDay][$ticker->ticker])) {
                                $this->_arrayTickerLastDay[$numOfDay][$ticker->ticker][$this->_temp] = array(
                                    'close' => $ticker->close,
                                    //                                    'vol' => $ticker->vol
                                );
                            } else {
                                $this->_arrayTickerLastDay[$numOfDay][$ticker->ticker] = array();
                                $this->_arrayTickerLastDay[$numOfDay][$ticker->ticker][$this->_temp] = array(
                                    'close' => $ticker->close,
                                    //                                    'vol' => $ticker->vol
                                );
                            }
                        }
                    }
                }
            } while ($this->_temp < $numOfDay);
        }

        return $this->_arrayTickerLastDay[$numOfDay];
    }


    /**TODO: get all ticker from viet stock
     * @return ImportFromBoardPrice
     */
    public function getAllTickerFromVietStock() {
        $board = $this->getImportFromBoardPriceModel();
        $board = $board->getAllDataTickerCurrentDayFromVietStock(false, false);
        return $board;
    }

    public function getRankExchange($day, $currentDay) {
        $tickerUptoModel = $this->getTickerUptoModel();
        $count = 0;
        if (is_null($currentDay))
            $currentDay = WorkWithTime::getCurrentDate();
        $lastDay = $currentDay;
        $arrayDay = array();
        do {
            $lastDay = WorkWithTime::last_day($lastDay);
            $allTicker = $tickerUptoModel->query()->where('date', $lastDay);
            if ($allTicker->count() > 10) {
                $count += 1;
                $arrayDay[] = $lastDay;
            }
        } while ($lastDay != $day && $count < 80);

        return array(
            'count' => $count,
            'day' => $arrayDay
        );
    }


    private $_keyByDateAndNumOfTrade;

    private function getKeyByDateAndNumOfTrade($numOfTrade, $date) {
        $this->_keyByDateAndNumOfTrade = $numOfTrade . $date;
        return $this->_keyByDateAndNumOfTrade;
    }

    private $_keyBySmaAndDate;

    private function getKeyBySmaAndDate($sma, $date) {
        return $this->_keyBySmaAndDate = $sma . $date;
    }

    private $_TickerUptoModel;

    /**
     * @return TickerUpto
     */
    private function getTickerUptoModel() {
        if (is_null($this->_TickerUptoModel))
            $this->_TickerUptoModel = new TickerUpto();
        return $this->_TickerUptoModel;
    }

    private $_ImportFromBoardPriceModel;

    /**
     * @return ImportFromBoardPrice
     */
    private function getImportFromBoardPriceModel() {
        if (is_null($this->_ImportFromBoardPriceModel))
            $this->_ImportFromBoardPriceModel = new ImportFromBoardPrice();
        return $this->_ImportFromBoardPriceModel;
    }

    private $_TickerLastDayModel;

    /**
     * @return TickerLastDay
     */
    private function getTickerLastDayModel() {
        if (is_null($this->_TickerLastDayModel))
            $this->_TickerLastDayModel = new TickerLastDay();
        return $this->_TickerLastDayModel;
    }

    private $_TickerInDayModel;

    /**
     * @return TickerInDay
     */
    private function getTickerInDayModel() {
        if (is_null($this->_TickerInDayModel))
            $this->_TickerInDayModel = new TickerInDay();
        return $this->_TickerInDayModel;
    }
}
