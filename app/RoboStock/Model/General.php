<?php
/**
 * Created by IntelliJ IDEA.
 * User: vjcspy
 * Date: 10/3/15
 * Time: 1:19 PM
 */

namespace App\RoboStock\Model;

use App\CacheNextDay;
use App\IndexUpto;
use App\RoboStock\Model\IndexUpto\IndexUptoHelper;
use App\RoboStock\Model\Log\LogHelper;
use App\RoboStock\Model\Overview\MarketOverview\CashFlowHelper;
use App\RoboStock\Model\Overview\MarketOverview\MarketOverviewHelper;
use App\RoboStock\Model\Ticker\TickerHelper;
use App\RoboStock\Model\TickerBoard\TickerBoardHelper;
use App\RoboStock\Model\TickerClass\TickerClassHelper;
use App\RoboStock\Model\TickerLastDay\TickerLastDayHelper;
use App\RoboStock\Model\TickerUpto\CheckHsdc;
use App\RoboStock\Model\TickerUpto\ImportFromBoardPrice;
use App\RoboStock\Model\VariableDatabase\VariableDatabaseHelper;
use App\RoboStock\Model\WorkWithTime\WorkWithTime;
use App\TickerBoard;
use App\TickerClass;
use App\TickerForeign;
use App\TickerLastDay;
use App\TickerUpto;
use Illuminate\Support\Facades\Cache;

class General {
    /**
     * @var \Modules\Admin\Entities\LogWork
     */
    protected $_logWork;
    /**
     * @var \Modules\Admin\Entities\LogWorking
     */
    protected $_logWorking;

    /**
     * General constructor.
     */
    public function __construct() {
        $this->_logWork = app()->make('\Modules\Admin\Entities\LogWork');
        $this->_logWorking = app()->make('\Modules\Admin\Entities\LogWorking');
    }

    /**
     *TODO: update từ vietstock vào databse. cu 10s 1 lan
     */
    public function updateTickerInday() {
        $time_start = microtime(true);
        $import = new ImportFromBoardPrice();
        $array = $import->getAllDataTickerCurrentDayFromVietStock(false, true);
        $time_end = microtime(true);
        //dividing with 60 will give the execution time in minutes other wise seconds
        $execution_time = ($time_end - $time_start);

        //execution time of the script
        echo 'Total Execution Time:' . $execution_time . "(s)\n";
        echo('Num Of ticker updated: ' . count($array));
    }


    /**
     *TODO: update co phieu o bang nao. Hang ngay. Dau ngay
     */
    public function updateTickerBoard() {
        $this->_logWorking->updateWorking('checkBoard', 'Check Tickers Board', 1);

        $time_start = microtime(true);
        $tickerBoard = new TickerBoardHelper();
        $arrayChange = $tickerBoard->importDataFromVietStockBoard();
        $time_end = microtime(true);
        //dividing with 60 will give the execution time in minutes other wise seconds
        $execution_time = ($time_end - $time_start);

        $this->_logWork->addLog('checkBoard', 'Total Execution Time:' . $execution_time / 60 . "(minute)\n" . json_encode($arrayChange));
        $this->_logWorking->updateWorking('checkBoard', 'Check Tickers Board', 0);
    }

    /**
     *TODO: update ticker class
     */
    public function updateTickerClass() {
        $tickerClass = new TickerClassHelper();
        $tickerClass->importFromFile();
    }

    /**
     *TODO: tao cache ticker last day. Hang ngay
     */
    public function initTickerLastDay($checkExchange = true, $lastDate = null) {
        $this->_logWorking->updateWorking('initTickerLastDay', 'Create cache ticker last day', 1);

        $time_start = microtime(true);
        $tickerLastDay = new TickerLastDayHelper();
        $time_end = microtime(true);
        //dividing with 60 will give the execution time in minutes other wise seconds
        $execution_time = ($time_end - $time_start);
        $data = $tickerLastDay->initTickerLastDay($checkExchange, $lastDate);

        $this->_logWork->addLog('initTickerLastDay', 'Total Execution Time:' . $execution_time / 60 . "(minute)\n" . json_encode($data));
        $this->_logWorking->updateWorking('initTickerLastDay', 'Create cache ticker last day', 0);
    }

    /**
     *TODO: TAO Cache index hang ngay. cu 5 phut 1 lan
     */
    public function updateIndexUpto() {
        $index = new IndexUptoHelper();
        $index->getDataFromCafeF();
    }


    /**TODO: tao cache de tinh toan bang plcs nextday
     * @return bool
     */
    public function initCachePlcsNextDay($nextDate = null) {
        $this->_logWorking->updateWorking('initCachePLCS', 'Create cache PLCS', 1);

        $time_start = microtime(true);
        $nextDay = new MarketOverviewHelper();
        $date = $nextDay->createCacheNextDay($nextDate);
        $time_end = microtime(true);
        //dividing with 60 will give the execution time in minutes other wise seconds
        $execution_time = ($time_end - $time_start);

        $this->_logWork->addLog('Init PLCS', 'Total Execution Time:' . $execution_time / 60 . "(minute)");
        $this->_logWorking->updateWorking('initCachePLCS', 'Create cache PLCS', 0);
        return $date;
    }

    /**
     *TODO: check Dieu chinh chay 8h50phut hang ngay
     */
    public function checkHsdc() {
        $time_start = microtime(true);
        $hsdc = new CheckHsdc();
        $data = $hsdc->checkReferenceStandard();
        $time_end = microtime(true);
        //dividing with 60 will give the execution time in minutes other wise seconds
        $execution_time = ($time_end - $time_start);
        $logDb = new LogHelper();
        $logDb->addLogDb('checkDc', "Excution Time" . $execution_time . "\n" . json_encode($data));
    }

    public function updateTickerUpto() {
        $time_start = microtime(true);
        $importFromBoard = new ImportFromBoardPrice();
        $data = $importFromBoard->updateTickerFromVietStock();
        $time_end = microtime(true);
        $execution_time = ($time_end - $time_start);
        $logDb = new LogHelper();
        $logDb->addLogDb('updateTickerUpto', "Excution Time" . $execution_time . "\n" . json_encode($data));
    }

    /**TODO: Tạo cache dữ liệu view:marketoverview
     */
    public function initDataMarketOverview($refresh = false) {
        $this->_logWorking->updateWorking('initDataMarketOverview', 'Create cache Market Overview', 1);
        if ($refresh) {
            Cache::forget('dataTickerBoard');
            Cache::forget('dataTickerClass');
            Cache::forget('dataTickerLastDay');
            Cache::forget('dataPennyTwoDayAgo');
            Cache::forget('dataPlcsNextDay');
            /*cashflow*/
            Cache::forget('dataTickerCashFlow');
            /*TrendData*/
            Cache::forget('dataTickerTrendHighLow');
            Cache::forget('dataTickerTrendSMA');
            Cache::forget('dataTickerTrendUptrend');
            Cache::forget('dataTickerTrendRs');
        }


        if (!Cache::has('dataTickerBoard')) {
            /*FIXME: Cái này sẽ lỗi trong trường hợp mà ngày hôm đấy đổi board. Cần sửa bằng cách xóa cache lúc check board*/
            $tickerBoardModel = $this->getTickerBoardModel()->all();
            $arrayTickerBoard = array();
            foreach ($tickerBoardModel as $ticker) {
                $arrayTickerBoard[$ticker->ticker] = array(
                    'ticker' => $ticker->ticker,
                    'board' => $ticker->board
                );
            }

            Cache::put('dataTickerBoard', json_encode($arrayTickerBoard), WorkWithTime::getRemainTime());
        }

        if (!Cache::has('dataTickerClass')) {
            $tickerClass = $this->getTickerClassModel()->all();
            $arrayTickerClass = array();
            foreach ($tickerClass as $ticker) {
                $arrayTickerClass[$ticker->ticker] = array(
                    'ticker' => $ticker->ticker,
                    'top' => $ticker->top,
                    'bp' => $ticker->bp,
                    'cd' => $ticker->cd,
                );
            }

            Cache::put('dataTickerClass', json_encode($arrayTickerClass), WorkWithTime::getRemainTime());
        }

        if (!Cache::has('dataTickerLastDay')) {
            $tickerClass = Cache::get('dataTickerClass');
            $arrayTickerClass = json_decode($tickerClass, true);
            $tickerLastDays = $this->getTickerLastDayModel()->all();
            $arrayTickerLastDay = array();
            foreach ($tickerLastDays as $ticker) {
                if (isset($arrayTickerClass[$ticker->ticker]) && ($arrayTickerClass[$ticker->ticker]['top'] == "top1" || $arrayTickerClass[$ticker->ticker]['top'] == "top2"))
                    $arrayTickerLastDay[$ticker->ticker] = array(
                        'ticker' => $ticker->ticker,
                        'close' => $ticker->close,
                        'vol' => $ticker->vol
                    );
                else
                    $arrayTickerLastDay[$ticker->ticker] = array(
                        'ticker' => $ticker->ticker,
                        'close' => $ticker->close,
                    );
            }

            Cache::put('dataTickerLastDay', json_encode($arrayTickerLastDay), WorkWithTime::getRemainTime());
        }

        if (!Cache::has('dataPennyTwoDayAgo')) {
            $tickerClass = Cache::get('dataTickerClass');
            $arrayTickerClass = json_decode($tickerClass, true);
            $tickerHelper = new TickerHelper();
            $tickerTwoDayAgo = $tickerHelper->getArrayTickerLastDay(2);
            $arrayPennyTwoDayAgo = array();
            foreach ($tickerTwoDayAgo as $ticker => $temp) {
                if (isset($arrayTickerClass[$ticker]) && $arrayTickerClass[$ticker]['bp'] == "penny") {
                    $arrayPennyTwoDayAgo[$ticker] = $temp['2']['close'];
                }
            }
            Cache::put('dataPennyTwoDayAgo', json_encode($arrayPennyTwoDayAgo), WorkWithTime::getRemainTime());
        }

        //DO: get cache PLCS next day
        if (!Cache::has('dataPlcsNextDay')) {
            $cacheNextDay = $this->getCacheNextDayModel()->query()->where('date', WorkWithTime::getCurrentDate())->where('entity', 'plcs')->first();
            $arrayPlcsNextDay = $cacheNextDay->data;
            Cache::put('dataPlcsNextDay', $arrayPlcsNextDay, WorkWithTime::getRemainTime());
        }
        /*DO: get cache cashflow*/
        if (!Cache::has('dataTickerCashFlow')) {
            $lastDate = VariableDatabaseHelper::getVariable('data_lastDay');
            $cacheNextDay = $this->getCacheNextDayModel()->query()->where('entity', 'cash_flow')->orderBy('id', 'desc')->first();
            $arrayCashFlow = $cacheNextDay->data;
            Cache::put('dataTickerCashFlow', $arrayCashFlow, WorkWithTime::getRemainTime());
        }

//        if (!Cache::has('dataPlcsNextDay')) {
//            $cacheNextDay = $this->getCacheNextDayModel()->query()->where('date', WorkWithTime::getCurrentDate())->where('entity', 'plcs')->first();
//            $arrayPlcsNextDay = $cacheNextDay->data;
//            Cache::put('dataPlcsNextDay', $arrayPlcsNextDay, WorkWithTime::getRemainTime());
//        }


        if (!Cache::has('dataTickerTrendHighLow')) {
            /*DO: Trend Data*/
            $tickerClass = Cache::get('dataTickerClass');

            $tickerHelper = new TickerHelper();

            $arrayHighLow = array();
            $arrayHighLow['1M'] = array();
            $arrayHighLow['3M'] = array();
            $arrayHighLow['6M'] = array();

            $arraySMA = array();
            $arraySMA['20'] = array();
            $arraySMA['50'] = array();
            $arraySMA['100'] = array();

            $arrayUpTrend = array();


            $arrayTickerClass = json_decode($tickerClass, true);
            foreach ($arrayTickerClass as $ticker) {
                if ($ticker['top'] != 'top1' && $ticker['top'] != 'top2')
                    continue;

                /*DO: hight low*/
                $arrayHighLow['1M'][$ticker['ticker']] = $tickerHelper->getMinMaxPriceTickerInNumOfTrade($ticker['ticker'], 34, WorkWithTime::getCurrentDate());
                $arrayHighLow['3M'][$ticker['ticker']] = $tickerHelper->getMinMaxPriceTickerInNumOfTrade($ticker['ticker'], 89, WorkWithTime::getCurrentDate());
                $arrayHighLow['6M'][$ticker['ticker']] = $tickerHelper->getMinMaxPriceTickerInNumOfTrade($ticker['ticker'], 144, WorkWithTime::getCurrentDate());

                /*DO: up trend*/
                $arrayUpTrend[$ticker['ticker']] = $tickerHelper->getMinMaxPriceTickerInNumOfTrade($ticker['ticker'], 76, WorkWithTime::getCurrentDate(), false, 21);

            }
            /*DO: SMA*/
            $arraySMA['10'] = $tickerHelper->getSMA(10, WorkWithTime::getCurrentDate());

            $arraySMA['20'] = $tickerHelper->getSMA(20, WorkWithTime::getCurrentDate());
            $arraySMA['50'] = $tickerHelper->getSMA(50, WorkWithTime::getCurrentDate());
            $arraySMA['100'] = $tickerHelper->getSMA(100, WorkWithTime::getCurrentDate());

            /*DO: R6*/
            $arrayRS = $tickerHelper->getArrayTickerLastDay(6);
            /*DO: init data*/
            $trend = array(
                'higtLow' => $arrayHighLow,
            );

            Cache::put('dataTickerTrendHighLow', json_encode($trend), WorkWithTime::getRemainTime());
            $trend = array(
                'sma' => $arraySMA,
            );
            Cache::put('dataTickerTrendSMA', json_encode($trend), WorkWithTime::getRemainTime());
            $upTrend = array(
                'uptrend' => $arrayUpTrend,
            );
            Cache::put('dataTickerTrendUptrend', json_encode($upTrend), WorkWithTime::getRemainTime());
            $trend = array(
                'rs' => $arrayRS,
            );
            Cache::put('dataTickerTrendRs', json_encode($trend), WorkWithTime::getRemainTime());
        }
        $remain = WorkWithTime::getRemainTime() / (60 * 60) . "hour(s)";

        $this->_logWork->addLog('init MarketOverview', "Remain time:" . $remain . "\n");
        $this->_logWorking->updateWorking('initDataMarketOverview', 'Create cache Market Overview', 1);
        return $remain;
    }

    /*TODO: tao data cash_flow de lay ra yester_day*/
    public function initDataCashFLowYesterDay($date = null) {
        $this->_logWorking->updateWorking('initDataCashFLowYesterDay', 'Create cache Cash Flow YesterDay', 1);

        $time_start = microtime(true);
        $cashFlowHelper = new CashFlowHelper();
        $cashFlowHelper->initDataCashFlow(null);
        $time_end = microtime(true);
        $execution_time = ($time_end - $time_start);

        $this->_logWork->addLog('Inited data cashflow', "Excution Time" . $execution_time / 60 . "\n");
        $this->_logWorking->updateWorking('initDataCashFLowYesterDay', 'Create cache Cash Flow YesterDay', 0);
    }


    public function dieuChinh() {
        $time_start = microtime(true);
        $checkDc = new CheckHsdc();
        $checkDc->ajustAllTickerFromTickerAdjustTable();
        $time_end = microtime(true);
        $execution_time = ($time_end - $time_start);
        $logDb = new LogHelper();
        $logDb->addLogDb('Dieu chinh', "Excution Time" . $execution_time . "\n");
    }

    public function checkExchangeInday() {
        $import = new ImportFromBoardPrice();
        $arrayTicker = $import->getAllDataTickerCurrentDayFromVietStock(false, false);
        var_dump($import->isExchangeInDayNew($arrayTicker));
    }

    public function refreshIntraDay() {
        VariableDatabaseHelper::updateVariable('current_intraday', 0);
    }


    public function demoUsageTime() {
        $rustart = getrusage();

        /*Do some thing here*/
        $a = 1;

        $ru = getrusage();
        echo "This process used " . $this->rutime($ru, $rustart, "utime") .
            " ms for its computations\n";
        echo "It spent " . $this->rutime($ru, $rustart, "stime") .
            " ms in system calls\n";
    }

    function rutime($ru, $rus, $index) {
        return ($ru["ru_$index.tv_sec"] * 1000 + intval($ru["ru_$index.tv_usec"] / 1000))
        - ($rus["ru_$index.tv_sec"] * 1000 + intval($rus["ru_$index.tv_usec"] / 1000));
    }

    private $_TickerBoardModel;

    /**
     * @return TickerBoard
     */
    private function getTickerBoardModel() {
        if (is_null($this->_TickerBoardModel))
            $this->_TickerBoardModel = new TickerBoard();
        return $this->_TickerBoardModel;
    }

    private $_TickerClassModel;

    /**
     * @return TickerClass
     */
    private function getTickerClassModel() {
        if (is_null($this->_TickerClassModel))
            $this->_TickerClassModel = new TickerClass();
        return $this->_TickerClassModel;
    }

    private $_TickerLastDayModel;

    /**
     * @return TickerLastDay
     */
    private function getTickerLastDayModel() {
        if (is_null($this->_TickerLastDayModel))
            $this->_TickerLastDayModel = new TickerLastDay();
        return $this->_TickerLastDayModel;
    }

    private $_CacheNextDayModel;

    /**
     * @return CacheNextDay
     */
    private function getCacheNextDayModel() {
        if (is_null($this->_CacheNextDayModel))
            $this->_CacheNextDayModel = new CacheNextDay();
        return $this->_CacheNextDayModel;
    }

    public function emtyDb() {
        return;
        $tickerUpto = new TickerUpto();
        $tickerUpto->truncate();
        $tickerForeign = new TickerForeign();
        $tickerForeign->truncate();
        $tickerClass = new TickerClass();
        $tickerClass->truncate();
        $index = new IndexUpto();
        $index->trancate();
        return "OK";
    }

    public function test() {
        $tickerHelper = new TickerHelper();
        var_dump($tickerHelper->getMinMaxPriceTickerInNumOfTrade('APC', 76, WorkWithTime::getCurrentDate(), false, 21));
    }
}
