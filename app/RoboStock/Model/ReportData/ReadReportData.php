<?php
/**
 * Created by IntelliJ IDEA.
 * User: vjcspy
 * Date: 10/19/15
 * Time: 12:16 PM
 */

namespace App\RoboStock\Model\ReportData;
include_once('../app/RoboStock/Model/Curl/simple_html_dom.php');
use App\ReportTickerDataQuarter;
use App\ReportTypeRow;
use App\ReportTypeSize;
use App\RoboStock\Model\WorkWithUrl\WorkWithUrl;

class ReadReportData {
    private $_urlData = 'http://finance.vietstock.vn/Controls/Report/Data/GetReport.ashx?rpt',
        $_type = 'Type=',
        $_code = '&scode=',
        $_bizType = '&bizType',
        $_rptUnit = '&rptUnit=',
        $_rptTermTypeID = '&rptTermTypeID=',
        $_page = '&page=';

    /**
     * ReadReportData constructor.
     */
    public function __construct() {
        $this->_arrayDataReportRowId = array();
    }


    public function test() {

        $reportType = 'LC';
        $ticker = 'CTG';
        $countPage = 1;
        $url = $url = $this->_urlData . $this->_type . $reportType . $this->_code . $ticker . $this->_bizType . '3' . $this->_rptUnit . '1000000' . $this->_rptTermTypeID . '2' . $this->_page . $countPage;
        return $this->getDataReportQuarterFromVietStock($url);
    }

    public function testSaveQuartReport($arrayTicker = null) {
        $arrayTicker = array('BVH', 'CTG');
        foreach ($arrayTicker as $ticker) {
            $this->saveDataReportTickerQuarter($ticker, 'CDKT');
        }
        return null;
    }

    private function saveDataReportTickerQuarter($ticker, $reportType) {
        if (is_null($ticker) || is_null($reportType))
            return 'Null';
        $numOfPageCheck = 20;
        $countPage = 0;
        $reportTickerType = null;

        do {
            $countPage += 1;
            $url = $this->_urlData . $this->_type . $reportType . $this->_code . $ticker . $this->_bizType . '1' . $this->_rptUnit . '1000000' . $this->_rptTermTypeID . '2' . $this->_page . $countPage;
            $dataReport = $this->getDataReportQuarterFromVietStock($url);
            if (is_null($dataReport))
                break;
            $arrayRowIdData = array();
            $arrayRowQ1 = array();
            $arrayRowQ2 = array();
            $arrayRowQ3 = array();
            $arrayRowQ4 = array();
            foreach ($dataReport['data'] as $rowId => $row) {
                $arrayRowIdData[] = array(
                    'row_id' => $rowId,
                    'row_name' => $row[0]
                );
                $arrayRowQ1[$rowId] = $row[2];
                $arrayRowQ2[$rowId] = $row[3];
                $arrayRowQ3[$rowId] = $row[4];
                $arrayRowQ4[$rowId] = $row[5];
            }
            if (is_null($reportTickerType))
                $reportTickerType = $this->getReportTickerType($arrayRowIdData, $reportType);

            //lay quy hien tai:
            if ($countPage == 1) {
                $q = substr($dataReport['dataQ'], 5, 1);
                $y = substr($dataReport['dataQ'], 7, 4);
                $currentQuarter = array(
                    'q' => $q,
                    'y' => $y
                );
            }
            $dec = ($countPage - 1) * 4 + 1 - 1;
            $dataQ = $this->getQuaterly($dec, $currentQuarter);
            $this->saveToDbQuarterTable($ticker, $reportType, $dataQ, $arrayRowQ1, $reportTickerType);

            $dec = ($countPage - 1) * 4 + 2 - 1;
            $dataQ = $this->getQuaterly($dec, $currentQuarter);
            $this->saveToDbQuarterTable($ticker, $reportType, $dataQ, $arrayRowQ2, $reportTickerType);

            $dec = ($countPage - 1) * 4 + 3 - 1;
            $dataQ = $this->getQuaterly($dec, $currentQuarter);
            $this->saveToDbQuarterTable($ticker, $reportType, $dataQ, $arrayRowQ3, $reportTickerType);

            $dec = ($countPage - 1) * 4 + 4 - 1;
            $dataQ = $this->getQuaterly($dec, $currentQuarter);
            $this->saveToDbQuarterTable($ticker, $reportType, $dataQ, $arrayRowQ4, $reportTickerType);

        } while ($countPage < $numOfPageCheck);
        return $reportTickerType;
    }

    private function saveToDbQuarterTable($ticker, $reportType, $quarter, $data, $reportTickerType) {
        $model = $this->getReportTickerDataQuarterModel();
        $report = $model->firstOrNew(array(
            'ticker' => $ticker,
            'report_type' => $reportType,
            'quarter' => json_encode($quarter)
        ));
        $report->data = json_encode($data);
        $report->report_ticker_type = $reportTickerType;
        $report->save();
    }

    /**
     * @param $url
     * @return array|mixed|string
     */
    private function getDataReportQuarterFromVietStock($url) {
        $workWithUrl = new WorkWithUrl();
        $data = $workWithUrl->getDataFromUrl($url);
        if ($data == false)
            return null;
        $html = str_get_html($data);

        //BR_rowHeader
        $dataTime = $html->find('#BR_rowHeader')[0]->children[2]->nodes[0]->plaintext;
//        $dataTime = $html->find('#idBR_thead')[0];
        $data = array();

        foreach ($html->find('#BR_tBody') as $table) {
            foreach ($table->find('tr') as $tr) {
                $row = array();
                foreach ($tr->find('td') as $td) {
                    $row[] = ($td->plaintext);
                }
                $data[$tr->id] = $row;

            }
        }
        return array(
            'dataQ' => $dataTime,
            'data' => $data
        );
    }

    /**Lay quy dua theo quy hien tai, Dec la khoang cach bao nhieu quy
     * @param $dec
     * @param null $currentQuaterly
     * @return array|null
     */
    private function getQuaterly($dec, $currentQuaterly = null) {
        /*FIXME: must change get in db*/
        if (is_null($currentQuaterly))
            $currentQuaterly = array(
                'q' => 2,
                'y' => 2015
            );

        /*Check dec*/
        if ($dec > 0)
            $isIn = true;
        else if ($dec < 0)
            $isIn = false;
        else
            return $currentQuaterly;

        $n = intval($dec / 4);
        $d = intval($dec % 4);

        if ($isIn) {
            $currentQuaterly['y'] -= $n;
            switch ($currentQuaterly['q'] - $d) {
                case -2:
                    $currentQuaterly['q'] = 2;
                    $currentQuaterly['y'] -= 1;
                    break;
                case -1:
                    $currentQuaterly['q'] = 3;
                    $currentQuaterly['y'] -= 1;
                    break;
                case 0:
                    $currentQuaterly['q'] = 4;
                    $currentQuaterly['y'] -= 1;
                    break;
                case 1:
                    $currentQuaterly['q'] = 1;
                    $currentQuaterly['y'] -= 0;
                    break;
                case 2:
                    $currentQuaterly['q'] = 2;
                    $currentQuaterly['y'] -= 0;
                    break;
                case 3:
                    $currentQuaterly['q'] = 3;
                    $currentQuaterly['y'] -= 0;
                    break;
                case 4:
                    $currentQuaterly['q'] = 4;
                    $currentQuaterly['y'] -= 0;
                    break;
            }
        } else {
            $currentQuaterly['y'] -= $n;
            switch ($currentQuaterly['q'] - $d) {
                case 7:
                    $currentQuaterly['q'] = 3;
                    $currentQuaterly['y'] += 1;
                    break;
                case 6:
                    $currentQuaterly['q'] = 2;
                    $currentQuaterly['y'] += 1;
                    break;
                case 5:
                    $currentQuaterly['q'] = 1;
                    $currentQuaterly['y'] += 1;
                    break;
                case 4:
                    $currentQuaterly['q'] = 4;
                    $currentQuaterly['y'] += 0;
                    break;
                case 3:
                    $currentQuaterly['q'] = 3;
                    $currentQuaterly['y'] += 0;
                    break;
                case 2:
                    $currentQuaterly['q'] = 2;
                    $currentQuaterly['y'] += 0;
                    break;
                case 1:
                    $currentQuaterly['q'] = 1;
                    $currentQuaterly['y'] += 0;
                    break;
            }
        }
        return $currentQuaterly;
    }

    /**
     * @param array $rowIdData {row_id:'row id', row_name: 'row name'}
     * @param $reportType
     * @return string
     */
    private function getReportTickerType(Array $rowIdData, $reportType) {
        $size = count($rowIdData);
        $resultCheckSize = $this->checkSize($size, $reportType);
        if ($resultCheckSize['existed'] == true) {
            $arrayRowId = array();
            foreach ($rowIdData as $row) {
                $arrayRowId[] = $row['row_id'];
            }
            $resultCheckRowId = $this->checkRowId($arrayRowId, $resultCheckSize['reportTickerType'], $reportType);
            foreach ($resultCheckSize['reportTickerType'] as $reportTickerType) {
                if ($resultCheckRowId[$reportTickerType]['isMatch'] == true)
                    return $reportTickerType;
            }
        }
        // Chạy đến đây chứng tỏ không tồn tại report ticker type
        // Tao mới

        //table: report_type_row
        $newType = 'type' . microtime();
        $modelTypeRow = $this->getReportTypeRowModel();
        foreach ($rowIdData as $row) {
            $modelTypeRow->create(array(
                'report_type' => $reportType,
                'report_ticker_type' => $newType,
                'row_id' => $row['row_id'],
                'row_name' => $row['row_name']
            ));
        }
        //table: report_type_size
        $modelTypeSize = $this->getReportTypeSizeModel();
        $modelTypeSize->create(array(
            'report_type' => $reportType,
            'report_ticker_type' => $newType,
            'size' => $size
        ));
        return $newType;
    }


    /**
     * @param $size
     * @param $reportType
     * @return array
     */
    private function checkSize($size, $reportType) {
        // check trong bang report_type_size
        $existed = false;

        // Kiểu loại là một array bởi vì có thể nhiều loại cùng chung một size
        $reportTickerType = array();

        // Nếu lưu cache size các loại ở đây thì không hợp lý bởi vì lúc save có thể thêm loại mới
        $reportTypeSizeModel = $this->getReportTypeSizeModel();
        $allReportSize = $reportTypeSizeModel->where('report_type', $reportType)->get();
        foreach ($allReportSize as $report) {
            if ($report->size == $size) {
                $existed = true;
                $reportTickerType[] = $report->report_ticker_type;
            }
        }
        return array(
            'existed' => $existed,
            'reportTickerType' => $reportTickerType,
            'reportType' => $reportType);
    }


    private $_arrayDataReportRowId;

    // Nếu checkSize là chưa tồn tại thì tạo mới luôn. Còn nếu đã tồn tại cùng size đấy rồi thì check xem các row id có trùng khớp không.
    /**
     * @param array $rowIds
     * @param array $reportTickerTypes
     * @param $reportType
     * @return array
     */
    private function checkRowId(Array $rowIds, Array $reportTickerTypes, $reportType) {
        // Cần lưu cache từng loại và row id của nó.
        $model = $this->getReportTypeRowModel();
        if (!isset($this->_arrayDataReportRowId[$reportType]))
            $this->_arrayDataReportRowId[$reportType] = array();
        $isType = array();
        foreach ($reportTickerTypes as $reportTickerType) {
            $isType[$reportTickerType] = array(
                'reportType' => $reportType,
                'reportTickerType' => $reportTickerType,
                'isMatch' => true
            );
            if (!isset($this->_arrayDataReportRowId[$reportType][$reportTickerType])) {
                $allRowId = $model->query()->where(array(
                    'report_type' => $reportType,
                    'report_ticker_type' => $reportTickerType
                ))->get();
                $dataRows = array();
                foreach ($allRowId as $row) {
                    $dataRows[] = $row->row_id;
                }
                $this->_arrayDataReportRowId[$reportType][$reportTickerType] = $dataRows;
            }
            foreach ($rowIds as $rowId) {
                if (!in_array($rowId, $this->_arrayDataReportRowId[$reportType][$reportTickerType])) {
                    $isType[$reportTickerType] = array(
                        'reportType' => $reportTickerType,
                        'reportTickerType' => $reportTickerType,
                        'isMatch' => false,
                    );
                    break;
                };
            }
        }
        return $isType;
    }

    private $_ReportTypeSizeModel;

    /**
     * @return ReportTypeSize
     */
    private function getReportTypeSizeModel() {
        if (is_null($this->_ReportTypeSizeModel))
            $this->_ReportTypeSizeModel = new ReportTypeSize();
        return $this->_ReportTypeSizeModel;
    }

    private $_ReportTypeRowModel;

    /**
     * @return ReportTypeRow
     */
    private function getReportTypeRowModel() {
        if (is_null($this->_ReportTypeRowModel))
            $this->_ReportTypeRowModel = new ReportTypeRow();
        return $this->_ReportTypeRowModel;
    }

    private $_ReportTickerDataQuarterModel;

    /**
     * @return ReportTickerDataQuarter
     */
    private function getReportTickerDataQuarterModel() {
        if (is_null($this->_ReportTickerDataQuarterModel))
            $this->_ReportTickerDataQuarterModel = new ReportTickerDataQuarter();
        return $this->_ReportTickerDataQuarterModel;
    }
}
