<?php
/**
 * Created by IntelliJ IDEA.
 * User: vjcspy
 * Date: 9/20/15
 * Time: 7:57 PM
 */

namespace App\RoboStock\Model\TickerClass;


use App\TickerClass;

class TickerClassHelper {
    /**
     *
     */
    public function importFromFile() {
        $myfile = fopen("app/RoboStock/Model/TickerClass/phanloai.csv", "r") or die("Unable to open file!");
// Output one line until end-of-file
        $count = 0;
        while (!feof($myfile)) {
            if ($count == 0) {
                $count += 1;
                fgets($myfile);
                continue;
            }
            $currentRow = fgets($myfile);
            $currentRow = str_replace(' ', '', $currentRow);
            $currentRow = preg_replace('/[\r\n]/', '', $currentRow);
            $data = explode(',', $currentRow);
            if(count($data) != 4)
                continue;
            try {
                $ticker = array(
                    'ticker' => $data[0],
                    'top' => $data[1],
                    'bp' => $data[2],
                    'cd' => $data[3],
                );
                $count += 1;
                $this->saveToClassDb($ticker);
            } catch (Exception $e) {
                continue;
            }
        }
        fclose($myfile);
        echo 'Save ' . $count . '(s) Sucsses ';
    }

    private function saveToClassDb($ticker) {
        $model = $this->getTickerClassModel();
        if (is_null($model->query()->where('ticker', $ticker['ticker'])->first())) {
            $model = $this->getTickerClassModel();
            $model->create($ticker);
        } else {
            $model = $this->getTickerClassModel();
            $ticker = $model->query()->where('ticker', $ticker['ticker'])->first();
            $ticker->top = $ticker['top'];
            $ticker->bp = $ticker['bp'];
            $ticker->cd = $ticker['cd'];
            $ticker->save();
        }
    }

    private $_tickerClassModel;

    private function getTickerClassModel() {
        if (is_null($this->_tickerClassModel))
            $this->_tickerClassModel = new TickerClass();
        return $this->_tickerClassModel;
    }
}
