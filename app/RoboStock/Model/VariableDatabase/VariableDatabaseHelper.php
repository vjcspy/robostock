<?php
/**
 * Created by IntelliJ IDEA.
 * User: vjcspy
 * Date: 9/22/15
 * Time: 10:30 PM
 */

namespace App\RoboStock\Model\VariableDatabase;


use App\VariableDatabase;

class VariableDatabaseHelper {
    public static function updateVariable($name, $value) {
        $model = VariableDatabaseHelper::getVariableDatabaseModel();
        $item = $model->firstOrNew(array(
            'name' => $name,
        ));
        $item->value = $value;
        $item->save();
    }

    public static function getVariable($name) {
        $model = VariableDatabaseHelper::getVariableDatabaseModel();
        $var = $model->query()->where('name', $name)->first();
        if (!is_null($var))
            return $var->value;
        else
            return null;
    }

    private static $_VariableDatabaseModel;

    /**
     * @return VariableDatabase
     */
    private static function getVariableDatabaseModel() {
        if (is_null(VariableDatabaseHelper::$_VariableDatabaseModel))
            VariableDatabaseHelper::$_VariableDatabaseModel = new VariableDatabase();
        return VariableDatabaseHelper::$_VariableDatabaseModel;
    }
}
