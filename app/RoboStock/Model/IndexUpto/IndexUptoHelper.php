<?php
/**
 * Created by IntelliJ IDEA.
 * User: vjcspy
 * Date: 9/19/15
 * Time: 1:25 AM
 */

namespace App\RoboStock\Model\IndexUpto;


use App\IndexUpto;
use App\RoboStock\Model\WorkWithTime\WorkWithTime;
use App\RoboStock\Model\WorkWithUrl\WorkWithUrl;

class IndexUptoHelper {

    private $_cafeFUrl = 'http://banggia.cafef.vn/stockhandler.ashx?index=true';


    /**
     * MAINFUNCTION
     * TODO: Update to table index_upto
     * @return bool
     */
    public function getDataFromCafeF($update = true) {
        $workWithUrl = new WorkWithUrl();
        $jsonData = $workWithUrl->getDataFromUrl($this->_cafeFUrl);
        $data = json_decode($jsonData, true);

        foreach ($data as $index) {
            if ($index['name'] == 'HNXUPCOMINDEX')
                $index['name'] = 'UPCOMINDEX';
            $index['date'] = WorkWithTime::getCurrentDate();
            $index['vol'] = floatval(str_replace(',', '', $index['volume']));
            unset($index['volume']);
            if ($update)
                $this->insertToTable($index);
        }
        return $data;
    }

    private function insertToTable($index) {
        $model = $this->getIndexModel();
        $currentIndex = $model->firstOrNew(array(
            'name' => $index['name'],
            'date' => $index['date']
        ));
        $currentIndex->name = $index['name'];
        $currentIndex->date = WorkWithTime::getCurrentDate();
        $currentIndex->change = $index['change'];
        $currentIndex->index = $index['index'];
        $currentIndex->percent = $index['percent'];
        $currentIndex->vol = $index['vol'];
        $currentIndex->value = $index['value'];
        $currentIndex->save();
    }

    private $_indexModel;

    private function getIndexModel() {
        if (is_null($this->_indexModel)) {
            $this->_indexModel = new IndexUpto();
        }
        return $this->_indexModel;
    }
}
