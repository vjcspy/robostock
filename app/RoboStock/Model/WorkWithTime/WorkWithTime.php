<?php
/**
 * Created by PhpStorm.
 * User: vjcspy
 * Date: 9/16/15
 * Time: 12:46 AM
 */
namespace App\RoboStock\Model\WorkWithTime;
class WorkWithTime {
    private static $_currentDate;

    /**
     * @param null $fomat
     * @return mixed
     */
    public static function getCurrentDate($fomat = null) {
        if (is_null(WorkWithTime::$_currentDate)) {
            date_default_timezone_set('Asia/Bangkok');
            if (is_null($fomat))
                WorkWithTime::$_currentDate = date('Ymd');
            else
                WorkWithTime::$_currentDate = date($fomat);
        }
        return WorkWithTime::$_currentDate;
    }

    private static $_lastDay;

    public static function  last_day($string = null, $first_last = 'lastDay', $format = 'standard') {
        if (is_null(WorkWithTime::$_lastDay)) {
            date_default_timezone_set('Etc/GMT-7');
            if (is_null($string)) {
                $string = WorkWithTime::getCurrentDate();
            }
            $result = strtotime($string);
            if ($first_last == 'lastDay') {
                $result = strtotime('-1 day', $result);
            }
            if ($format == 'unix') {
                return $result;
            }
            if ($format == 'standard') {
                $lastDate = date('Ymd', $result);
                return $lastDate;
            }
        }
        return WorkWithTime::$_lastDay;
    }

    public static function nextDay($currentDay = null, $format = 'standard') {
        date_default_timezone_set('Asia/Bangkok');
        if (is_null($currentDay)) {
            $currentDay = WorkWithTime::getCurrentDate();
        }
        $result = strtotime($currentDay);
        $nextDay = strtotime('+1 day', $result);
        if ($format == 'unix') {
            return $nextDay;
        }
        if ($format == 'standard') {
            $nextDay = date('Ymd', $nextDay);
            return $nextDay;
        }
        return $nextDay;
    }

    public static function getCurrentDateTime() {
        date_default_timezone_set('Asia/Bangkok');
        return date('l jS \of F Y h:i:s A');
    }

    public static function getCurrentTime() {
        date_default_timezone_set('Asia/Bangkok');
        return date('h:i A');
    }

    /**TODO: Trả lời số s còn lại tính từ thời điểm hiện tại đến lúc nửa đêm nếu 2 giá trị truyền vào là null
     * @param null $currentTime
     * @param null $nextTime
     * @return int
     */
    public static function getRemainTime($currentTime = null, $nextTime = null) {
        date_default_timezone_set('Asia/Bangkok');

        if (is_null($nextTime)) {
            $result = strtotime(WorkWithTime::getCurrentDate());
            $dateNextTime = strtotime('+1 day', $result);
        } else
            $dateNextTime = strtotime($nextTime);
        if (is_null($currentTime))
            $dateCurrentTime = time();
        else
            $dateCurrentTime = strtotime($currentTime);
        $remain = $dateNextTime - $dateCurrentTime;
        return $remain;
    }

    public static function getCurrentQuater() {
        $curMonth = date("m", time());
        return $curQuarter = ceil($curMonth / 3);
    }

    public static function getArrayQuarterByDec($dec = 63) {
        $arrayQuarter = array();

        $currentQ = array(
            'q' => WorkWithTime::getCurrentQuater(),
            'y' => WorkWithTime::getCurrentDate('Y'),
            'string' => WorkWithTime::getCurrentQuater() . '/' . WorkWithTime::getCurrentDate('Y')
        );
        $arrayQuarter[] = $currentQ;
        for ($i = 1; $i <= $dec; $i++) {
            $q = intval($currentQ['q']);
            $y = intval($currentQ['y']);

            $dY = intval($i / 4);
            $dQ = intval($i % 4);

            $y -= $dY;

            if (($q - $dQ) > 0)
                $q -= $dQ;
            else {
                $q -= ($dQ - 4);
                $y -= 1;
            }
            $arrayQuarter[] = array(
                'q' => $q,
                'y' => $y,
                'string' => $q . '/' . $y
            );
        }
        return $arrayQuarter;
    }

    public static function getArrayYearByDec($dec = 20) {
        $arrayYear = array();

        $currentYear = 2020;
        $count = 0;
        do {
            $arrayYear[] = array(
                'y' => $currentYear,
                'string' => 'Năm ' . $currentYear
            );
            $currentYear -= 1;
            $count += 1;
        } while ($count < $dec);
        return $arrayYear;
    }
}
