<?php
//    include_once('simple_html_dom.php');

    /**
     * Created by PhpStorm.
     * User: vjcspy
     * Date: 7/30/15
     * Time: 3:57 PM
     */
    class SM_AutoPunch
    {
        function getDataFromUrl($url)
        {
            $ch = curl_init();

            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

            $result = curl_exec($ch);

            curl_close($ch);

            return ($result);
        }

        public function test()
        {
            /*TODO: work perfect*/
            $ch = curl_init();
            $param = array(
                'actionID'    =>
                    'chkAuthentication',
                'txtPassword' =>
                    '5367233',
                'txtUserName' =>
                    'khoild',
            );
            curl_setopt($ch, CURLOPT_COOKIEJAR, "/tmp/cookieFileName");
            curl_setopt($ch, CURLOPT_URL, "http://hr.smartosc.com/login.php");
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $param);

            ob_start();      // prevent any output
            curl_exec($ch); // execute the curl command
            ob_end_clean();  // stop preventing output

            curl_close($ch);
            unset($ch);

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-GB; rv:1.8.1.6) Gecko/20070725 Firefox/2.0.0.6');
            curl_setopt($ch, CURLOPT_COOKIEFILE, "/tmp/cookieFileName");
            curl_setopt($ch, CURLOPT_URL, "http://hr.smartosc.com/index.php");
            curl_setopt($ch, CURLOPT_COOKIEJAR, "/tmp/cookieFileNameNew");
            ob_start();      // prevent any output
            $buf2 = curl_exec($ch);
            ob_end_clean();  // stop preventing output
            curl_close($ch);

            echo($buf2);
        }


        /*-------------------------Function for APH-----------------------------*/
        public $_param = array();

        public function loginHr($userName = null, $userPass = null)
        {
            if ($userName == null || $userPass == null) {
                $userName = 'huypq';
                $userPass = 'huyhuyhoang1A';
            }
            $param = array(
                'actionID'    =>
                    'chkAuthentication',
                'txtPassword' =>
                    $userPass,
                'txtUserName' =>
                    $userName,
            );
            $header = array(
                'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
                'Accept-Encoding: gzip, deflate',
                'Accept-Language: en-US,en;q=0.5',
                'Connection: keep-alive',
                //                'Cookie: __utma=40609824.1186764867.1436283478.1436283478.1437367460.2; __utmz=40609824.1436283478.1.1.utmcsr
                //=(direct)|utmccn=(direct)|utmcmd=(none); _ga=GA1.2.1186764867.1436283478; __zlcmid=VbemenV7r5Rc6z; PHPSESSID
                //=ennaedevah20fkhf7r5ukaip16',
                'DNT: 1',
                'Host: hr.smartosc.com',
                'User-Agent: Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:39.0) Gecko/20100101 Firefox/39.0',
            );
            $this->_param = $param;
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_COOKIEJAR, "/tmp/cookieFileName");
            curl_setopt($ch, CURLOPT_URL, "http://hr.smartosc.com/login.php");
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $this->_param);
            curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:39.0) Gecko/20100101 Firefox/39.0');
            curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_REFERER, 'http://hr.smartosc.com/login.php');    // set referer
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);// ssl certificate
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);

            ob_start();      // prevent any output
            $result = curl_exec($ch); // execute the curl command
            ob_end_clean();  // stop preventing output

            curl_close($ch);
            unset($ch);

            return $result;
        }


        protected $_dataPagePunchTime = false;

        public function getPagePunchTime()
        {
            /*TODO: return page punch time*/
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_COOKIEFILE, "/tmp/cookieFileName");
            curl_setopt($ch, CURLOPT_URL, "http://hr.smartosc.com/lib/controllers/CentralController.php?timecode=Time&action=Show_Punch_Time");

            $buf2 = curl_exec($ch);

            curl_close($ch);
            $this->_dataPagePunchTime = $buf2;

            return $this->_dataPagePunchTime;
        }

        protected $_dataInform = array();
        protected $_action     = null;

        public function readDataFromPagePunchTime($data = null)
        {

            if (is_null($data)) {
                $data = $this->_dataPagePunchTime;
            }
            $htmlData = str_get_html($data);
            if(!$htmlData){
                return false;
            }
            $dataInForm = array();
            /*FIND DATA IN FORM*/

            $element = $htmlData->find('input[id=txtDate]');
            $dataInForm['txtDate'] = $element[0]->value;

            $element = $htmlData->find('input[id=txtTime]');
            $dataInForm['txtTime'] = $element[0]->value;

            $dataInForm['txtNote'] = '';

            $element = $htmlData->find('input[id=startTime]');


            if (isset($element[0])) {
                /*TODO: PUNCH-OUT*/
                $this->_action = 'punchOut';
                $dataInForm['startTime'] = str_replace(' ', '+', $element[0]->value);
                $element = $htmlData->find('input[id=timeEventId]');
                $dataInForm['timeEventId'] = $element[0]->value;
            }


            $this->_dataInform = $dataInForm;

            /*TODO: get button DATA*/
            $action = null;
            $element = $htmlData->find('form[id=frmPunchTime]', 0);
            $actionText = $element->children[0]->children[1]->children[3]->children[2]->children[0]->value;
            if ($actionText == 'Out')
                $action = 'punchOut';
            else
                $action = 'punchIn';

            $this->_action = $action;

            return $this->_dataInform;
        }

        public function autoXXX()
        {
            if ($this->_action == 'punchOut') {
                if (is_null($this->_param) || is_null($this->_dataInform)) {
                    return false;
                } else {
                    $url = 'http://hr.smartosc.com/lib/controllers/CentralController.php?timecode=Time&action=Punch_Out';
                    $refererUrl = 'http://hr.smartosc.com/lib/controllers/CentralController.php?timecode=Time&action=Show_Punch_Time';

                    return $this->sendPostDataWithCookie($url, $this->_dataInform, $refererUrl);
                }
            } else {
                if ($this->_action = 'punchIn') {
                    $url = 'http://hr.smartosc.com/lib/controllers/CentralController.php?timecode=Time&action=Punch_In';
                    $refererUrl = 'http://hr.smartosc.com/lib/controllers/CentralController.php?timecode=Time&action=Show_Punch_Time';

                    return $this->sendPostDataWithCookie($url, $this->_dataInform, $refererUrl);
                }
            }

            return false;
        }

        public function sendPostDataWithCookie($url, $param, $referer = null)
        {
            if (is_null($referer)) {
                $referer = 'http://google.com';
            }
            $header = array(
                'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
                'Accept-Encoding: gzip, deflate',
                'Accept-Language: en-US,en;q=0.5',
                'Connection: keep-alive',
                //                'Cookie: __utma=40609824.1186764867.1436283478.1436283478.1437367460.2; __utmz=40609824.1436283478.1.1.utmcsr
                //=(direct)|utmccn=(direct)|utmcmd=(none); _ga=GA1.2.1186764867.1436283478; __zlcmid=VbemenV7r5Rc6z; PHPSESSID
                //=ennaedevah20fkhf7r5ukaip16',
                'DNT: 1',
                'Host: hr.smartosc.com',
                'User-Agent: Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:39.0) Gecko/20100101 Firefox/39.0',
                'Referer: http://hr.smartosc.com/lib/controllers/CentralController.php?timecode=Time&action=Show_Punch_Time',
            );
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_COOKIEFILE, "/tmp/cookieFileName");
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $param);
            curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_REFERER, $referer);    // set referer
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);// ssl certificate
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
            $result = curl_exec($ch);
            curl_close($ch);

            return $result;

        }
    }
