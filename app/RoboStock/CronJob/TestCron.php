<?php
/**
 * Created by IntelliJ IDEA.
 * User: vjcspy
 * Date: 10/1/15
 * Time: 10:42 AM
 */

namespace App\RoboStock\CronJob;


use App\RoboStock\Model\Log\LogHelper;
use App\RoboStock\Model\WorkWithTime\WorkWithTime;
use Monolog\Handler\StreamHandler;
use Monolog\Logger;

class TestCron {

    public function test() {
        $log = new Logger('name');
        $log->pushHandler(new StreamHandler('./test.log', Logger::WARNING));

        $log->addWarning(WorkWithTime::getCurrentDateTime());
    }
    public function testAddToDb(){
        $log = new LogHelper();
        $log->addLogDb('testLog', WorkWithTime::getCurrentDateTime());
    }
}
