<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', 'RobostockController@index');
Route::get('/ang', 'PagesController@ang');
/*
 * Create router by controller
 * - Tao controller welcomeController
 * - Tao function contact
 * */
Route::get('/robostock', 'RobostockController@getAlltickerinday');
/*
 * Create router by view
 * - Tao view pages.abouts
 * - return ve view pages.abouts*/
Route::get('/abouts', function () {
    return view('pages.abouts');
});
Route::get('/aboutme', 'PagesController@aboutme');
Route::get('/temp1', 'PagesController@template1');
Route::get('/temp2', 'PagesController@template2');

/*Articles*/
Route::get('/articles', 'ArticlesController@index');
Route::controller('/articles', 'ArticlesController');

/*add data*/
Route::get('/test', 'PagesController@test');
Route::get('/num', 'PagesController@num');
//Route::get('/allticker', 'RobostockController@allticker');
Route::controller('/robostock', 'RobostockController');

Route::controller('/ng', 'AngController');
Route::controller('/master', 'MasterAngularController');

/*TODO: MAIN ROBOSTOCK BEGIN HERE ______________________________________________________*/
//auth
Route::get('home', 'HomeController@index');

// Authentication routes...
Route::get('auth/login', 'Auth\AuthController@getLogin');
Route::post('auth/login', 'Auth\AuthController@postLogin');
Route::get('auth/logout', 'Auth\AuthController@getLogout');

// Registration routes...
Route::get('auth/register', 'Auth\AuthController@getRegister');
Route::post('auth/register', 'Auth\AuthController@postRegister');

// Password reset link request routes...
Route::get('password/email', 'Auth\PasswordController@getEmail');
Route::post('password/email', 'Auth\PasswordController@postEmail');

// Password reset routes...
Route::get('password/reset/{token}', 'Auth\PasswordController@getReset');
Route::post('password/reset', 'Auth\PasswordController@postReset');


//TODO: DASHBOARD
Route::controller('/dashboard', 'RobostockDashboardController');
Route::get('/dashboard', 'RobostockDashboardController@index');
// TODO: report
Route::controller('/report', 'RobostockReportController');

//TODO: FilterApp
Route::controller('/filter', 'RobostockFilterAppController');

//DO: Chứa các giá trị toàn cục
Route::controller('/market', 'MarketOverview\MarketController');
//DO: Phân lớp và chỉ s
Route::controller('/market-plcs', 'MarketOverview\PlcsController');
