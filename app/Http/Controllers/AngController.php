<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class AngController extends Controller {
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index() {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create() {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request) {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function show($id) {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function edit($id) {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request $request
     * @param  int $id
     * @return Response
     */
    public function update(Request $request, $id) {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return Response
     */
    public function destroy($id) {
        //
    }

    public function getService() {
        return view('pages.angular.service');
    }

    public function getValidForm() {
        return view('pages.angular.form');
    }

    public function getCreateDirective() {
        return view('pages.angular.createDirective');
    }

    public function getBind() {
        return view('pages.angular.ngBind');
    }

    public function getBindHtml() {
        return view('pages.angular.ngBindHtml');
    }

    public function getBindTemplate() {
        return view('pages.angular.ngBindTemplate');
    }

    public function getCreateDirective2() {
        return view('pages.angular.createDirectiveMore');
    }
}
