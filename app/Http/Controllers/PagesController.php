<?php

namespace App\Http\Controllers;

use App\MarketOverview;
use App\RoboStock\FilterApp\Model\CollectTypeRow;
use App\RoboStock\FilterApp\Model\Filter\FilterGroups;
use App\RoboStock\Model\IndexUpto\IndexUptoHelper;
use App\RoboStock\Model\Overview\MarketOverview\TrendOverviewHelper;
use App\RoboStock\Model\ReportData\ReadReportData;
use App\RoboStock\Model\Ticker\TickerHelper;
use App\RoboStock\Model\TickerBoard\TickerBoardHelper;
use App\RoboStock\Model\TickerClass\TickerClassHelper;
use App\RoboStock\Model\TickerUpto\CheckHsdc;
use App\RoboStock\Model\TickerUpto\ImportFile;
use App\RoboStock\Model\TickerUpto\ImportFromBoardPrice;
use App\RoboStock\Model\WorkWithTime\WorkWithTime;
use App\RoboStock\Report\Model\ReportTickerDataQuarterHelper;
use App\RoboStock\Report\Model\ReportTypeRowHelper;
use App\RoboStock\Report\Run\GetDataReport;
use App\TickerUpto;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Cache;
use SimpleExcel\SimpleExcel;
use SimpleExcel\Spreadsheet\Worksheet;

class PagesController extends Controller {
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index() {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create() {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request) {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function show($id) {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function edit($id) {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request $request
     * @param  int $id
     * @return Response
     */
    public function update(Request $request, $id) {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return Response
     */
    public function destroy($id) {
        //
    }

    /*TODO: Crate page in laravel
        - Crate controller
        - Create view
        - Create router
    */
    /**
     * @return $this
     */
    public function aboutme() {
        return view('pages.aboutme')->with(array(
            'name' => 'mr.vjcsy',
            'age' => '23'
        ));
    }

    public function template1() {
        return view('pages.template1');
    }

    public function template2() {
        return view('pages.template2');
    }

    public function test() {
        /*TODO: test Import sotckdata.txt file*/
//        $import = new ImportFile();
//        $import->importFileNotCheckData();

        /*TODO: test get Data from url price*/
//        $boardPrice = new ImportFromBoardPrice();
//        var_dump($data = $boardPrice->getAllDataTickerCurrentDayFromVietStock(true, true));

        /*TODO: test work with time*/
//        $time = new WorkWithTime();
//        echo $time->getCurrentDateTime();

        /*TODO: check hsdc*/
//        $hsdc = new CheckHsdc();
//        $tickerAjust = $hsdc->checkReferenceStandard();
//        var_dump($tickerAjust);

        /*TODO: all ticker in current day*/
//        $boardPrice = new ImportFromBoardPrice();
//        $tickers = $boardPrice->getAllDataTickerCurrentDayFromVietStock(true,true);
//        return view('robostock.frontend.allTickerInDay')->with('tickers',$tickers);

        /*TODO: search*/
//        return view('robostock.frontend.searchTicker');

        /*TODO: import ticker_board*/
//        $ticker_board = new TickerBoardHelper();
//        $ticker_board->updateTickerFromVietStock();

        /*TODO: import index*/
//        $index = new IndexUptoHelper();
//        $index->getDataFromCafeF();

        /*TODO: update all data*/
//        $import = new ImportFromBoardPrice();
//        var_dump($import->updateAllfdsfData());

        /*TODO: test get ticker with < date*/
//        $checkHsdc = new CheckHsdc();
//        var_dump($checkHsdc->testDate());

        /*TODO: test import nnData*/
//        $import = new ImportFile();
//        $import->importDataNn();

        /*TODO: test tickerClass*/
//        $import = new TickerClassHelper();
//        $import->importFromFile();

        /*TODO: test init data market overview*/
//        $market = new MarketOverviewHelper();
//        $market->initDataInTableMarketOverview('20150921');

        /*TODO: test init array ticker by num of trade*/
//        $helper = new TickerHelper();
//        var_dump($helper->getMinMaxPriceTickerInNumOfTrade('MBB', 5, '20150918'));

        /*TODO: get sma*/

//        $helper = new TickerHelper();
//        var_dump($helper->getArrayTickerLastDay(10));

        /*TODO test time*/
//        $tickerHelper = new TrendOverviewHelper();
//        $tickerHelper->initTrendData();}

        /*TODO: import class*/
//        $tickerClass  = new TickerClassHelper();
//        $tickerClass->importFromFile();

        /*TODO: test report data*/
//        $rp = new ReadReportData();
//        var_dump($rp->test());


        /*TODO: test get penny two day ago*/
//        $tickerClass = Cache::get('dataTickerClass');
//        var_dump($tickerClass);
//        $arrayTickerClass = json_decode($tickerClass, true);
//        $tickerHelper = new TickerHelper();
//        $tickerTwoDayAgo = $tickerHelper->getArrayTickerLastDay(2);
//        $arrayPennyTwoDayAgo = array();
//        foreach ($tickerTwoDayAgo as $ticker => $temp) {
//            if (isset($arrayTickerClass[$ticker]) && $arrayTickerClass[$ticker]['bp'] == "penny") {
//                $arrayPennyTwoDayAgo[$ticker] = $temp['close'];
//            }
//        }
//        var_dump($arrayPennyTwoDayAgo);
//        /*TODO tinh khoang cach*/
//        $tickerHelper = new TickerHelper();
//        var_dump($tickerHelper->getMinMaxPriceTickerInNumOfTrade('APC', 76, WorkWithTime::getCurrentDate(), false, 21));
        /*TODO: init intraday*/
//        $tickerTrend = new TrendOverviewHelper();
//        $tickerTrend->initDataIntraDay();

//        $reportData = new GetDataReport();
//        $reportData->testGetData();

//        $reportHelper = new ReportTickerDataQuarterHelper();
//        var_dump($reportHelper->getArrayTickerExisted());

//        return view('pages.angular.test');

//        $collectTypeRow = new CollectTypeRow();
//        $collectTypeRow->setArrayCriteria(array('1'));
//        $collectTypeRow->setReportType('CSTC');
//        var_dump($collectTypeRow->collect());

//        $helper = new ReportTypeRowHelper();
//       echo  $helper->getRowId(1, 'CSTC_1448954857.8293');


        /*TEST FILTER*/
        $filters = array(
            array(
                'filter_type' => FilterGroups::FILTER_TYPE_ABSOLUTE,
                'report_type' => 'CSTC',
                'criteria_id' => 'cstc_eps',
                'time' => '2014',
                'check_value' => 'gt|0'
            ), /*array(
                'filter_type' => FilterGroups::FILTER_TYPE_ABSOLUTE,
                'report_type' => 'CSTC',
                'criteria_id' => 'cstc_bvps',
                'time' => '1-2014,2-2014,3-2014',
                'check_value' => 'gt|0'
            ),*/
            array(
                'filter_type' => FilterGroups::FILTER_TYPE_RELATIVE,
                'report_type' => 'CSTC',
                'criteria_id' => 'cstc_bvps',
                'time' => '1-2014,2-2014,3-2014|1-2013,2-2013',
                'check_value' => 'gt|0'
            )
        );
        $app = app();

        $filterGroup = $app->make('App\RoboStock\FilterApp\Model\Filter\FilterGroups');

        foreach ($filters as $item) {
            $filterGroup->addFilter($item);
        }

        $result = array();

        if ($filterGroup->hasFilterQuarter()) {
            $collection = $app->make('App\RoboStock\FilterApp\Model\DataFilter\Quarter\Collection');
            $collection->setFilterGroups($filterGroup);
            $collection->collect();
            $result['quarter'] = $collection->runFilter();
        }
        if ($filterGroup->hasFilterYear()) {
            $collection = $app->make('App\RoboStock\FilterApp\Model\DataFilter\Year\Collection');

            $collection->setFilterGroups($filterGroup);

            $collection->collect();

            $result['year'] = $collection->runFilter();

        }
        var_dump(array_merge_recursive($result['year'], $result['quarter']));
//        var_dump($result);

//        /*TEST SAVE DATA YEAR*/
//        $helper = new GetDataReport();
//        $helper->testGetData();
    }


    public function postSearchData(Request $request) {
        $dataInform = $request->all();
        $ticker = $dataInform['ticker'];
        $date = $dataInform['date'];
        if ($ticker != '' && $date != '') {
            $model = new TickerUpto();
            $matches = array(
                'ticker' => $ticker,
                'date' => $date
            );
            $ticker = $model->query()->where($matches)->first();
            if ($ticker) {
                return view('robostock.frontend.ticker')->with('ticker', $ticker);
            } else {
                die('Not found');
            }
        }
    }

    public function num() {
        echo 'Total: ' . TickerUpto::count();
    }


    public function ang() {
        return view('pages.angular.appAndController')->with('testKey', 'Test Key');
    }

}
