<?php

namespace App\Http\Controllers;

use App\CacheNextDay;
use App\RoboStock\Model\ExportData\ExportDataHelper;
use App\RoboStock\Model\Overview\MarketOverview\TrendOverviewHelper;
use App\RoboStock\Model\Ticker\TickerHelper;
use App\RoboStock\Model\TickerAdjust\TickerAdjustHelper;
use App\RoboStock\Model\TickerBoard\TickerBoardHelper;
use App\RoboStock\Model\WorkWithTime\WorkWithTime;
use App\TickerAjust;
use App\TickerBoard;
use App\TickerClass;
use App\TickerLastDay;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Response;

class RobostockDashboardController extends Controller {
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index() {
        return view('sb-admin.robostock.dashboard');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create() {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request) {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function show($id) {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function edit($id) {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request $request
     * @param  int $id
     * @return Response
     */
    public function update(Request $request, $id) {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return Response
     */
    public function destroy($id) {
        //
    }

    public function getDashboard() {
        return view('sb-admin.robostock.dashboard');
    }

    public function getMarketOverview() {
        /*DO: Sàn*/
        $tickerBoard = Cache::get('dataTickerBoard');
        /*DO: Class*/
        $tickerClass = Cache::get('dataTickerClass');
        /*DO: Dữ liệu ngày hôm qua*/
        $tickerLastDay = Cache::get('dataTickerLastDay');
        /*DO: Cache PLCS*/
        $arrayPlcsNextDay = Cache::get('dataPlcsNextDay');
        /*DO: Cache CashFlow*/
        $arrayCashFlow = Cache::get('dataTickerCashFlow');
        /*DO: xu thế*/
        $dataTickerTrend = Cache::get('dataTickerTrendHighLow');
        $dataSmaTrend = Cache::get('dataTickerTrendSMA');
        $dataUpTrend = Cache::get('dataTickerTrendUptrend');
        $dataRs = Cache::get('dataTickerTrendRs');
        $dataPennyCloseTwoDayAgo = Cache::get('dataPennyTwoDayAgo');

        return view('sb-admin.robostock.marketoverview')->with(array(
                'tickerBoard' => $tickerBoard,
                'tickerClass' => $tickerClass,
                'tickerLastDay' => $tickerLastDay,
                'cachePlcsNextDay' => $arrayPlcsNextDay,
                'trendData' => $dataTickerTrend,
                'trendSmaData' => $dataSmaTrend,
                'uptrendData' => $dataUpTrend,
                'dataRs' => $dataRs,
                'dataPennyCloseTwoDayAgo' => $dataPennyCloseTwoDayAgo,
                'cashFlowYesterday' => $arrayCashFlow
            )
        );
    }

    public function getDownload() {

        $tickerAjustModel = new TickerAjust();
        $allTicker = $tickerAjustModel->all();

        $tickerBoardHelper = new TickerBoardHelper();
        $jsonTickers = $tickerBoardHelper->getArrayTicker();
        return view('sb-admin.robostock.download.download')->with(array(
            'tickers' => $jsonTickers,
            'tickersAdj' => $allTicker
        ));
    }

    public function postDownloadData(Request $request) {
        $all = $request->all();
        if (!$all['ticker'] == '') {
            $arrayTicker = explode(',', $all['ticker']);
        } else {
            $arrayTicker = null;
        }
        $arrayDate = explode('-', $all['from']);
        $from = $arrayDate[0] . $arrayDate[1] . $arrayDate[2];
        $from = str_replace(' ', '', $from);
        $to = $arrayDate[3] . $arrayDate[4] . $arrayDate[5];
        $to = str_replace(' ', '', $to);
        $helper = new ExportDataHelper();
        $fileName = $helper->ImportFile('upto', $arrayTicker, $from, $to, null, $all['format']);
        return Response::download("./" . $fileName);
    }

    public function postFastDownloadData(Request $request) {
        $tickerAdjustHelper = new TickerAdjustHelper();
        $arrayTicker = $tickerAdjustHelper->getArrayTickersAdjust(null, 20);
        $helper = new ExportDataHelper();
        $fileName = $helper->ImportFile('upto', $arrayTicker, '20000101', WorkWithTime::getCurrentDate(), null, 'ami');
        return Response::download("./" . $fileName);
    }

    /*TODO: cái này chỉ làm tạm*/

    public function getLogin() {
        return view('sb-admin.robostock.login');
    }

}
