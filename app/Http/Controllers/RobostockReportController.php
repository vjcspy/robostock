<?php

namespace App\Http\Controllers;

use App\ReportTickerDataQuarter;
use App\ReportTypeRow;
use App\RoboStock\Model\TickerBoard\TickerBoardHelper;
use App\RoboStock\Report\Model\ExportExcel;
use App\RoboStock\Report\Model\ReportTickerDataQuarterHelper;
use App\RoboStock\Report\Model\ReportTickerDataYearHelper;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Response;

class RobostockReportController extends Controller {
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index() {
        return view('sb-admin.robostock.report.report');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create() {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request) {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function show($id) {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function edit($id) {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request $request
     * @param  int $id
     * @return Response
     */
    public function update(Request $request, $id) {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return Response
     */
    public function destroy($id) {
        //
    }

    public function getReport() {

        $tickerBoardHelper = new TickerBoardHelper();
        $jsonTickers = $tickerBoardHelper->getArrayTicker();
        return view('sb-admin.robostock.report.report')->with(array(
            'tickers' => $jsonTickers,
        ));
    }

    public function getDataReport(Request $request) {
        $allData = $request->all();

        $data = array();

        switch ($allData['dataType']) {
            case 'time':
                $ticker = $allData['ticker'];
                $reportType = $allData['reportType'];
                $reportTimeType = $allData['timeType'];
                if ($reportTimeType == 'quarter')
                    $helper = new ReportTickerDataQuarterHelper();
                $arrayQuarterTime = $helper->getArrayTimeQuarter($ticker, $reportType);
                $data = array(
                    'status' => 'success',
                    'quartTime' => $arrayQuarterTime
                );
                break;
            case 'criteria':
                $ticker = $allData['ticker'];
                $reportType = $allData['reportType'];
                $dataHelper = new ReportTickerDataQuarterHelper();
                /*FIXME: trong truong hop lay theo nam co the khac cac row voi lay theo quy*/
                $reportTickerType = $dataHelper->getReportTickerType($ticker, $reportType);
                if (is_null($reportTickerType))
                    $data = array(
                        'error' => true,
                        'message' => 'Không tìm thấy kiểu report của cổ phiếu này'
                    );
                else {
                    $reportRowModel = $this->getReportTypeRowModel();
                    $collection = $reportRowModel->query()->where('report_ticker_type', $reportTickerType)->orderBy('id', 'asc')->get();
                    $dataReportCriteria = array();
                    foreach ($collection as $item) {
                        $dataReportCriteria[] = array(
                            'row_id' => $item->row_id,
                            'row_name' => $item->row_name
                        );
                    }
                    $data = array(
                        'dataCriteria' => $dataReportCriteria
                    );
                }
                break;
            case 'dataReportTicker':
                $ticker = $allData['ticker'];
                $reportType = $allData['reportType'];

                if (isset($allData['t1']))
                    $t1 = $allData['t1'];
                else
                    $t1 = null;

                if (isset($allData['t2']))
                    $t2 = $allData['t2'];
                else
                    $t2 = null;

                $checkTime1 = $this->checkTime($t1);
                $checkTime2 = $this->checkTime($t2);

                $reportTickerQuarterHelper = new ReportTickerDataQuarterHelper();
                $reportTickerYearHelper = new ReportTickerDataYearHelper();

                $dataT1 = null;
                $dataT2 = null;

                if (!is_null($checkTime1) && !is_null($checkTime2)) {
                    // Neu la kieu quarter
                    if ($checkTime1['timeType'] == 'quarter') {
                        $dataT1 = $reportTickerQuarterHelper->getDataReportTickerQuarter($ticker, $reportType, $checkTime1);
                    }
                    if ($checkTime2['timeType'] == 'quarter') {
                        $dataT2 = $reportTickerQuarterHelper->getDataReportTickerQuarter($ticker, $reportType, $checkTime2);
                    }

                    if ($checkTime1['timeType'] == 'year') {
                        $dataT1 = $reportTickerYearHelper->getDataReportTickerYear($ticker, $reportType, $checkTime1);
                    }
                    if ($checkTime2['timeType'] == 'year') {
                        $dataT2 = $reportTickerYearHelper->getDataReportTickerYear($ticker, $reportType, $checkTime2);
                    }

                    if (is_null($dataT1) || is_null($dataT2)) {
                        $data = array(
                            'error' => true,
                            'message' => 'Không tìm thấy dữ liệu thời gian'
                        );
                    } else {
                        $data = array(
                            'dataT1' => $dataT1,
                            'dataT2' => $dataT2
                        );
                    }
                } else {
                    $data = array(
                        'error' => true,
                        'message' => 'Không tìm thấy dữ liệu thời gian'
                    );
                }

                break;
        }

        echo json_encode($data);
    }

    private function checkTime($time) {
        if (is_null($time))
            return null;
        $arrayTime = explode('-', $time);
        if (count($arrayTime) > 1) {
            $arrayTimes = json_decode($time, true);
            $times = array();
            foreach ($arrayTimes as $arrayTime) {
                $arrayTime = explode('-', $arrayTime);
                $q = $arrayTime[0];
                $y = $arrayTime[1];
                $currentTime = array(
                    'q' => $q,
                    'y' => $y
                );
                $times[] = $currentTime;
            }
            return array(
                'timeType' => 'quarter',
                'times' => $times);
        } else {
            //Lay theo nam
            return array(
                'timeType' => 'year',
                'y' => $time
            );
        }
    }

    private function getDataTickerReport($ticker, $checkTime) {
        switch ($checkTime['timeType']) {
        }
    }


    private $_ReportTypeRowModel;

    /**
     * @return ReportTypeRow
     */
    private function getReportTypeRowModel() {
        if (is_null($this->_ReportTypeRowModel))
            $this->_ReportTypeRowModel = new ReportTypeRow();
        return $this->_ReportTypeRowModel;
    }

    private $_ReportTickerDataQuarterModel;

    /**
     * @return ReportTickerDataQuarter
     */
    private function getReportTickerDataQuarterModel() {
        if (is_null($this->_ReportTickerDataQuarterModel))
            $this->_ReportTickerDataQuarterModel = new ReportTickerDataQuarter();
        return $this->_ReportTickerDataQuarterModel;
    }

    public function postDownloadExcel(Request $request) {
        $all = $request->all();
        $exportExcel = new ExportExcel();
        $fileName = $exportExcel->initFileDownload($all);
        $fileName = $fileName . '.xls';
        return Response::download("../storage/exports/" . $fileName);
    }
}
