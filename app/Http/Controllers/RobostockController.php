<?php

namespace App\Http\Controllers;

use App\IndexUpto;
use App\LogWork;
use App\RoboStock\Model\Email\EmailHelper;
use App\RoboStock\Model\ExportData\ExportDataHelper;
use App\RoboStock\Model\IndexUpto\IndexUptoHelper;
use App\RoboStock\Model\Overview\Foreign\ForeignHelper;
use App\RoboStock\Model\Overview\Index\IndexOverviewHelper;
use App\RoboStock\Model\Overview\MarketOverview\MarketOverviewHelper;
use App\RoboStock\Model\TickerBoard\TickerBoardHelper;
use App\RoboStock\Model\TickerLastDay\TickerLastDayHelper;
use App\RoboStock\Model\TickerUpto\CheckHsdc;
use App\RoboStock\Model\TickerUpto\ImportFile;
use App\RoboStock\Model\TickerUpto\ImportFromBoardPrice;
use App\RoboStock\Model\WorkWithTime\WorkWithTime;
use App\TickerAjust;
use App\TickerClass;
use App\TickerUpto;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Response;

class RobostockController extends Controller {
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index() {
        return view('robostock.frontend.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create() {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request) {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function show($id) {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function edit($id) {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request $request
     * @param  int $id
     * @return Response
     */
    public function update(Request $request, $id) {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return Response
     */
    public function destroy($id) {
        //
    }


    /**
     * TODO: Show All ticker in day
     * @return $this
     */
    public function getAlltickerinday() {
        $boardPrice = new ImportFromBoardPrice();
        $tickers = $boardPrice->getAllDataTickerCurrentDayFromVietStock(true, true);
        return view('robostock.frontend.allTickerInDay')->with('tickers', $tickers);
    }


    /**
     * TODO: Show All ticker Ajust in day
     * @return $this
     */
    public function getHsdc() {
        $tickerAjustModel = new TickerAjust();
        $allTicker = $tickerAjustModel->all();
        if (is_null($allTicker))
            return view('robostock.frontend.checkhsdc')->with('tickers', array());
        else
            return view('robostock.frontend.checkhsdc')->with('tickers', $allTicker);
    }

    /**
     * TODO: show page search Ticker -> data will direct to postSearchData
     * @return \Illuminate\View\View
     */
    public function getSearch() {
        return view('robostock.frontend.searchTicker');
    }

    public function postSearchData(Request $request) {
        $dataInform = $request->all();
        $ticker = $dataInform['ticker'];
        $date = $dataInform['date'];
        if ($ticker != '' || $date != '') {
            $matches = array();
            $model = new TickerUpto();
            if ($ticker != '') {
                $matches['ticker'] = $ticker;
            }
            if ($date != '') {
                $matches['date'] = $date;
            }
            $tickers = $model->query()->where($matches)->orderBy('date', 'desc')->get();

            /*DO:Lấy class ticker*/
            $tickerClassModel = $this->getTickerClassModel();
            $tickerClass = $tickerClassModel->query()->where('ticker', $ticker)->first();

            /*DO:lấy hsdc của cổ phiếu ngày hôm nay đề phòng trường hợp chưa kịp điều chỉnh*/
            $allTickerHsdc = array();
            $tickerAdjustModel = new TickerAjust();
            foreach ($tickers as $item) {
                $matches = array(
                    'date' => WorkWithTime::getCurrentDate(),
                    'ticker' => $item->ticker,
                );
                $first = $tickerAdjustModel->query()->where($matches)->where('active', '<>', '1')->first();
                if (!is_null($first)) {
                    $allTickerHsdc[$item->ticker] = $first;
                }
            }

            if ($ticker)
                return view('robostock.frontend.ticker')->with(array(
                    'tickers' => $tickers,
                    'hsdc' => $allTickerHsdc,
                    'tickerClass' => $tickerClass
                ));
            else
                die('Not found');
        }
        return null;
    }

    /**
     *TODO: Điều chỉnh lại bảng ticker_upto theo bảng ticker_adjust
     */
    public function getAutoadjustticker() {
        $dc = new CheckHsdc();
        $dc->ajustAllTickerFromTickerAdjustTable();
        echo 'done';
    }

    /**
     *TODO: Dùng để update toàn bộ dữ liệu ngày hiện tại từ sàn vào db
     * IMPORTANCE: chạy cuối ngày
     */
    public function getUpdatedatainday() {
        $import = new ImportFromBoardPrice();
        var_dump($import->updateAllData());
    }


    private $_tickerClassModel;

    private function getTickerClassModel() {
        if (is_null($this->_tickerClassModel))
            $this->_tickerClassModel = new TickerClass();
        return $this->_tickerClassModel;
    }

    public function getTestemail() {
        $mailHelper = new EmailHelper();
        echo $mailHelper->test();
    }

    public function getCheckhsdc() {
        $check = new CheckHsdc();
        var_dump($check->checkReferenceStandard());

    }

    /*TODO: Show market Overview*/
    public function getMarketoverview() {
        return view('robostock.frontend.marketOverview');
    }

    public function postDatamarketoverview(Request $request) {
        $data = $request->all();
        $date = $data['date'];
        $marketOverviewHelper = new MarketOverviewHelper();
        $allClass = $marketOverviewHelper->initDataInTableMarketOverview($date);
        if ($allClass == null)
            die('');
        $up = json_decode($allClass['cashFlow']->data_up, true);
        $down = json_decode($allClass['cashFlow']->data_down, true);
        $a = array();
        $a['content'] = view('robostock.frontend.block.marketoverviewticket')->with(array(
            'tickers' => $allClass['marketOverview'],
            'cashFLow' => $allClass['cashFlow'],
            'up' => $up,
            'down' => $down,
        ))->render();
        echo $result = json_encode($a);
//        return (new Response($result, 304))->header('Content-Type', $result)->setContent($request);
    }

    public function getCheckupdateboard() {
        $helper = new TickerBoardHelper();
        var_dump($helper->checkUpdateBoard());
    }


    /*TODO: lay h hien tai cua server*/
    public function getCurrent() {
        echo WorkWithTime::getCurrentDateTime();
    }

    public function getNextDay() {
        echo WorkWithTime::nextDay();
    }

    public function getLastDay() {
        echo WorkWithTime::last_day();
    }


    /*TODO: update table index_upto hang ngay*/
    public function getUpdateindexupto() {
        $helper = new IndexUptoHelper();
        $helper->getDataFromCafeF();
        echo 'Done';
    }


    /*TODO: get view foreign*/
    public function getForeign() {
        return view('robostock.frontend.foreign.foreign');
    }

    public function postDataforeignoverview(Request $request) {
        $dataAll = $request->all();
        $date = $dataAll['date'];
        $board = $dataAll['board'];
//        $mb = $dataAll['mb'];
        if (is_null($date) || $date == '') {
            return;
        }
        $helper = new ForeignHelper();
        $allForeignData = $helper->getDataForeign($date, null, $board);
        if (count($allForeignData['dataForeign']) == 0)
            return;
        $a = array();
        $a['content'] = view('robostock.frontend.foreign.blockItemForeign')->with(array(
            'board' => $board,
            'tickers' => $allForeignData['dataForeign'],
        ))->render();
        $result = json_encode($a);
        echo $result;
    }

    public function getInittickerlastday() {
        $tickerLastDayHelper = new TickerLastDayHelper();
        $tickerLastDayHelper->initTickerLastDay();
        echo 'Done';
    }


    /*TODO: INDEX*/

    public function getSearchindex() {
        return view('robostock.frontend.index.searchIndex');
    }

    public function postSearchindex(Request $request) {
        $dataInform = $request->all();
        $ticker = $dataInform['ticker'];
        $date = $dataInform['date'];
        if ($ticker != '' || $date != '') {
            $matches = array();
            $model = new IndexUpto();
            if ($ticker != '') {
                $matches['name'] = $ticker;
            }
            if ($date != '') {
                $matches['date'] = $date;
            }
            $tickers = $model->query()->where($matches)->orderBy('date', 'desc')->get();
            $indexOverViewHelper = new IndexOverviewHelper();
            $indexOverview = $indexOverViewHelper->getDataCurrentDate($date);
            return view('robostock.frontend.index.index')->with(array(
                'tickers' => $tickers,
                'over' => $indexOverview
            ));

        }
        return null;
    }

    /*  TODO: end index*/

    /*TODO: download*/

    public function getDownload() {
        return view('robostock.frontend.download.download');
    }

    public function postDownloaddata(Request $request) {
        $all = $request->all();
        $ticker = $all['ticker'];
        $from = $all['from'];
        $to = $all['to'];
        $fomat = $all['format'];
        $helper = new ExportDataHelper();
        $fileName = $helper->ImportFile('upto', $ticker, $from, $to, null, $fomat);
        return Response::download("./" . $fileName);
    }

    public function getLog() {
        $tickers = $this->getLogWorkModel()->query()->where('date', WorkWithTime::getCurrentDate())->get();
        return view('robostock.frontend.log')->with(array(
            'tickers' => $tickers
        ));
    }

    private $_LogWorkModel;

    /**
     * @return LogWork
     */
    private function getLogWorkModel() {
        if (is_null($this->_LogWorkModel))
            $this->_LogWorkModel = new LogWork();
        return $this->_LogWorkModel;
    }

//    function __construct() {
//        $this->middleware('auth');
//    }
}
