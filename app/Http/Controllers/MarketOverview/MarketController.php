<?php

namespace App\Http\Controllers\MarketOverview;

use App\IndexUpto;
use App\OverviewIntraday;
use App\RoboStock\Model\IndexUpto\IndexUptoHelper;
use App\RoboStock\Model\TickerUpto\ImportFromBoardPrice;
use App\RoboStock\Model\WorkWithTime\WorkWithTime;
use App\TickerInDay;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Cache;

class MarketController extends Controller {
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index() {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create() {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request) {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function show($id) {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function edit($id) {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request $request
     * @param  int $id
     * @return Response
     */
    public function update(Request $request, $id) {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return Response
     */
    public function destroy($id) {
        //
    }

    public function postTickerInday(Request $request) {
        $variable = 0;
        if ($variable > 1) {
            /*TODO: cache tickerIndayModel*/
            if (!Cache::has('tickerIndayModel')) {
                $model = $this->getTickerIndayModel();
                /*FIXME: chi luu trong 1 ngay*/
                Cache::put('tickerIndayModel', $model, 60 * 24);
            }
            $model = Cache::get('tickerIndayModel');
            $tickers = $model->all();
            $arrayTickerInday = array();
            foreach ($tickers as $ticker) {
                $arrayTickerInday[$ticker->ticker] = array(
                    'open' => $ticker->open,
                    'high' => $ticker->high,
                    'low' => $ticker->low,
                    'close' => $ticker->close,
                    'vol' => $ticker->vol,
                );
            }
            $result = json_encode($arrayTickerInday);
            echo($result);
        } else {
            /*TODO: get FROM VIETSTOCK BOARD*/
            $tickerFromBoard = new ImportFromBoardPrice();
            $arrayTickerInday = $tickerFromBoard->getAllDataTickerCurrentDayFromVietStock(false, false);
            $indexCafeF = new IndexUptoHelper();
            $dataIndexs = $indexCafeF->getDataFromCafeF(false);
            $arrayIndexInday = array();
            foreach ($dataIndexs as $index) {
                $arrayIndexInday[$index['name']] = array(
                    'percent' => $index['percent'],
                    'change' => $index['change'],
                    'index' => $index['index'],
                    'value' => $index['value'],
                    'vol' => $index['volume']
                );
            }

            $data = array(
                'tickerInDay' => $arrayTickerInday,
                'indexInday' => $arrayIndexInday,
                'dataIntraday' => $this->getDataChartIntraday());
            echo(json_encode($data));
        }
    }

    private function getDataChartIntraday() {
        date_default_timezone_set('Asia/Bangkok');
        $model = $this->getOverviewIntradayModel();
        $allData = $model->query()->where('date', WorkWithTime::getCurrentDate())->orderBy('id_inday', 'desc')->get();
        $arrayDataChart = array();
        foreach ($allData as $d) {
            $a = $d->created_at;
            $b = strtotime($a);
            $c = date('h:i A', $b);
            $arrayDataChart[] = array(
                'bPercent' => $d->blue_percent,
                'pPercent' => $d->penny_percent,
                'create_at' => $c
            );
        }
        return $arrayDataChart;
    }

    private $_OverviewIntradayModel;

    /**
     * @return OverviewIntraday
     */
    private function getOverviewIntradayModel() {
        if (is_null($this->_OverviewIntradayModel))
            $this->_OverviewIntradayModel = new OverviewIntraday();
        return $this->_OverviewIntradayModel;
    }

    private $_TickerIndayModel;

    /**
     * @return TickerInday
     */
    private function getTickerIndayModel() {
        if (is_null($this->_TickerIndayModel))
            $this->_TickerIndayModel = new TickerInDay();
        return $this->_TickerIndayModel;
    }

    private $_IndexUptoModel;

    /**
     * @return IndexUpto
     */
    private function getIndexUptoModel() {
        if (is_null($this->_IndexUptoModel))
            $this->_IndexUptoModel = new IndexUpto();
        return $this->_IndexUptoModel;
    }
}
