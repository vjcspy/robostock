<?php

namespace App\Http\Controllers\MarketOverview;

use App\TickerUpto;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Cache;

class PlcsController extends Controller {
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index() {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create() {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request) {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function show($id) {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function edit($id) {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request $request
     * @param  int $id
     * @return Response
     */
    public function update(Request $request, $id) {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return Response
     */
    public function destroy($id) {
        //
    }

    public function postPercentIndex(Request $request) {
        $all = $request->all();
//        $date = $all['date'];
//        $plcs = new PlcsHelper();
        $testdata = array(
            'a' => 1,
            'b' => 2
        );
        $result = json_encode($testdata);
        echo $result;
    }

    public function getTestCache() {
        Cache::put('key', 'value', 10);
        if (Cache::has('key')) {
            $value = Cache::get('key');
            echo $value;
        }
    }

    private $_TickerUptoModel;

    /**
     * @return TickerUpto
     */
    private function getTickerUptoModel() {
        if (is_null($this->_TickerUptoModel))
            $this->_TickerUptoModel = new TickerUpto();
        return $this->_TickerUptoModel;
    }
}
