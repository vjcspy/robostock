<?php

namespace App\Http\Controllers;

use App\RoboStock\FilterApp\Model\Filter\FilterGroups;
use App\RoboStock\FilterApp\Model\ReportCriteriaRowHelper;
use App\RoboStock\Model\Ticker\TickerHelper;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Response;

class RobostockFilterAppController extends Controller {


    private $_reportCriteriaRowHelper;
    private $_filterGroups;
    private $_quarterCollection;
    private $_yearCollection;
    private $_tickerHelper;

    public function __construct(ReportCriteriaRowHelper $reportCriteriaRowHelper, FilterGroups $filterGroups, \App\RoboStock\FilterApp\Model\DataFilter\Quarter\Collection $quarterCollection, \App\RoboStock\FilterApp\Model\DataFilter\Year\Collection $yearCollection, TickerHelper $tickerHelper) {
        $this->_reportCriteriaRowHelper = $reportCriteriaRowHelper;
        $this->_filterGroups = $filterGroups;
        $this->_quarterCollection = $quarterCollection;
        $this->_yearCollection = $yearCollection;
        $this->_tickerHelper = $tickerHelper;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index() {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create() {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request) {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function show($id) {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function edit($id) {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request $request
     * @param  int $id
     * @return Response
     */
    public function update(Request $request, $id) {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return Response
     */
    public function destroy($id) {
        //
    }

    public function getPage() {
        //get All criteria by type:
        $criteriaGroupByType = $this->_reportCriteriaRowHelper->getAllCriteriaGroupByType();

        return view('sb-admin.robostock.filterapp.filterapp')->with(array(
            'criteria' => json_encode($criteriaGroupByType),
        ));
    }


    public function postFilter(Request $request) {
        $datas = $request->all();

        $filterGroup = $this->_filterGroups;
        foreach ($datas['filterData'] as $data) {
//            if (!$data['isYear']) {
//                $data['time1'] = substr($data['time1'], 0, 1) . '-' . substr($data['time1'], 1);
//                $data['time2'] = substr($data['time2'], 0, 1) . '-' . substr($data['time2'], 1);
//            }
            if ($data['filterType'] == FilterGroups::FILTER_TYPE_ABSOLUTE)
                $time = $data['time1'];
            else
                $time = $data['time1'] . '|' . $data['time2'];

            if ($data['condition'] == 'in')
                $checkValue = $data['condition'] . '|' . $data['checkValue'] . '|' . $data['checkValue2'];
            else if ($data['condition'] == 'all')
                $checkValue = 'lt|' . 99999999999;
            else
                $checkValue = $data['condition'] . '|' . $data['checkValue'];
            $currentData = array(
                'filter_type' => $data['filterType'],
                'report_type' => $data['reportType'],
                'criteria_id' => $data['criteriaId'],
                'time' => $time,
                'check_value' => $checkValue
            );

            $filterGroup->addFilter($currentData);
        }
        $result = array();
        $arrayTicker = array();

        if ($filterGroup->hasFilterQuarter()) {
            $collection = $this->_quarterCollection;

            $collection->setFilterGroups($filterGroup);

            $collection->collect();

            $result['quarter'] = $collection->runFilter();
            $arrayTicker = array();
            foreach ($result['quarter'] as $key => $item) {
                $arrayTicker[] = $key;
            }
        }
        if ($filterGroup->hasFilterYear()) {
            $collection = $this->_yearCollection;

            $collection->setFilterGroups($filterGroup);

            $collection->collect();

            $result['year'] = $collection->runFilter();
            $arrayTicker = array();
            foreach ($result['year'] as $key => $item) {
                $arrayTicker[] = $key;
            }
        }

        $clone =  $result;
        if ($filterGroup->hasFilterYear() && $filterGroup->hasFilterQuarter()) {
            $result = array_merge_recursive($result['year'], $result['quarter']);
            $arrayTicker = array();
            foreach ($result as $key => $item) {
                if (array_key_exists($key, $clone['year']) && array_key_exists($key, $clone['quarter']))
                    $arrayTicker[] = $key;
                else
                    unset($result[$key]);
            }
        }


        $data = array(
            'resultFilter' => $result,
            'rocData' => $this->_tickerHelper->getRateOfChangeInDayByArrayTicker($arrayTicker));

        $exportExcel = app()->make('App\RoboStock\FilterApp\Model\ExportExcel\Export');
        $fileName = $exportExcel->export($data);
        $data['fileName'] = $fileName;

        return response()->json($data);
    }

    public function postDownloadExcel(Request $request) {
        $data = $request->all();
        $fileName = $data['fileName'] . '.xls';
        return Response::download("../storage/exports/" . $fileName);
    }
}
