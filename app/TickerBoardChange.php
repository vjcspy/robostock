<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TickerBoardChange extends Model {
    protected $table = 'ticker_board_change';
    protected $fillable = ['ticker', 'date', 'from', 'to'];
    //
}
