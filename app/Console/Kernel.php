<?php

namespace App\Console;

use App\RoboStock\CronJob\TestCron;
use App\RoboStock\Model\General;
use App\RoboStock\Model\Overview\MarketOverview\TrendOverviewHelper;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel {
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        \App\Console\Commands\Inspire::class,
        Commands\TestCommand::class,
        Commands\TestFilter::class,
        \Modules\Admin\Console\TaskCommandAbstract::class
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule) {
        /*DO: tao index upto*/
        date_default_timezone_set('Asia/Bangkok');
        $schedule->call(function () {
            $general = new General();
            $general->updateIndexUpto();
        })->cron('* 8,9,10,11,12,13,14,15,16 * * 1-5');

        /*DO: Tao ticker inday */
        $schedule->call(function () {
            $general = new General();
            $general->updateTickerInday();
        })->cron('*/5 * * * 1-5');

        /*DO: check dieu chinh tu thu 2-6 va luc 8h50AM: */
        $schedule->call(function () {
            $general = new General();
            $general->checkHsdc();
        })->cron('50 8 * * 1-5');

        /*DO: check Board tu thu 2 den thu 6 vao luc 8h55AM*/
        $schedule->call(function () {
            $general = new General();
            $general->updateTickerBoard();
        })->cron('55 8 * * 1-5');

        /*DO: tao cache ticker_lastday tu thu 2 den thu 6 vao luc 1AM*/
        $schedule->call(function () {
            $general = new General();
            $general->initTickerLastDay(false);
        })->cron('30 1,9 * * 1-5');

        /*DO: tao cache plcs 23H*/
        $schedule->call(function () {
            $general = new General();
            $general->initCachePlcsNextDay();
        })->daily()->at('23:00');

        /*DO: update Ticker Upto 18PM*/
        $schedule->call(function () {
            $general = new General();
            $general->updateTickerUpto();
        })->cron('0 18 * * 1-5');

        /*DO: Dieu chinh du lieu 8:57 AM*/
        $schedule->call(function () {
            $general = new General();
            $general->dieuChinh();
        })->cron('57 8 * * 1-5');

        /*DO: Refresh intraday*/
        $schedule->call(function () {
            $general = new General();
            $general->refreshIntraDay();
        })->cron('0 5 * * *');

        /*DO: init data intraday*/
        $schedule->call(function () {
            $general = new TrendOverviewHelper();
            $general->initDataIntraDay();
        })->cron('*/5 9,10,11,12,13,14,15,16 * * 1-5');

        /*DO: init market overview*/
        $schedule->call(function () {
            $general = new General();
            $general->initDataMarketOverview(true);
        })->cron('35 5,9 * * 1-5');
        /*FOR TEST*/
        $schedule->call(function () {
            $testCron = new TestCron();
            $testCron->testAddToDb();
        })->cron('16 20 * * *');

        /*DO: init Cash Flow to day*/
        $schedule->call(function () {
            $general = new General();
            $general->initDataCashFLowYesterDay(true);
        })->cron('35 23 * * 1-5');
        /*FOR TEST*/
        $schedule->call(function () {
            $testCron = new TestCron();
            $testCron->testAddToDb();
        })->cron('16 20 * * *');
    }
}
