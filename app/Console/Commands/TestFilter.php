<?php

namespace App\Console\Commands;

use App\RoboStock\FilterApp\Model\Filter\FilterGroups;
use Illuminate\Console\Command;

class TestFilter extends Command {
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'iz:testFilter';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description.';

    private $_filterGroups;
    private $_quarterCollection;
    private $_yearCollection;

    /**
     * Create a new command instance.
     *
     * @param FilterGroups $filterGroups
     * @param \App\RoboStock\FilterApp\Model\DataFilter\Quarter\Collection $quarterCollection
     * @param \App\RoboStock\FilterApp\Model\DataFilter\Year\Collection $yearCollection
     * @internal param FilterGroups $filterGroup
     */
    public function __construct(FilterGroups $filterGroups, \App\RoboStock\FilterApp\Model\DataFilter\Quarter\Collection $quarterCollection, \App\RoboStock\FilterApp\Model\DataFilter\Year\Collection $yearCollection) {
        $this->_filterGroups = $filterGroups;
        $this->_quarterCollection = $quarterCollection;
        $this->_yearCollection = $yearCollection;
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle() {
        do {
            $this->addNewFilter();
            $continue = $this->confirm('Tiếp tục add filter? [y|N]');
        } while ($continue);
        $this->comment('Đang lọc. Xin hãy chờ!');
        $result = $this->filter();
        $this->info('Found: ');
        if (isset($result['quarter']))
            foreach ($result['quarter'] as $item) {
                $this->line($item);
            }
        elseif (isset($result['year']))
            foreach ($result['year'] as $item) {
                $this->line($item);
            }
        else
            foreach ($result as $item) {
                $this->line($item);
            }
    }

    private function filter() {
        $filterGroup = $this->_filterGroups;

        foreach ($this->arrayFilters as $item) {
            $filterGroup->addFilter($item);
        }

        $result = array();

        if ($filterGroup->hasFilterQuarter()) {
            $collection = $this->_quarterCollection;

            $collection->setFilterGroups($filterGroup);

            $collection->collect();

            $result['quarter'] = $collection->runFilter();
        }
        if ($filterGroup->hasFilterYear()) {
            $collection = $this->_yearCollection;

            $collection->setFilterGroups($filterGroup);

            $collection->collect();

            $result['year'] = $collection->runFilter();

        }

        if ($filterGroup->hasFilterYear() && $filterGroup->hasFilterQuarter())
            $result = array_intersect($result['quarter'], $result['year']);
        return $result;
    }

    private $arrayFilters = array();

    private function addNewFilter() {

//        $filterType = $this->choice('Chọn kiểu lọc?(Mặc định là tuyệt đối)', [FilterGroups::FILTER_TYPE_ABSOLUTE, FilterGroups::FILTER_TYPE_RELATIVE], 0);
//        $this->info('Đã chọn kiểu lọc: ' . $filterType);
        $reportType = $this->choice('Chọn kiểu Report?(Mặc định là CDKT)', ['CDKT', 'LC', 'KQKD', 'CSTC'], 0);
//        $this->info('Đã chọn kiểu Report: ' . $reportType);
        $criteria_id = $this->ask('Chọn Criteria_id?');
//        $this->info('Đã chọn criteria_id: ' . $criteria_id);
        $time = $this->ask('Chọn Time?');
//        $this->info('Đã chọn Time: ' . $time);
        $checkValue = $this->ask('Chọn giá trị check_value?');
//        $this->info('Đã chọn check_value: ' . $checkValue);
        if (strpos($time, '|') !== false)
            $filterType = FilterGroups::FILTER_TYPE_RELATIVE;
        else
            $filterType = FilterGroups::FILTER_TYPE_ABSOLUTE;
        $filter = array(
            'filter_type' => $filterType,
            'report_type' => $reportType,
            'criteria_id' => $criteria_id,
            'time' => $time,
            'check_value' => $checkValue
        );
        $this->arrayFilters[] = $filter;
    }
}
