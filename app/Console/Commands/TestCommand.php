<?php

namespace App\Console\Commands;

use App\RoboStock\Report\Model\ReportTickerDataQuarterHelper;
use App\RoboStock\Report\Model\ReportTickerDataYearHelper;
use App\RoboStock\Report\Run\GetDataReport;
use App\TickerInDay;
use Illuminate\Console\Command;

class TestCommand extends Command {
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'iz:test';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct() {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle() {
        //
        $output = $this->getOutput();
        $model = new TickerInDay();
        $allTickers = $model->all();

//        $helper = new ReportTickerDataQuarterHelper();
//        $arrayTickerExisted = $helper->getArrayTickerExisted('CSTC');
        $helper = new GetDataReport();

        $type = ['BCTC', 'KQKD', 'CDKT', 'LC'];
        foreach ($type as $reportType) {


            foreach ($allTickers as $allTicker) {
//            if (in_array($allTicker->ticker, $arrayTickerExisted)) {
//                // green text
//                $output->writeln('<info>Saved(inDB): ' . $allTicker->ticker . '</info>');
//                continue;
//            }
//            if ($count > 50)
//                return 'Worked with 50 ticker';
                $output->writeln('<comment>Saving: "' . $allTicker->ticker . '"</comment>');
                $type = $helper->saveDataReportTickerYear($allTicker->ticker,$reportType);
                // green text
                if ($type != null)
                    $output->writeln('<info>Saved: ' . $allTicker->ticker . ' with type: ' . $type . ' </info>');
                else
                    $output->writeln('<error>Can\'t save: ' . $allTicker->ticker . ' </error>');
            }
        }
    }
}
