<?php

namespace App\Console\Commands;

use App\TickerInDay;
use Illuminate\Console\Command;

class RobostockReport extends Command {
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'robostock:report';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct() {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle() {
        $output = $this->getOutput();
        $model = new TickerInDay();
        $allTickers = $model->all();

        $helper = new ReportTickerDataYearHelper();
        $arrayTickerExisted = $helper->getArrayTickerExisted('LC');
        $helper = new GetDataReport();

        foreach ($allTickers as $allTicker) {
            if (in_array($allTicker->ticker, $arrayTickerExisted)) {
                // green text
                $output->writeln('<info>Saved(inDB): ' . $allTicker->ticker . '</info>');
                continue;
            }
//            if ($count > 50)
//                return 'Worked with 50 ticker';
            $output->writeln('<comment>Saving: "' . $allTicker->ticker . '"</comment>');
            $type = $helper->saveDataReportTickerYear($allTicker->ticker, 'CSTC');
            // green text
            if ($type != null)
                $output->writeln('<info>Saved: ' . $allTicker->ticker . ' with type: ' . $type . ' </info>');
            else
                $output->writeln('<error>Can\'t save: ' . $allTicker->ticker . ' </error>');
        }
    }
}
