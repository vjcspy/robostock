<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class IndexUpto extends Model {
    protected $table = 'index_upto';
    protected $fillable = ['change', 'index', 'name', 'percent', 'vol', 'value', 'date'];
    //
}
