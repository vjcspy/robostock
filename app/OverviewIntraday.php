<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OverviewIntraday extends Model {
    protected $table = 'overview_intraday';
    protected $fillable = ['id_inday', 'blue_percent', 'penny_percent', 'date'];

    //
}
