<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TickerForeign extends Model {
    protected $table = 'ticker_foreign';
    protected $fillable = ['nnb','nnm','ticker','date'];
    //
}
