<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ReportTypeSize extends Model {
    protected $table = 'report_type_size';
    protected $fillable = ['report_type', 'report_ticker_type', 'size'];
    //
}
