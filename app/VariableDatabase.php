<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VariableDatabase extends Model {
    protected $table = 'variable';
    protected $fillable = ['name', 'value'];
    //
}
