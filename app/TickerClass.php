<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TickerClass extends Model {
    protected $table = 'ticker_class';
    protected $fillable = ['ticker', 'top', 'bp', 'cd'];
    //
}
