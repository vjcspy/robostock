<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ReportCriteriaRow extends Model {
    protected $table = 'report_criteria_row';
    protected $fillable = ['criteria_id', 'report_type', 'row_name'];
    //
}
