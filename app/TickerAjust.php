<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TickerAjust extends Model {
    protected $table = 'ticker_adjust';
    protected $fillable = ['ticker', 'hsdc', 'date', 'active','board'];
    //
}
