<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LogWork extends Model {
    protected $table = 'log_work';
    protected $fillable = ['data', 'work'];
    //
}
