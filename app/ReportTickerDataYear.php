<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ReportTickerDataYear extends Model
{
    //
    protected $table = 'report_ticker_data_year';
    protected $fillable = ['report_type', 'report_ticker_type', 'ticker', 'data', 'year'];
}
