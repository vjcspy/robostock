<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ReportTickerDataQuarter extends Model {
    protected $table = 'report_ticker_data_quarter';
    protected $fillable = ['report_type', 'report_ticker_type', 'ticker', 'data', 'quarter'];
    //
}
