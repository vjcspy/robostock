<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MarketOverview extends Model {
    protected $table = 'market_overview';
    protected $fillable = ['date', 'class', 'sum_nnm', 'sum_nnb', 'percent', 'vol', 'value', 'sv1', 'svbq'];
    //
}
