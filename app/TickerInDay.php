<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TickerInDay extends Model {
    //
    protected $table = 'ticker_in_day';
    protected $fillable = ['ticker', 'date', 'refer', 'ceiling', 'floor', 'open', 'average', 'high', 'low', 'close', 'vol', 'nnb', 'nnm', 'board'];
}
