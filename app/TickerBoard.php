<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TickerBoard extends Model {
    protected $table = 'ticker_board';
    protected $fillable = ['ticker', 'board'];
}
