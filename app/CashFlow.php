<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CashFlow extends Model {
    protected $table = 'overview_cash_flow';
    protected $fillable = ['date', 'up', 'down', 'hold', 'data_up', 'data_down'];

    //
}
