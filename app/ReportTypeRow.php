<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ReportTypeRow extends Model {
    protected $table = 'report_type_row';
    protected $fillable = ['report_type', 'report_ticker_type', 'row_id', 'row_name','criteria_id'];
    //
}
