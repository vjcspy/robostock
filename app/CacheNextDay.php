<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CacheNextDay extends Model
{
    protected $table = 'cache_next_day';
    protected $fillable = ['date','entity','data'];
    //
}
