<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TickerLastDay extends Model {
    protected $table = 'ticker_lastday';
    protected $fillable = ['ticker', 'date', 'refer', 'ceiling', 'floor', 'open', 'average', 'high', 'low', 'close', 'vol', 'board'];
    //
}
