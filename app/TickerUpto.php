<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TickerUpto extends Model
{
    protected $table = 'ticker_upto';
    protected $fillable = ['ticker','date','open','high','low','close','average','vol'];
    //
}
