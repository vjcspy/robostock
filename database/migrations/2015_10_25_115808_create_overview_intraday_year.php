<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOverviewIntradayYear extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('overview_intraday', function (Blueprint $table) {
            $table->increments('id');
            $table->string('id_inday');
            $table->string('blue_percent');
            $table->string('penny_percent');
            $table->date('date');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::drop('overview_intraday');
    }
}
