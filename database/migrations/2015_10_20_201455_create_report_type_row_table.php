<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReportTypeRowTable extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('report_type_row', function (Blueprint $table) {
            $table->increments('id');
            $table->string('report_type');//loai bao cao tai chinh
            $table->string('report_ticker_type');// co phieu thuoc loai nao
            $table->string('row_id');
            $table->text('row_name');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::drop('report_type_row');
    }
}
