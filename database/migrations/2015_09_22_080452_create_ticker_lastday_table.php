<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTickerLastdayTable extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('ticker_lastday', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('ticker');
            $table->date('date');
            $table->decimal('refer', 15, 1);
            $table->decimal('ceiling', 15, 1);
            $table->decimal('floor', 15, 1);
            $table->decimal('open', 15, 1);
            $table->decimal('high', 15, 1);
            $table->decimal('low', 15, 1);
            $table->decimal('close', 15, 1);
            $table->decimal('average', 15, 1);
            $table->bigInteger('vol');
//            $table->bigInteger('nnm');
//            $table->bigInteger('nnb');
//            $table->string('board');
            $table->float('hsdc');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::drop('ticker_lastday');
    }
}
