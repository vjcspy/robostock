<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOverviewCashFlowTable extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('overview_cash_flow', function (Blueprint $table) {
            $table->increments('id');
            $table->date('date');
            $table->decimal('up', 15, 2);
            $table->decimal('down', 15, 2);
            $table->decimal('hold', 15, 2);
            $table->string('data_up', 300);
            $table->string('data_down', 300);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::drop('overview_cash_flow');
    }
}
