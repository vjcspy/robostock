<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTickerAdjustTable extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('ticker_adjust', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('ticker');
            $table->date('date');
            $table->string('board');
            $table->string('active');
            $table->decimal('hsdc',10,10);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::drop('ticker_adjust');
    }
}
