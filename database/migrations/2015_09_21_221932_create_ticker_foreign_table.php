<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTickerForeignTable extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('ticker_foreign', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('ticker');
            $table->date('date');
            $table->bigInteger('nnm');
            $table->bigInteger('nnb');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::drop('ticker_foreign');
    }
}
