<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMarketOverviewTable extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('market_overview', function (Blueprint $table) {
            $table->increments('id');
            $table->date('date');
            $table->string('class');
            $table->bigInteger('sum_nnm');/*cal*/
            $table->bigInteger('sum_nnb');/*cal*/
            $table->decimal('percent', 2, 2);
            $table->bigInteger('vol');
            $table->decimal('value', 15, 2);
            $table->decimal('sv1', 15, 2);
            $table->decimal('svbq', 15, 2);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::drop('market_overview');
    }
}
