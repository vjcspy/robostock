<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTickerUptoTable extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('ticker_upto', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('ticker');
            $table->date('date');
            $table->decimal('open', 15, 1);
            $table->decimal('high', 15, 1);
            $table->decimal('low', 15, 1);
            $table->decimal('close', 15, 1);
            $table->decimal('average', 15, 1);
            $table->bigInteger('vol');
//            $table->bigInteger('nnm'); /*DO: da dua ra bang rieng*/
//            $table->bigInteger('nnb'); /*DO: da dua ra bang rieng*/
//            $table->float('hsdc');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::drop('ticker_upto');
    }
}
