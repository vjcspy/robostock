<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReportTypeSizeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('report_type_size', function (Blueprint $table) {
            $table->increments('id');
            $table->string('report_type'); // Loai bao cao
            $table->string('report_ticker_type'); // co phieu thuoc loai nao
            $table->integer('size');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('report_type_size');
    }
}
