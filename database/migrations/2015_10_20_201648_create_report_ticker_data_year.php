<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReportTickerDataYear extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('report_ticker_data_year', function (Blueprint $table) {
            $table->increments('id');
            $table->string('report_type'); // Loai bao cao
            $table->string('report_ticker_type'); // co phieu thuoc loai nao
            $table->string('ticker'); // co phieu thuoc loai nao
            $table->longText('data');
            $table->string('year');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::drop('report_ticker_data_year');
    }
}
