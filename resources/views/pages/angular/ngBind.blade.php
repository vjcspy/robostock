@extends('pages.angular.template.angTemlate')
@section('content')
    <script>
        var myApp = angular.module('myApp', [], function ($interpolateProvider) {
            $interpolateProvider.startSymbol('//');
            $interpolateProvider.endSymbol('//');
        });

        myApp.controller('controller1', ['$scope', function ($scope) {
            $scope.cancel = function ($e) {
                if ($e.keyCode == 27) {
                    /*cần phải khai báo user dưới dạng object. Nếu khai báo $scope.user.name sẽ lỗi bởi vì nó không hiểu đấy là object*/
                    $scope.user = {
                        name: 'abc',
                        age: '20'
                    };
                    $scope.user.name = 'Cancel';
                }

            }

        }]);


    </script>

    {{--Khi khai báo trong ng-model là user.name thì hiểu là 1 object. Object user với property là name--}}

    <div ng-app="myApp">
        <div ng-controller="controller1">
            <input type="text" ng-model="user.name" ng-model-options="{updateOn: 'blur'}" ng-keyup="cancel($event)"
                   name="userName" placeholder="Type any thing...">
            <pre>user.name <span ng-bind="user.name"></span></pre>
        </div>

    </div>
@stop
