@extends('pages.angular.template.angTemlate')
@section('content')
    <script>
        var myApp = angular.module('myApp', [], function ($interpolateProvider) {
            $interpolateProvider.startSymbol('//');
            $interpolateProvider.endSymbol('//');

        });

        myApp.controller('controller1', function ($scope) {

        });

        myApp.directive('newDirective', function () {
            var html = '<table>';
            html += '<tr>';
            html += '<td>Username:</td>';
            html += '<td><input type="text"/></td>';
            html += '</tr>';
            html += '<tr>';
            html += '<td>Password:</td>';
            html += '<td><input type="password"/></td>';
            html += '</tr>';
            html += '<tr>';
            html += '<td></td>';
            html += '<td><input type="Button" value="Login"/></td>';
            html += '</tr>';
            html += '</table>';

            return {
                template: html
            };
        });
    </script>

    <div ng-app="myApp">
        <div ng-controller="controller1">
            <div new-directive></div>
        </div>

    </div>
@stop
