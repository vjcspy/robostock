@extends('pages.angular.template.angTemlate')
@section('content')
    <div ng-app="myApp">
        <div ng-controller="mainController">

            <h1>AngularJS Form Validation</h1>

            <!-- FORM -->
            <!-- pass biến $valid để kiểm tra form của chúng ta valid hay không-->
            <form name="userForm" ng-submit="submitForm(userForm.$valid)" novalidate>
                <!-- thuộc tính novalidate chặn HTML5 validation bởi vì chúng ta sẽ sử dụng AngularJs validation -->

                <!-- NAME -->
                <div>
                    <label>Name</label>
                    <input type="text" name="name" ng-model="name" required>

                    <p ng-show="userForm.name.$invalid && !userForm.name.$pristine">You name is required.</p>
                </div>

                <!-- USERNAME -->
                <div>
                    <label>Username</label>
                    <input type="text" name="username" ng-model="username" ng-minlength="3" ng-maxlength="8">

                    <p ng-show="userForm.username.$error.minlength" class="help-block">Username is too short.</p>

                    <p ng-show="userForm.username.$error.maxlength" class="help-block">Username is too long.</p>
                </div>

                <!-- EMAIL -->
                <div>
                    <label>Email</label>
                    <input type="email" name="email" ng-model="email">

                    <p ng-show="userForm.email.$invalid && !userForm.email.$pristine">Enter a valid email.</p>
                </div>

                <!-- SUBMIT BUTTON -->
                <button type="submit">Submit</button>

            </form>
        </div>
    </div>
    <script>
        var app = angular.module('myApp', [], function ($interpolateProvider) {
            $interpolateProvider.startSymbol('<%');
            $interpolateProvider.endSymbol('%>');
        });
        app.controller('mainController', function ($scope) {
            $scope.submitForm = function (isValid) {
                if (isValid)
                    alert('true');
                else
                    alert('false');
            };
        });

    </script>

@stop
