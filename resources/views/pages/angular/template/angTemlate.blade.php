<html>
<head>
    <script src="{{asset('js/angular.min.js')}}"></script>
    <script src="//ajax.googleapis.com/ajax/libs/angularjs/1.4.7/angular-animate.js"></script>
    <script src="{{asset('js/jquery-1.11.3.min.js')}}"></script>
    <script src="{{asset('js/angular-sanitize.js')}}"></script>

    <!-- jQuery -->
    <script src="{{asset('sb-admin/bower_components/jquery/dist/jquery.min.js')}}"></script>

    <!-- Bootstrap Core CSS -->
    <link href="{{asset('sb-admin/bower_components/bootstrap/dist/css/bootstrap.min.css')}}" rel="stylesheet">

    <!-- Bootstrap Core JavaScript -->
    <script src="{{asset('sb-admin/bower_components/bootstrap/dist/js/bootstrap.min.js')}}"></script>

    <!-- Angular UI -->
    <link href="{{asset('ui-bootstrap-custom-build/ui-bootstrap-custom-0.14.3-csp.css')}}" rel="stylesheet">
{{--    <script src="{{asset('ui-bootstrap-custom-build/bower_components/angular-bootstrap/ui-bootstrap.min.js')}}"></script>--}}
    <script src="{{asset('ui-bootstrap-custom-build/bower_components/angular-bootstrap/ui-bootstrap-tpls.min.js')}}"></script>


    <meta charset="UTF-8">
</head>
<body>
@yield('content')
</body>
</html>
