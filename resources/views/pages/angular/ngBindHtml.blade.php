@extends('pages.angular.template.angTemlate')
@section('content')
    <script>
        var myApp = angular.module('myApp', ['ngSanitize'], function ($interpolateProvider) {
            $interpolateProvider.startSymbol('//');
            $interpolateProvider.endSymbol('//');
        });
        myApp.controller('newController', function ($scope) {

        });

    </script>
    <div ng-app="myApp">
        <div ng-controller="newController">
            <input type="text" ng-model="myHTML"/>

            <p ng-bind-html="myHTML"></p>
        </div>
    </div>
@stop
