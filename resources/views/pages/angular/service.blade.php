<html>
<head>
    <script src="http://lara.dev/js/angular.min.js"></script>
    <meta charset="UTF-8">
</head>
<body>
<div ng-app="myApp">
    <div ng-controller="calculatorController">
        <div>Nhập số a : <input type="number" ng-model="a"></div>
        <div>Nhập số b : <input type="number" ng-model="b"></div>
        <div>
            <input type="button" ng-click="add()" value="Tính tổng">
            <input type="button" ng-click="subtract()" value="Tính hiệu">
            <input type="button" ng-click="multiply()" value="Tính tích">
            <input type="button" ng-click="divide()" value="Tính thương">
        </div>
        <div>Kết quả là : <%result%></div>
    </div>
</div>
<script type="text/javascript">

    // Khởi tạo module myApp
    var app = angular.module('myApp', [], function ($interpolateProvider) {
        $interpolateProvider.startSymbol('<%');
            $interpolateProvider.endSymbol('%>');
    });

    // Thiết lập 1 service tên là calculatorService
    app.service('calculatorService', function () {

        // Cung cấp chức năng tính tổng 2 số a và b
        this.add = function (a, b) {
            return a + b
        };

        // Cung cấp chức năng tính hiệu 2 số a và b
        this.subtract = function (a, b) {
            return a - b
        };

        // Cung cấp chức năng tính tích 2 số a và b
        this.multiply = function (a, b) {
            return a * b
        };

        // Cung cấp chức năng tính thương 2 số a và b
        this.divide = function (a, b) {
            return a / b
        };
    });

    // Controller đảm nhiệm tác vụ tính tổng 2 số a và b
    app.controller('calculatorController', function ($scope, calculatorService) {

        $scope.result = 0;

        $scope.add = function () {
            $scope.result = calculatorService.add($scope.a, $scope.b);
        };

        $scope.subtract = function () {
            $scope.result = calculatorService.subtract($scope.a, $scope.b);
        };

        $scope.multiply = function () {
            $scope.result = calculatorService.multiply($scope.a, $scope.b);
        };

        $scope.divide = function () {
            $scope.result = calculatorService.divide($scope.a, $scope.b);
        };
    });

</script>
</body>
</html>
