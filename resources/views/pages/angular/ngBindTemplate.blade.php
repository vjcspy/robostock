@extends('pages.angular.template.angTemlate')
@section('content')
    <script>

        var myApp = angular.module('myApp', ['ngSanitize'], function ($interpolateProvider) {
            $interpolateProvider.startSymbol('//');
            $interpolateProvider.endSymbol('//');
        });

        myApp.controller('myController', ['$scope', function ($scope) {
        }]);

    </script>
    <div ng-app="myApp">

        <div ng-controller="myController">
            Name <input type="text" ng-model="name"><br>
            Website: <input type="text" ng-model="website"><br>
            <pre ng-bind-template="//name// //website//"></pre>
        </div>
    </div>
@stop
