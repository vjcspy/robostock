<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Example - example-example7-production</title>
    <script src="http://lara.dev/js/angular.min.js"></script>
    <script>
        var myApp = angular.module('myApp', [], function ($interpolateProvider) {
            $interpolateProvider.startSymbol('<%');
            $interpolateProvider.endSymbol('%>');});
            myApp.controller('myController', function ($scope) {
                $scope.sum = 0;

                $scope.sum_ab = function () {
                    $scope.sum = $scope.num_a + $scope.num_b;
                }
            });
    </script>
</head>
<body ng-app="myApp">
<div ng-controller="myController">
    Nhập số A : <input type="number" ng-model="num_a"> <br/>
    Nhập số B : <input type="number" ng-model="num_b"> <br/>
    <input type="button" ng-click="sum_ab()" value="Tính tổng"> <br/>

    <p>Tổng 2 số A và B là : <%sum%></p>
</div>
</body>
</html>
