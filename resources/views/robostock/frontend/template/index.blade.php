<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <title>Cổ phiếu 69</title>
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="{{ asset('css/bootstrap.min.css') }}">
    <script type="text/javascript" src="{{ asset('js/jquery-1.11.3.min.js') }}"></script>
    {{--<script type="text/javascript" src="{{ asset('js/jquery-migrate-1.2.1.min.js') }}"></script>--}}
    <script type="text/javascript" src="{{ asset('js/msJs.js') }}"></script>
    <!-- Optional theme -->
    {{--<link rel="stylesheet" href="{{ asset('css/bootstrap-theme.min.css') }}">--}}
    {{--<script src="//cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.2/modernizr.js"></script>--}}
    <!-- Latest compiled and minified JavaScript -->
    <script type="text/javascript" src="{{ asset('js/bootstrap.min.js') }}"></script>
    {{--<script type="text/javascript" src="{{ asset('js/prototype.js') }}"></script>--}}
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- Custom styles for this template -->
    <script>
        jQuery(window).load(function () {
            // Animate loader off screen
            jQuery(".se-pre-con").fadeOut("slow");

        });
    </script>
    <link href="{{ asset('css/mainStyle.css') }}" rel="stylesheet">

    {{--<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->--}}
    {{--[if lt IE 9]>--}}
    {{--<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>--}}
    {{--<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>--}}
    {{--<![endif]-->--}}
    @yield('script')

</head>
<body>
<div class="container wrapper">
    <div class="se-pre-con"></div>
    <!-- Static navbar -->
    <nav class="navbar navbar-default">
        <div class="container-fluid">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar"
                        aria-expanded="false" aria-controls="navbar"><span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="#"><strong>Manager</strong></a></div>
            <div id="navbar" class="navbar-collapse collapse">
                <ul class="nav navbar-nav">
                    <li class=""><a href="{{action('RobostockController@getHsdc')}}">HSDC</a></li>
                    <li><a href="{{action('RobostockController@getSearch')}}">Search</a></li>
                    <li><a href="{{action('RobostockController@getAlltickerinday')}}">Ticker In Day</a></li>
                    <li class="dropdown">
                        <a aria-expanded="false" aria-haspopup="true" role="button" data-toggle="dropdown"
                           class="dropdown-toggle" href="#">Chức năng <span class="caret"></span></a>
                        <ul class="dropdown-menu">
                            <li class="dropdown-header">Method</li>
                            <li><a href="{{action('RobostockController@getCheckhsdc')}}">Check điều chỉnh</a></li>
                            <li><a href="{{action('RobostockController@getInittickerlastday')}}">Tạo cached ticker_lastday</a></li>
                            <li><a href="{{action('RobostockController@getUpdateindexupto')}}">Update dữ liệu index hôm nay</a></li>
                            <li><a href="{{action('RobostockController@getTestemail')}}"> Test send email</a></li>
                            <li><a href="{{action('RobostockController@getLog')}}"> Log</a></li>
                            <li class="divider" role="separator"></li>
                            <li class="dropdown-header">Tổng quan</li>
                            <li><a href="{{action('RobostockController@getSearchindex')}}">View Index</a></li>
                            <li><a href="{{action('RobostockController@getMarketoverview')}}">Market Overview</a></li>
                            <li><a href="{{action('RobostockController@getForeign')}}">Foreign Overview </a></li>
                            <li><a href="{{action('RobostockController@getDownload')}}">Tải dữ liệu </a></li>
                        </ul>
                    </li>
                </ul>
                <ul class="nav navbar-nav navbar-right">
                    <li class="active"><a href="./" style="color: black">Cổ phiếu 69 <span
                                    class="sr-only">(current)</span></a></li>
                    <!--<li><a href="../navbar-static-top/">Static top</a></li>
                    <li><a href="../navbar-fixed-top/">Fixed top</a></li>-->
                </ul>
            </div>
            <!--/.nav-collapse -->
        </div>
        <!--/.container-fluid -->
    </nav>

    <!-- Main component for a primary marketing message or call to action -->
    <div class="jumbotron" id="main-mes" style="display: block">
        @yield('content')
    </div>
</div>
<!-- /container -->

<!-- Bootstrap core JavaScript
    ================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
{{--<script type="text/javascript" src="{{ asset('css/jquery.min.js') }}"></script>--}}
</body>
</html>

