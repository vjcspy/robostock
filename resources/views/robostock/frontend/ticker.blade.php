@extends('robostock.frontend.template.index');
@section('script')
    <link rel="stylesheet" href="{{ asset('css/jquery.dataTables.min.css') }}">
    {{--<script type="text/javascript" src="{{ asset('js/jquery-1.11.3.min.js') }}"></script>--}}
    <script type="text/javascript" src="{{ asset('js/jquery.dataTables.min.js') }}"></script>
@stop
@section('content')
    <div class="panel panel-default">
        <!-- Default panel contents -->
        <div class="panel-heading"><h3>Result Ticker: @if(!is_null($tickerClass)) {{$tickerClass->ticker}}
                - {{$tickerClass->top}}- {{$tickerClass->bp}}- {{$tickerClass->cd}} @endif</h3></div>

        <!-- Table -->
        <table id="example" class="table">
            <thead>
            <tr>
                <th width="35px">Ticker</th>
                <th width="35px">Date</th>
                <th width="30px">Open</th>
                <th width="30px">High</th>
                <th width="30px">Low</th>
                <th width="30px">Close</th>
                <th width="30px">Average</th>
                {{--<th width="30px">NNM</th>--}}
                {{--<th width="30px">NNB</th>--}}
                <th width="40px">Vol</th>
            </tr>
            </thead>
            <tbody>
            @foreach($tickers as $ticker)
                @if(isset($hsdc[$ticker->ticker]) && $ticker->date != App\RoboStock\Model\WorkWithTime\WorkWithTime::getCurrentDate())
                    <tr>
                        <th scope="row">{{$ticker->ticker}}</th>
                        <td>{{$ticker->date  }}</td>
                        <td>{{round($ticker->open * $hsdc[$ticker->ticker]->hsdc,1)}}</td>
                        <td>{{round($ticker->high * $hsdc[$ticker->ticker]->hsdc,1)}}</td>
                        <td>{{round($ticker->low * $hsdc[$ticker->ticker]->hsdc,1)}}</td>
                        <td>{{round($ticker->close* $hsdc[$ticker->ticker]->hsdc,1)}}</td>
                        <td>{{$ticker->average}}</td>
                        {{--<td>{{$ticker->nnm}}</td>--}}
                        {{--<td>{{$ticker->nnb}}</td>--}}
                        <td>{{$ticker->vol}}</td>
                    </tr>
                @else
                    <tr>
                        <th scope="row">{{$ticker->ticker}}</th>
                        <td>{{$ticker->date  }}</td>
                        <td>{{$ticker->open}}</td>
                        <td>{{$ticker->high}}</td>
                        <td>{{$ticker->low}}</td>
                        <td>{{$ticker->close}}</td>
                        <td>{{$ticker->average}}</td>
                        {{--<td>{{$ticker->nnm}}</td>--}}
                        {{--<td>{{$ticker->nnb}}</td>--}}
                        <td>{{$ticker->vol}}</td>
                    </tr>
                @endif
            @endforeach
            </tbody>
        </table>
    </div>
    <script>
        jQuery(document).ready(function () {
            jQuery('#example').DataTable();
        });
    </script>
@stop
