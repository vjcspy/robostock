@extends('robostock.frontend.template.index');
@section('script')
    <link rel="stylesheet" href="{{ asset('css/jquery.dataTables.min.css') }}">
    {{--<script type="text/javascript" src="{{ asset('js/jquery-1.11.3.min.js') }}"></script>--}}
    <script type="text/javascript" src="{{ asset('js/jquery.dataTables.min.js') }}"></script>
@stop
@section('content')
    <div class="panel panel-default">
        <!-- Default panel contents -->
        <div class="panel-heading"></div>

        <!-- Table -->
        <table id="example" class="table">
            <thead>
            <tr>
                <th>Ticker</th>
                <th>Date</th>
                <th>Change</th>
                <th>Close</th>
                <th>Percent</th>
                <th>Value</th>
                <th>Vol</th>
                <th>Tang Giam</th>
                <th>Nnm</th>
                <th>Nnb</th>
                <th>Rong</th>
                {{--<th width="30px">NNM</th>--}}
                {{--<th width="30px">NNB</th>--}}
            </tr>
            </thead>
            <tbody>
            @foreach($tickers as $ticker)
                <tr>
                    <th scope="row">{{$ticker->name}}</th>
                    <td>{{$ticker->date  }}</td>
                    <td>{{$ticker->change}}</td>
                    <td>{{$ticker->index}}</td>
                    <td>{{$ticker->percent}}</td>
                    <td>{{$ticker->value}}</td>
                    <td>{{$ticker->vol}}</td>
                    <td>@if($ticker->name=='VNINDEX'){{$over['upVni'].'/'.$over['downVni'].'/'.$over['holdVni']}} @endif
                        @if($ticker->name=='HNXINDEX'){{$over['upHax'].'/'.$over['downHax'].'/'.$over['holdHax']}} @endif
                    </td>
                    <td>@if($ticker->name=='VNINDEX'){{$over['nnmVni']}} @endif
                        @if($ticker->name=='HNXINDEX'){{$over['nnmHax']}} @endif
                    </td>
                    <td>@if($ticker->name=='VNINDEX'){{$over['nnbVni']}} @endif
                        @if($ticker->name=='HNXINDEX'){{$over['nnbHax']}} @endif</td>
                    <td>@if($ticker->name=='VNINDEX'){{$over['nnmVni']-$over['nnbVni']}} @endif
                        @if($ticker->name=='HNXINDEX'){{$over['nnmHax']-$over['nnbHax']}} @endif</td>
                    {{--<td>{{$ticker->nnm}}</td>--}}
                    {{--<td>{{$ticker->nnb}}</td>--}}
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
    <script>
        jQuery(document).ready(function () {
            jQuery('#example').DataTable();
        });
    </script>
@stop
