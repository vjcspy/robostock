@extends('robostock.frontend.template.index')
@section('content')
    <div style="">
        <h2>Find Ticker</h2>
        {!! Form::open(['url' => 'robostock/search-data'],array('onsubmit'=>'checkForm(); return false;')) !!}
        <div class="">
            <input id="tickertext" class="form-control" placeholder="Ticker" width="30px" style="width: 130px"
                   name="ticker"><br>
            <input type="date" class="form-control"
                   max="{{\App\RoboStock\Model\WorkWithTime\WorkWithTime::getCurrentDate()}}" placeholder="Date"
                   width="30px" style="width: 130px" name="date" value="{{--{{date("Y-m-d")}}--}}" id="date">
        </div>
        <br>
        <button type="submit" class="btn btn-default" onclick="return checkForm(); return false;">Search</button>
        {!! Form::close() !!}
    </div>
    <script>
        function checkForm() {
            return true;
            // regular expression to match required date format
            re = /^\d{4}\/\d{2}\/\d{2}$/;

            if (jQuery('#tickertext').val() == '' && !jQuery('#date').val().match(re)) {
                alert("Invalid date format: " + jQuery('#date').val());
                jQuery('#date').val().focus();
                return false;
            }
        }
    </script>
@stop
