@extends('robostock.frontend.template.index');
@section('script')
    <link rel="stylesheet" href="{{ asset('css/jquery.dataTables.min.css') }}">
    <script type="text/javascript" src="{{ asset('js/jquery.dataTables.min.js') }}"></script>
@stop
@section('content')
    <?php echo csrf_field(); ?>
    <div class="panel panel-default">
        <!-- Default panel contents -->
        <div class="panel-heading">
            <h3>Foreign Overview </h3>

            <div class="" style="align-content: left;padding-left: 0%">
                <div class="" style="padding-left: 0px">
                    <div class="form-group">
                        <label for="exampleInputName2">Date </label>
                        <input style="width: 200px" type="date" class="form-control" id="date"
                               placeholder="YYYYMMDD" value="{{App\RoboStock\Model\WorkWithTime\WorkWithTime::getCurrentDate('Y-m-d')}}">
                        <label for="exampleInputEmail2">Board</label>
                        <select id="board" style="width: 200px" class="selectpicker form-control">
                            <option value="hose">HOSE</option>
                            <option value="hax">HAX</option>
                            <option value="all">All</option>
                        </select>
                        {{--<label for="exampleInputEmail2">Buy/Sell</label>
                        <select id="mb" style="width: 200px" class="selectpicker form-control">
                            <option value="1">Buy</option>
                            <option value="2">Sell</option>
                        </select>--}}
                    </div>
                    <br>
                    <button type="submit" class="btn btn-default" onclick="ajaxItem()">Search </button>
                </div>
            </div>
        </div>
        <!-- Table -->
        <div id="mainContent">
            <table id="example" class="table">
                <thead>
                <tr>
                    <th>Ticker</th>
                    <th>Percent</th>
                    <th>NNM</th>
                    <th>GTM</th>
                    <th>%NNM</th>
                    <th>NNB</th>
                    <th>GTB</th>
                    <th>%NNB</th>
                </tr>
                </thead>
                <tbody>

                {{--<tr>--}}
                {{--<th scope="row">{{$ticker->class}}</th>--}}
                {{--<td>{{$ticker->percent  }}</td>--}}
                {{--<td>{{$ticker->val}}</td>--}}
                {{--<td>{{$ticker->vale}}</td>--}}
                {{--<td>{{$ticker->sv1}}</td>--}}
                {{--<td>{{$ticker->svbq}}</td>--}}
                {{--</tr>--}}

                </tbody>
            </table>
        </div>
    </div>
    <script>
        jQuery(document).ready(function () {
            jQuery('#example').DataTable();
        });
    </script>
    <script>
        function ajaxItem() {
            jQuery(".se-pre-con").fadeIn("slow");
            var date = jQuery('#date').val();
            var board = jQuery('#board').val();
            var mb = jQuery('#mb').val();
            var url = '{{action('RobostockController@postDataforeignoverview')}}';
            jQuery.ajax({
                url: url,
                data: {
                    'date': date,
                    'board': board,
                    'mb': mb,
                    '_token': '<?php echo csrf_token(); ?>'
                },
                type: 'POST',
                dataType: 'json',
                success: function (data) {
                    updateDataAj(data);
                },
                complete: function () {
                    jQuery(".se-pre-con").fadeOut("slow");
                }
            });
        }
        function updateDataAj(data) {
//            var wrapper = document.createElement('div');
//            wrapper.innerHTML = data.content;
//            var con = jQuery(wrapper);
//            console.log(data.content);
            jQuery('#mainContent')
                    .empty().append(data.content);

//                    .append(con.filter('#ajax-content'));
//                    .append($con.filter('div#another-id'))
//                    .append($con.filter('div#and-another-id'));
            $('#example').DataTable( {
                "order": [[ 8, "desc" ]]
            } );
            jQuery(".se-pre-con").fadeOut("slow");
        }
    </script>
@stop
