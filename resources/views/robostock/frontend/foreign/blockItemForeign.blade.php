<table id="example" class="table">
    <thead>
    <tr>
        <th >Ticker</th>
        <th >Percent</th>
        <th >NNM</th>
        <th >GTM</th>
        <th >%NNM</th>
        <th >NNB</th>
        <th >GTB</th>
        <th >%NNB</th>
        <th >GTMBR</th>
    </tr>
    </thead>
    <tbody>
    @foreach($tickers as $ticker)
        <tr>
            <th scope="row">{{$ticker['ticker']}}</th>
            <td @if($ticker['percent']>0) style="color: green" @else style="color: red" @endif  >{{number_format($ticker['percent'], 2, '.', ',')}}%</td>
            <td>{{number_format($ticker['nnm'], 2, '.', ',')}}</td>
            <td>{{number_format($ticker['gtm'], 2, '.', ',')}}</td>
            <td>{{$ticker['pM']}}%</td>
            <td>{{number_format($ticker['nnb'], 2, '.', ',')}}</td>
            <td>{{number_format($ticker['gtb'], 2, '.', ',')}}</td>
            <td>{{$ticker['pB']}}%</td>
            <td>{{number_format($ticker['nnm'] - $ticker['nnb'], 2, '.', ',')}}</td>
        </tr>
    @endforeach
    </tbody>
</table>
