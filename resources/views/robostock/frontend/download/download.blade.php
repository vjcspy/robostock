@extends('robostock.frontend.template.index')
@section('content')
    <div style="">
        <h2>Find Ticker</h2>
        {!! Form::open(['url' => 'robostock/downloaddata'],array('onsubmit'=>'checkForm(); return false;')) !!}
        <div class="">
            <input id="tickertext" class="form-control" placeholder="Ticker: Bỏ trống thì lấy cả sàn" width="30px" style=""
                   name="ticker">
            <input type="text" class="form-control"
                   max="{{\App\RoboStock\Model\WorkWithTime\WorkWithTime::getCurrentDate()}}" placeholder="From: Không được bỏ trống"
                   width="30px" style="" name="from" value="2015-09-17" id="from">
            <input type="text" class="form-control"
                   max="{{\App\RoboStock\Model\WorkWithTime\WorkWithTime::getCurrentDate()}}" placeholder="To Date: Bỏ trống thì chỉ lấy ngày From"
                   width="30px" style="" name="to" value="2015-09-18" id="to">
            <input type="text" class="form-control"
                   max="{{\App\RoboStock\Model\WorkWithTime\WorkWithTime::getCurrentDate()}}" placeholder="Format: dang ami hoặc meta"
                   width="30px" style="" name="format" value="meta" id="format">
        </div>
        <br>
        <button type="submit" class="btn btn-default" onclick="return checkForm(); return false;">Search</button>
        {!! Form::close() !!}
    </div>
    <script>
        function checkForm() {
            return true;
            // regular expression to match required date format
            re = /^\d{4}\/\d{2}\/\d{2}$/;

            if (jQuery('#tickertext').val() == '' && !jQuery('#date').val().match(re)) {
                alert("Invalid date format: " + jQuery('#date').val());
                jQuery('#date').val().focus();
                return false;
            }
        }
    </script>
@stop
