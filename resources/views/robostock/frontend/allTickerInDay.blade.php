@extends('robostock.frontend.template.index');
@section('script')
    <link rel="stylesheet" href="{{ asset('css/jquery.dataTables.min.css') }}">
    {{--<script type="text/javascript" src="{{ asset('js/jquery-1.11.3.min.js') }}"></script>--}}
    <script type="text/javascript" src="{{ asset('js/jquery.dataTables.min.js') }}"></script>
@stop
@section('content')
    <h1>Manager Ticker</h1>
    <div class="bs-example" data-example-id="hoverable-table">
        <table id="example" width="100%" cellspacing="0" class="display table table-hover myTable">
            <thead>
            <tr>
                <th width="35px">Ticker</th>
                <th width="45">Date</th>
                <th width="30px">Open</th>
                <th width="30px">High</th>
                <th width="30px">Low</th>
                <th width="30px">Close</th>
                <th width="40px">Vol</th>
            </tr>
            </thead>
            <tfoot>
            <tr>
                <th>Ticker</th>
                <th>Date</th>
                <th>Open</th>
                <th>High</th>
                <th>Low</th>
                <th>Close</th>
                <th>Vol</th>
            </tr>
            </tfoot>
            <tbody>
            @foreach($tickers as $ticker)
                <tr>
                    <th scope="row">{{$ticker['ticker']}}</th>
                    <td>{{\App\RoboStock\Model\WorkWithTime\WorkWithTime::getCurrentDate('Y-m-d') }}</td>
                    <td>{{$ticker['open']}}</td>
                    <td>{{$ticker['high']}}</td>
                    <td>{{$ticker['low']}}</td>
                    <td>{{$ticker['close']}}</td>
                    <td>{{$ticker['vol']}}</td>
                </tr>
            @endforeach
            </tbody>
        </table>
        <script>
            jQuery(document).ready(function () {
                jQuery('#example').DataTable();
            });
        </script>
    </div>
@stop
