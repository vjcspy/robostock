@extends('robostock.frontend.template.index');
@section('script')
    <link rel="stylesheet" href="{{ asset('css/jquery.dataTables.min.css') }}">
    <script type="text/javascript" src="{{ asset('js/jquery.dataTables.min.js') }}"></script>
@stop
@section('content')
    <?php echo csrf_field(); ?>
    <div class="panel panel-default">
        <!-- Default panel contents -->
        <div class="panel-heading">
            <h3>Market Overview </h3>

            <div class="row" style="align-content: center;padding-left: 40%">
                <div class="col-lg-6">
                    <div class="input-group">
                        <span class="input-group-btn"><button id="abc" class="btn btn-default" type="button"
                                                              onclick="ajaxItem();">Date
                            </button></span>
                        <input id="date" type="date" class="form-control" placeholder="Search for date..."
                               value="{{App\RoboStock\Model\WorkWithTime\WorkWithTime::getCurrentDate('Y-m-d')}}"
                               style="width: 200px">
                    </div>
                </div>
            </div>
        </div>
        <!-- Table -->
        <div id="mainContent">
            <table id="example" class="table">
                <thead>
                <tr>
                    <th width="35px">ID</th>
                    <th width="35px">Class</th>
                    <th width="35px">Percent</th>
                    <th width="30px">Vol</th>
                    <th width="30px">Value</th>
                    <th width="30px">SV1</th>
                    <th width="30px">VAL21</th>
                    <th width="30px">SVBQ</th>
                </tr>
                </thead>
                <tbody>

                {{--<tr>--}}
                {{--<th scope="row">{{$ticker->class}}</th>--}}
                {{--<td>{{$ticker->percent  }}</td>--}}
                {{--<td>{{$ticker->val}}</td>--}}
                {{--<td>{{$ticker->vale}}</td>--}}
                {{--<td>{{$ticker->sv1}}</td>--}}
                {{--<td>{{$ticker->svbq}}</td>--}}
                {{--</tr>--}}

                </tbody>
            </table>
        </div>
    </div>
    <script>
        jQuery(document).ready(function () {
            jQuery('#example').DataTable();
            ajaxItem();
        });
    </script>
    <script>
        function ajaxItem() {
            jQuery(".se-pre-con").fadeIn("slow");
            var date = jQuery('#date').val();
            var url = '{{action('RobostockController@postDatamarketoverview')}}';
            jQuery.ajax({
                url: url,
                data: {
                    'date': date,
                    '_token': '<?php echo csrf_token(); ?>'
                },
                type: 'POST',
                dataType: 'json',
                success: function (data) {
                    updateDataAj(data);
                },
                complete: function () {
                    jQuery(".se-pre-con").fadeOut("slow");
                }
            });
        }
        function updateDataAj(data) {
//            var wrapper = document.createElement('div');
//            wrapper.innerHTML = data.content;
//            var con = jQuery(wrapper);
//            console.log(data.content);
            jQuery('#mainContent')
                    .empty().append(data.content);

//                    .append(con.filter('#ajax-content'));
//                    .append($con.filter('div#another-id'))
//                    .append($con.filter('div#and-another-id'));
            jQuery('#example').DataTable();
            jQuery(".se-pre-con").fadeOut("slow");
        }
    </script>
@stop
