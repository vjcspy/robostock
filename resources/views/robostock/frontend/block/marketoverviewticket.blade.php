<table id="cashFlow" class="table">
    <thead>
    <tr>
        <th align="center">Up</th>
        <th align="center">Down</th>
        <th align="center">Hold</th>
        <th align="center">Data Up</th>
        <th align="center">Data Down</th>

    </tr>
    </thead>
    <tbody>
    <tr>
        <th scope="row" style="color: green">{{$cashFLow->up}}</th>
        <td style="color: red">{{$cashFLow->down}}</td>
        <td style="color:yellow">{{$cashFLow->hold}}</td>
        <td style="color:green">{{$up[0]['ticker']}}({{round($up[0]['percent']*100,2)}}),{{$up[1]['ticker']}}({{round($up[1]['percent']*100,2)}}),{{$up[2]['ticker']}}({{round($up[2]['percent']*100,2)}})</td>
        <td style="color:red">{{$down[0]['ticker']}}{{--({{round($down[0]['percent']*100,2)}})--}},{{$down[1]['ticker']}}{{--({{round($down[1]['percent']*100,2)}})--}},{{$down[2]['ticker']}}{{--({{round($down[2]['percent']*100,2)}})--}}</td>
    </tr>
    </tbody>
</table>
<br>
<table id="example" class="table">
    <thead>
    <tr>

        <th width="35px">Id</th>
        <th width="35px">Class</th>
        <th width="35px">Percent</th>
        <th width="30px">Vol</th>
        <th width="30px">Value</th>
        <th width="30px">SV-1</th>
        {{--<th width="30px">VAL21</th>--}}
        <th width="30px">SV BQ</th>
    </tr>
    </thead>
    <tbody>
    @foreach($tickers as $ticker)
        <tr>
            <th scope="row">
                @if($ticker->class=='all') 1 @endif
                @if($ticker->class == 'vni') 2 @endif
                @if($ticker->class == 'hax') 3 @endif
                @if($ticker->class=='dauco') 4 @endif
                @if($ticker->class=='coban') 5 @endif
                @if($ticker->class =='blue') 6 @endif
                @if($ticker->class =='penny') 7 @endif
            </th>
            <th scope="row">{{$ticker->class}}</th>
            <td @if($ticker['percent']>0) style="color: green" @else style="color: red" @endif >{{$ticker->percent}}
                %
            </td>
            <td>{{number_format($ticker->vol, 2, '.', ',')}}</td>
            <td>{{number_format($ticker->value, 2, '.', ',')}}</td>
            <td>{{number_format($ticker->sv1, 2, '.', ',')}}</td>
            {{--<td>{{number_format($ticker->value/($ticker->svbq), 2, '.', ',')}}</td>--}}
            <td>{{number_format($ticker->svbq, 2, '.', ',')}}</td>
        </tr>
    @endforeach
    </tbody>
</table>
