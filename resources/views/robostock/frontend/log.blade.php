@extends('robostock.frontend.template.index');
@section('script')
    <link rel="stylesheet" href="{{ asset('css/jquery.dataTables.min.css') }}">
    {{--<script type="text/javascript" src="{{ asset('js/jquery-1.11.3.min.js') }}"></script>--}}
    <script type="text/javascript" src="{{ asset('js/jquery.dataTables.min.js') }}"></script>
@stop
@section('content')
    <div class="panel panel-default">
        <!-- Default panel contents -->
        <div class="panel-heading"><h3>Log</h3></div>

        <!-- Table -->
        <table id="example" class="table">
            <thead>
            <tr>
                <th width="35px">ID</th>
                <th width="55px">Work</th>
                <th>Data</th>
            </tr>
            </thead>
            <tbody>
            @foreach($tickers as $ticker)
                <tr>
                    <th scope="row">{{$ticker->id}}</th>
                    <td>{{$ticker->work  }}</td>
                    <td>{{$ticker->data}}</td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
    <script>
        jQuery(document).ready(function () {
            jQuery('#example').DataTable();
        });
    </script>
@stop
