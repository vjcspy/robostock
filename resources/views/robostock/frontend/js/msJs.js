// JavaScript Document
function changeActive(bt, value) {
    jQuery('#main-mes').prop('disabled', true);
    if (jQuery(bt).hasClass('btn-success')) {
        jQuery(bt).removeClass('btn-success');
        jQuery(bt).addClass('btn-danger');
        jQuery(bt).html('Deactive');
    } else {
        jQuery(bt).removeClass('btn-danger');
        jQuery(bt).addClass('btn-success');
        jQuery(bt).html('Active');
        for (vi = 0; vi < jQuery('.btn').length; vi++) {
            if (vi != parseInt(value - 1)) {
                var i = vi + 1;
                jQuery('#btA-' + i).removeClass('btn-success').addClass('btn-danger').html('Deactive');
            }
        }
    }
    var data = {'set_active': value};
    jQuery.ajax({
        url: 'workingXml.php',
        data: data,
        dataType: 'json',
        type: 'POST',
        success: function (data) {
            jQuery('#main-mes').prop('disabled', false);
            if (data != 'false') {
                console.log('changeActive value =  ' + data);
                alert('Save Ok!');
            }
        }
    });
}

function addNewData() {
    jQuery('#main-mes').prop('disabled', true);
    var title = jQuery('#title').val();
    var content = jQuery('#content').val();
    var version = jQuery('#version').val();
    var startDay = jQuery('#startDay').val();
    var endDay = jQuery('#endDay').val();
    var checkEnable = jQuery('#checkEnable').val();
    var link = jQuery('#link').val();
    var type = jQuery('#type').val();
    var data = {
        'add_new': '1',
        'title': title,
        'content': content,
        'version': version,
        'startDay': startDay,
        'endDay': endDay,
        'checkEnable': checkEnable,
        'link': link,
        'type': type
    };
    jQuery.ajax({
        url: 'workingXml.php',
        data: data,
        dataType: 'json',
        type: 'POST',
        success: function (data) {
            jQuery('#main-mes').prop('disabled', false);
            console.log('result ' + data);
            if (data != 'false') {
                console.log('Add New value =  ' + data);
                alert('Save Ok!');
                window.location = "index.php";
            }
        }
    });
}

