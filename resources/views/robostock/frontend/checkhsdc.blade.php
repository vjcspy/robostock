@extends('robostock.frontend.template.index');
@section('script')
    <link rel="stylesheet" href="{{ asset('css/jquery.dataTables.min.css') }}">
    {{--<script type="text/javascript" src="{{ asset('js/jquery-1.11.3.min.js') }}"></script>--}}
    <script type="text/javascript" src="{{ asset('js/jquery.dataTables.min.js') }}"></script>
@stop
@section('content')
    <h1>Điều Chỉnh Dữ Liệu</h1>
    <div class="bs-example" data-example-id="hoverable-table">
        <table id="example" width="100%" cellspacing="0" class="display table table-hover myTable">
            <thead>
            <tr>
                <th width="35px">ID</th>
                <th>Date</th>
                <th>Ticker</th>
                <th>Board</th>
                <th>HSDC</th>
                <th>{{--Update--}}</th>
                <th>{{--Cancel--}}</th>
                <th>{{--Delete--}}</th>
            </tr>
            </thead>
            <tfoot>
            <tr>
                <th>Id</th>
                <th>Date</th>
                <th>Ticker</th>
                <th>Board</th>
                <th>HSDC</th>
                <th>{{--Update--}}</th>
                <th>{{--Cancel--}}</th>
                <th>{{--Delete--}}</th>
            </tr>
            </tfoot>
            <tbody>
            @foreach($tickers as $ticker)
                <tr>
                    <th scope="row">{{$ticker->id}}</th>
                    <td>{{$ticker->date}}</td>
                    <td>{{$ticker->ticker}}</td>
                    <td>{{$ticker->board}}</td>
                    <td>{{$ticker->hsdc}}</td>
                    <td>
                        <button type="button" class="btn btn-success btW @if($ticker->active == 1) disabled @endif"
                                onclick="ajustTicker({{$ticker['ticker']}})">
                            Update
                        </button>
                    </td>
                    <td>
                        <button type="button" class="btn btn-warning btW @if($ticker->active == 1) disabled @endif">Cancel</button>
                    </td>
                    <td>
                        <button type="button" class="btn btn-danger btW @if($ticker->active == 1) disabled @endif">Delete</button>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
        <script>
            jQuery(document).ready(function () {
                $('#example').DataTable( {
                    "order": [[ 1, "desc" ]]
                } );


            });

            function ajustTicker(ticker) {
                var url = action('RobostockController@getUpdateAjustTicker', ticker);
            }
        </script>
    </div>
@stop
