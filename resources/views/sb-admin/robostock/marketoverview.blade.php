@extends('sb-admin.robostock.Main')
@section('page-wrapper')
    <div id="page-wrapper" ng-app="market" ng-controller="marketController"
         ng-init="tickerBoard = {{$tickerBoard}};tickerClass={{$tickerClass}};tickerLastDay = {{$tickerLastDay}};cachePlcsNextDay = {{$cachePlcsNextDay}};trendData = {{$trendData}};trendSmaData={{$trendSmaData}};uptrendData = {{$uptrendData}};dataRs = {{$dataRs}};dataPennyCloseTwoDayAgo = {{$dataPennyCloseTwoDayAgo}};dataCashFlowYesterday={{$cashFlowYesterday}}">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">MarketOverview</h1>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->
        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-heading" style="color:red ">
                        SAU NÀY SẼ LÀM BẢNG GIÁ Ở ĐÂY ?????
                    </div>
                    <!-- /.panel-heading -->
                    <div class="panel-body">
                        <div class="dataTable_wrapper">
                            <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                                <thead>
                                <tr>
                                    <th></th>
                                    <th>Phần trăm</th>
                                    <th>Khối lượng</th>
                                    <th>Giá trị</th>
                                    <th>Sv-1</th>
                                    <th>Sv-bq</th>
                                </tr>
                                </thead>
                                <tbody>

                                <tr class="gradeC">
                                    <td>Misc</td>
                                    <td>Misc</td>
                                    <td>PSP browser</td>
                                    <td>PSP</td>
                                    <td class="center">-</td>
                                    <td class="center">C</td>
                                </tr>
                                <tr class="gradeU">
                                    <td>Other browsers</td>
                                    <td>Other browsers</td>
                                    <td>All others</td>
                                    <td>-</td>
                                    <td class="center">-</td>
                                    <td class="center">U</td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                        <!-- /.table-responsive -->
                        <div class="well">
                            <h4>Chức năng của MarketOverview</h4>

                            <p><a target="_blank"
                                  href="#">https://cophieu69.com/</a>.
                            </p>
                            <a class="btn btn-default btn-lg btn-block" target="_blank" href="#">Xem cách đánh giá thị
                                trường tổng quan</a>
                        </div>
                    </div>
                    <!-- /.panel-body -->
                </div>
                <!-- /.panel -->
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->
        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-heading" style="color: steelblue">
                        Chỉ số index theo sàn, số mã tăng giảm, tổng NN mua bán, đồ thị Intraday
                    </div>
                    <!-- /.panel-heading -->
                    <div class="panel-body">
                        <canvas id="line" class="chart chart-line" chart-data="intraday.dataChartIndexFore"
                                chart-labels="intraday.labelsChartIndexFore" chart-legend="true"
                                chart-series="intraday.seriesChartIndexFore"
                                chart-click="onClickChartIndexFore" chart-colours="intraday.colors">
                        </canvas>
                        <div class="table-responsive">
                            <table class="table table-striped table-bordered table-hover">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Index</th>
                                    <th>Percent</th>
                                    <th>Khối lượng</th>
                                    <th>Giá trị</th>
                                    <th>Tăng/Giảm/Giữ giá</th>
                                    <th>GTNN Mua</th>
                                    <th>GTNN Bán</th>
                                    <th>GTMB Ròng</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td>VN-Index</td>
                                    <td>//index.vni.index//</td>
                                    <td>//index.vni.percent//</td>
                                    <td>//index.vni.vol//</td>
                                    <td>//index.vni.value//</td>
                                    <td>//index.vni.udh//</td>
                                    <td>//(index.vni.gtnnm)// tỷ</td>
                                    <td>//index.vni.gtnnb// tỷ</td>
                                    <td>//(index.vni.gtnnm - index.vni.gtnnb).toFixed(2)// tỷ</td>
                                </tr>
                                <tr>
                                    <td>HNX-Index</td>
                                    <td>//index.hnx.index//</td>
                                    <td>//index.hnx.percent//</td>
                                    <td>//index.hnx.vol//</td>
                                    <td>//index.hnx.value//</td>
                                    <td>//index.hnx.udh//</td>
                                    <td>//index.hnx.gtnnm// tỷ</td>
                                    <td>//index.hnx.gtnnb// tỷ</td>
                                    <td>//(index.hnx.gtnnm - index.hnx.gtnnb).toFixed(2)// tỷ</td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                        <!-- /.table-responsive -->
                    </div>
                    <!-- /.panel-body -->
                </div>
                <!-- /.panel -->
            </div>
            <!-- /.col-lg-6 -->
            {{--<div class="col-lg-6">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Phân bổ dòng tiền
                    </div>
                    <!-- /.panel-heading -->
                    <div class="panel-body">
                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>First Name</th>
                                    <th>Last Name</th>
                                    <th>Username</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td>1</td>
                                    <td>Mark</td>
                                    <td>Otto</td>
                                    <td>@mdo</td>
                                </tr>
                                <tr>
                                    <td>2</td>
                                    <td>Jacob</td>
                                    <td>Thornton</td>
                                    <td>@fat</td>
                                </tr>
                                <tr>
                                    <td>3</td>
                                    <td>Larry</td>
                                    <td>the Bird</td>
                                    <td>@twitter</td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                        <!-- /.table-responsive -->
                    </div>
                    <!-- /.panel-body -->
                </div>
                <!-- /.panel -->
            </div>--}}
            <!-- /.col-lg-6 -->
        </div>
        <!-- /.row -->
        <div class="row" ng-app="marketoverview-plcs">
            <div class="col-lg-6" ng-controller="plcs">
                <div class="panel panel-default">
                    <div class="panel-heading" style="color: steelblue">
                        Phân lớp và chỉ số
                    </div>
                    <!-- /.panel-heading -->
                    <div class="panel-body">
                        <div class="table-responsive">
                            <table class="table table-striped">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Thay đổi</th>
                                    <th>Khối lượng</th>
                                    <th>Giá trị</th>
                                    <th>SV-1</th>
                                    <th>SV-BQ</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td>ALL</td>
                                    <td ng-class="allPercent">//all.percent//</td>
                                    <td>//all.vol//</td>
                                    <td>//all.value//</td>
                                    <td ng-class="allSv1Percent">//all.sv1//</td>
                                    <td ng-class="allSvbqPercent">//all.svbq//</td>
                                </tr>
                                <tr>
                                    <td>HOSE</td>
                                    <td ng-class="hosePercent">//hose.percent//</td>
                                    <td>//hose.vol//</td>
                                    <td>//hose.value//</td>
                                    <td ng-class="hoseSv1Percent">//hose.sv1//</td>
                                    <td ng-class="hoseSvbqPercent">//hose.svbq//</td>
                                </tr>
                                <tr>
                                    <td>HNX</td>
                                    <td ng-class="haxPercent">//hax.percent//</td>
                                    <td>//hax.vol//</td>
                                    <td>//hax.value//</td>
                                    <td ng-class="haxSv1Percent">//hax.sv1//</td>
                                    <td ng-class="haxSvbqPercent">//hax.svbq//</td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td>Cơ bản</td>
                                    <td ng-class="cobanPercent">//coban.percent//</td>
                                    <td>//coban.vol//</td>
                                    <td>//coban.value//</td>
                                    <td ng-class="cobanSv1Percent">//coban.sv1//</td>
                                    <td ng-class="cobanSvbqPercent">//coban.svbq//</td>
                                </tr>
                                <tr>
                                    <td>Đầu cơ</td>
                                    <td ng-class="daucoPercent">//dauco.percent//</td>
                                    <td>//dauco.vol//</td>
                                    <td>//dauco.value//</td>
                                    <td ng-class="daucoSv1Percent">//dauco.sv1//</td>
                                    <td ng-class="daucoSvbqPercent">//dauco.svbq//</td>
                                </tr>
                                <tr>
                                    <td>Blue</td>
                                    <td ng-class="bluePercent">//blue.percent//</td>
                                    <td>//blue.vol//</td>
                                    <td>//blue.value//</td>
                                    <td ng-class="blueSv1Percent">//blue.sv1//</td>
                                    <td ng-class="blueSvbqPercent">//blue.svbq//</td>
                                </tr>
                                <tr>
                                    <td>Penny</td>
                                    <td ng-class="pennyPercent">//penny.percent//</td>
                                    <td>//penny.vol//</td>
                                    <td>//penny.value//</td>
                                    <td ng-class="pennySv1Percent">//penny.sv1//</td>
                                    <td ng-class="pennySvbqPercent">//penny.svbq//</td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                        <!-- /.table-responsive -->
                    </div>
                    <!-- /.panel-body -->
                </div>
                <!-- /.panel -->
            </div>
            <!-- /.col-lg-6 -->
            <div class="col-lg-6">
                <div class="panel panel-default">
                    <div class="panel-heading  form-inline" style="color: steelblue">
                        Phân bố dòng tiền - <select id="cachFlowSelect" class="selectpicker form-control"
                                                    ng-model="cashFlowSelect"
                                                    ng-click="changeShowCashFlow()" style="height: 30px;">
                            <option value="total">Total Value</option>
                            <option value="net">Net Value</option>
                        </select>
                        <label style="float:right;font-size: 10px;color:black"><strong>đơn vị: tỷ</strong></label>
                    </div>
                    <!-- /.panel-heading -->
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-lg-4">
                                <div class="table-responsive" ng-style="tableDetailStyle">
                                    <table class="table table-striped" style="font-size: 9px">
                                        <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Yesterday</th>
                                            <th>Today</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td>Net</td>
                                            <td>//cashFlowTable.dataDetail.yesterday.net//</td>
                                            <td>//cashFlowTable.dataDetail.today.net//</td>
                                        </tr>
                                        <tr>
                                            <td>Index</td>
                                            <td iz-percent-effect
                                                true-percent="//cashFlowTable.dataDetail.yesterday.index//">
                                                //cashFlowTable.dataDetail.yesterday.index+ '%'//
                                            </td>
                                            <td iz-percent-effect
                                                true-percent="//cashFlowTable.dataDetail.today.index//">
                                                //cashFlowTable.dataDetail.today.index + '%'//
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <div class="col-lg-8">
                                <canvas id="bar" class="chart chart-bar" chart-data="dataChart"
                                        chart-labels="labelsChart" chart-options="optionsChart"
                                        chart-legend="legendChart"
                                        chart-colours="coloursChart" chart-series="seriesTrend">
                                </canvas>
                            </div>
                        </div>
                        <!-- /.row -->
                        <div class="table-responsive">
                            <table class="table table-striped">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Top tăng</th>
                                    <th>Top Giảm</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td>1</td>
                                    <td>//cashFlowTable.dataCashFlow[0].up//
                                        <abc style="color:green">//cashFlowTable.dataCashFlow[0].upPercent//</abc>
                                    </td>
                                    <td>//cashFlowTable.dataCashFlow[0].down//
                                        <abc style="color:red">//cashFlowTable.dataCashFlow[0].downPercent//</abc>
                                    </td>
                                </tr>
                                <tr>
                                    <td>2</td>
                                    <td>//cashFlowTable.dataCashFlow[1].up//
                                        <abc style="color:green">//cashFlowTable.dataCashFlow[1].upPercent//</abc>
                                    </td>
                                    <td>//cashFlowTable.dataCashFlow[1].down//
                                        <abc style="color:red">//cashFlowTable.dataCashFlow[1].downPercent//</abc>
                                    </td>
                                </tr>
                                <tr>
                                    <td>3</td>
                                    <td>//cashFlowTable.dataCashFlow[2].up//
                                        <abc style="color:green">//cashFlowTable.dataCashFlow[2].upPercent//</abc>
                                    </td>
                                    <td>//cashFlowTable.dataCashFlow[2].down//
                                        <abc style="color:red">//cashFlowTable.dataCashFlow[2].downPercent//</abc>
                                    </td>
                                </tr>
                                <tr>
                                    <td>4</td>
                                    <td>//cashFlowTable.dataCashFlow[3].up//
                                        <abc style="color:green">//cashFlowTable.dataCashFlow[3].upPercent//</abc>
                                    </td>
                                    <td>//cashFlowTable.dataCashFlow[3].down//
                                        <abc style="color:red">//cashFlowTable.dataCashFlow[3].downPercent//</abc>
                                    </td>
                                </tr>
                                <tr>
                                    <td>5</td>
                                    <td>//cashFlowTable.dataCashFlow[4].up//
                                        <abc style="color:green">//cashFlowTable.dataCashFlow[4].upPercent//</abc>
                                    </td>
                                    <td>//cashFlowTable.dataCashFlow[4].down//
                                        <abc style="color:red">//cashFlowTable.dataCashFlow[4].downPercent//</abc>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                        <!-- /.table-responsive -->
                    </div>
                    <!-- /.panel-body -->
                </div>
                <!-- /.panel -->
            </div>
            <!-- /.col-lg-6 -->
        </div>
        <!-- /.row TODO: Nước ngoài -->
        <div class="row">
            <div class="col-lg-12">

                <div class="panel panel-default">
                    <div class="panel-heading form-inline" style="color: steelblue">
                        Top nước ngoài mua bán
                        {{--<select id="boardBForeign" class="selectpicker form-control" ng-model="mSelect"
                                ng-click="changeBoardBForeign('m')">
                            <option value="all">Tất cả</option>
                            <option value="hax">HNX</option>
                            <option value="hose">HOSE</option>
                        </select>--}}
                        <div class="table-responsive" style="width:60%;margin: 0 auto;color:black;font-size: 12px">
                            <table class="table">
                                <thead>
                                <tr>
                                    <th>Tổng KL mua</th>
                                    <th>Tổng KL bán</th>
                                    <th>Tổng GT mua</th>
                                    <th>Tổng GT bán</th>
                                    <th>Tổng KL ròng</th>
                                    <th>Tổng GT ròng</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td>//(detailedForeign.sumNnM).formatMoney(0, '.', ',')//</td>
                                    <td>//(detailedForeign.sumNnB).formatMoney(0, '.', ',')//</td>
                                    <td>//(detailedForeign.sumGtM).formatMoney(0, '.', ',')//</td>
                                    <td>//(detailedForeign.sumGtB).formatMoney(0, '.', ',')//</td>
                                    <td>//(detailedForeign.sumNnM-detailedForeign.sumNnB).formatMoney(0, '.', ',')//
                                    </td>
                                    <td>//(detailedForeign.sumGtM-detailedForeign.sumGtB).formatMoney(0, '.', ',')//
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <!-- /.panel-heading -->
                    <div class="panel-body">
                        <div class="table-responsive">
                            {{--<table datatable="" dt-options="showCase.dtOptions" dt-columns="showCase.dtColumns" dt-instance="showCase.dtInstance" class="row-border hover"></table>--}}
                            <table ng-table="customConfigParams"
                                   class="table table-condensed table-bordered table-striped" show-filter="true">
                                {{--<thead>
                                <tr>
                                    <th>Mã</th>
                                    <th>Sàn</th>
                                    <th>Thay đổi</th>
                                    <th>Khối lượng</th>
                                    <th>Nước ngoài mua</th>
                                    <th>Giá trị mua</th>
                                    <th>% NN mua</th>
                                    <th>Nước ngoài bán</th>
                                    <th>Giá trị bạn</th>
                                    <th>% NN bán</th>
                                    <th>Giá trị ròng</th>
                                </tr>
                                </thead>--}}
                                {{--<tbody>--}}
                                <tr ng-repeat="tickera in $data" on-finish-render="ngRepeatFinished">
                                    <td title="'Mã'" filter='{ticker:"text"}' sortable="'ticker'" align="center">
                                        //tickera.ticker//
                                    </td>
                                    <td title="'Sàn'" filter='{board:"text"}' sortable="'board'" align="center">
                                        //tickera.board//
                                    </td>
                                    <td title="'Thay đổi'" sortable="'tpercent'" true-percent="//tickera.tpercent//"
                                        iz-percent-effect align="center">//tickera.percent//
                                    </td>
                                    <td title="'Tổng Khối lượng'" sortable="'tvol'" align="right">//tickera.vol//</td>
                                    <td title="'KL NN mua'" sortable="'tnnm'" align="right">//(tickera.nnm)//</td>
                                    <td title="'Giá trị NN mua'" sortable="'tmValue'" align="right">//tickera.mValue//
                                    </td>
                                    <td title="'% NN mua'" sortable="'bPercent'" align="center">//tickera.bPercent//
                                    </td>
                                    <td title="'KL NN bán'" sortable="'tnnb'" align="right">//tickera.nnb//</td>
                                    <td title="'Giá trị NN bán'" sortable="'tbValue'" align="right">//tickera.bValue//
                                    </td>
                                    <td title="'% NN bán'" sortable="'sPercent'" align="center">//tickera.sPercent//
                                    </td>
                                    <td title="'Giá trị ròng'" sortable="'tnet'" align="right">//tickera.net//</td>
                                </tr>
                                {{-- <tr ng-repeat="user in $data">
                                     <td title="'Name'" filter="{ name: 'text'}" sortable="'name'">
                                         //user.name//</td>
                                     <td title="'Age'" filter="{ age: 'number'}" sortable="'age'">
                                         //user.age//</td>
                                 </tr>--}}




                                {{--  <tr>
                                      <td>//nnm2.ticker//</td>
                                      <td ng-class="nnm2Percent">//nnm2.percent//</td>
                                      <td>//nnm2.vol//</td>
                                      <td>//nnm2.value//</td>
                                      <td>//nnm2.percentValue//</td>
                                      <td>//nnm2.net//</td>
                                  </tr>
                                  <tr>
                                      <td>//nnm3.ticker//</td>
                                      <td ng-class="nnm3Percent">//nnm3.percent//</td>
                                      <td>//nnm3.vol//</td>
                                      <td>//nnm3.value//</td>
                                      <td>//nnm3.percentValue//</td>
                                      <td>//nnm3.net//</td>
                                  </tr>
                                  <tr>
                                      <td>//nnm4.ticker//</td>
                                      <td ng-class="nnm4Percent">//nnm4.percent//</td>
                                      <td>//nnm4.vol//</td>
                                      <td>//nnm4.value//</td>
                                      <td>//nnm4.percentValue//</td>
                                      <td>//nnm4.net//</td>
                                  </tr>
                                  <tr>
                                      <td>//nnm5.ticker//</td>
                                      <td ng-class="nnm5Percent">//nnm5.percent//</td>
                                      <td>//nnm5.vol//</td>
                                      <td>//nnm5.value//</td>
                                      <td>//nnm5.percentValue//</td>
                                      <td>//nnm5.net//</td>
                                  </tr>--}}
                                {{--</tbody>--}}
                            </table>
                        </div>
                        <!-- /.table-responsive -->
                    </div>
                    <!-- /.panel-body -->
                </div>
                <!-- /.panel -->
            </div>
            <!-- /.col-lg-6 -->
            {{--<div class="col-lg-6">
                <div class="panel panel-default">
                    <div class="panel-heading form-inline" style="color: steelblue">
                        Top 5 nước ngoài bán -
                        <select id="boardBForeign" class="selectpicker form-control" ng-model="bSelect"
                                ng-click="changeBoardBForeign('b')">
                            <option value="all">Tất cả</option>
                            <option value="hax">HNX</option>
                            <option value="hose">HOSE</option>
                        </select>
                    </div>
                    <!-- /.panel-heading -->
                    <div class="panel-body">
                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Thay đổi</th>
                                    <th>Khối lượng</th>
                                    <th>Giá trị</th>
                                    <th>% NN mua</th>
                                    <th>Bán ròng</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td>//nnb1.ticker//</td>
                                    <td ng-class="nnb1Percent">//nnb1.percent//</td>
                                    <td>//nnb1.vol//</td>
                                    <td>//nnb1.value//</td>
                                    <td>//nnb1.percentValue//</td>
                                    <td>//nnb1.net//</td>
                                </tr>
                                <tr>
                                    <td>//nnb2.ticker//</td>
                                    <td ng-class="nnb2Percent">//nnb2.percent//</td>
                                    <td>//nnb2.vol//</td>
                                    <td>//nnb2.value//</td>
                                    <td>//nnb2.percentValue//</td>
                                    <td>//nnb2.net//</td>
                                </tr>
                                <tr>
                                    <td>//nnb3.ticker//</td>
                                    <td ng-class="nnb3Percent">//nnb3.percent//</td>
                                    <td>//nnb3.vol//</td>
                                    <td>//nnb3.value//</td>
                                    <td>//nnb3.percentValue//</td>
                                    <td>//nnb3.net//</td>
                                </tr>
                                <tr>
                                    <td>//nnb4.ticker//</td>
                                    <td ng-class="nnb4Percent">//nnb4.percent//</td>
                                    <td>//nnb4.vol//</td>
                                    <td>//nnb4.value//</td>
                                    <td>//nnb4.percentValue//</td>
                                    <td>//nnb4.net//</td>
                                </tr>
                                <tr>
                                    <td>//nnb5.ticker//</td>
                                    <td ng-class="nnb5Percent">//nnb5.percent//</td>
                                    <td>//nnb5.vol//</td>
                                    <td>//nnb5.value//</td>
                                    <td>//nnb5.percentValue//</td>
                                    <td>//nnb5.net//</td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                        <!-- /.table-responsive -->
                    </div>
                    <!-- /.panel-body -->
                </div>
                <!-- /.panel -->
            </div>--}}
            <!-- /.col-lg-6 -->
        </div>
        <!-- /.row TODO: ĐO xu thế -->
        <div class="row">
            <div class="col-lg-6">
                <div class="panel panel-default">
                    <div class="panel-heading form-inline" style="color: steelblue">
                        Đo xu thế - <select id="trendSelect" class="selectpicker form-control" ng-model="trendSelect"
                                            ng-click="changeShowTrend()">
                            <option value="all">TOP 300</option>
                            <option value="top1">TOP 150</option>
                        </select>
                    </div>
                    <!-- /.panel-heading -->
                    <div class="panel-body">
                        <div class="table-responsive">
                            <table class="table table-hover">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Tiến hiệu tich cực</th>
                                    <th>Tiến hiệu tiêu cực</th>
                                    <th>Bổ trợ</th>
                                    <th>Đánh giá</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr bgcolor="">
                                    <td>NEW HIGH/LOW 1M</td>
                                    <td>//trendShow.oneM.pos//</td>
                                    <td>//trendShow.oneM.neg//</td>
                                    <td></td>
                                    <td>//trendShow.oneM.info//</td>
                                </tr>
                                <tr bgcolor="">
                                    <td>NEW HIGH/LOW 3M</td>
                                    <td>//trendShow.thirdM.pos//</td>
                                    <td>//trendShow.thirdM.neg//</td>
                                    <td></td>
                                    <td>//trendShow.thirdM.info//</td>
                                </tr>
                                <tr bgcolor="">
                                    <td>NEW HIGH/LOW 6M</td>
                                    <td>//trendShow.sixM.pos//</td>
                                    <td>//trendShow.sixM.neg//</td>
                                    <td></td>
                                    <td>//trendShow.sixM.info//</td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                <tr bgcolor="">
                                    <td>So với SMA20</td>
                                    <td>//trendShow.sma20.pos//</td>
                                    <td>//trendShow.sma20.neg//</td>
                                    <td>//trendShow.sma20.add//</td>
                                    <td>//trendShow.sma20.info//</td>
                                </tr>
                                <tr bgcolor="">
                                    <td>So với SMA50</td>
                                    <td>//trendShow.sma50.pos//</td>
                                    <td>//trendShow.sma50.neg//</td>
                                    <td>//trendShow.sma50.add//</td>
                                    <td>//trendShow.sma50.info//</td>
                                </tr>
                                <tr bgcolor="">
                                    <td>So với SMA100</td>
                                    <td>//trendShow.sma100.pos//</td>
                                    <td>//trendShow.sma100.neg//</td>
                                    <td>//trendShow.sma100.add//</td>
                                    <td>//trendShow.sma100.info//</td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                <tr bgcolor="">
                                    <td>UPTREND</td>
                                    <td>//trendShow.uptrend.pos//</td>
                                    <td>//trendShow.uptrend.neg//</td>
                                    <td></td>
                                    <td>//trendShow.uptrend.info//</td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                <tr bgcolor="">
                                    <td>Độ nóng lạnh RS</td>
                                    <td>//trendShow.rs.pos//</td>
                                    <td>//trendShow.rs.neg//</td>
                                    <td></td>
                                    <td>//trendShow.rs.info//</td>
                                </tr>
                                <tr bgcolor="">
                                    <td>Độ nóng lạnh MT</td>
                                    <td>//trendShow.sma10.pos//</td>
                                    <td>//trendShow.sma10.neg//</td>
                                    <td></td>
                                    <td>//trendShow.sma10.info//</td>
                                </tr>
                                <tr bgcolor="">
                                    <td>Độ nóng lạnh R1</td>
                                    <td>//trendShow.r1.pos//</td>
                                    <td>//trendShow.r1.neg//</td>
                                    <td></td>
                                    <td>//trendShow.r1.info//</td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                <tr bgcolor="">
                                    <td>Penny</td>
                                    <td>//trendShow.penny.closeUpTwoDay//</td>
                                    <td>//trendShow.penny.closeDownTwoDay//</td>
                                    <td>//trendShow.penny.neg//</td>
                                    <td></td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                        <!-- /.table-responsive -->
                    </div>
                    <!-- /.panel-body -->
                </div>
                <!-- /.panel -->
            </div>
            <!-- /.col-lg-6 -->
            <div class="col-lg-6">
                <div class="panel panel-default">
                    <div class="panel-heading form-inline" style="color: steelblue">
                        Đo cường độ tăng giảm
                    </div>
                    <!-- /.panel-heading -->
                    <div class="panel-body">
                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                <tr>
                                    <th colspan="2"></th>
                                    <th colspan="2">SV-1</th>
                                    <th colspan="2">SV BQ</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td>SV-1</td>
                                    <td></td>
                                    <td>Vol Tăng</td>
                                    <td>Vol Giảm</td>
                                    <td>Vol Tăng</td>
                                    <td>Vol Giảm</td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td>Tổng</td>
                                    <td>//inde.sv1.volIn//</td>
                                    <td>//inde.sv1.volDe//</td>
                                    <td>//inde.svbq.volIn//</td>
                                    <td>//inde.svbq.volDe//</td>
                                </tr>
                                <tr>
                                    <td>Giá tăng</td>
                                    <td>//inde.all.priceIn//</td>
                                    <td>//inde.all.priceInSv1VolIn//</td>
                                    <td>//inde.all.priceInSv1VolDe//</td>
                                    <td>//inde.all.priceInSvbqVolIn//</td>
                                    <td>//inde.all.priceInSvbqVolDe//</td>
                                </tr>
                                <tr>
                                    <td>Giá giảm</td>
                                    <td>//inde.all.priceDe//</td>
                                    <td>//inde.all.priceDeSv1VolIn//</td>
                                    <td>//inde.all.priceDeSv1VolDe//</td>
                                    <td>//inde.all.priceDeSvbqVolIn//</td>
                                    <td>//inde.all.priceDeSvbqVolDe//</td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                        <!-- /.table-responsive -->
                    </div>
                    <!-- /.panel-body -->
                </div>
                <!-- /.panel -->
            </div>
            <!-- /.col-lg-6 -->
        </div>
        <!-- /.row -->
    </div>
@stop
@section('end-page-js-component')
    {{--ng table--}}
    <link href="{{asset('sb-admin/bower_components/ng-table/dist/ng-table.min.css')}}" rel="stylesheet">
    <script src="{{asset('sb-admin/bower_components/ng-table/dist/ng-table.js')}}"></script>

    <!-- MarketOverview JavaScript -->
    <script src="{{asset('sb-admin/marketoverview/marketoverview.js')}}"></script>

@stop
@section('end-page-script')
    <script>
        $(document).ready(function () {
            $('#dataTables-example').DataTable({
                responsive: true
            });
        });
    </script>
@stop
