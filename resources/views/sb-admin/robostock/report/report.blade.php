@extends('sb-admin.robostock.Main')
@section('page-wrapper')
    <div id="page-wrapper" ng-app="reportApp" ng-controller="reportController">
        <div class="row">
            </br>
        </div>
        <div class="row" ng-init="tickers = {{$tickers}}">
            <div class="col-xs-1 col-sm-1 col-md-1 col-lg-1">
            </div>
            <div class="col-xs-10 col-sm-10 col-md-10 col-lg-10" style="border: red">
                <uib-tabset justified="true">
                    <uib-tab heading="Kết quả kinh doanh">
                        <iz-report-directive iz-report-type="KQKD"
                                             iz-transfer="izTransferKQKD(transfer)"></iz-report-directive>
                    </uib-tab>
                    <uib-tab heading="Cân đối kế toán">
                        <iz-report-directive iz-report-type="CDKT"
                                             iz-transfer="izTransferCDKT(transfer)"></iz-report-directive>
                    </uib-tab>
                    <uib-tab heading="Lưu chuyển tiền tệ">
                        <iz-report-directive iz-report-type="LC"
                                             iz-transfer="izTransferLC(transfer)"></iz-report-directive>
                    </uib-tab>
                    <uib-tab heading="Chỉ số tài chính">
                        <iz-report-directive iz-report-type="CSTC"
                                             iz-transfer="izTransferCSTC(transfer)"></iz-report-directive>
                    </uib-tab>
                </uib-tabset>
            </div>
            <div class="col-xs-1 col-sm-1 col-md-1 col-lg-1">

            </div>
        </div>
    </div>
@stop
@section('end-page-js-component')
    {{--Bootstrap multi select--}}
    <script src="{{asset('sb-admin/non_bower/bootstrap_multi_select/js/bootstrap-multiselect.js')}}"></script>
    <link href="{{asset('sb-admin/non_bower/bootstrap_multi_select/css/bootstrap-multiselect.css')}}" rel="stylesheet">

    <!-- Report JavaScript -->
    <script src="{{asset('sb-admin/report/report.js')}}"></script>
    {{--Directive for report--}}
    <script src="{{asset('sb-admin/report/directiveReport.js')}}"></script>

    {{--UI Bootstrap--}}
    <script src="{{asset('sb-admin/bower_components/angular-bootstrap/ui-bootstrap.min.js')}}"></script>
    <script src="{{asset('sb-admin/bower_components/angular-bootstrap/ui-bootstrap-tpls.min.js')}}"></script>
    <link href="{{asset('sb-admin/bower_components/angular-bootstrap/ui-bootstrap-csp.css')}}" rel="stylesheet">

    {{--angula mask--}}
    <script src="{{asset('sb-admin/bower_components/angular-ui-mask/dist/mask.min.js')}}"></script>

    {{--Report css--}}
    <link href="{{asset('sb-admin/report/report.css')}}" rel="stylesheet">


@stop
