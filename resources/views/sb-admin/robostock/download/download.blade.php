@extends('sb-admin.robostock.Main')
@section('page-wrapper')
    <div id="page-wrapper" ng-app="downloadApp" ng-controller="downloadCtrl">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header"><br></h1>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <div class="row">
            <!-- /.panel -->
            <div class="col-lg-12">
                <div class="panel panel-info">
                    <div class="panel-heading">
                        Cổ phiếu điều chỉnh gần đây
                    </div>
                    <!-- /.panel-heading -->
                    <div class="panel-body">
                        <div class="bs-example" data-example-id="hoverable-table">
                            <table id="example" width="100%" cellspacing="0" class="display table table-hover myTable">
                                <thead>
                                <tr>
                                    <th width="35px">ID</th>
                                    <th>Date</th>
                                    <th>Ticker</th>
                                    <th>Board</th>
                                    <th>HSDC</th>
                                    <th>{{--Update--}}</th>
                                    <th>{{--Cancel--}}</th>
                                </tr>
                                </thead>
                                <tfoot>
                                <tr>
                                    <th>Id</th>
                                    <th>Date</th>
                                    <th>Ticker</th>
                                    <th>Board</th>
                                    <th>HSDC</th>
                                    <th>{{--Update--}}</th>
                                    <th>{{--Cancel--}}</th>
                                </tr>
                                </tfoot>
                                <tbody>
                                @foreach($tickersAdj as $ticker)
                                    <tr>
                                        <th scope="row">{{$ticker->id}}</th>
                                        <td>{{$ticker->date}}</td>
                                        <td>{{$ticker->ticker}}</td>
                                        <td>{{$ticker->board}}</td>
                                        <td>{{$ticker->hsdc}}</td>
                                        <td>
                                            <button type="button"
                                                    class="btn btn-success btW"
                                                    onclick="downloadTickerAdj('{{$ticker['ticker']}}','ami')">
                                                Ami
                                            </button>
                                        </td>
                                        <td>
                                            <button type="button"
                                                    class="btn btn-warning btW"
                                                    onclick="downloadTickerAdj('{{$ticker['ticker']}}','meta')">
                                                Meta
                                            </button>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                            <script>
                                jQuery(document).ready(function () {
                                    $('#example').DataTable({
                                        "order": [[1, "desc"]]
                                    });


                                });
                            </script>
                        </div>
                    </div>
                </div>
                <!-- /.panel-body -->
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-info">
                    <div class="panel-heading">
                        Tải về
                    </div>
                    <!-- /.panel-heading -->
                    <div class="panel-body">
                        <form id="formTickerDownload" class="form-inlinea" ng-init="tickers = {{$tickers}}"
                              action="{{action('RobostockDashboardController@postDownloadData')}}" method="post">
                            <div class="form-group">
                                <label class="sr-onlya" for="exampleInputEmail3">Ticker</label>
                                <input id="tickertext" class="form-control"
                                       placeholder="Ticker: Bỏ trống thì lấy cả sàn"
                                       width="30px" style=""
                                       name="ticker" ng-model="tickerD">
                            </div>
                            <div class="form-group">
                                <label class="sr-onlya" for="exampleInputPassword3">Date</label>
                                <input date-range-picker type="text" class="form-control"
                                       placeholder="From: Không được bỏ trống"
                                       width="30px" style="" name="from" ng-model="tickerDownload.date" id="from"
                                       options="{separator: ':'}"/>
                            </div>
                            <div class="form-group">
                                <label class="sr-onlya" for="exampleInputPassword3">Type</label>
                                <select class="form-control" name="format" id="format">
                                    <option value="ami">Ami</option>
                                    <option value="meta">Meta</option>
                                </select>
                            </div>
                            <button type="button" class="btn btn-default"
                                    style="align-content: center;position: relative;left: 37%;"
                                    ng-click="downloadTicker()">Download
                            </button>
                            <button type="button" class="btn btn-default"
                                    style="align-content: center;position: relative;left: 45%;"
                                    ng-click="fastDownloadTicker()">Fast Download
                            </button>
                            {{--<input type="hidden" name="_token" value="{{ csrf_token() }}">--}}
                        </form>
                    </div>
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
    </div>
    @stop
    @section('end-page-js-component')

            <!-- MarketOverview JavaScript -->
    <script src="{{asset('sb-admin/download/download.js')}}"></script>

@stop
