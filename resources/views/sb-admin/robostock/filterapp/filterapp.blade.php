@extends('sb-admin.robostock.Main')
@section('page-wrapper')
    <div id="page-wrapper" ng-app="filterApp">
        <div class="row">
            </br>
        </div>
        <div class="col-xs-1 col-sm-1 col-md-1 col-lg-1">
        </div>
        <div class="col-xs-10 col-sm-10 col-md-10 col-lg-10">
            <h2>Filter Application</h2>

            <div class="panel panel-default" ng-controller="FilterAppCtrl">
                <div class="panel-body">
                    <div ng-init="dataCriterias = {{$criteria}}">
                        <button type="button" class="btn btn-primary"
                                ng-click="isCollapsed = !isCollapsed;showHideFilter()">
                            //showFilterLabel//
                        </button>
                        <hr>
                        <div uib-collapse="isCollapsed">
                            <div class="panel panel-default">
                                <div class="panel-body">
                                    <form class="form-inline">
                                        <div class="form-group formgroupFix">
                                            <label for="exampleInputName2">Kiểu báo cáo</label>
                                            <select name="report_type" id="reportType" class="form-control"
                                                    ng-model="reportType" ng-change="changeReportType()">
                                                <option value="CSTC"> Chỉ số tài chính</option>
                                                <option value="KQKD"> Kết quả kinh doanh</option>
                                                <option value="CDKT"> Cân đối kế toán</option>
                                                <option value="LC"> Lưu chuyển tiền tệ</option>
                                            </select>
                                        </div>
                                        <div class="form-group formgroupFix">
                                            <label for="exampleInputEmail2">Tiêu chí</label>
                                            <select name="criteria_id" id="criteria_id" class="form-control"
                                                    ng-model="criteriaId">
                                                <option ng-repeat="criteria in data.criterias" value="//criteria.id//">
                                                    //criteria.name//
                                                </option>
                                            </select>
                                        </div>
                                        <div class="form-group formgroupFix">
                                            <label for="exampleInputEmail2">Filter Type</label>
                                            <select name="criteria_id" id="criteria_id" class="form-control"
                                                    uib-popover="So sánh tương đối là so sánh giữa 2 kì với nhau.So sánh tuyệt đối là kiểm tra giá trị trong 1 kì"
                                                    popover-trigger="mouseenter" ng-model="filterType">
                                                <option value="ABSOLUTE"> Tuyệt đối</option>
                                                <option value="RELATIVE"> Tương đối</option>
                                            </select>
                                        </div>
                                        <button type="submit" class="btn btn-info pull-right"
                                                ng-click="addToFilterGroups()">Add
                                        </button>
                                    </form>

                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default" ng-hide="data.filterGroups.length == 0"
                             style="font-size: 10px">
                            <div class="panel-heading">
                                <h3 class="panel-title">Filter</h3>
                            </div>
                            <div class="panel-body">
                                <form action="" method="POST"
                                      class="form-inline animate-repeat animated bounceIn" role="form"
                                      ng-repeat="filter in data.filterGroups" style="margin-top: 5px">
                                    <div class="form-group">
                                        <div class="input-group">
                                        <span class="input-group-btn">
                                        <button class="btn btn-default" type="button"
                                                ng-click="removeFilter(filter.criteriaId)"><i class="fa fa-times"></i>
                                        </button>
                                        </span>
                                            <input type="text" readonly class="form-control"
                                                   ng-model="filter.criteriaName" style="width: 200px">
                                        </div>
                                    </div>
                                    -
                                    <div class="form-group">
                                        <div class="input-group">
                                        <span class="input-group-btn">
                                        <button popover-is-open="$first" class="btn btn-default" uib-popover="Click"
                                                popover-trigger="mouseenter" type="button"
                                                ng-click="filter.isYear = !filter.isYear">//filter.timeLabel//
                                        </button>
                                        </span>
                                            <input ng-model="filter.time1" type="text" ng-hide="!filter.isYear"
                                                   class="form-control"
                                                   style="width: 100px" ui-mask="Năm 9999" ui-mask-placeholder
                                                   ui-mask-placeholder-char="_">
                                            <input ng-model="filter.time1" type="text" ng-hide="filter.isYear"
                                                   class="form-control"
                                                   style="width: 100px" uib-popover="Các quý ngăn cách bởi dấu ,"
                                                   popover-trigger="mouseenter" >
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <input ng-disabled="filter.isAbsolute" ng-model="filter.time2" type="text"
                                               ng-hide="!filter.isYear"
                                               class="form-control"
                                               style="width: 100px" ui-mask="Năm 9999" ui-mask-placeholder
                                               ui-mask-placeholder-char="_">
                                        <input ng-disabled="filter.isAbsolute" ng-model="filter.time2" type="text"
                                               ng-hide="filter.isYear"
                                               class="form-control"
                                               style="width: 100px" >
                                    </div>
                                    <div class="form-group">
                                        <div class="input-group">
                                        <span class="input-group-btn">
                                        {{--<button class="btn btn-default" type="button" ng-click="">Điều kiện</button>--}}
                                            </span>
                                            <select name="name" id="inputID" class="form-control"
                                                    ng-model="filter.condition" uib-popover=""
                                                    popover-trigger="mouseenter">
                                                <option value="gt">Lớn hơn</option>
                                                <option value="lt">Bé hơn</option>
                                                <option value="in">Nằm giữa</option>
                                                <option value="all">Tất cả</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <input type="text" name="name" id="inputID" class="form-control"
                                               ng-hide="filter.condition == 'all'"
                                               ng-model="filter.checkValue"
                                               title="" required="required" uib-popover="Giá trị để so sánh"
                                               popover-trigger="mouseenter" style="width: 150px">
                                    </div>
                                    <div class="form-group">
                                        <input type="text" name="name" id="inputID" class="form-control"
                                               ng-hide="filter.condition != 'in'"
                                               ng-model="filter.checkValue2"
                                               title="" required="required" uib-popover="Giá trị thứ hai"
                                               popover-trigger="mouseenter" style="width: 150px">
                                    </div>
                                </form>
                                <button type="submit" class="btn btn-success pull-right" style="margin-top: 20px"
                                        ng-hide="data.filterGroups.length == 0" ng-click="sendFilter()">Filter
                                </button>
                            </div>
                        </div>
                        <div class="panel panel-default" ng-hide="!isResulted">
                            <div class="panel-heading">
                                <div class="panel-title">Filter Result
                                    <div class="pull-right" style="margin-top: -3px;">
                                        <button type="button" class="btn btn-default btn-sm" ng-click="downloadExcel()">
                                            <span class="glyphicon glyphicon-save" aria-hidden="true"></span> Excel
                                        </button>
                                    </div>
                                </div>
                            </div>
                            <div class="panel-body">
                                <table ng-table-dynamic="tableParams with result.dataRow"
                                       class="table table-condensed table-bordered table-striped" show-filter="false">
                                    <tr ng-repeat="row in $data">
                                        <td ng-repeat="col in $columns" percent-color="A"
                                            percent-value="//row[col.field]//" percent-label="//col.field//">
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xs-1 col-sm-1 col-md-1 col-lg-1">

        </div>
    </div>
@stop
@section('end-page-js-component')
    {{--angula mask--}}
    <script src="{{asset('sb-admin/bower_components/angular-ui-mask/dist/mask.min.js')}}"></script>

    <link href="{{asset('sb-admin/bower_components/ng-table/dist/ng-table.min.css')}}" rel="stylesheet">
    <script src="{{asset('sb-admin/bower_components/ng-table/dist/ng-table.js')}}"></script>

    {{--Bootstrap multi select--}}
    <script src="{{asset('sb-admin/non_bower/bootstrap_multi_select/js/bootstrap-multiselect.js')}}"></script>
    <link href="{{asset('sb-admin/non_bower/bootstrap_multi_select/css/bootstrap-multiselect.css')}}" rel="stylesheet">

    {{--UI Bootstrap--}}
    <script src="{{asset('sb-admin/bower_components/angular-bootstrap/ui-bootstrap.min.js')}}"></script>
    <script src="{{asset('sb-admin/bower_components/angular-bootstrap/ui-bootstrap-tpls.min.js')}}"></script>
    <link href="{{asset('sb-admin/bower_components/angular-bootstrap/ui-bootstrap-csp.css')}}"
          rel="stylesheet"/>

    {{--Report css--}}
    <link href="{{asset('sb-admin/filterApp/report.css')}}" rel="stylesheet"/>

    <!-- Report JavaScript -->
    <script src="{{asset('sb-admin/filterApp/filterApp.js')}}"></script>
@stop
