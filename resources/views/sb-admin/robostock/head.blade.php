<script>
    /*TODO: variable*/
    window.currentDate = '{{App\RoboStock\Model\WorkWithTime\WorkWithTime::getCurrentDate()}}';

    /*DO: Market*/
    window.urlGetTickerInday = '{{action('MarketOverview\MarketController@postTickerInday')}}';
    /*DO: PLCS*/
    window.urlPostIndexPercent = '{{action('MarketOverview\PlcsController@postPercentIndex')}}';

    /*download*/
    window.urlDownload = '{{action('RobostockDashboardController@postDownloadData')}}';
    window.urlFastDownload = '{{action('RobostockDashboardController@postFastDownloadData')}}';

    /*report*/
    window.urlReportGetData = '{{action('RobostockReportController@getDataReport')}}';

    /*postfilter*/

    window.urlPostFilter = '{{action('RobostockFilterAppController@postFilter')}}';

    /*download excel filter*/
    window.urlPostDownloadExcelFilter = '{{action('RobostockFilterAppController@postDownloadExcel')}}';

    /*download excel report*/
    window.urlDownloadExcelReport = '{{action('RobostockReportController@postDownloadExcel')}}';
</script>
