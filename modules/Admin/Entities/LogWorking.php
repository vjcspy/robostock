<?php namespace Modules\Admin\Entities;

use Illuminate\Database\Eloquent\Model;

class LogWorking extends Model {
    protected $table = 'log_working';
    protected $fillable = ['log_type', 'work_id', 'work_name', 'is_working'];


    public function updateWorking($work_id, $work_name, $is_working = 1) {
        $work = $this->firstOrNew([
            'work_id' => $work_id
        ]);
        $work->work_name = $work_name;
        $work->is_working = $is_working;
        $work->save();
    }
}
