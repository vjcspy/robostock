<?php namespace Modules\Admin\Entities;

use Illuminate\Database\Eloquent\Model;

class LogWork extends Model {
    protected $table = 'log_work';
    protected $fillable = ['log_type', 'work', 'data'];

    public function addLog($work, $data, $logType = 1) {
        $this->create([
            'log_type' => $logType,
            'work' => $work,
            'data' => $data
        ]);
    }
}
