<?php
/**
 * Created by mr.vjcspy@gmail.com/khoild@smartosc.com.
 * Date: 2/1/16
 * Time: 2:52 PM
 */

namespace Modules\Admin\Config\Partial;


trait CoreConfig {

    public function getCoreConfig() {
        return config('admin.core_config');
    }
}
