<?php

return [
    'name' => 'Admin',
    'core_config' => [
        'site' => [
            'General' => [
                'site_name' => [
                    'type' => 'text',
                ],
                'logo-black' => [
                    'type' => 'text'
                ],
                'logo-white' => [
                    'type' => 'text'
                ]
            ],
            'Cache Configuration' => [
                'page_cache_lifetime' => [
                    'type' => 'text',
                    'validate' => 'validate-digits, validate-zero-or-greater'
                ],
                'page_cache' => [
                    'type' => 'select',
                    'validate' => 'validate-digits, validate-zero-or-greater',
                    'optionsData' => [
                        [
                            'id' => '',
                            'name' => '--Please Select--'
                        ], [
                            'id' => '1',
                            'name' => 'Enable'
                        ],
                        [
                            'id' => '0',
                            'name' => 'Disable'
                        ],
                    ]
                ]
            ],
        ]
    ]
];
