<?php
/**
 * Created by mr.vjcspy@gmail.com/khoild@smartosc.com.
 * Date: 2/1/16
 * Time: 2:53 PM
 */

namespace Modules\Admin\Config;


use Modules\Admin\Config\Partial\CoreConfig;
use Modules\Admin\Config\Partial\Validate;

class Config {
    use Validate;
    use CoreConfig;

    public function getConfig() {
        return config('admin');
    }
}
