<?php

Route::group(['middleware' => 'web', 'prefix' => 'izAdmin', 'namespace' => 'Modules\Admin\Http\Controllers'], function () {
    Route::get('/', 'AdminController@index');

});
Route::group(['prefix' => 'api/v1', 'namespace' => 'Modules\Admin\Http\Controllers'], function () {
    Route::resource('/config', 'Api\ConfigController', ['except' => ['create', 'store', 'destroy', 'edit']]);
    Route::resource('/control', 'Api\System\ControlController', ['except' => ['create', 'store', 'destroy', 'edit']]);

});

Route::group(['middleware' => 'web', 'prefix' => 'table', 'namespace' => 'Modules\Admin\Http\Controllers'], function () {
    Route::controller('/article', 'Api\DataTable\Article\DataController');
});
