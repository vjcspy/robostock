<?php namespace Modules\Admin\Http\Controllers\Api\System;

use Illuminate\Support\Facades\Artisan;
use Modules\Admin\Http\Controllers\Api\ApiAbstractController;
use Response;
use Illuminate\Http\Request;

/**
 * Class ConfigController
 * @package Modules\Admin\Http\Controllers\Api
 */
class ControlController extends ApiAbstractController {

    /**
     * @var \Modules\Admin\Entities\LogWork
     */
    protected $_logWork;
    /**
     * @var \Modules\Admin\Entities\LogWorking
     */
    protected $_logWorking;


    /**
     * ControlController constructor.
     */
    public function __construct() {
        $this->_logWork = app()->make('\Modules\Admin\Entities\LogWork');
        $this->_logWorking = app()->make('\Modules\Admin\Entities\LogWorking');
        parent::__construct();
    }


    /**
     * @return mixed
     */
    public function index() {
        try {
            $this->_response['working'] = $this->_logWorking->all();
            $this->_response['work'] = $this->_logWork->all();
        } catch (\Exception $e) {
            $this->_statusCode = self::STATUS_BAD_REQUEST;
        } finally {
            return $this->end();
        }
    }

    public function update(Request $request, $work_id) {
        try {
            $this->_statusCode = 200;
            $this->runTask($work_id);
        } catch (\Exception $e) {
            $this->_statusCode = 500;
        } finally {
            return $this->end();
        }
    }

    private function runTask($work_id) {
        Artisan::queue('task:abstract', [
            'work_id' => $work_id
        ]);
    }
}
