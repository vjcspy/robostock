<?php namespace Modules\Admin\Http\Controllers\Api;

use Pingpong\Modules\Routing\Controller;
use Response;

/**
 * Class ConfigController
 * @package Modules\Admin\Http\Controllers\Api
 */
class ApiAbstractController extends Controller {

    /**
     * @var mixed
     */
    protected $_config;

    /**
     * @var \Modules\Blog\Entities\CoreConfig
     */
    protected $_blogConfigModel;

    /**
     * @var int
     */
    protected $_statusCode = 200;

    /**
     * @var array
     */
    protected $_response = [];

    const STATUS_BAD_REQUEST = 400;
    const STATUS_OK = 200;
    const STATUS_SERVICE_UNAVAILABLE = 503;
    const STATUS_REQUEST_URL_TOO_LONG = 414;
    const STATUS_FORBIDDEN = 403;


    /**
     * ConfigController constructor.
     */
    public function __construct() {
        $this->_config = app()->make('\Modules\Admin\Config\Config');

//        $this->_blogConfigModel = app()['core_config'];
    }

    public function end() {
        return Response::json($this->_response, $this->_statusCode);
    }
}
