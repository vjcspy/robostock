<?php
/**
 * Created by mr.vjcspy@gmail.com/khoild@smartosc.com.
 * Date: 2/5/16
 * Time: 4:08 PM
 */

namespace Modules\Admin\Http\Controllers\Api\DataTable\Article;


use Illuminate\Http\Request;
use Response;
use Modules\Admin\Http\Controllers\Api\DataTable\Contract\DataTableAbstract;
use Modules\Admin\Http\Controllers\Api\DataTable\Contract\DataTableInterface;

/**
 * Class DataController
 * @package Modules\Admin\Http\Controllers\Api\DataTable\Article
 */
class DataController extends DataTableAbstract implements DataTableInterface {


    /**
     * @param Request $request
     * @return mixed
     */
    public function getIndex(Request $request) {
        $requestData = $this->getRequestData($request);
        $this->setModel(app()->make('\Modules\Blog\Entities\Article'));
        $dataArticle = $this->getData();
        $data =
            [
                "draw" => $requestData['draw'],
                "recordsTotal" => 4,
                "recordsFiltered" => 4,
                "data" => $dataArticle
            ];
        return Response::json($data);
    }
}
