<?php namespace Modules\Admin\Http\Controllers\Api;

use Illuminate\Http\Request;
use Pingpong\Modules\Routing\Controller;
use Response;

/**
 * Class ConfigController
 * @package Modules\Admin\Http\Controllers\Api
 */
class ConfigController extends Controller {

    /**
     * @var mixed
     */
    protected $_config;

    /**
     * @var \Modules\Blog\Entities\CoreConfig
     */
    protected $_blogConfigModel;

    /**
     * @var int
     */
    protected $_statusCode = 200;

    /**
     * @var array
     */
    protected $_response = [];


    /**
     * ConfigController constructor.
     */
    public function __construct() {
        $this->_config = app()->make('\Modules\Admin\Config\Config');

        $this->_blogConfigModel = app()['core_config'];
    }

    /**
     * @return mixed
     */
    public function index() {
        try {
            $this->_response['interface'] = $this->_config->getCoreConfig();
            $this->_response['configData'] = $this->_blogConfigModel->getConfig();
        } catch (\Exception $e) {
            $this->_statusCode = 400;
        } finally {
            return Response::json($this->_response, $this->_statusCode);
        }
    }

    /**
     * @param $group
     * @return mixed
     */
    public function show($group) {
        try {
            $this->_response[$group] = $this->_config->getCoreConfig()[$group];
        } catch (\Exception $e) {
            $this->_response = [
                "error" => "Config doesn't exists"
            ];
            $this->_statusCode = 404;
        } finally {
            return Response::json($this->_response, $this->_statusCode);
        }
    }

    public function update(Request $request, $id) {
        try {
            $this->_statusCode = 200;
            $allConfig = $request->all();
            foreach ($allConfig as $section => $data) {
                $currentGroup = $this->_blogConfigModel->firstOrNew(['group' => $section]);
                $currentGroup->value = json_encode($data);
                $currentGroup->save();
            }
            $this->_response = $currentGroup;
        } catch (\Exception $e) {
            $this->_statusCode = 500;
        } finally {
            return Response::json($this->_response, $this->_statusCode);
        }
    }
}
