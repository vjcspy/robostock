<?php namespace Modules\Admin\Console;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class TaskCommandAbstract extends Command {

    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = 'task:abstract {work_id}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Task for log Working';
    /**
     * @var \Modules\Admin\Entities\LogWorking
     */
    protected $_logWorking;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct() {
        $this->_logWorking = app()->make('\Modules\Admin\Entities\LogWorking');
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire() {
        $workId = $this->argument('work_id');
//        $this->updateLogWorking($workId);
        //do some thing here
//        $this->updateLogWorking($workId, false);
    }

    protected function updateLogWorking($work_id, $running = true) {
        $work = $this->_logWorking->query()->where('work_id', $work_id)->firstOrFail();
        if ($running) {
            if ($work->is_working == 1)
                throw new \Exception('Task is working');
            else {
                $work->is_working = 1;
                $work->save();
            }
        } else {
            $work->is_working = 0;
            $work->save();
        }
    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments() {
        return [
            ['example', InputArgument::REQUIRED, 'An example argument.'],
        ];
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions() {
        return [
            ['example', null, InputOption::VALUE_OPTIONAL, 'An example option.', null],
        ];
    }

}
