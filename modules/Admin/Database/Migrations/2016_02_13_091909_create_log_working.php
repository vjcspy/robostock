<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLogWorking extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('log_working', function (Blueprint $table) {
            $table->increments('id');
            $table->smallInteger('log_type');
            $table->string('work_id');
            $table->text('work_name');
            $table->smallInteger('is_working');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::drop('log_working');
    }

}
