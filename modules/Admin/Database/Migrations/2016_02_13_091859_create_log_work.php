<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLogWork extends Migration {

    /**
     * Run the migrations.
     * debug, info, notice, warning, error, critical, and alert.
     *
     * @return void
     */
    public function up() {
        Schema::create('log_work', function (Blueprint $table) {
            $table->increments('id');
            $table->smallInteger('log_type');
            $table->text('work');
            $table->text('data');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::drop('');
    }

}
