<?php
return false;
/**
 * Created by IntelliJ IDEA.
 * User: vjcspy
 * Date: 9/20/15
 * Time: 11:05 AM
 */
class WorkWithDb {
    private $link = null, $server = null, $id = null, $pass = null, $db_name;

    public function setDataToConnect($server, $id, $pass, $dbName) {
        $this->server = $server;
        $this->id = $id;
        $this->pass = $pass;
        $this->db_name = $dbName;
    }

    public function connectDb() {

        $this->link = mysqli_connect($this->server, $this->id, $this->pass, $this->db_name);
        if (!$this->link) {
            die('Could not connect: ' . mysql_error());
        }

//            echo 'Connected successfully</br>';

    }

    public function closeDb() {
        mysqli_close($this->link);
    }

    public function excuteSqlQuery($query) {

        $data = $this->link->query($query);

        return $data;

    }
}

$workWithDb = new WorkWithDb();
$workWithDb->setDataToConnect('localhost', 'root', 'angelalone05', 'laravel');
$workWithDb->connectDb();

$myfile = fopen("./indexall.TXT", "r") or die("Unable to open file!");
// Output one line until end-of-file
$count = 0;
while (!feof($myfile)) {
    if ($count == 0) {
        $count += 1;
        fgets($myfile);
        continue;
    }
    $currentRow = fgets($myfile);
    $data = explode(',', $currentRow);
//    echo(count($data) . "\n");
//    die();
    if (count($data) != 10 /*|| $data[0] != 'SFN'*/)
        continue;
    try {
        $ticker = array(
            'ticker' => $data[0],
            'date' => $data[2],
            //            'open' => $data[4],
            //            'high' => $data[5],
            //            'low' => $data[6],
            'close' => $data[7],
            //            'average' => $data[7],
            'vol' => doubleval(str_replace(',', '', $data[8])),
        );
        if (!in_array($ticker['ticker'], ['VNINDEX', 'HNXINDEX', 'HNX30', 'UPCOMINDEX', 'VN30']))
            continue;
        $count += 1;
        $sql = "INSERT INTO `index_upto` (`id`, `name`, `date`, `index`, `vol`, `created_at`, `updated_at`) VALUES (NULL, '" . $ticker['ticker'] . "', '" . $ticker['date'] . "', '" . $ticker['close'] . "', '" . $ticker['vol'] . "', '0000-00-00 00:00:00.000000', '0000-00-00 00:00:00.000000');";
        $workWithDb->excuteSqlQuery($sql);
    } catch (Exception $e) {
        continue;
    }
}
fclose($myfile);
echo 'Save ' . $count . '(s) Sucsses ' ."\n";
